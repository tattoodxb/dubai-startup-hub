<?php 

$request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="http://siebel.com/CustomUI">
     <soapenv:Header>
                        <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                                    <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                                                <wsse:Username>EAIUSERPROD</wsse:Username>
                                                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">EA1US3RPR0D</wsse:Password>
                                    </wsse:UsernameToken>
                        </wsse:Security>
            </soapenv:Header>
   <soapenv:Body>
      <cus:CreateStartupProfile_Input>
         <cus:DCCountry>India</cus:DCCountry>
         <cus:Process_spcInstance_spcId></cus:Process_spcInstance_spcId>
         <cus:DCStartupName>CRM Testing</cus:DCStartupName>
         <cus:DCEmail>test@dc.com</cus:DCEmail>
         <cus:Object_spcId></cus:Object_spcId>
         <cus:Siebel_spcOperation_spcObject_spcId></cus:Siebel_spcOperation_spcObject_spcId>
         <cus:DCEntrepreneurName>TEST</cus:DCEntrepreneurName>
         <cus:DCTags>Mobility</cus:DCTags>
         <cus:Error_spcCode></cus:Error_spcCode>
         <cus:DCNatureofBusiness>Investor</cus:DCNatureofBusiness>
         <cus:Error_spcMessage></cus:Error_spcMessage>
      </cus:CreateStartupProfile_Input>
   </soapenv:Body>
</soapenv:Envelope>';

$location = 'http://dcsoafmwprd1.dubaichamber.com:7021/STARTUPHUB/ProxyService/DC_Create_Startup_Profile_WF_PRXY?wsdl';

$action = 'http://siebel.com/CustomUI';

$version = 1;

$client = new SoapClient(NULL, array(
	'location' => $location,
	'uri'      => "",
	'trace'    => 1,
	'exceptions'=> 1,
));

$response = $client->__doRequest($request, $location, $action, $version);

//$array = json_decode(json_encode((array)simplexml_load_string($response)),1);

echo $response;