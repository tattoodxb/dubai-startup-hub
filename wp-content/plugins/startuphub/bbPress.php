<?php

/**
 * Message after create topic
 *
 */
function dshub_forum_topic_message( $topic_id, $forum_id ) {
	
	if ( get_post_field( 'post_status', $topic_id ) === bbp_get_pending_status_id() ) {
		dshub_add_notices('Thank you, Your topic is waiting for an approval', 'success');
	}
}
add_action('bbp_new_topic', 'dshub_forum_topic_message', 10, 2 );

/**
 * Message after create reply
 *
 */
function dshub_forum_reply_message( $data ) {
	
	$user = wp_get_current_user();
	$u_emails = array('Kimberly.james@dubaichamber.com', 'natalia.sycheva@dubaichamber.com', 'faisal.alshehhi@dubaichamber.com');
	if( !(in_array('administrator', $user->roles) || in_array($user->user_email, $u_emails)) )
	{
		$data['post_status'] = bbp_get_pending_status_id();
		dshub_add_notices('Thank you, your reply is waiting for an approval', 'success');	
	}
	else
	{		
		$data['post_status'] = bbp_get_public_status_id();
	}
	
	return $data;
	
}
add_filter( 'bbp_new_reply_pre_insert', 'dshub_forum_reply_message' );

/**
 * Topic trends
 *
 */
function dshub_forum_today_trends() {
	global $wpdb;

	$date = date('Y-m-d');

	$query = $wpdb->prepare( "SELECT DISTINCT( post_parent ) AS topic_id, COUNT( ID ) as replies FROM {$wpdb->posts} WHERE ( post_status = '%s' AND post_type = '%s' AND DATE(post_date) = '%s' ) GROUP BY post_parent ORDER BY replies DESC LIMIT 5;", bbp_get_public_status_id(), bbp_get_reply_post_type(), $date );
	$trends = $wpdb->get_results( $query );

	return ( empty($trends) || is_wp_error($trends) ) ? array() : $trends;
}
dshub_forum_today_trends();

function dshub_forum_today_main_trends() {
	global $wpdb;

	$date = date('Y-m-d');

	$query = $wpdb->prepare( "SELECT DISTINCT( post_parent ) AS topic_id, COUNT( ID ) as replies FROM {$wpdb->posts} WHERE ( post_status = '%s' AND post_type = '%s' ) GROUP BY post_parent ORDER BY replies DESC LIMIT 3;", bbp_get_public_status_id(), bbp_get_reply_post_type());
	$trends_topic = $wpdb->get_results( $query );

	foreach ($trends_topic as $t_arr) {
		$post_id[] = $t_arr->topic_id;
	}
	$post_parent = implode (", ", $post_id);
	$query_main = $wpdb->prepare( "select distinct post_parent AS topic_id from shub_posts where id in ($post_parent);", false);
	//var_dump($query_main);
	$trends = $wpdb->get_results( $query_main );
	//var_dump($trends);

	return ( empty($trends) || is_wp_error($trends) ) ? array() : $trends;
}
dshub_forum_today_main_trends();

/**
 * Forum topic resolve
 *
 */
/*
function dshub_forum_topic_resolve() {
	$post = $_POST;
	
	if ( !wp_verify_nonce( $post['csrf_token'], 'csrf_check' ) )
		wp_send_json_error( array( 'message' => 'Can not access' ) );

	if ( bbp_get_topic_author_id( $post['topic_id'] ) != bbp_get_current_user_id() )
		wp_send_json_error( array( 'message' => 'You can not access' ) );

	update_post_meta( $post['topic_id'], '_shub_resolved', 1 );

	wp_send_json_success( array( 'message' => 'Topic has been resolved' ) );
}
add_action('wp_ajax_nopriv_shub_topic_resolve', 'dshub_forum_topic_resolve');
add_action('wp_ajax_shub_topic_resolve', 'dshub_forum_topic_resolve');
*/

/**
 * Forum sub topic
 *
 */

/*
function dshub_forum_sub_topics() {
	$post = $_POST;
	
	if ( !wp_verify_nonce( $post['csrf_token'], 'csrf_check' ) )
		wp_send_json_error( array( 'message' => 'Can not access' ) );

	$subforums = bbp_forum_get_subforums( array( 'post_parent' => $post['parent_id'] ) );
	foreach ($subforums as $child_forum) :
		$post_title[$child_forum->ID] = $child_forum->post_title;
	endforeach;

	wp_send_json_success( array( 'message' => $post_title ) );
}
add_action('wp_ajax_nopriv_shub_forum_child', 'dshub_forum_sub_topics');
add_action('wp_ajax_shub_forum_child', 'dshub_forum_sub_topics');
*/

/**
 * Whether topic has resolved or not
 *
 */
function dshub_forum_topic_has_resolved( $topic_id = 0 ) {
	return (bool) get_post_meta( $topic_id, '_shub_resolved', true );
}

/**
 * Reply like/ dislike
 *
 */
/*
function dshub_forum_reply_like() {
	$post = $_POST;
	
	if ( !wp_verify_nonce( $post['csrf_token'], 'csrf_check' ) || !is_user_logged_in() )
		wp_send_json_error( array( 'message' => 'Can not access' ) );

	$meta_key = ($post['mode'] == 'like') ? '_shub_likes' : '_shub_dislikes';

	$reply_liker_set = get_post_meta( $post['reply_id'], $meta_key, true );
	$reply_liker_arr = ( !empty( $reply_liker_set ) ) ? explode(',', $reply_liker_set) : array();

	if ( in_array( bbp_get_current_user_id(), $reply_liker_arr ) )
		wp_send_json_error();

	array_push( $reply_liker_arr, bbp_get_current_user_id() );
	update_post_meta( $post['reply_id'], $meta_key, implode( ',', $reply_liker_arr ) );

	wp_send_json_success( array( 'new_count' => sizeof( $reply_liker_arr ) ) );
}
add_action('wp_ajax_nopriv_shub_reply_like', 'dshub_forum_reply_like');
add_action('wp_ajax_shub_reply_like', 'dshub_forum_reply_like');
*/

/**
 * Get reply like count
 *
 */
function dshub_forum_reply_like_count( $reply_id = 0, $mode = 'like' ) {
	$meta_key = ($mode == 'like') ? '_shub_likes' : '_shub_dislikes';

	$reply_liker_set = get_post_meta( $reply_id, $meta_key, true );
	$reply_liker_arr = ( !empty( $reply_liker_set ) ) ? explode(',', $reply_liker_set) : array();

	return sizeof( $reply_liker_arr );
}

/**
 * Get the helpful users
 *
 */
function dshub_forum_helpul_users() {
	global $wpdb;

	//$query = "SELECT DISTINCT(user.ID), COUNT(meta.meta_id) AS replies FROM {$wpdb->users} AS user LEFT JOIN {$wpdb->postmeta} AS meta ON (FIND_IN_SET(user.ID, meta.meta_value) > 0) AND meta.meta_key = '_shub_likes' ORDER BY replies DESC LIMIT 5";
	$query = "SELECT SUM((CHAR_LENGTH( a.meta_value ) - CHAR_LENGTH( REPLACE( a.meta_value,  ',',  '' ) ) +1 )) AS replies, b.post_author as ID FROM `shub_postmeta` a, shub_posts b WHERE meta_key =  '_shub_likes' AND a.post_id = b.id GROUP BY b.post_author ORDER BY replies DESC LIMIT 4";
	$helful = $wpdb->get_results( $query );
	return ( empty($helful) || is_wp_error($helful) ) ? array() : $helful;
}

function dshub_forum_helpul_category($post_id) {
	global $wpdb;
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => array('forum','topic','reply'), 'posts_per_page' => -1));
	$query = get_page_children($post_id, $all_wp_pages);
	foreach($query as $child){

		$help_query = "select meta_value from $wpdb->postmeta where post_id=".$child->ID." and meta_key='_shub_likes'";
		//echo $help_query;
		$helful = $wpdb->get_results( $help_query );
		foreach ($helful as $value) {
			$meta_arr = explode(",", $value->meta_value);
			$page += count($meta_arr);
		}
	}
	return $page;
}


