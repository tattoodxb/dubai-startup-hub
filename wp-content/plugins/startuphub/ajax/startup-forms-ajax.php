<?php

ini_set('display_errors', 1);
require_once("../../../../wp-load.php");
global $wpdb;

if (wp_verify_nonce($_POST['csrf_token'], 'csrf_check')) {
    if ($_POST['action'] == 'user_valid_response') {
        if (isset($_POST["user_name"]) && isset($_POST["user_pass"])) {
            $user_name = $_POST["user_name"];
            $user_pass = $_POST["user_pass"];
            $user = wp_signon(array('user_login' => $user_name, 'user_password' => $user_pass), false);
            if (is_wp_error($user)) {
                $response = $user->get_error_message();
                echo $response;
                die();
            } else {
                $response = "success";
                echo $response;
                die();
            }
        } else {
            $response = "required";
            echo $response;
            die();
        }
    }


    if ($_POST['action'] == 'save_enquiry') {

        if (isset($_POST["first_name"]) && isset($_POST["family_name"]) && isset($_POST["subject"]) && isset($_POST["email"]) && isset($_POST["comments"])) {
            $first_name = sanitize_text_field($_POST["first_name"]);
            $family_name = sanitize_text_field($_POST["family_name"]);
            $email = sanitize_text_field($_POST["email"]);
            $subject = sanitize_text_field($_POST["subject"]);
            $message = sanitize_text_field($_POST["comments"]);

            $data = array('firstname' => $first_name,
                'lastname' => $family_name,
                'email' => $email,
                'subject' => $subject,
                'message' => $message,
                'date_added' => date('Y-m-d H:i:s'));

            /* INSERT ENQUIRY DETAILS */
            global $wpdb;
            $table_name = $wpdb->prefix . 'enquiries';
            $res = $wpdb->insert($table_name, $data);
            if ($res) {
                // write the email content
                $headers = array('Content-Type: text/html; charset=UTF-8');

                $message = "<html>
								<body>
									<p>-- Enquiry via Dubai Startup Hub Contact Form</p><br>
									<table width='100%' border='1'>
										<tr>
											<td>Sender Name: </td>
											<td>" . $first_name . " " . $family_name . "</td>
										<tr>
										<tr>
											<td>Sender Email:</td>
											<td>" . $email . "</td>
										<tr>
										<tr>
											<td>Subject</td>
											<td>" . $subject . "</td>
										<tr>
										<tr>
											<td>Message</td>
											<td>" . $message . "</td>
										<tr>
									<table>
								</body>
							</html>";

                $subject = $subject;

                $to = "dubaistartuphub@dubaichamber.com"; //live
                //$to = "Refqah.Istaitiyeh@wunderman.com, cristine.ramos@wunderman.com"; //staging
                // send the email using wp_mail()
                if (!wp_mail($to, $subject, $message, $headers)) {
                    $contact_errors = true;
                }

                $response = "success";
                echo $response;
                die();
            } else {
                $response = "failed";
                echo $response;
                die();
            }
        } else {
            $response = "required";
            echo $response;
            die();
        }
    }


    if ($_POST['action'] == 'save_subscriber') {

        if (isset($_POST["email"])) {

            $email = sanitize_text_field($_POST["email"]);
            $firstname = sanitize_text_field($_POST["firstname"]);
            $lastname = sanitize_text_field($_POST["lastname"]);

            if (!shub_is_email_exist($email)) {
                $data = array('email' => $email, 'firstname' => $firstname, 'lastname' => $lastname,
                    'date_added' => date('Y-m-d H:i:s'));

                /* INSERT ENQUIRY DETAILS */
                global $wpdb;
                $table_name = $wpdb->prefix . 'newsletter_subscribers';
                $res = $wpdb->insert($table_name, $data);
                if ($res) {
                    $response = "success";
                    echo $response;
                    die();
                } else {
                    $response = "failed";
                    echo $response;
                    die();
                }
            } else {
                /* if email already exists */
                $response = "duplicate";
                echo $response;
                die();
            }
        } else {
            $response = "required";
            echo $response;
            die();
        }
    }

    if ($_POST['action'] == 'event_attend_response') {

        if (isset($_POST["event_id"])) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'event_attendees';

            $user = wp_get_current_user();
            $user_id = $user->ID;
            $user_email = $user->user_email;
            $user_firstname = $user->user_firstname;

            $post_id = $_POST["event_id"];

            $attendee_count = $wpdb->get_var("SELECT COUNT(id) FROM $table_name where post_id=$post_id and user_id=$user_id");

            if ($attendee_count == "0" || $attendee_count == "") {
                $date_created = date('Y-m-d h:m:s');
                $item = array('user_id' => $user_id, 'post_id' => $post_id, 'status' => 0, 'date_created' => $date_created);
                $result = $wpdb->insert($table_name, $item);
                if ($user_email != "") {
                    /* $to = $user_email;
                      $subject = 'Dubai 360 - Thank you for your interest';
                      $body = '<font face="Arial" size="2">Dear '.$user_firstname.',<br><br>Thank you for your interest in Dubai 360°. Please note that due to limited capacity we are only able to confirm participations on first come first serve basis.<br><br>We will be in touch with you shortly to let you know the status of your participation.<br><br>Kind regards,<br><br>DAC Team<br><img src="http://www.dubaiassociationconference.com/images/email-signature.jpg" border="0"></font>';
                      $headers = array('Content-Type: text/html; charset=UTF-8');
                      wp_mail( $to, $subject, $body, $headers );

                      $a_to = 'Dania.Keilani@Dubaichamber.com';
                      $a_subject = 'Dubai 360 - Interest Requested';
                      $a_body = '<font face="Arial" size="2">You have received a new Dubai 360 activity request for Dubai Association Conference. Login the admin panel to approve/reject: <br><br>http://www.dubaiassociationconference.com/wp-login.php<br><br>Kind regards,<br><br>DAC Team<br><img src="http://www.dubaiassociationconference.com/images/email-signature.jpg" border="0"></font>';
                      $headers = array('Content-Type: text/html; charset=UTF-8');
                      wp_mail( $a_to, $a_subject, $a_body, $headers ); */
                }
                $response = "added";
                echo $response;
                die();
            } else {
                $response = "present";
                echo $response;
                die();
            }
        }
    }

    if ($_POST['action'] == 'shub_topic_resolve') {

        $post = $_POST;

        if (!wp_verify_nonce($post['csrf_token'], 'csrf_check'))
            wp_send_json_error(array('message' => 'Can not access'));

        if (bbp_get_topic_author_id($post['topic_id']) != bbp_get_current_user_id())
            wp_send_json_error(array('message' => 'You can not access'));

        update_post_meta($post['topic_id'], '_shub_resolved', 1);

        wp_send_json_success(array('message' => 'Topic has been resolved'));
    }

    if ($_POST['action'] == 'shub_reply_like') {

        $post = $_POST;

        if (!wp_verify_nonce($post['csrf_token'], 'csrf_check') || !is_user_logged_in())
            wp_send_json_error(array('message' => 'Can not access'));

        $meta_key = ($post['mode'] == 'like') ? '_shub_likes' : '_shub_dislikes';

        $reply_liker_set = get_post_meta($post['reply_id'], $meta_key, true);
        $reply_liker_arr = (!empty($reply_liker_set) ) ? explode(',', $reply_liker_set) : array();

        if (in_array(bbp_get_current_user_id(), $reply_liker_arr))
            wp_send_json_error();

        array_push($reply_liker_arr, bbp_get_current_user_id());
        update_post_meta($post['reply_id'], $meta_key, implode(',', $reply_liker_arr));

        wp_send_json_success(array('new_count' => sizeof($reply_liker_arr)));
    }

    if ($_POST['action'] == 'shub_forum_child') {

        $post = $_POST;

        if (!wp_verify_nonce($post['csrf_token'], 'csrf_check'))
            wp_send_json_error(array('message' => 'Can not access'));

        $subforums = bbp_forum_get_subforums(array('post_parent' => $post['parent_id']));
        foreach ($subforums as $child_forum) :
            $post_title[$child_forum->ID] = $child_forum->post_title;
        endforeach;

        wp_send_json_success(array('message' => $post_title));
    }

    if ($_POST['action'] == 'get_tags_response') {

        if (empty($_GET['term']))
            exit;

        $q = strtolower($_GET["term"]);
        // remove slashes if they were magically added
        if (get_magic_quotes_gpc())
            $q = stripslashes($q);
        $tags = get_tags();
        $items = array();
        foreach ($tags as $tag) {
            $items[$tag->slug] = $tag->name;
        }

        $result = array();
        foreach ($items as $key => $value) {
            if (strpos(strtolower($key), $q) !== false) {
                array_push($result, array("id" => $key, "label" => $value, "value" => strip_tags($value)));
            }
            if (count($result) > 11)
                break;
        }
        // json_encode is available in PHP 5.2 and above, or you can install a PECL module in earlier versions
        $output = json_encode($result);
        if ($_GET["callback"]) {
            // Escape special characters to avoid XSS attacks via direct loads of this
            // page with a callback that contains HTML. This is a lot easier than validating
            // the callback name.
            $output = htmlspecialchars($_GET["callback"]) . "($output);";
        }
        echo $output;
        wp_die();
    }



    if ($_POST['action'] == 'ma_save_enquiry') {
        $name = sanitize_text_field(trim($_POST['ma_name']));
        $email = sanitize_text_field(trim($_POST['ma_email']));
        $contact = sanitize_text_field(trim($_POST['ma_contact']));
        $message = sanitize_text_field(trim($_POST['ma_message']));
        $created = date('Y-m-d H:i:s');
        global $wpdb;
        $table = $wpdb->prefix . "market_access_enquiries";
        $res = $wpdb->insert($table, array('name' => $name, 'email' => $email, 'contact' => $contact, 'date_created' => $created));

        $a_to = 'marketaccess@dubaichamber.com';
        //$a_to = 'Refqah.Istaitiyeh@wunderman.com, santosh@tattoo360.com';
        $a_subject = 'Market Access Enquiry';
        $a_body = '<font face="Arial" size="2">Please be informed that a form has been submitted in market access page. Please find the user details below
            <br><br>
            Name: ' . $name . ' <br>
            Email: ' . $email . ' <br>
            Contact: ' . $contact . ' <br>
            Message: ' . $message . ' <br>
            <br><br>Kind regards,<br><br>Dubai Startuphub Team<br></font>';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        if (!wp_mail($a_to, $a_subject, $a_body, $headers)) {
            $response = "error";
            echo $response;
            die();
        } else {
            $response = "success";
            echo $response;
            die();
        }

        exit();
    }

    if ($_POST['action'] == 'smart_save_enquiry') {
        $name = sanitize_text_field(trim($_POST['ma_name']));
        $email = sanitize_text_field(trim($_POST['ma_email']));
        $contact = sanitize_text_field(trim($_POST['ma_contact']));
        $created = date('Y-m-d H:i:s');
        global $wpdb;
        $table = $wpdb->prefix . "smartpreneur_enquiries";
        $res = $wpdb->insert($table, array('name' => $name, 'email' => $email, 'contact' => $contact, 'date_created' => $created));

        if ($res) {
            echo 'success';
        } else {
            echo 'error';
        }

        exit();
    }

    if ($_POST['action'] == 'cf_save_enquiry') {
        $name = sanitize_text_field(trim($_POST['ma_name']));
        $email = sanitize_text_field(trim($_POST['ma_email']));
        $contact = sanitize_text_field(trim($_POST['ma_contact']));
        $created = date('Y-m-d H:i:s');
        global $wpdb;
        $table = $wpdb->prefix . "cofounder_enquiries";
        $res = $wpdb->insert($table, array('name' => $name, 'email' => $email, 'contact' => $contact, 'date_created' => $created));


        $a_to = 'dubaistartuphub@dubaichamber.com';
        $a_subject = 'Co Founder Form';
        $a_body = '<font face="Arial" size="2">Please be informed that a form has been submitted in co founder dubai page. Please find the user details below
            <br><br>
            Name: ' . $name . ' <br>
            Email: ' . $email . ' <br>
            Contact: ' . $contact . ' <br>
            <br><br>Kind regards,<br><br>Dubai Startuphub Team<br></font>';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        if (!wp_mail($a_to, $a_subject, $a_body, $headers)) {
            $response = "error";
            echo $response;
            die();
        } else {
            $response = "success";
            echo $response;
            die();
        }
        exit();
    }

    if ($_POST['action'] == 'save_guest_speaker') {
        $user = wp_get_current_user();
        $user_id = $user->ID;
        $user_email = $user->user_email;
        $user_firstname = $user->user_firstname;
        $user_lastname = $user->user_lastname;

        $a_to = 'dubaistartuphub@dubaichamber.com';
        $a_subject = 'Startups new guest speaker request';
        $a_body = '<font face="Arial" size="2">Please be informed that a new user has applied to be a guest speaker. Please find the user details below
            <br><br>
            Name: ' . $user_firstname . ' ' . $user_lastname . ' <br>
            Email: ' . $user_email . ' <br>
            <br><br>Kind regards,<br><br>Dubai Startuphub Team<br></font>';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        ;
        if (!wp_mail($a_to, $a_subject, $a_body, $headers)) {
            $response = "error";
            echo $response;
            die();
        } else {
            $response = "added";
            echo $response;
            die();
        }
    }

    if ($_POST['action'] == 'save_competition') {
        $user = wp_get_current_user();
        $user_id = $user->ID;
        $user_email = $user->user_email;
        $user_firstname = $user->user_firstname;
        $user_lastname = $user->user_lastname;

        $a_to = 'dubaistartuphub@dubaichamber.com';
        $a_subject = 'Startups interested in hearing more on upcoming Competitions request';
        $a_body = '<font face="Arial" size="2">Please be informed that a new user is interested in hearing about upcoming competitions. Please find the user details below:
            <br><br>
            Name: ' . $user_firstname . ' ' . $user_lastname . ' <br>
            Email: ' . $user_email . ' <br>
            <br><br>Kind regards,<br><br>Dubai Startuphub Team<br></font>';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        if (!wp_mail($a_to, $a_subject, $a_body, $headers)) {
            $response = "error";
            echo $response;
            die();
        } else {
            $response = "added";
            echo $response;
            die();
        }
    }
}
?>