<?php
/*
Plugin Name: Startup Services
Description: This plugin is for Startup Hub Dubai custom functions, it includes news, events, testimonials
Version:     1.0
Author:      Tattoo 360
*/

// bbPress customization
require_once( plugin_dir_path( __FILE__ ) . '/bbPress.php' );

/**
 * Assets include
 *
 */
function dshub_load_scripts() {
    wp_enqueue_script( 'contact-form', plugins_url( 'assets/js/contact-form.js', __FILE__ ), array('jquery'), $GLOBALS['wp_version'], true );
}
add_action('wp_enqueue_scripts', 'dshub_load_scripts');

/**
 * Page meta box
 *
 */
function dc_pages_meta_box($post) {
    $post_id = $post->ID;
    $sh_overview_text = get_post_meta($post_id, 'sh_overview_text', true);
    $sh_topbanner_template = get_post_meta($post_id, 'sh_topbanner_template', true);
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
<div class="dc_pages_meta">
    <div>
        <p><strong><?php echo __('Top Banner Template') ?></strong></p>
        <select id="sh_topbanner_template" name="sh_topbanner_template" style="width:100%">        
            <option value="">Default</option>
            <option value="top-banner1" <?php if ($sh_topbanner_template=="top-banner1"){?>selected<?php }?>>Top Banner 1</option>
            <option value="top-banner2" <?php if ($sh_topbanner_template=="top-banner2"){?>selected<?php }?>>Top Banner 2 (Contact)</option>
            <option value="top-banner3" <?php if ($sh_topbanner_template=="top-banner3"){?>selected<?php }?>>Top Banner 3 (Survey)</option>
            <option value="top-banner-news" <?php if ($sh_topbanner_template=="top-banner-news"){?>selected<?php }?>>Top Banner 4 (News)</option>
            <option value="top-banner-dashboard" <?php if ($sh_topbanner_template=="top-banner-dashboard"){?>selected<?php }?>>Top Banner 4 (Dashboard)</option>
            <option value="top-banner-forum" <?php if ($sh_topbanner_template=="top-banner-forum"){?>selected<?php }?>>Top Banner 5 (Forum)</option>
			     <option value="top-banner-marketaccess" <?php if ($sh_topbanner_template=="top-banner-marketaccess"){?>selected<?php }?>>Top Banner 6 (Market Access)</option>
           <option value="top-banner-education" <?php if ($sh_topbanner_template=="top-banner-education"){?>selected<?php }?>>Top Banner 7 (Education)</option>
           <option value="top-banner-education-inner" <?php if ($sh_topbanner_template=="top-banner-education-inner"){?>selected<?php }?>>Top Banner 8 (Education Inner)</option>
		   <option value="top-banner-cofounder" <?php if ($sh_topbanner_template=="top-banner-cofounder"){?>selected<?php }?>>Top Banner 9 (CoFounder)</option>
        </select>
    </div>
    <div>
        <p><strong><?php echo __('Overview Text') ?></strong></p>
        <input type="text" id="sh_overview_text" name="sh_overview_text" value="<?php echo $sh_overview_text; ?>" style="width:100%" />
    </div>
</div>
<?php
}
function dc_news_meta_box($post) {
    $post_id = $post->ID;
    $sh_news_date = get_post_meta($post_id, 'sh_news_date', true);
    $sh_news_display_date = get_post_meta($post_id, 'sh_news_display_date', true);
    $sh_news_location = get_post_meta($post_id, 'sh_news_location', true);
    $anchored_chk = get_post_meta($post->ID, 'sh_news_sticky', true);
    $checked = ($anchored_chk) ? 'checked="checked"' : '';
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
<div class="dc_news_meta">
    <div>
        <p><strong><?php echo __('News Date') ?></strong></p>
        <input class="datepicker" type="text" id="sh_news_date" name="sh_news[date]" value="<?php echo $sh_news_date; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('News Display Date') ?></strong></p>
        <input type="text" id="sh_news_display_date" name="sh_news[display_date]" value="<?php echo $sh_news_display_date; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('News Location') ?></strong></p>
        <input type="text" id="sh_news_location" name="sh_news[location]" value="<?php echo $sh_news_location; ?>" style="width:100%" />
    </div>
    <div>
        <p><label for="sh_news_sticky"><input id="sh_news_sticky" name="sh_news_sticky" type="checkbox" value="true" <?php echo $checked; ?> /> <b>Show/Sticky in Homepage</b></label></p>
    </div>
</div>
<?php
}
function dc_events_meta_box($post) {
    $post_id = $post->ID;
    $sh_events_date = get_post_meta($post_id, 'sh_events_date', true);
    $sh_events_date_end = get_post_meta($post_id, 'sh_events_date_end', true);
    $sh_events_display_date = get_post_meta($post_id, 'sh_events_display_date', true);
    $sh_events_location = get_post_meta($post_id, 'sh_events_location', true);
    $sh_events_address = get_post_meta($post_id, 'sh_events_address', true);
    $sh_events_city = get_post_meta($post_id, 'sh_events_city', true);
    $sh_events_ticket = get_post_meta($post_id, 'sh_events_ticket', true);
    $sh_events_eid = get_post_meta($post_id, 'sh_events_eid', true);
    $anchored_chk = get_post_meta($post->ID, 'sh_event_sticky', true);
    $checked = ($anchored_chk) ? 'checked="checked"' : '';

    $register_chk = get_post_meta($post->ID, 'sh_event_enable', true);
    $reg_checked = ($register_chk) ? 'checked="checked"' : '';

    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
<div class="dc_events_meta">
    <div>
        <p><strong><?php echo __('Events Start Date') ?></strong></p>
        <input class="datepicker" type="text" id="sh_events_date" name="sh_events[date]" value="<?php echo $sh_events_date; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Events End Date') ?></strong></p>
        <input class="datepicker" type="text" id="sh_events_date_end" name="sh_events[date_end]" value="<?php echo $sh_events_date_end; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Events Display Date') ?></strong></p>
        <input type="text" id="sh_events_display_date" name="sh_events[display_date]" value="<?php echo $sh_events_display_date; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Events Location (Venue)') ?></strong></p>
        <input type="text" id="sh_events_location" name="sh_events[location]" value="<?php echo $sh_events_location; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Events Location (Address)') ?></strong></p>
        <input type="text" id="sh_events_address" name="sh_events[address]" value="<?php echo $sh_events_address; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Events Location (City)') ?></strong></p>
        <input type="text" id="sh_events_city" name="sh_events[city]" value="<?php echo $sh_events_city; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Ticket URL') ?></strong></p>
        <input type="text" id="sh_events_ticket" name="sh_events[ticket]" value="<?php echo $sh_events_ticket; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('External ID') ?></strong></p>
        <input type="text" id="sh_events_eid" name="sh_events[eid]" value="<?php echo $sh_events_eid; ?>" style="width:100%" />
    </div>
    <div>
        <p><label for="sh_event_enable"><input id="sh_event_enable" name="sh_event_enable" type="checkbox" value="true" <?php echo $reg_checked; ?> /> <b>Enable Registrations</b></label></p>
    </div>
    <div>
        <p><label for="sh_event_sticky"><input id="sh_event_sticky" name="sh_event_sticky" type="checkbox" value="true" <?php echo $checked; ?> /> <b>Show/Sticky in Homepage</b></label></p>
    </div>
</div>
<?php
}
function dc_meetup_meta_box($post) {
    $post_id = $post->ID;
    $sh_meetup_date = get_post_meta($post_id, 'sh_meetup_date', true);
    $sh_meetup_date_end = get_post_meta($post_id, 'sh_meetup_date_end', true);
    $sh_meetup_display_date = get_post_meta($post_id, 'sh_meetup_display_date', true);
    $sh_meetup_location = get_post_meta($post_id, 'sh_meetup_location', true);
    $sh_meetup_ticket = get_post_meta($post_id, 'sh_meetup_ticket', true);
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
<div class="dc_meetup_meta">
    <div>
        <p><strong><?php echo __('Meet Up Start Date') ?></strong></p>
        <input class="datepicker" type="text" id="sh_meetup_date" name="sh_meetup[date]" value="<?php echo $sh_meetup_date; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Meet Up End Date') ?></strong></p>
        <input class="datepicker" type="text" id="sh_meetup_date_end" name="sh_meetup[date_end]" value="<?php echo $sh_meetup_date_end; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Meet Up Display Date') ?></strong></p>
        <input type="text" id="sh_meetup_display_date" name="sh_meetup[display_date]" value="<?php echo $sh_meetup_display_date; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Meet Up Location') ?></strong></p>
        <input type="text" id="sh_meetup_location" name="sh_meetup[location]" value="<?php echo $sh_meetup_location; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Ticket URL') ?></strong></p>
        <input type="text" id="sh_meetup_ticket" name="sh_meetup[ticket]" value="<?php echo $sh_meetup_ticket; ?>" style="width:100%" />
    </div>
</div>
<?php
}

function dc_programme_meta_box($post) {
    $post_id = $post->ID;
    $sh_programme_link  = get_post_meta($post_id, 'sh_programme_link', true);
    $sh_programme_label = get_post_meta($post_id, 'sh_programme_label', true);
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
<div class="dc_program_meta_box">
    <div>
        <p><strong><?php echo __('Program URL') ?></strong></p>
        <input type="text" id="sh_programme_link" name="sh_programme[link]" value="<?php echo $sh_programme_link; ?>" style="width:100%" placeholder="http://www.example.com/"/>
    </div>
	<div>
        <p><strong><?php echo __('Button Label') ?></strong></p>
        <input type="text" id="sh_programme_label" name="sh_programme[label]" value="<?php echo $sh_programme_label; ?>" style="width:100%" placeholder="i.e LEARN MORE / APPLY NOW"/>
    </div>
</div>
<?php
}


function dc_testimonial_meta_box($post) {
    $post_id = $post->ID;
    $sh_testimonial_author = get_post_meta($post_id, 'sh_testimonial_author', true);
    $sh_testimonial_display_date = get_post_meta($post_id, 'sh_testimonial_display_date', true);
    $sh_testimonial_location = get_post_meta($post_id, 'sh_testimonial_location', true);
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
<div class="dc_testimonial_meta">
    <div>
        <p><strong><?php echo __('Author Name') ?></strong></p>
        <input type="text" id="sh_testimonial_author" name="sh_testimonial[author]" value="<?php echo $sh_testimonial_author; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Author Display Date') ?></strong></p>
        <input type="text" id="sh_testimonial_display_date" name="sh_testimonial[display_date]" value="<?php echo $sh_testimonial_display_date; ?>" style="width:100%" />
    </div>
    <div>
        <p><strong><?php echo __('Author Location') ?></strong></p>
        <input type="text" id="sh_testimonial_location" name="sh_testimonial[location]" value="<?php echo $sh_testimonial_location; ?>" style="width:100%" />
    </div>
</div>
<?php
}

function dc_add_meta_box()
{
    add_meta_box('dc_pages_meta', __('Pages meta'), "dc_pages_meta_box", "page", "side", "high", null);
    add_meta_box('dc_news_meta', __('News meta'), "dc_news_meta_box", "news", "side", "high", null);
    add_meta_box('dc_events_meta', __('Events meta'), "dc_events_meta_box", "events", "side", "high", null);
    add_meta_box('dc_events_meta', __('Events meta'), "dc_events_meta_box", "post", "side", "high", null);
    add_meta_box('dc_meetup_meta', __('Meet Up meta'), "dc_meetup_meta_box", "meetup", "side", "high", null);
    add_meta_box('dc_testimonial_meta', __('Testimonials meta'), "dc_testimonial_meta_box", "testimonials", "side", "high", null);
	add_meta_box('dc_programme_meta', __('Programme meta'), "dc_programme_meta_box", "programmes", "side", "high", null);
}
add_action("add_meta_boxes", "dc_add_meta_box");

function dc_save_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
      return $post_id;

    if(!current_user_can("edit_post", $post_id))
      return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
      return $post_id;
    
    if(isset($_POST['sh_topbanner_template'])) {
      update_post_meta($post_id, "sh_topbanner_template", $_POST["sh_topbanner_template"]);
    }

    if(isset($_POST['sh_overview_text'])) {
      update_post_meta($post_id, "sh_overview_text", $_POST["sh_overview_text"]);
    }

    update_post_meta($post_id, "sh_event_sticky", '');

    if(isset($_POST['sh_event_sticky'])) {
      update_post_meta($post_id, "sh_event_sticky", $_POST["sh_event_sticky"]);
    }

    update_post_meta($post_id, "sh_event_enable", '');

    if(isset($_POST['sh_event_enable'])) {
      update_post_meta($post_id, "sh_event_enable", $_POST["sh_event_enable"]);
    }

    update_post_meta($post_id, "sh_news_sticky", '');

    if(isset($_POST['sh_news_sticky'])) {
      update_post_meta($post_id, "sh_news_sticky", $_POST["sh_news_sticky"]);
    }

    foreach ($_POST['sh_news'] as $key => $val) {
        update_post_meta($post_id, 'sh_news_'. $key, $val);
    }
    foreach ($_POST['sh_events'] as $key => $val) {
        update_post_meta($post_id, 'sh_events_'. $key, $val);
    }
    foreach ($_POST['sh_meetup'] as $key => $val) {
        update_post_meta($post_id, 'sh_meetup_'. $key, $val);
    }
    foreach ($_POST['sh_testimonial'] as $key => $val) {
        update_post_meta($post_id, 'sh_testimonial_'. $key, $val);
    }
	foreach ($_POST['sh_programme'] as $key => $val) {
        update_post_meta($post_id, 'sh_programme_'. $key, $val);
    }
}
add_action("save_post", "dc_save_meta_box", 10, 3);

function create_my_news_type() {
  register_post_type( 'news',
    array(
      'labels' => array(
      'name' => __( 'News','startup' ),
      'singular_name' => __( 'News','startup'),
      'add_new' => 'Add New',
      'add_new_item' => 'Add New News',
      'edit' => 'Edit',
      'edit_item' =>'Edit News',
      'new_item' => 'New News',
      'view' => 'View News',
      'view_item' => 'View News',
      'search_items' => 'Search News',
      'not_found' => 'No News found',
      'not_found_in_trash' => 'No News found in Trash',
      'parent' => 'Parent News',
      ),
        'publicly_queryable' => true,
        'show_ui' => true, 
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_position' => null,
      'public' => true,
      'menu_position' => 5,
      'hierarchical' => true,
      'query_var' => true,
      'supports' => array( 'title', 'editor','author', 'excerpt', 'thumbnail', 'comments', 'custom-fields'  ),
      'taxonomies'   => array( 'news_tag',  'post_tag' ),
      'rewrite' => array( 'slug' => 'latest-updates/news' )
    )
  );
}
add_action( 'init', 'create_my_news_type' );

function create_my_events_type() {
  register_post_type( 'events',
    array(
      'labels' => array(
      'name' => __( 'Events','startup' ),
      'singular_name' => __( 'Events','startup'),
      'add_new' => 'Add New',
      'add_new_item' => 'Add New Events',
      'edit' => 'Edit',
      'edit_item' =>'Edit Events',
      'new_item' => 'New Events',
      'view' => 'View Events',
      'view_item' => 'View Events',
      'search_items' => 'Search Events',
      'not_found' => 'No Events found',
      'not_found_in_trash' => 'No Events found in Trash',
      'parent' => 'Parent Events',
      ),
        'publicly_queryable' => true,
        'show_ui' => true, 
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_position' => null,
      'public' => true,
      'menu_position' => 5,
      'hierarchical' => true,
      'query_var' => true,
      'supports' => array( 'title', 'editor','author', 'excerpt', 'thumbnail', 'comments', 'custom-fields'  ),
      'taxonomies'   => array( 'events_tag',  'post_tag' ),
      'rewrite' => array( 'slug' => 'events' )
    )
  );
}
add_action( 'init', 'create_my_events_type' );

function create_my_meetup_type() {
  register_post_type( 'meetup',
    array(
      'labels' => array(
      'name' => __( 'Meet Up','startup' ),
      'singular_name' => __( 'Meet Up','startup'),
      'add_new' => 'Add New',
      'add_new_item' => 'Add New Meet Up',
      'edit' => 'Edit',
      'edit_item' =>'Edit Meet Up',
      'new_item' => 'New Meet Up',
      'view' => 'View Meet Up',
      'view_item' => 'View Meet Up',
      'search_items' => 'Search Meet Up',
      'not_found' => 'No Meet Up found',
      'not_found_in_trash' => 'No Meet Up found in Trash',
      'parent' => 'Parent Meet Up',
      ),
        'publicly_queryable' => true,
        'show_ui' => true, 
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_position' => null,
      'public' => true,
      'menu_position' => 5,
      'hierarchical' => true,
      'query_var' => true,
      'supports' => array( 'title', 'editor','author', 'excerpt', 'thumbnail', 'comments', 'custom-fields'  ),
      'taxonomies'   => array( 'meetup_tag',  'post_tag' ),
      'rewrite' => array( 'slug' => 'meetup' )
    )
  );
}
add_action( 'init', 'create_my_meetup_type' );

function create_my_testimonial_type() {
  register_post_type( 'testimonials',
    array(
      'labels' => array(
      'name' => __( 'Testimonials','startup' ),
      'singular_name' => __( 'Testimonials','startup'),
      'add_new' => 'Add New',
      'add_new_item' => 'Add New Testimonials',
      'edit' => 'Edit',
      'edit_item' =>'Edit Testimonials',
      'new_item' => 'New Testimonials',
      'view' => 'View Testimonials',
      'view_item' => 'View Testimonials',
      'search_items' => 'Search Testimonials',
      'not_found' => 'No Testimonials found',
      'not_found_in_trash' => 'No Testimonials found in Trash',
      'parent' => 'Parent Testimonials',
      ),
        'publicly_queryable' => true,
        'show_ui' => true, 
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_position' => null,
      'public' => true,
      'menu_position' => 5,
      'hierarchical' => true,
      'query_var' => true,
      'supports' => array( 'title', 'editor','author', 'excerpt', 'thumbnail', 'comments', 'custom-fields'  ),
      'rewrite' => array( 'slug' => 'testimonials' )
    )
  );
}
add_action( 'init', 'create_my_testimonial_type' );

function create_my_team_type() {
  register_post_type( 'team',
    array(
      'labels' => array(
      'name' => __( 'Team Member','startup' ),
      'singular_name' => __( 'Team Member','startup'),
      'add_new' => 'Add New',
      'add_new_item' => 'Add New Team Member',
      'edit' => 'Edit',
      'edit_item' =>'Edit Team Member',
      'new_item' => 'New Team Member',
      'view' => 'View Team Member',
      'view_item' => 'View Team Member',
      'search_items' => 'Search Team Member',
      'not_found' => 'No Team Member found',
      'not_found_in_trash' => 'No Team Member found in Trash',
      'parent' => 'Parent Team Member',
      ),
        'publicly_queryable' => true,
        'show_ui' => true, 
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_position' => null,
      'public' => true,
      'menu_position' => 5,
      'hierarchical' => true,
      'query_var' => true,
      'supports' => array( 'title', 'editor','author', 'excerpt', 'thumbnail', 'comments', 'custom-fields'  ),
      'rewrite' => array( 'slug' => 'team' )
    )
  );
}
add_action( 'init', 'create_my_team_type' );


function create_my_programme_type() {
  register_post_type( 'programmes',
    array(
      'labels' => array(
      'name' => __( 'Programmes','startup' ),
      'singular_name' => __( 'Programme','startup'),
      'add_new' => 'Add New',
      'add_new_item' => 'Add New Programme',
      'edit' => 'Edit',
      'edit_item' =>'Edit Programme',
      'new_item' => 'New Programme',
      'view' => 'View Programmes',
      'view_item' => 'View Programme',
      'search_items' => 'Search Programme',
      'not_found' => 'No Programme found',
      'not_found_in_trash' => 'No Programme found in Trash',
      'parent' => 'Parent Programme',
      ),
        'publicly_queryable' => true,
        'show_ui' => true, 
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_position' => null,
      'public' => true,
      'menu_position' => 5,
      'hierarchical' => true,
      'query_var' => true,
      'supports' => array( 'title', 'author'  ),
      'rewrite' => array( 'slug' => 'programmes' )
    )
  );
}
add_action( 'init', 'create_my_programme_type' );

function create_my_publication_type() {
  register_post_type( 'publications',
    array(
      'labels' => array(
      'name' => __( 'Publications','startup' ),
      'singular_name' => __( 'Publications','startup'),
      'add_new' => 'Add New',
      'add_new_item' => 'Add New Publications',
      'edit' => 'Edit',
      'edit_item' =>'Edit Publications',
      'new_item' => 'New Publications',
      'view' => 'View Publications',
      'view_item' => 'View Publications',
      'search_items' => 'Search Publications',
      'not_found' => 'No Publications found',
      'not_found_in_trash' => 'No Publications found in Trash',
      'parent' => 'Parent Publications',
      ),
        'publicly_queryable' => true,
        'show_ui' => true, 
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_position' => null,
      'public' => true,
      'menu_position' => 5,
      'hierarchical' => true,
      'query_var' => true,
      'supports' => array( 'title', 'editor','author', 'excerpt', 'thumbnail', 'comments', 'custom-fields'  ),
      'rewrite' => array( 'slug' => 'publications' )
    )
  );
}
add_action( 'init', 'create_my_publication_type' );


if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Overview Image',
            'id' => 'page-overview-image',
            'post_type' => 'page'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Overview Image',
            'id' => 'news-overview-image',
            'post_type' => 'news'
        )
    );
}

add_action("admin_init", "pdf_init");
add_action('save_post', 'save_pdf_link');
function pdf_init(){
    add_meta_box("my-pdf", "PDF Document", "pdf_link", "publications", "normal", "high");
    add_meta_box("my-arabic-pdf", "PDF Arabic Document", "pdf_link_ar", "publications", "normal", "high");
}
function pdf_link(){
    global $post;
    $custom  = get_post_custom($post->ID);
    $link    = $custom["link"][0];
    $count   = 0;
    echo '<div class="link_header">';
    $query_pdf_args = array(
        'post_type' => 'attachment',
        'post_mime_type' =>'application/pdf',
        'post_status' => 'inherit',
        'posts_per_page' => -1,
        );
    $query_pdf = new WP_Query( $query_pdf_args );
    $pdf = array();
    echo '<select name="link">';
    echo '<option class="pdf_select">SELECT pdf FILE</option>';
    foreach ( $query_pdf->posts as $file) {
       if($link == $pdf[]= $file->guid){
          echo '<option value="'.$pdf[]= $file->guid.'" selected="true">'.$pdf[]= $file->guid.'</option>';
         }else{
          echo '<option value="'.$pdf[]= $file->guid.'">'.$pdf[]= $file->guid.'</option>';
         }
        $count++;
    }
    echo '</select><br /></div>';
    echo '<p>Selecting a pdf file from the above list to attach to this post.</p>';
    echo '<div class="pdf_count"><span>Files:</span> <b>'.$count.'</b></div>';
}
function pdf_link_ar(){
    global $post;
    $custom  = get_post_custom($post->ID);
    $link    = $custom["link_ar"][0];
    $count   = 0;
    echo '<div class="link_header">';
    $query_pdf_args = array(
        'post_type' => 'attachment',
        'post_mime_type' =>'application/pdf',
        'post_status' => 'inherit',
        'posts_per_page' => -1,
        );
    $query_pdf = new WP_Query( $query_pdf_args );
    $pdf = array();
    echo '<select name="link_ar">';
    echo '<option class="pdf_select">SELECT pdf FILE</option>';
    foreach ( $query_pdf->posts as $file) {
       if($link == $pdf[]= $file->guid){
          echo '<option value="'.$pdf[]= $file->guid.'" selected="true">'.$pdf[]= $file->guid.'</option>';
         }else{
          echo '<option value="'.$pdf[]= $file->guid.'">'.$pdf[]= $file->guid.'</option>';
         }
        $count++;
    }
    echo '</select><br /></div>';
    echo '<p>Selecting a pdf file from the above list to attach to this post.</p>';
    echo '<div class="pdf_count"><span>Files:</span> <b>'.$count.'</b></div>';
}
function save_pdf_link(){
    global $post;
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){ return $post->ID; }
    update_post_meta($post->ID, "link", $_POST["link"]);
    update_post_meta($post->ID, "link_ar", $_POST["link_ar"]);
}
add_action( 'admin_head', 'pdf_css' );
function pdf_css() {
    echo '<style type="text/css">
    .pdf_select{
        font-weight:bold;
        background:#e5e5e5;
        }
    .pdf_count{
        font-size:9px;
        color:#0066ff;
        text-transform:uppercase;
        background:#f3f3f3;
        border-top:solid 1px #e5e5e5;
        padding:6px 6px 6px 12px;
        margin:0px -6px -8px -6px;
        -moz-border-radius:0px 0px 6px 6px;
        -webkit-border-radius:0px 0px 6px 6px;
        border-radius:0px 0px 6px 6px;
        }
    .pdf_count span{color:#666;}
        </style>';
}
function pdf_file_url(){
    global $wp_query;
    $custom = get_post_custom($wp_query->post->ID);
    return $custom['link'][0];
}
function pdf_file_ar_url(){
    global $wp_query;
    $custom = get_post_custom($wp_query->post->ID);
    return $custom['link_ar'][0];
}

/*
function validuser_ajax_process_request() {        
    if (wp_verify_nonce($_POST['csrf_token'], 'csrf_check' ) )
    {
        if ( isset( $_POST["user_name"] ) && isset($_POST["user_pass"])) {
            $user_name = $_POST["user_name"];
            $user_pass = $_POST["user_pass"];
            $user = wp_signon( array( 'user_login' => $user_name, 'user_password' => $user_pass), false );      
            if ( is_wp_error($user) )
            {
                $response = $user->get_error_message();
                echo $response;
                die();
            }
            else
            {
                $response = "success";
                echo $response;     
                die();           
            }
        }
        else
        {
          $response = "required";
          echo $response;
          die();
        }
    }
    else
    {
        $response = 'insecure';
        echo $response;
        die();
    }
}
add_action('wp_ajax_nopriv_user_valid_response', 'validuser_ajax_process_request');
add_action('wp_ajax_user_valid_response', 'validuser_ajax_process_request');
*/

/*
function gettags_ajax_process_request() {        
    //sleep( 2 );
    // no term passed - just exit early with no response
    if (empty($_GET['term'])) exit ;
    $q = strtolower($_GET["term"]);
    // remove slashes if they were magically added
    if (get_magic_quotes_gpc()) $q = stripslashes($q);
    $tags = get_tags();
    $items = array();
    foreach ( $tags as $tag ) {
      $items[$tag->slug] = $tag->name;
    }
    
    $result = array();
    foreach ($items as $key=>$value) {
      if (strpos(strtolower($key), $q) !== false) {
        array_push($result, array("id"=>$key, "label"=>$value, "value" => strip_tags($value)));
      }
      if (count($result) > 11)
        break;
    }
    // json_encode is available in PHP 5.2 and above, or you can install a PECL module in earlier versions
    $output = json_encode($result);
    if ($_GET["callback"]) {
      // Escape special characters to avoid XSS attacks via direct loads of this
      // page with a callback that contains HTML. This is a lot easier than validating
      // the callback name.
      $output = htmlspecialchars($_GET["callback"]) . "($output);";
    }
  echo $output;
  wp_die();
}
add_action('wp_ajax_nopriv_get_tags_response', 'gettags_ajax_process_request');
add_action('wp_ajax_get_tags_response', 'gettags_ajax_process_request');
*/
	
function dshub_add_notices( $message, $type = 'error' ) {
  $_SESSION['messages'][$type][] = $message;
}

function dshub_has_notices( $type = 'error' ) {
  return (bool) isset($_SESSION['messages'][$type]);
}

function dshub_show_notices( $type = 'error' ) {
  if ( dshub_has_notices( $type ) ) {
    echo '<p>'. implode("</p>\n<p>", $_SESSION['messages'][$type]) .'</p>';
    unset($_SESSION['messages'][$type]);
  }
}

function wpse_enqueue_datepicker() {

    // Load the datepicker script (pre-registered in WordPress).
    wp_enqueue_script( 'jquery-ui-datepicker' );
    // You need styling for the datepicker. For simplicity I've linked to Google's hosted jQuery UI CSS.
    wp_register_style( 'jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui' );  

}
add_action( 'admin_enqueue_scripts', 'wpse_enqueue_datepicker', 1 );

function admin_footer() {
  $screen = get_current_screen();
  if ( $screen->id == 'meetup' || $screen->id == 'events' || $screen->id == 'news') {
    ?><script type="text/javascript">
      jQuery(document).ready(function(){
        jQuery('.datepicker').datepicker({
          dateFormat : 'yy-mm-dd'
        });
      });
    </script><?php
    
  }

} //  admin_footer()
add_action( 'admin_print_scripts', 'admin_footer', 1000 );

add_action( 'register_form', 'startup_add_registration_fields', 1000 );
function startup_add_registration_fields() {
    //Get and set any values already sent
    $user_company = ( isset( $_POST['user_company'] ) ) ? $_POST['user_company'] : '';
    $user_industry = ( isset( $_POST['user_industry'] ) ) ? $_POST['user_industry'] : '';

	// Social media login retrieve data
	if(isset($_SESSION['wsl::userprofile']) && $_SESSION['wsl::userprofile']) {
		$wsl_data = json_decode($_SESSION['wsl::userprofile']);
		$first_name = $wsl_data->firstName;
		$last_name = $wsl_data->lastName;
	}
	else
	{
		$first_name = '';
		$last_name = '';
	}
	
	?>
		<div class="rowField fx" data-animate="fadeInUp">	
			<label for="first_name"><?php _e( 'First Name', 'theme-my-login' ); ?></label>
			<input name="first_name" id="first_name" class="form-control" type="text" value="<?php echo $first_name ?>" required  />
		</div>
		
		<div class="rowField fx" data-animate="fadeInUp">
			<label for="last_name"><?php _e( 'Last Name', 'theme-my-login' ); ?></label>
			<input name="last_name" id="last_name" class="form-control"  type="text" value="<?php echo $last_name ?>" required />
		</div>
       
		<div class="form-group fx" data-animate="fadeInUp">
			<label for="company">
				Company
			</label>
			<input name="user_company" id="user_company" class="form-control" type="text" size="25" placeholder="Company">
		</div>

		<div class="form-group fx" data-animate="fadeInUp">
			<label for="company">
				Country
			</label>
			<select name="user_country" id="user_country" class="form-control">
                <option value="">Select Country</option>
				<?php
                global $wpdb;
                $table_name = $wpdb->prefix . 'country_list';
                $query = $wpdb->get_results("SELECT id, country FROM $table_name ORDER BY sort_order");
                foreach($query as $row)
                {
                    echo '<option value="'.$row->country.'">'.$row->country.'</option>';
                }
                ?>
            </select>
		</div>
        <div class="form-group fx" data-animate="fadeInUp">
			<label for="company">
				Your organisation type
			</label>
			<select name="user_org" id="user_org" class="form-control organisation-type">
				<option value="">Select organisation type</option>
				<option value="Startup">Startup</option>
				<!-- <option value="Corporate/ Government">Corporate/ Government</option>
				<option value="Investor">Investor</option> -->
				<option value="Co-working Space">Co-working Space</option>
			</select>
		</div>
        <div class="form-group fx" data-animate="fadeInUp">
			<p class="startup-market" style="display: none">
				<label for="company">
					Your startup market
				</label>
				 <select name="user_industry" id="user_industry" class="form-control field-start-market">
                <option value="">Select startup market</option>
                <?php
                global $wpdb;
                $table_name = $wpdb->prefix . 'industry';
                $query = $wpdb->get_results("SELECT id, industry_name FROM $table_name");
                foreach($query as $row)
                {
                    echo '<option value="'.$row->industry_name.'">'.$row->industry_name.'</option>';
                }
                ?>
            </select>
			</p>
		</div>	
      
    <?php
}

/*
// contact form ajax request
add_action('wp_ajax_nopriv_save_enquiry', 'save_user_enquiry');
add_action('wp_ajax_save_enquiry', 'save_user_enquiry');
function save_user_enquiry() {
	
	if (wp_verify_nonce($_POST['csrf_token'], 'csrf_check' ) )
	{		
		
		if ( isset( $_POST["first_name"] ) && isset( $_POST["family_name"] ) 
			 && isset( $_POST["subject"] ) && isset( $_POST["email"] ) && isset( $_POST["comments"] ) )
		{
			$first_name  = sanitize_text_field( $_POST["first_name"] );
			$family_name = sanitize_text_field( $_POST["family_name"] );
			$email       = sanitize_text_field( $_POST["email"] );
			$subject     = sanitize_text_field( $_POST["subject"] );
			$message     = sanitize_text_field( $_POST["comments"] );
			
			$data =  array('firstname' => $first_name,
						   'lastname'  => $family_name,
						   'email'     => $email,
						   'subject'   => $subject,
						   'message'   => $message,
						   'date_added'=> date('Y-m-d H:i:s'));
			
			// INSERT ENQUIRY DETAILS 
			global $wpdb;
            $table_name = $wpdb->prefix . 'enquiries';
			$res = $wpdb->insert($table_name, $data);	
			if($res)
			{
				// write the email content
				$headers = array('Content-Type: text/html; charset=UTF-8');
	
				$message = "<html>
								<body>
									<p>-- Enquiry via Dubai Startup Hub Contact Form</p><br>
									<table width='100%' border='1'>
										<tr>
											<td>Sender Name: </td>
											<td>".$first_name." ".$family_name."</td>
										<tr>
										<tr>
											<td>Sender Email:</td>
											<td>".$email."</td>
										<tr>
										<tr>
											<td>Subject</td>
											<td>".$subject."</td>
										<tr>
										<tr>
											<td>Message</td>
											<td>".$message."</td>
										<tr>
									<table>
								</body>
							</html>";		

				$subject = $subject;

				//$to = "dubaistartuphub@dubaichamber.com"; //live
				$to = "Refqah.Istaitiyeh@wunderman.com, cristine.ramos@wunderman.com"; //staging

				// send the email using wp_mail()
				if( !wp_mail($to, $subject, $message, $headers) ) {
					$contact_errors = true;
				}

				$response = "success";
				echo $response;
				die();
			}
			else
			{
				$response = "failed";
				echo $response;
				die();
			}
			
		}
		else
		{
		  $response = "required";
		  echo $response;
		  die();
		}		
      
    }
    else
    {
        $response = 'insecure';
        echo $response;
        die();
    }
}
*/

/*
// subscribe to newsletter form ajax request
add_action('wp_ajax_nopriv_save_subscriber', 'save_user_subcription');
add_action('wp_ajax_save_subscriber', 'save_user_subcription');
function save_user_subcription() {
	
	if (wp_verify_nonce($_POST['csrf_token'], 'csrf_check' ) )
	{		
		
		if ( isset( $_POST["email"] ) )
		{
			
			$email = sanitize_text_field( $_POST["email"] );
			
			
			if(!shub_is_email_exist($email))
			{
				$data  =  array('email'     => $email,
						    'date_added'=> date('Y-m-d H:i:s'));
			
				// INSERT ENQUIRY DETAILS 
				global $wpdb;
				$table_name = $wpdb->prefix . 'newsletter_subscribers';
				$res = $wpdb->insert($table_name, $data);	
				if($res)
				{
					$response = "success";
					echo $response;
					die();
				}
				else
				{
					$response = "failed";
					echo $response;
					die();
				}
			}
			else
			{
				// if email already exists
				$response = "duplicate";
				echo $response;
				die();
			}
			
			
		}
		else
		{
		  $response = "required";
		  echo $response;
		  die();
		}		
      
    }
    else
    {
        $response = 'insecure';
        echo $response;
        die();
    }
}
*/

function shub_is_email_exist($email)
{
	//echo "SELECT email FROM '$table_name' WHERE email = '$email' LIMIT 1" ; exit;
	global $wpdb;
	$table_name =  $wpdb->prefix . 'newsletter_subscribers';
	$res        =  $wpdb->get_results( "SELECT email FROM ".$table_name." WHERE email = '".$email."' LIMIT 1" );
	//echo json_encode($res); exit;
	if($wpdb->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}


/*
function addevent_ajax_process_request() {
    //CSRF Check
    if (wp_verify_nonce($_POST['csrf_token'], 'csrf_check' ) )
    {
        if ( isset( $_POST["event_id"] ) ) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'event_attendees';

            $user = wp_get_current_user();
            $user_id = $user->ID;
            $user_email = $user->user_email;
            $user_firstname = $user->user_firstname;

            $post_id = $_POST["event_id"];
            
            $attendee_count = $wpdb->get_var("SELECT COUNT(id) FROM $table_name where post_id=$post_id and user_id=$user_id");            

            if($attendee_count=="0" || $attendee_count == "")
            {
                $date_created = date('Y-m-d h:m:s');
                $item = array('user_id' => $user_id, 'post_id' => $post_id, 'status' => 0, 'date_created' => $date_created);
                $result = $wpdb->insert($table_name, $item);
                if($user_email!="")
                {
				    
                    //$to = $user_email;
                    //$subject = 'Dubai 360 - Thank you for your interest';
                    //$body = '<font face="Arial" size="2">Dear '.$user_firstname.',<br><br>Thank you for your interest in Dubai 360°. Please note that due to limited capacity we are only able to confirm participations on first come first serve basis.<br><br>We will be in touch with you shortly to let you know the status of your participation.<br><br>Kind regards,<br><br>DAC Team<br><img src="http://www.dubaiassociationconference.com/images/email-signature.jpg" border="0"></font>';
                    //$headers = array('Content-Type: text/html; charset=UTF-8');
                    //wp_mail( $to, $subject, $body, $headers );

                    //$a_to = 'Dania.Keilani@Dubaichamber.com';
                    //$a_subject = 'Dubai 360 - Interest Requested';
                    //$a_body = '<font face="Arial" size="2">You have received a new Dubai 360 activity request for Dubai Association Conference. Login the admin panel to approve/reject: <br><br>http://www.dubaiassociationconference.com/wp-login.php<br><br>Kind regards,<br><br>DAC Team<br><img src="http://www.dubaiassociationconference.com/images/email-signature.jpg" border="0"></font>';
                    //$headers = array('Content-Type: text/html; charset=UTF-8');
                    //wp_mail( $a_to, $a_subject, $a_body, $headers ); 
                }
                $response = "added";
                echo $response;     
                die();  
            }
            else
            {
                $response = "present";
                echo $response;     
                die();           
            }
        }
    }
    else
    {
        $response = 'Security Failed';
        echo $response;
        die();
    }
}
add_action('wp_ajax_event_attend_response', 'addevent_ajax_process_request');
add_action('wp_ajax_nopriv_event_attend_response', 'addevent_ajax_process_request');
*/


function tm_additional_profile_fields( $user ) {

    $user_id = $_GET['user_id'];
    $userdata = get_user_meta ($user_id);
	
	//echo '<pre>'; print_r($userdata); exit;

    ?>
    <h3>Extra profile information</h3>

    <table class="form-table">
   	 <tr>
   		 <th><label>Company</label></th>
   		 <td>
   			 <input type="text" value="<?php echo $userdata['Company'][0] ?>" readonly style="width:350px;">
   		 </td>
   	 </tr>
	 <tr>
   		 <th><label>OrganisationType</label></th>
   		 <td>
   			 <input type="text" value="<?php echo $userdata['OrganisationType'][0] ?>" readonly style="width:350px;">
   		 </td>
   	 </tr>
	 <tr>
   		 <th><label>Country</label></th>
   		 <td>
   			 <input type="text" value="<?php echo $userdata['Country'][0] ?>" readonly style="width:350px;">
   		 </td>
   	 </tr>
    </table>
    <?php
}

add_action( 'show_user_profile', 'tm_additional_profile_fields' );
add_action( 'edit_user_profile', 'tm_additional_profile_fields' );

function shub_show_return_home_button()
{
	  global $post;
  	if(in_array($post->post_name, array('marketaccess','university-campaigns','competitions','expo2020','dubai-startup-hub-workshops-trainings','publications-reports-library')))
  	{
  		return false;
  	}	
	  return true;	
}

function shub_bbp_auto_check_subscribe( $checked, $topic_subscribed  ) {

    if( $topic_subscribed == 0 )
        $topic_subscribed = true;

    return checked( $topic_subscribed, true, false );
}
add_filter( 'bbp_get_form_topic_subscribed', 'shub_bbp_auto_check_subscribe', 10, 2 );

/*
add_action('admin_head', 'forum_live_css');

function forum_live_css() {
  echo '<style>
    #forum_dashboard_widget .bullet {
        height: 10px;
		width: 10px;
		background: #ddd;
		display: inline-block;
		border-radius: 50%;
		margin-right: 5px;
    } 
	#forum_dashboard_widget .bullet.on {
	     background: #3fd85a;
	}
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}


	.switch input {display:none;}


	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
}

  </style>';
}

add_action('wp_dashboard_setup', 'forum_live_widgets');
  
function forum_live_widgets() {
	global $wp_meta_boxes;
	
	wp_add_dashboard_widget('forum_dashboard_widget', '<span class="bullet"></span>Forum Live', 'forum_live_widget_content');
}
 
function forum_live_widget_content() {
	
?>
<p>Phasellus nec sem in justo pellentesque facilisis. Curabitur at lacus ac velit ornare lobortis. Fusce egestas elit eget lorem. Etiam feugiat lorem non metus. Aliquam eu nunc.</p>


<?php

	global $wpdb;
	$table_name =  $wpdb->prefix . 'forum_go_live';
	$res        =  $wpdb->get_results( "SELECT status FROM ".$table_name." LIMIT 1" );
	$checked = "";
	//echo json_encode($res); exit;
	if($wpdb->num_rows > 0)
	{
		$stat = $res[0]->status;
		
		$checked = ($stat == "on")? "checked": "";
	}
	else
	{
		$stat = "";	
	}
	
?>

<label class="switch">
  <input type="checkbox" id="forumlive_switch" value="<?php echo $stat ?>" <?php echo $checked ?> >
  <span class="slider round"></span>
</label>

<form id="forumlive_form" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post">
	<?php wp_nonce_field('update_forumlive_status'); ?>
	<input type="hidden" name="forum_live_status" id="forum_live_status" value="<?php echo $stat ?>">
	<input type="hidden" name="action" value="custom_action_hook">
	<input type="submit" id="btn-submit-forumlive" style="display:none;">
</form>
<?php
}

function my_admin_enqueue($hook) {
    if ('index.php' === $hook) {
       wp_enqueue_script('forum_live_script', get_stylesheet_directory_uri() . '/js/admin_custom.js');
    }  
}

add_action('admin_enqueue_scripts', 'my_admin_enqueue');


add_action( 'admin_post_custom_action_hook', 'forumlive_status_submit' );

function forumlive_status_submit() {
	if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] )) {
		if($_POST['action'] == "custom_action_hook")
		{
			$retrieved_nonce = $_REQUEST['_wpnonce'];
            if (!wp_verify_nonce($retrieved_nonce, 'update_forumlive_status' ))
            {
                die( 'Failed security check' );
            }
			else
			{
				$val = sanitize_text_field($_POST['forum_live_status']);
				global $wpdb;

				$table = $wpdb->prefix."forum_go_live";
				//var_dump($val);
				//echo $wpdb->update($table, array('status'=>$val), array('status'=> '!= ""'));
				$sql =  $wpdb->prepare("UPDATE ".$table." 
									   SET status = %s  
									   WHERE status != '' ",  
									   $val );
				//echo $sql; exit;
				$wpdb->query($sql);
                $url = home_url('wp-admin');
				wp_redirect($url);
			}
         
		}
		
	}
}


function shub_get_forum_stat()
{
	global $wpdb;
	$table_name =  $wpdb->prefix . 'forum_go_live';
	$res        =  $wpdb->get_results( "SELECT status FROM ".$table_name." LIMIT 1" );

	if($wpdb->num_rows > 0)
	{
		$stat = $res[0]->status;
		
	}
	else
	{
		$stat = "";	
	}
	
	return $stat;
}

function shub_show_forum_live()
{
	$post_id = get_the_ID();
	$post_type = get_post_type();
	$post_name = get_post($post_id)->post_name;

	if($post_type != 'forum'){
		if($post_name != 'forum'){
			if(shub_get_forum_stat() == "on" ){
			 return get_template_part( 'templates/forum-live-bottom-sticky' );
			}
		}
	}
	
}
*/


/**
 * Remove the password fields from the registration form for the social login
 *
 **/
function dshub_hide_passwords_to_social( $provider, $redirect_to, $hybridauth_user_profile ) {
  if ( class_exists( 'Theme_My_Login_Custom_Passwords' ) ) {
    $cls = Theme_My_Login_Custom_Passwords::get_object();
    remove_action( 'register_form', array( $cls, 'password_fields' ) );
    remove_action( 'registration_errors', array( $cls, 'password_errors' ) );
  }
}
add_action( 'wsl_process_login_new_users_gateway_start', 'dshub_hide_passwords_to_social', 10, 3 );