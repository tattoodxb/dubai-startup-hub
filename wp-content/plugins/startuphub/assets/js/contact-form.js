jQuery(function($) {
	var eles = $('.wpcf7-form').find('#trade-license-authority, #trade-license-type, #city, #nationality');
	if (eles.length) {
		$.get(siteURL +'/wp-content/plugins/startuphub/assets/data/contact_form.json', function(res) {
			$.each(eles, function(eleKey, ele) {
				var element = $(ele);
				$.each(res[element.attr('id')], function(key, obj) {
					element.append('<option value="'+ obj.name +'">'+ obj.name +'</option>');
				});
			});
		});
	}
});