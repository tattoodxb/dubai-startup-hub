<?php
/*
Plugin Name: Startup Hub Events
Description: This plugin is for Dubai Startup Hub Events
Version:     1.0
Author:      Tattoo 360
*/

if(is_admin())
{
    new Dac_Events_List_Table();
}
class Dac_Events_List_Table
{
    public function __construct()
    {
        add_action( 'admin_menu', array($this, 'add_menu_event_list_table_page' ));
    }

    public function add_menu_event_list_table_page()
    {
        add_submenu_page( 'users.php', 'Event Attendees', 'Event Attendees', 'edit_users', 'event-attendees', array($this, 'list_table_page') );
        add_submenu_page( null, 'Event Attendees', 'Event Attendees', 'edit_users', 'event-attendees-list', array($this, 'list_users_table_page') );
    }

    public function list_table_page()
    {
        $exampleListTable = new Event_List_Table();
        $exampleListTable->prepare_items();
        ?>
            <div class="wrap">
                <div id="icon-users" class="icon32"></div>
                <h2>Event Attendees</h2>
                <?php $exampleListTable->display(); ?>
            </div>
        <?php
    }

    public function list_users_table_page()
    {
        $eventUsersListTable = new Event_Users_List_Table();
        $eventUsersListTable->prepare_items();

        ?>
            <div class="wrap">
                <div id="icon-users" class="icon32"></div>
                <h2>Event Attendees - Users List <a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=event-attendees');?>"><?php _e('back to list', 'shub_events')?></a></h2>
                <?php 
                if(isset($_GET["post_id"]))
                {
                    $post_id = $_GET["post_id"];                
                ?>
                <h3>EVENT NAME: <?php echo get_the_title( $post_id ); ?></h3>
                <?php 
                }
                ?>
                <?php $eventUsersListTable->display(); ?>
            </div>
        <?php
    }
}

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Event_List_Table extends WP_List_Table
{
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $data = $this->table_data();
        $this->process_bulk_action();
        usort( $data, array( &$this, 'sort_data' ) );
        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);
        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );
        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }
    public function get_columns()
    {
        $columns = array(
            'id'          => 'ID',
            'title'       => 'Event Title',
            'attendee_count' => 'Attendees Registered',
            'action'        => 'Action'
        );
        return $columns;
    }
    public function get_hidden_columns()
    {
        return array();
    }
    public function get_sortable_columns()
    {
        return array('title' => array('title', false));
    }
    private function table_data()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'event_attendees';

        $args = array(
            'post_type'=> 'events',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'sh_event_enable',
                    'value' => 'true',
                ),
            ),
        );
        $query = new WP_Query( $args );
        $posts = $query->posts;

        $data = array();
        foreach($posts as $post) {
            $post_id = $post->ID;
            $attendee_count = $wpdb->get_var("SELECT COUNT(id) FROM $table_name where post_id=$post_id");
            $data[] = array(
                    'id'          => $post_id,
                    'title'       => $post->post_title,
                    'attendee_count' => $attendee_count,
                    'action'        => '<a href="?page=event-attendees-list&post_id='.$post_id.'">View List</a>'
                    );    
        }
        /*$data[] = array(
                    'id'          => 1,
                    'title'       => 'Event 1',
                    'attendee_count' => '3',
                    'action'        => 'View List'
                    );*/
        return $data;
    }
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'id':
            case 'title':
            case 'attendee_count':
            case 'action':
                return $item[ $column_name ];
            default:
                return print_r( $item, true ) ;
        }
    }
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'title';
        $order = 'asc';
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
        $result = strcmp( $a[$orderby], $b[$orderby] );
        if($order === 'asc')
        {
            return $result;
        }
        return -$result;
    }
}

class Event_Users_List_Table extends WP_List_Table
{
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $this->process_bulk_action();
        $data = $this->table_data();        
        usort( $data, array( &$this, 'sort_data' ) );
        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);
        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );
        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }
    public function get_columns()
    {
        $columns = array(
            'id'          => 'ID',
            'title'       => 'Registered Name',
            'email' => 'Email',
            'status' => 'Status',
            'action'        => 'Action'
        );
        return $columns;
    }
    public function get_hidden_columns()
    {
        return array();
    }
    public function get_sortable_columns()
    {
        return array('title' => array('title', false));
    }
    private function table_data()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'event_attendees';

        if(isset($_GET["post_id"]))
        {
            $post_id = $_GET["post_id"];
        }
        else
        {
            $post_id = "0";
        }

        $query = $wpdb->get_results("SELECT * FROM $table_name where post_id=$post_id");
        $data = array();
        foreach($query as $row)
        {
            $user = get_user_by( 'id', $row->user_id );
            $reg_user = $user->first_name . ' ' . $user->last_name;
            $status = $row->status;
            $row_id = $row->id;
            if($status=='0')
            {
                $status_label = '<span style="color:orange">Pending</span>';
            }
            else if($status=='1')
            {
                $status_label = '<span style="color:green">Approved</span>';
            }
            else if($status=='2')
            {
                $status_label = '<span style="color:red">Rejected</span>';
            }
            else if($status=='3')
            {
                $status_label = '<span style="color:blue">Waiting List</span>';
            }

            $data[] = array(
                    'id'          => $row_id,
                    'title'       => $reg_user,
                    'email' =>  $user->user_email,
                    'status' =>  $status_label,
                    'action'        => ''
                    );  
        }
        return $data;        
    }
    public function column_default( $item, $column_name )
    {
        return $item[$column_name];
    }
    public function column_action($item){
        $actions=array(
            'approvee'=>sprintf('<a href="?post_id=%s&page=%s&action=%s&id=%s">Approve</a>',$_REQUEST['post_id'], $_REQUEST['page'], 'approvee', $item['id']),
            'reject'=>sprintf('<a href="?post_id=%s&page=%s&action=%s&id=%s">Reject</a>', $_REQUEST['post_id'], $_REQUEST['page'], 'reject', $item['id']),
            'waiting'=>sprintf('<a href="?post_id=%s&page=%s&action=%s&id=%s">Waiting List</a>', $_REQUEST['post_id'], $_REQUEST['page'], 'waiting', $item['id']),
        );
        //Return the title contents
        return sprintf('%s %s',
            /*$1%s*/ 'Update Status',
            /*$2%s*/ $this->row_actions($actions)
        );
    }

    public function process_bulk_action(){
        global $wpdb;
        $tablename=$wpdb->prefix."event_attendees";
        $id = $_GET['id'];

        if( 'approvee'===$this->current_action() ) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'event_attendees';
            $query = $wpdb->get_results("SELECT * FROM $table_name where id=$id");
            foreach($query as $row)
            {
                $user = get_user_by( 'id', $row->user_id );            
            }

            $user_email = $user->user_email;
            $user_firstname = $user->first_name;
            if($user_email!="")
            {
                /*$to = $user_email;
                $subject = 'Startup Hub - Registration Accepted';
                $body = 'Dear '.$user_firstname.',<br><br>Thank you for your interest in Startup Hub. We are pleased to inform you that your registration for the selected Startup Hub activity has been successful. <br><br>We look forward to your participation!<br><br>Kind regards,<br><br>DAC Team';
                $headers = array('Content-Type: text/html; charset=UTF-8');
                wp_mail( $to, $subject, $body, $headers );*/
            }
            $result=$wpdb->query("UPDATE $tablename SET status=1 WHERE id=$id");
        }
        if( 'reject'===$this->current_action() ) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'event_attendees';
            $query = $wpdb->get_results("SELECT * FROM $table_name where id=$id");
            foreach($query as $row)
            {
                $user = get_user_by( 'id', $row->user_id );            
            }
            $user_email = $user->user_email;
            $user_firstname = $user->first_name;            
            if($user_email!="")
            {
                /*$to = $user_email;
                $subject = 'Startup Hub - Registration Rejected';
                $body = 'Dear '.$user_firstname.',<br><br>Thank you for your interest in Startup Hub. Unfortunately, due to limited capacity, we were not able to accommodate your request to attend the selected Startup Hub activity on this occasion. We do however hope you will enjoy the rest of the Dubai Association Conference.<br><br>Kind regards,<br><br>DAC Team';
                $headers = array('Content-Type: text/html; charset=UTF-8');
                wp_mail( $to, $subject, $body, $headers );*/
            }
            $result=$wpdb->query("UPDATE $tablename SET status=2 WHERE id=$id");
        }
        if( 'waiting'===$this->current_action() ) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'event_attendees';
            $query = $wpdb->get_results("SELECT * FROM $table_name where id=$id");
            foreach($query as $row)
            {
                $user = get_user_by( 'id', $row->user_id );            
            }
            $user_email = $user->user_email;
            $user_firstname = $user->first_name;            
            if($user_email!="")
            {
                /*$to = $user_email;
                $subject = 'Startup Hub - Thank you';
                $body = 'Dear '.$user_firstname.',<br><br>Many thanks for your interest in the Startup Hub activities. Kindly note that for the moment we have reached full capacity. We are doing our best to accommodate all requests and will update you as soon as we are able to increase capacity.<br><br>Kind regards,<br><br>DAC Team';
                $headers = array('Content-Type: text/html; charset=UTF-8');
                wp_mail( $to, $subject, $body, $headers );*/
            }
            $result=$wpdb->query("UPDATE $tablename SET status=3 WHERE id=$id");
        }
    }
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'title';
        $order = 'asc';
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
        $result = strcmp( $a[$orderby], $b[$orderby] );
        if($order === 'asc')
        {
            return $result;
        }
        return -$result;
    }
}

?>