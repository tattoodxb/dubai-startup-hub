<?php

function shub_edit_enquiry() {
     
    ?>
    
    <div class="wrap">
        <h2>Enquiry Details</h2>
        
        <?php
 
        $id   = $_GET['e_id']; 
        $row = shub_edit_enquiry_details($id);
	  
        ?>
		
		<form action="" method="post" name="shub_enquiry_form" id="shub_enquiry_form">
			
			<table class='wp-list-table widefat fixed striped posts'>
				<tr>
					<td class="manage-column ss-list-width">
						<label>Date Received</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->date_added ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Full Name</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->firstname .' '. $row->lastname; ?></label>
					</td>
				</tr>
				
				<tr>
					<td class="manage-column ss-list-width">
						<label>Email</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->email ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Subject</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->subject ?></label>
					</td>
					
				</tr>
				
				
				<tr>
					<td class="manage-column ss-list-width">
						<label>Message</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->message ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Status</label>
					</td>
					<td class="manage-column ss-list-width">
						
						<select name="e_status">
							<option value="open" <?php echo ($row->status != "closed")? "selected" : ""; ?>>open</option>	
							<option value="closed" <?php echo ($row->status == "closed")? "selected" : ""; ?>>closed</option>	
						</select>
					</td>
					
				</tr>
				
				<tr>
					<td class="manage-column ss-list-width">
						<label>Remarks</label>
					</td>
					<td class="manage-column ss-list-width">
						<textarea name="e_remarks" style="height:100px; width:100%;"><?php echo $row->remarks ?></textarea>
					</td>	
					<td class="manage-column ss-list-width">
						<label></label>
					</td>	
					<td class="manage-column ss-list-width">
						<label></label>
					</td>	
				</tr>
				
			</table>
			
			<table class='wp-list-table widefat fixed striped posts'>
				<tr>
					<td class="manage-column ss-list-width">

					</td>
					<td class="manage-column ss-list-width">

					</td>
					<td class="manage-column ss-list-width">

					</td>
					<td class="manage-column ss-list-width">
						<input type="hidden" name="e_id" id="e_id" style="width:100%" value="<?php echo $row->id ?>">	
						<input type="submit" name="update" value="Update" style="background: #46b450;
																							color: #fff;
																							border-color: #46b450;"> |
				
						<a href="admin.php?page=shub_enquiry_list">Back to List</a>
					
					</td>
					

				</tr>
			</table>
			
		</form>
    </div>
    <?php
}