<?php

/*
  Plugin Name: Startup Hub Subscriber And Enquiries
  Description:
  Version: 1
  Author: Cristine Ramos
 */


//menu items
add_action('admin_menu', 'shub_enquiry_menu');

function shub_enquiry_menu() {

    add_menu_page('Members & Subscribers', //page title
            'Members & Subscribers', //menu title
            'enquiry_manage_options', //capabilities
            'shub_members_list', //menu slug
            'shub_members_list', //function
            'dashicons-email'
    );

    //this submenu is HIDDEN, however, we need to add it anyways
    add_submenu_page('shub_members_list', //parent slug
            'Members', //page title
            'Members', //menu title
            'enquiry_manage_options', //capability
            'shub_members_list', //menu slug
            'shub_members_list'); //function
    
    //this submenu is HIDDEN, however, we need to add it anyways
    add_submenu_page('shub_members_list', //parent slug
            'Subcriber', //page title
            'Subcriber', //menu title
            'subscriber_manage_options', //capability
            'shub_subscriber_list', //menu slug
            'shub_subscriber_list'); //function

    //this submenu is HIDDEN, however, we need to add it anyways
    add_submenu_page('shub_members_list', //parent slug
            'Enquiries', //page title
            'Enquiries', //menu title
            'enquiry_manage_options', //capability
            'shub_enquiry_list', //menu slug
            'shub_enquiry_list'); //function

    //this submenu is HIDDEN, however, we need to add it anyways
    add_submenu_page('shub_members_list', //parent slug
            'Market Access', //page title
            'Market Access', //menu title
            'enquiry_manage_options', //capability
            'shub_marketaccess_enquiry_list', //menu slug
            'shub_marketaccess_enquiry_list'); //function
    //this submenu is HIDDEN, however, we need to add it anyways
    add_submenu_page('shub_members_list', //parent slug
            'Co Founder', //page title
            'Co Founder', //menu title
            'enquiry_manage_options', //capability
            'shub_cofounder_enquiry_list', //menu slug
            'shub_cofounder_enquiry_list'); //function
    //this submenu is HIDDEN, however, we need to add it anyways
    add_submenu_page(null, //parent slug
            'Update Status', //page title
            'Update Status', //menu title
            'enquiry_manage_options', //capability
            'shub_update_enquiry_stat', //menu slug
            'shub_update_enquiry_stat'); //function
    //this submenu is HIDDEN, however, we need to add it anyways
    add_submenu_page(null, //parent slug
            'Edit Enquiry', //page title
            'Edit Enquiry', //menu title
            'enquiry_manage_options', //capability
            'shub_edit_enquiry', //menu slug
            'shub_edit_enquiry'); //function

    //this submenu is HIDDEN, however, we need to add it anyways
    add_submenu_page(null, //parent slug
            'View Members', //page title
            'View Members', //menu title
            'enquiry_manage_options', //capability
            'shub_view_member', //menu slug
            'shub_view_member'); //function
}

function shub_edit_enquiry_details($id) {

    if (isset($_POST['update'])) {
        $user = wp_get_current_user();

        global $wpdb;

        $e_id = sanitize_text_field($_POST['e_id']);

        $data = array("remarks" => sanitize_text_field($_POST['e_remarks']),
            "id" => $e_id,
            "status" => sanitize_text_field($_POST['e_status']),
            "updated_by" => $user->user_email);

        $table = $wpdb->prefix . "enquiries";

        $wpdb->update($table, $data, array('id' => $e_id));
    }

    $e_id = sanitize_text_field($id);

    if ($e_id != "") {
        global $wpdb;

        $sql = "SELECT * 
				FROM " . $wpdb->prefix . "enquiries
				WHERE id='" . $e_id . "' LIMIT 1";

        $result = $wpdb->get_results($sql);

        return $result[0];
    }
}

function shub_view_member_details($id) {

    $e_id = sanitize_text_field($id);

    if ($e_id != "") {
        global $wpdb;

        $sql = "SELECT * 
                FROM " . $wpdb->prefix . "members
                WHERE id='" . $e_id . "' LIMIT 1";

        $result = $wpdb->get_results($sql);

        return $result[0];
    }
}

function shub_update_enquiry_status($id) {

    $e_id = sanitize_text_field($id);

    if ($e_id != "") {
        $user = wp_get_current_user();

        global $wpdb;

        $table = $wpdb->prefix . "enquiries";

        $wpdb->update($table, array('id' => $e_id, 'status' => 'closed', 'updated_by' => $user->user_email), array('id' => $e_id));
    }
}

function shub_get_all_enquiries() {
    global $wpdb;

    $sql = "SELECT * 
			FROM " . $wpdb->prefix . "enquiries
			ORDER BY date_added DESC";

    $result = $wpdb->get_results($sql);

    return $result;
}

function shub_get_all_members() {
    global $wpdb;

    $sql = "SELECT * 
            FROM " . $wpdb->prefix . "members
            ORDER BY date_created DESC";

    $result = $wpdb->get_results($sql);

    return $result;
}

function shub_get_all_subscribers() {
    global $wpdb;

    $sql = "SELECT * 
			FROM " . $wpdb->prefix . "newsletter_subscribers
			ORDER BY date_added DESC";    
    $result = $wpdb->get_results($sql);

    return $result;
}

function shub_get_all_market_access_enquiries() {
    global $wpdb;

    $sql = "SELECT * 
			FROM " . $wpdb->prefix . "market_access_enquiries
			ORDER BY date_created DESC";

    $result = $wpdb->get_results($sql);

    return $result;
}

function shub_get_all_cofounder_enquiries() {
    global $wpdb;

    $sql = "SELECT * 
			FROM " . $wpdb->prefix . "cofounder_enquiries
			ORDER BY date_created DESC";

    $result = $wpdb->get_results($sql);

    return $result;
}

function shub_admin_enqueue($hook) {
    //var_dump($hook); exit;
    if ('members-subscribers_page_shub_enquiry_list' == $hook || 'members-subscribers_page_shub_subscriber_list' == $hook || 'toplevel_page_shub_members_list' == $hook || 'toplevel_page_shub_enquiry_list' == $hook) {
        wp_enqueue_style('my_custom_style', plugin_dir_url(__FILE__) . 'datatables.css');
        wp_enqueue_script('my_custom_script', plugin_dir_url(__FILE__) . 'datatables.js');
        wp_enqueue_script('my_admin_script', plugin_dir_url(__FILE__) . 'admin_sys.js');
    }
}

add_action('admin_enqueue_scripts', 'shub_admin_enqueue');


define('ROOTDIR', plugin_dir_path(__FILE__));
require_once(ROOTDIR . 'enquiry-list.php');
require_once(ROOTDIR . 'members-list.php');
require_once(ROOTDIR . 'market-access-enquiry-list.php');
require_once(ROOTDIR . 'cofounder-enquiry-list.php');
require_once(ROOTDIR . 'subscriber-list.php');
require_once(ROOTDIR . 'enquiry-update-stat.php');
require_once(ROOTDIR . 'enquiry-edit.php');
require_once(ROOTDIR . 'member-details.php');

