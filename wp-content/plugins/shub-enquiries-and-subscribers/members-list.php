<?php

function shub_members_list() {
    ?>
   
    <div class="wrap">
        <h2>Members</h2>
        
        <?php
        $rows = shub_get_all_members();
	
        ?>
	
        <table class='wp-list-table widefat fixed striped posts'>
 
			<thead>
				 <tr>
					<th class="manage-column ss-list-width">DATE</th>
					<th class="manage-column ss-list-width">FULLNAME</th>
					<th class="manage-column ss-list-width">EMAIL</th>
					<th class="manage-column ss-list-width">MOBILE</th>
					<th class="manage-column ss-list-width">NATIONALITY</th>
					<th class="manage-column ss-list-width">COMPANY NAME</th>
					<th class="manage-column ss-list-width">JOB TITLE</th>
					<th class="manage-column ss-list-width"></th>

				</tr>
			</thead>
			<tbody>
			   <?php foreach ($rows as $row) { 
				?>
					<tr>
						<td class="manage-column ss-list-width"><?php echo $row->date_created; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->firstname .' '. $row->lastname; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->email; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->mobilenumber; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->nationality; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->companyname; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->jobtitle; ?></td>
						<td class="manage-column ss-list-width"><a href="admin.php?page=shub_view_member&e_id=<?php echo $row->id ?>">View Details</a></td>
					</tr>
				<?php } ?>
			</tbody>
        </table>
    </div>
    <?php
}