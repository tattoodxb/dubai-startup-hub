<?php

function shub_view_member() {
     
    ?>
    
    <div class="wrap">
        <h2>Member Details</h2>
        
        <?php
 
        $id   = $_GET['e_id']; 
        $row = shub_view_member_details($id);
	  
        ?>
		
		<form action="" method="post" name="shub_enquiry_form" id="shub_enquiry_form">
			
			<table class='wp-list-table widefat fixed striped posts'>
				<tr><th colspan="4"><b>Personal details</b></th></tr>
				<tr>
					<td class="manage-column ss-list-width">
						<label>First Name</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->firstname ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Last Name</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->lastname; ?></label>
					</td>
				</tr>
				
				<tr>
					<td class="manage-column ss-list-width">
						<label>Email</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->email ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Mobile</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->mobilenumber ?></label>
					</td>
					
				</tr>
				
				<tr>
					<td class="manage-column ss-list-width">
						<label>Nationality</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->nationality ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>How did you hear about us?</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->source ?></label>
					</td>
					
				</tr>
				<tr><th colspan="4"><b>Company details</b></th></tr>
				<tr>
					<td class="manage-column ss-list-width">
						<label>Company Name</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->companyname ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Job Title</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->jobtitle ?></label>
					</td>
					
				</tr>
				<tr>
					<td class="manage-column ss-list-width">
						<label>Website</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->website ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Industry Sector</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->industrysector ?></label>
					</td>
					
				</tr>
				<tr>
					<td class="manage-column ss-list-width">
						<label>Emirate/City</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->city ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Trade License Authority</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->tradelicense_authority ?></label>
					</td>
					
				</tr>
				<tr>
					<td class="manage-column ss-list-width">
						<label>Trade License #</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php echo $row->tradelicense_number ?></label>
					</td>
					<td class="manage-column ss-list-width">
						<label>Trade License File</label>
					</td>
					<td class="manage-column ss-list-width">
						<label><?php 
						if(!empty($row->tradelicense_file))
						{
							echo '<a href="' . $row->tradelicense_file . '" target="_blank">View File</a>';
						}
						else
						{
						 	echo 'N/A';
						}
						?></label>
					</td>
					
				</tr>
				<tr>
					<td class="manage-column ss-list-width">
						<label>Overview</label>
					</td>
					<td class="manage-column ss-list-width" colspan="3">
						<label><?php echo $row->overview ?></label>
					</td>
					
				</tr>
			</table>
			
			<table class='wp-list-table widefat fixed striped posts'>
				<tr>
					<td class="manage-column ss-list-width">

					</td>
					<td class="manage-column ss-list-width">

					</td>
					<td class="manage-column ss-list-width">

					</td>
					<td class="manage-column ss-list-width" style="text-align: right;">
						<a href="admin.php?page=shub_members_list" class="button">Back to Member List</a>
					
					</td>
					

				</tr>
			</table>
			
		</form>
    </div>
    <?php
}