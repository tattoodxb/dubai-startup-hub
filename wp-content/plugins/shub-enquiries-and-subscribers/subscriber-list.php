<?php

function shub_subscriber_list() {
    ?>
   
    <div class="wrap">
        <h2>Subscribers</h2>
        
        <?php
        $rows = shub_get_all_subscribers();
	
        ?>
        <table class='wp-list-table widefat fixed striped posts'>
            <thead>
				<tr>
					<th class="manage-column ss-list-width">DATE</th>
					<th class="manage-column ss-list-width">FIRST NAME</th>
                    <th class="manage-column ss-list-width">LAST NAME</th>
                    <th class="manage-column ss-list-width">EMAIL</th>
				</tr>
			</thead>
			<tbody>
            <?php foreach ($rows as $row) { ?>
                <tr>
					<td class="manage-column ss-list-width"><?php echo $row->date_added; ?></td>
                    <td class="manage-column ss-list-width"><?php echo $row->firstname; ?></td>
                    <td class="manage-column ss-list-width"><?php echo $row->lastname; ?></td>
                    <td class="manage-column ss-list-width"><?php echo $row->email; ?></td>
                </tr>
            <?php } ?>
			</tbody>
        </table>
    </div>
    <?php
}