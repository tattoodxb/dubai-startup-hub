<?php

function shub_marketaccess_enquiry_list() {
    ?>

    <div class="wrap">
        <h2>Market Access Enquiries</h2>

        <?php
        $rows = shub_get_all_market_access_enquiries();
        ?>

        <table class='wp-list-table widefat fixed striped posts'>

            <thead>
                <tr>
                    <th class="manage-column ss-list-width">DATE</th>
                    <th class="manage-column ss-list-width">FULLNAME</th>
                    <th class="manage-column ss-list-width">EMAIL</th>
                    <th class="manage-column ss-list-width">CONTACT</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row) { ?>
                    <tr>
                        <td class="manage-column ss-list-width"><?php echo $row->date_created; ?></td>
                        <td class="manage-column ss-list-width"><?php echo $row->name ?></td>
                        <td class="manage-column ss-list-width"><?php echo $row->email; ?></td>
                        <td class="manage-column ss-list-width"><?php echo $row->contact; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php
}
