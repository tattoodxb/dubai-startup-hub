<?php

function shub_enquiry_list() {
    ?>
   
    <div class="wrap">
        <h2>Enquiries</h2>
        
        <?php
        $rows = shub_get_all_enquiries();
	
        ?>
	
        <table class='wp-list-table widefat fixed striped posts'>
 
			<thead>
				 <tr>
					<th class="manage-column ss-list-width">DATE</th>
					<th class="manage-column ss-list-width">FULLNAME</th>
					<th class="manage-column ss-list-width">EMAIL</th>
					<th class="manage-column ss-list-width">SUBJECT</th>
					<th class="manage-column ss-list-width">MESSAGE</th>
					<th class="manage-column ss-list-width">STATUS</th>
					<th class="manage-column ss-list-width">REMARKS</th>
					<th class="manage-column ss-list-width"></th>

				</tr>
			</thead>
			<tbody>
			   <?php foreach ($rows as $row) { 
				if($row->status == "" || $row->status == "open" ){
					$url = "admin.php?page=shub_update_enquiry_stat&e_id=". $row->id;
					$str = '| <a href="javascript:void;" onclick="update_status(\''.$url.'\')">Mark as Closed</a>';
				}
				else
				{
					$str = "";
				}
				
				
				?>
					<tr>
						<td class="manage-column ss-list-width"><?php echo $row->date_added; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->firstname .' '. $row->lastname; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->email; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->subject; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->message; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->status; ?></td>
						<td class="manage-column ss-list-width"><?php echo $row->remarks; ?></td>
						<td class="manage-column ss-list-width"><a href="mailto:<?php echo $row->email; ?>?subject=RE:<?php echo $row->subject; ?>">Reply</a> <?php echo $str ?> | <a href="admin.php?page=shub_edit_enquiry&e_id=<?php echo $row->id ?>">Edit</a></td>
					</tr>
				<?php } ?>
			</tbody>
        </table>
    </div>
    <?php
}