<?php
/* Template Name: News Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<div class="white-wrapper" id="mainContent">
  <div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
			<h3 class="titleHeader">Find out about the latest news</h3></div>
      <?php
            $args = array(
                'post_type'=> 'news',
                'post_status' => 'publish',
                'meta_key' => 'sh_news_date',
                'orderby'=> 'meta_value',
                'order'    => 'DESC',
                'posts_per_page' => 10,
                'paged' => get_query_var( 'paged' )
            );

            if(isset($_GET["tag"]))
              {
                $tag = $_GET["tag"];
              }

              if($tag != "")
              {
                $args['tag'] = $tag;
                echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the keyword <em><b>'.$tag.'</b></em></div>';

              }

              if(isset($_GET["date"]))
              {
                $date = $_GET["date"];
              }

              if($date != "")
              {
                  $date = date('Y-m-d', strtotime($date));
                  $args['meta_query'] = array(
                    array(
                      'key'     => 'sh_news_date',
                      'value'   => $date,
                      'compare' => '=',
                      'type' => 'DATE'
                    ),
                  );
                  echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the events on <em><b>'.$date.'</b></em></div>';
              }

            $wp_query = new WP_Query($args);
            $i=1;
            while (have_posts()) : the_post();
              $sh_news_location = get_post_meta($post->ID, 'sh_news_location', $single = true);
              $sh_news_display_date = get_post_meta($post->ID, 'sh_news_display_date', $single = true);
              $post_tags = get_the_tags();
            ?>
             <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">               
                <div class="questionBox paddingno" >
                    <div class="imageEvents"><a href="<?php echo the_permalink();?>" class="ls-modal"><?php if (class_exists('MultiPostThumbnails')) :
                            MultiPostThumbnails::the_post_thumbnail(
                                get_post_type(),
                                'news-overview-image' , null, 'post-thumbnail', array('class' => 'img-responsive' )
                            );
                        endif; ?></a></div>                 
                    <div class="eventBoxHolder">
                        <div class="titleEventbold coloringBlue"><?php echo $sh_news_display_date;?><span class="titleEventNormal coloringBlue">  //  </span><span class="titleEventNormal coloringBlue"><?php echo $sh_news_location;?></span></div>
                        <div class="titleEvent coloringOrange"><?php echo get_the_title();?></div>

                        <div class="descEvent"><?php echo get_the_excerpt();?></div>
                        <span class="readmoreRightcolumn"><a href="<?php echo the_permalink();?>" class="ls-modal">Read more</a></span>
                        <div class="tags">
                              <?php
                                    $posttags = $post_tags;
                                    if ($posttags) {
                                        foreach($posttags as $tag) {
                                ?>
                                <span class="tagCopy"><?php echo $tag->name;?></span>
                                <?php
                                        }
                                    }
                                ?>
                          </div>
                    </div>

                </div>

            </div>
    <?php 
            $i++;
            endwhile; 
            ?>
            <?php
            if($i==1)
                {
                    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue"><br>No result found for your search, please refine your search</div>';
                }
            ?>
  </div>
</div>
<?php get_footer(); ?>