jQuery( document ).ready(function($) {
    $('.btn_article').on('click', function(e){
		e.preventDefault();
		var strURL = $(this).attr('href');
		var strCategory = $('#slct option:selected').val();
		document.location.href = strURL + strCategory;
	});

    $('.top_search_button').on('click', function(e){
        e.preventDefault();
        var keyword = $('#top_search').val();
        document.location.href = siteURL + 'search/?keyword=' + keyword;
    });

    $('.forum_search_btn').on('click', function(e){
        e.preventDefault();
        var keyword = $('#forum_search').val();
        document.location.href = siteURL + 'forum/?keyword=' + keyword;
    });

    $('#top_search').keyup(function(e){
        //console.log(e.keyCode);
        if(e.keyCode == 13)
        {
            $('.top_search_button').trigger("click");
        }
    });

    $('#forum_search').keyup(function(e){
        //console.log(e.keyCode);
        if(e.keyCode == 13)
        {
            $('.forum_search_btn').trigger("click");
        }
    });

    $('.refine_keyword_search').on('click', function(e){
        e.preventDefault();
        var keyword = $('.refine_keyword').val();
        document.location.href = currentURL + '?tag=' + keyword;
    });

    $('.refine_keyword').keyup(function(e){
        //console.log(e.keyCode);
        if(e.keyCode == 13)
        {
            $('.refine_keyword_search').trigger("click");
        }
    });

    $('.browse_date').change(function(e){
        //console.log(e.keyCode);
        var search_date = $('.browse_date').val();
        document.location.href = currentURL + '?date=' + search_date; 
    });

    $('.ls-modal').fancybox({
        autoSize: false, // shouldn't be true ?
        fitToView: true,
        maxWidth: 1200,
        topRatio: 0,
        type: 'iframe',
        beforeLoad: function () {
            var url = $(this.element).attr("href")+'?page=popup';
            this.href = url
        },
        afterShow: function () {
            this.inner.css({
                overflow: 'hidden'
            })
            var isMobile = window.matchMedia("only screen and (max-width: 760px)");
            if (isMobile.matches) {
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            },
        overflow: 'hide',
        scrolling: 'no',
        iframe: {
            scrolling: 'no',
            preload: true
        },
        helpers: {
            media: {},
            overlay: {
                css: { 'overflow': 'hidden' }
                }
        }
    });

    /*$('.ls-modal').fancybox({
        type: 'iframe',
        height:'600px',
        beforeLoad: function () {
            var url = $(this.element).attr("href")+'?page=popup';
            this.href = url
        },
        beforeShow: function () {
            var isMobile = window.matchMedia("only screen and (max-width: 760px)");
            if (isMobile.matches) {
                this.width = $(document).width();
            }
            else {
                this.width = $(document).width() - 200;
               // this.height = $(document).height() - 500;
            }
        }
    });*/
    var cache = {};
    $( "#tags" ).autocomplete({
      source: function( request, response ) {
        var term = request.term;
        if ( term in cache ) {
          response( cache[ term ] );
          return;
        }
        var requestdata = {
            term: request.term,
            action: 'get_tags_response',
            csrf_token: the_ajax_script.csrf_token,
        };
        $.getJSON( the_ajax_script.ajaxurl, requestdata, function( data, status, xhr ) {
          cache[ term ] = data;
          response( data );
        });
      }
    });
    /*$( "#tags" ).autocomplete({
        source: function( request, response ) {
        $.ajax( {
              url: the_ajax_script.ajaxurl,
              dataType: "jsonp",
              data: {
                term: request.term,
                action: 'get_tags_response',
                csrf_token: the_ajax_script.csrf_token,
              },
              success: function( data ) {
                    response( data );
              }
            } );
          }
    });*/

    $("#dateFilter").datepicker();

    $('.social_link').click(function (e) {
        e.preventDefault();
        var strURL = $(this).attr("href");
        var LeftPosition = (screen.width) ? (screen.width-520)/2 : 0;
        var TopPosition = (screen.height) ? (screen.height-300)/2 : 0;
        window.open(strURL ,"popup","width=680,height=460,toolbar=yes,top="+TopPosition+", left="+LeftPosition+"");          
        return false;
    });    

    $('.reply-btn').click(function (e) {
        var data_id = $(this).data('id');
        $('#reply_box'+data_id).toggle();
    });

    $('.login-submit').click(function (e) {
        e.preventDefault();
        var user_name = $('#sh_username');
        var user_pass = $('#sh_password');
        var err = 0;
        if(user_name.val()=="")
        {
            user_name.closest(".loginFieldHolder").addClass('has-error');
            err = 1;
        }
        else
        {
            user_name.closest(".loginFieldHolder").removeClass('has-error');
        }
        if(user_pass.val()=="")
        {
            user_pass.closest(".loginFieldHolder").addClass('has-error');
            err = 1;
        }
        else
        {
            user_pass.closest(".loginFieldHolder").removeClass('has-error');
        }

        if(err == 1)
        {
            return false;
        }
        else
        {
            var data = {
                action: 'user_valid_response',
                user_name: user_name.val(),
                user_pass: user_pass.val(),
                csrf_token: the_ajax_script.csrf_token,
            };
            $.post(the_ajax_script.ajaxurl, data, function(response) {
                if(response!="success")
                {
                    jQuery('#alertmodal .modal-body').html(response);
                    jQuery('#alertmodal').modal('show');
                }
                if(response=="success")
                {
                    document.location.href = document.location.href;
                }

            });
            return false;
        }
    });

    $('.register-submit').click(function (e) {
        e.preventDefault();
        var Full_name = $('#Full_name');
        var Username = $('#Username');
        var Password = $('#Password');
        var CPassword = $('#CPassword');
        var Company = $('#Company');
        var Email = $('#Email');
        var Industry = $('#Industry');
        var email_label = $('.email_label');
        var pass_label = $('.pass_label');
        var user_label = $('.user_label');
        var err = 0;
        if(Full_name.val()=="")
        {
            Full_name.addClass('has-error');
            err = 1;
        }
        else
        {
            Full_name.removeClass('has-error');
        }

        if(Username.val()=="")
        {
            Username.addClass('has-error');
            err = 1;
        }
        else if(Username.val().length<6)
        {
            Username.addClass('has-error');
            user_label.html("Username must be at least 6 characters.");
            err = 1;
        }
        else
        {
            Username.removeClass('has-error');
            user_label.html("");
        }

        if(Password.val()=="")
        {
            Password.addClass('has-error');
            err = 1;
        }
        else if(Password.val().length<8)
        {
            Password.addClass('has-error');
            pass_label.html("Password must be at least 8 characters.");
            err = 1;
        }
        else
        {
            Password.removeClass('has-error');
            pass_label.html("");
        }

        if(CPassword.val()=="")
        {
            CPassword.addClass('has-error');
            err = 1;
        }
        else if(Password.val()!=CPassword.val())
        {
            CPassword.addClass('has-error');
            err = 1;
        }
        else
        {
            CPassword.removeClass('has-error');
        }

        if(Company.val()=="")
        {
            Company.addClass('has-error');
            err = 1;
        }
        else
        {
            Company.removeClass('has-error');
        }

        if(Email.val()=="")
        {
            Email.addClass('has-error');
            err = 1;
        }
        else if( !isValidEmailAddress( Email.val() ) ) 
        {
            Email.addClass('has-error');
            email_label.html('Invalid email address');
            err = 1;
        }
        else
        {
            Email.removeClass('has-error');
            email_label.html('');
        }

        if(Industry.val()=="")
        {
            Industry.addClass('has-error');
            err = 1;
        }
        else
        {
            Industry.removeClass('has-error');
        }

        if(err == 1)
        {
            return false;
        }
        else
        {
            $('#registerfrm').submit();
        }
    });
});

$(function() {
    $('.shub-form-submit').on('click', function(e) {
        $(this).parents('form').submit();
    });

    // New forum topic validation
    $('#new_form_topic').validate({
        rules: {
            bbp_forum_id: {
                required: true
            },
            bbp_topic_title: {
                required: true
            },
            bbp_topic_content: {
                required: true
            },
        },
        messages: {
            bbp_forum_id: {
                required: 'Please select the topic'
            },
            bbp_topic_title: {
                required: 'Please enter the title'
            },
            bbp_topic_content: {
                required: 'Please enter the content'
            },
        },
        errorPlacement: function(error, element) {
            error.insertBefore(element);
        }
    });

    $('.need-signin').click(function(e) {
        $("html, body").animate({ scrollTop: 0 }, "slow", function() {
            if (!$('#loginfrm').is(':visible'))
                $('.loginBtn').trigger('click');
            
            $('#sh_username').focus();
        });
    });

    $(".startConversation").click(function () {
        if(pro_status == "1")
        {
            $(".conversationBox").show();
        }
        else
        {
            jQuery('#alertmodal .modal-body').html('You have not completed your profile yet. <a href="'+siteURL+'/dashboard">click here to complete the profile</a>');
            jQuery('#alertmodal').modal('show');
        }
     });

    // Forum topic resolve
    $('.shub-topic-resolve').on('click', function() {
        var ele = $(this);
        var opt = confirm('Are you sure?');
        if (opt) {
            var data = {
                topic_id: ele.data('topic'),
                action: 'shub_topic_resolve',
                csrf_token: the_ajax_script.csrf_token
            }
            $.post(the_ajax_script.ajaxurl, data, function(res) {
                if (res.success)
                    ele.replaceWith('Yes');
            });
        }

        return false;
    });

    // Forum reply mark as like
    $('.shub-reply-like').on('click', function() {
        var ele = $(this);

        var data = {
            reply_id: ele.data('reply'),
            action: 'shub_reply_like',
            mode: ele.data('mode'),
            csrf_token: the_ajax_script.csrf_token
        }
        $.post(the_ajax_script.ajaxurl, data, function(res) {
            if (res.success) {
                ele.find('span').html(res.data.new_count);
            }
        });

        return false;
    });

    $('.user-controls').on('click', function() {
        $(".login_Holder").slideToggle('1000');
        return false;
    });

    showNotices();
});

// Short notices
function showNotices() {
    if ($('#alertmodal').hasClass('show-alert')){
        $('#alertmodal').modal('show');
        $('#alertmodal').removeClass('show-alert');
    }
}

$(window).load(function () {
    function equaliseIt() {
        $('.latestupdate_boxes').each(function () {
            var highestBox = 0;
            $('.latestupdatecolumn', this).each(function () {
                if ($(this).height() > highestBox)
                    highestBox = $(this).height();
            });

            $('.latestupdatecolumn', this).height(highestBox);
        });
    }
    //call the function at page load
    equaliseIt();
});

function showChildComment(id) {
    if ($("#Continue_reading" + id).text() == "Continue reading") {
        $("#rowComments" + id).show("normal");
        $("#Continue_reading" + id).text("Close");            
    }
    else {
        $("#rowComments" + id).hide("normal");
        $("#Continue_reading" + id).text("Continue reading");
    }
}

function validate_accountsetting()
{    
    var up_Full_name = jQuery('#up_Full_name');
    var up_Company = jQuery('#up_Company');
    var up_Email = jQuery('#up_Email');
    var up_Industry = jQuery('#up_Industry');

    var err = 0;
    
    if(up_Full_name.val()=="")
    {
        up_Full_name.addClass('error');
        err = 1;
    }
    else
    {
        up_Full_name.removeClass('error');
    }

    if(up_Company.val()=="")
    {
        up_Company.addClass('error');
        err = 1;
    }
    else
    {
        up_Company.removeClass('error');
    }

    if(up_Email.val()=="")
    {
        up_Email.addClass('error');
        err = 1;
    }
    else if( !isValidEmailAddress( up_Email.val() ) ) 
    { 
        up_Email.addClass('error');
        err = 1;
    }
    else
    {
        up_Email.removeClass('error');
    }    

    if(up_Industry.val()=="")
    {
        up_Industry.addClass('error');
        err = 1;
    }
    else
    {
        up_Industry.removeClass('error');
    }

    if(err == 1)
    {
        return false;
    }
    else
    {
        document.updateuser.submit();
    }
}

function validate_passwordchange()
{
    var current_password = jQuery('#current_password');
    var password = jQuery('#new_password');
    var cpassword = jQuery('#con_password');

    
    var err = 0;
    if(current_password.val()=="")
    {
        current_password.addClass('error');
        err = 1;
    }
    else
    {
        current_password.removeClass('error');
    }

    var err = 0;
    if(password.val()=="")
    {
        password.addClass('error');
        err = 1;
    }
    else
    {
        password.removeClass('error');
    }

    if(password.val().length < 8)
    {
        password.addClass('error');
        jQuery('.change-pass-error').html("Password should be atleast 8 characters");
        err = 1;
    }
    else
    {
        jQuery('.change-pass-error').html("");
    }

    if(cpassword.val()=="")
    {
        cpassword.addClass('error');
        err = 1;
    }
    else
    {
        cpassword.removeClass('error');
    }

    if(password.val()!=cpassword.val())
    {
        cpassword.addClass('error');
        jQuery('.change-pass-error').html("Password don't match");
        err = 1;
    }

    if(err == 1)
    {
        return false;
    }
    else
    {
        document.changepassword.submit();
    }

}