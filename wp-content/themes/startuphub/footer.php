<?php 
$template_url = get_template_directory_uri();
wp_footer(); 
?>
<footer id="footer-style-1">
<div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footerLogo">
        <img src="<?php echo $template_url;?>/images/footer_logo.png" />
        <div class="pull-right">
            <div class="socialmediaIcons">
             <a href="https://twitter.com/DubaiStartupHub" target="_blank"><img src="<?php echo $template_url;?>/images/twitter_footer_logo.png" /></a>   
            </div>
            <div class="socialmediaIcons">
                <a href="https://www.facebook.com/dubaistartuphub/" target="_blank"><img src="<?php echo $template_url;?>/images/facebook_footer_logo.png" /></a>
            </div>
            <div class="socialmediaIcons">
                <a href="https://www.linkedin.com/company/dubai-startup-hub" target="_blank"><img src="<?php echo $template_url;?>/images/linkedin_footer_logo.png" /></a>
            </div>
            <div class="socialmediaIcons">
                <a href="https://www.instagram.com/dubaistartuphub/" target="_blank"><img src="<?php echo $template_url;?>/images/instagram_footer_logo.png" /></a>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="widget">
                <h3 class="footerCopy">Dubai Startup Hub is a semi government initiative rooted in the Dubai Chamber</h3>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="title">
                    <p>© 2017 All rights reserved</p>
                    <div class="col-lg-5 hide"><img src="<?php echo $template_url;?>/images/smartdubailogo-480x480@2x.png" class="img-responsive" /></div>
                    <div class="col-lg-5 hide"><img src="<?php echo $template_url;?>/images/dsh_footerlogos_bluemix@2x.png" class="img-responsive" /></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">      
        </div>
</div>
</footer>
<script src="<?php echo $template_url;?>/js/jquery.js"></script>
<script src="<?php echo $template_url;?>/js/jquery-ui.js"></script>
<script src="<?php echo $template_url;?>/js/bootstrap.min.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.mousewheel.pack.js"></script>
<script src="<?php echo $template_url;?>/js/menu.js"></script>
<script src="<?php echo $template_url;?>/js/owl.carousel.min.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.simple-text-rotator.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.scrollbar.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.validate.min.js"></script>
<script src="<?php echo $template_url;?>/js/wow.min.js"></script>
<script src="<?php echo $template_url;?>/js/main.js"></script>
<script src="<?php echo $template_url;?>/js/custom.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.fancybox.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.ellipsis.js"></script>
<script type="text/javascript">
    $(window).load(function() {
    <?php 
    if(isset($_GET["register"]))
    {
        $register = $_GET["register"];

        if($register == "error")
        {

    ?>
        $(".menu_Holder").show();
        $(".login_Holder").show();
        $(".registrationbox").show();
    <?php
        }
        }
    ?>
    });
</script>
</body>
</html>