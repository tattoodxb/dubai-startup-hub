<?php
/* Template Name: About Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<div class="white-wrapper" id="mainContent">


        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
                <h3 class="titleHeader">Meet the team</h3>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 removeLeftpadding">

                <div class="latestupdate_boxes removeLeftpadding  no-margin">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 first">
                        <div class="latestupdatecolumn meetTeamColumn">

                            <div class="latestupdateImageHolder aboutHolder">

                                <img src="<?php echo $template_url?>/images/essa.jpg" class="img-responsive aboutHolderimg">

                            </div>

                            <div class="col-lg-12 latestupdateContentBox">

                                <!--<div class="latestupdateDate">Communications manager </div>-->
                                <div class="latestupdateTitle">Essa</div>
                                <div class="aboutDesc coloringBlue">I tell all our startups the most important is to believe in your idea, believe in what you want to achieve, and believe that it would be something big in the future...</div>



                            </div>
                            <div class="clearfix"></div>


                        </div>
                    </div><!-- end col-lg-3 -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="latestupdatecolumn meetTeamColumn">

                            <div class="latestupdateImageHolder aboutHolder">

                                <img src="<?php echo $template_url?>/images/rami.jpg" class="img-responsive aboutHolderimg">

                            </div>

                            <div class="col-lg-12 latestupdateContentBox">

                                <!--<div class="latestupdateDate">Communications manager </div>-->
                                <div class="latestupdateTitle">Rami </div>
                                <div class="aboutDesc coloringBlue">Many promising startups come strong with beautiful products, yet fail to grab customer attention and sustain it. Creating engagement and staying open to new avenues for your product development are the essentials for building a suitable venture...</div>



                            </div>
                            <div class="clearfix"></div>


                        </div>
                    </div><!-- end col-lg-3 -->

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="latestupdatecolumn meetTeamColumn">

                            <div class="latestupdateImageHolder aboutHolder">

                                <img src="<?php echo $template_url?>/images/farhad.jpg" class="img-responsive aboutHolderimg">

                            </div>

                            <div class="col-lg-12 latestupdateContentBox">

                                <!--<div class="latestupdateDate">Communications manager </div>-->
                                <div class="latestupdateTitle">Farhad</div>
                                <div class="aboutDesc coloringBlue">It is said that one may fail repeatedly, but it is the one success that matters. There is no idea or dream that cannot be achieved as we are now living in the future and the only limitation is ourselves....</div>



                            </div>
                            <div class="clearfix"></div>


                        </div>
                    </div><!-- end col-lg-3 -->

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="latestupdatecolumn meetTeamColumn">

                            <div class="latestupdateImageHolder aboutHolder">

                                <img src="<?php echo $template_url?>/images/natalia.jpg" class="img-responsive aboutHolderimg">

                            </div>

                            <div class="col-lg-12 latestupdateContentBox">

                                <!--<div class="latestupdateDate">Communications manager </div>-->
                                <div class="latestupdateTitle">Natalia</div>
                                <div class="aboutDesc coloringBlue">Entrepreneurship for me is about doing things, spending the flying time on creating value and not sitting on ideas...</div>



                            </div>
                            <div class="clearfix"></div>


                        </div>
                    </div><!-- end col-lg-3 -->

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="latestupdatecolumn meetTeamColumn">

                            <div class="latestupdateImageHolder aboutHolder">

                                <img src="<?php echo $template_url?>/images/noora.jpg" class="img-responsive aboutHolderimg">

                            </div>

                            <div class="col-lg-12 latestupdateContentBox">

                                <!--<div class="latestupdateDate">Communications manager </div>-->
                                <div class="latestupdateTitle">Noora</div>
                                <div class="aboutDesc coloringBlue">Being passionate and curious is critical along entrepreneur’s journey...</div>



                            </div>
                            <div class="clearfix"></div>


                        </div>
                    </div><!-- end col-lg-3 -->

                </div>

            </div><!-- end col-lg-6 -->

            <?php 
                  get_template_part( 'templates/meet-up-widget' );
                  ?>
    </div>

<?php get_template_part( 'templates/our-initiatives' ); ?>    
<?php get_template_part( 'templates/latest-updates' ); ?>
<?php get_template_part( 'templates/social-media' ); ?>
<?php get_template_part( 'templates/our-partners' ); ?>
<?php get_footer(); ?>