<?php
$template_url = get_template_directory_uri();
?>
<div class="white-wrapper">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
            <h3 class="titleHeader">Our partners</h3>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding rowPartners" >
                <div class="col-lg-2 Partnerlogo" style="background:url(<?php echo $template_url;?>/images/thecribb-logo.png)">
                </div>
                <div class="col-lg-2 Partnerlogo" style="background:url(<?php echo $template_url;?>/images/AstroLabs-logo.png)">                  
                </div>
                <div class="col-lg-2 Partnerlogo" style="background:url(<?php echo $template_url;?>/images/1776-logo.png)">
                </div>
                <div class="col-lg-2 Partnerlogo" style="background:url(<?php echo $template_url;?>/images/dtec-logo.png)">
                </div>
                <div class="col-lg-2 Partnerlogo" style="background:url(<?php echo $template_url;?>/images/in5-logo.png)">
                </div>
                <div class="col-lg-2 Partnerlogo" style="background:url(<?php echo $template_url;?>/images/Magnitt-logo.png)">
                </div>
                <div class="col-lg-2 Partnerlogo" style="background:url(<?php echo $template_url;?>/images/potential-logo.png)">
                </div>
            </div>
        </div>
</div>