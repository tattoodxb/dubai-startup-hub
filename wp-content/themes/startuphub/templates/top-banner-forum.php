<?php
$template_url = get_template_directory_uri();
?>
<div class="aboutContentBanner forumcontainer">
	<div><div class="BannerCopyForum">Join the conversation</div></div>
	<div class="row reorder-xs">
	    <?php bbp_get_template_part( 'content', 'forum-stats' ); ?>
	    <?php bbp_get_template_part( 'user', 'helpful' ); ?>
	    <?php bbp_get_template_part( 'content', 'topic-trends' ); ?>
	</div>
</div>

<?php bbp_get_template_part( 'form', 'topic' ); ?>
<?php //bbp_get_template_part( 'content', 'archive-forum' ); ?>