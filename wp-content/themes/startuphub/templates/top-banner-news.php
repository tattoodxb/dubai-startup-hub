<?php
$template_url = get_template_directory_uri();
if(isset($_GET["date"]))
{
    $date = $_GET["date"];
}
if(isset($_GET["tag"]))
{
    $tag = $_GET["tag"];
}
?><div class="aboutContentBanner">
    <div class="row reorder-xs">
    <div class="col-lg-3 col-md-12 col-sm-12">
        <div class="aboutCopyleft">Clarify and direction in your startup journey</div>
        <div class="colorAboutLight">become part of Dubai Startup Hub and you will be empowered to turn your idea into tangible reality.</div>
        <div class="AboutBtn"><?php echo get_the_title();?></div>
        <a href="<?php echo home_url();?>" class="menuReturn">Return to homepage <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i></a> 
       </div>
    <div class="col-lg-3 col-md-6 col-sm-6 pull-right mobileBoxes">
        <div class="widget JoinConversation">
            <div class="titleOrangeBox">
                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Join the conversation
            </div>
            <div class="rowOrangeBox firstrow no-border">
                <div class="rowOrangeDate signupTitle">Sign up / Sign in to use of the return  </div>
                <div class="inpuNewsletterHolder">
                    <a href="<?php echo home_url('forum');?>" class="menuReturn">Go to forum <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 pull-right mobileBoxes">
            <div class="widget  RefineSearch">
                <div class="titleOrangeBox">
                    <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Refine your search
                </div>
                <div class="rowOrangeBox firstrow no-border">
                    <div class="rowOrangeDate signupTitle">Filter by keywords</div>
                    <div class="inpuNewsletterHolder midsize">
                        <input name="refine_keyword" style="width:95%" id="tags" class="form-control NewsletterInput refine_keyword" placeholder="Keywords" type="text" value="<?php echo $tag;?>">
                        <div class="submitOk refine_keyword_search">OK</div>
                    </div>
                    <div class="rowOrangeDate signupTitle">Browse by date</div>
                    <div class="inpuNewsletterHolder">
                        <input name="browse_date" id="dateFilter" class="form-control NewsletterInput browse_date" placeholder="Date" type="text" value="<?php echo $date;?>">
                        <div class="submitOk searchConversation datefield"><i class="fa fa-chevron-down"></i></div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</div>