<?php
$template_url = get_template_directory_uri();
?><div class="white-wrapper" id="mainContent">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
                  <h3 class="titleHeader">Upcoming events</h3></div>
                  <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
                      <div class="widget">
                        <?php
                        $current_date = date('Y-m-d H:i:s');
                        $args = array(
                            'post_type'=> 'events',
                            'post_status' => 'publish',
                            'meta_key' => 'sh_events_date',
                            'meta_query' => array(
                                array(
                                    'key' => 'sh_events_date',
                                    'value' => $current_date,
                                    'compare' => '>=',
                                    'type' => 'DATE'
                                )
                            ),
                            'orderby'=> 'meta_value_num',
                            'order'    => 'ASC',
                            'posts_per_page' => 3,
                            'paged' => get_query_var( 'paged' )
                        );
                        
                        $wp_query = new WP_Query($args);
                        $i=1;
                        while (have_posts()) : the_post();
                        $sh_events_location = get_post_meta($post->ID, 'sh_events_location', $single = true);
                        $sh_events_display_date = get_post_meta($post->ID, 'sh_events_display_date', $single = true);

                        $start_date = get_post_meta($post->ID, 'sh_events_date', $single = true);
                        $end_date = get_post_meta($post->ID, 'sh_events_date', $single = true);

                        $start_date = date('F d, Y', strtotime($start_date));
                        $end_date = date('F d, Y', strtotime($end_date));


                      if($sh_events_display_date=="")
                      {
                          if($start_date == $end_date)
                          {
                              $sh_events_display_date = $start_date;  
                          }
                          else
                          {
                              $sh_events_display_date = $start_date . '-' .$end_date;
                          }
                          
                      }
                        ?>
                         <div class="rowevent">
                             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 removeLeftpadding">
                                 <a href="<?php echo the_permalink();?>" class="ls-modal"><?php 
                                 the_post_thumbnail('thumbnail', ['class' => 'img-responsive', 'title' => get_the_title()]);
                                 ?></a>
                             </div>
                             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                 <div class="titleEventbold"><?php echo $sh_events_display_date;?><span class="titleEventNormal">  //  </span><span class="titleEventNormal" ><?php echo $sh_events_location;?></span></div>
                      
<div class="titleEvent"><?php echo get_the_title();?></div>
<div class="descEvent"><?php echo get_the_excerpt();?></div>
                                 <br />
                                 <a href="<?php echo the_permalink();?>" class="ls-modal">Read more</a>
                                 </div>
                             <div class="clearfix"></div>
                             </div>
                          <?php 
                    endwhile; 
                    ?>                            
                      </div><!-- end widget -->
                  </div><!-- end col-lg-6 -->

                  <?php 
                  get_template_part( 'templates/meet-up-widget' );
                  ?>
<div class="clearfix"></div>
              <div >
                  <a href="<?php echo home_url('events');?>" class="menuReturn loadmoreBtn">Load more <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
              </div>
           
              </div>
    </div>