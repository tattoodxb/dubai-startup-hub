<?php
$template_url = get_template_directory_uri();
?><div class="dirt-white-wrapper">
    <div class="container">
        <div id="services" class="owl-carousel">
            <?php
            $args = array(
                'post_type'=> 'testimonials',
                'post_status' => 'publish',
                'orderby'=> 'menu_order',
                'order'    => 'ASC',
                'posts_per_page' => 10,
                'paged' => get_query_var( 'paged' )
            );
            $wp_query = new WP_Query($args);
            $i=1;
            while (have_posts()) : the_post();
                $sh_testimonials_location = get_post_meta($post->ID, 'sh_testimonial_location', $single = true);
                $sh_testimonials_author = get_post_meta($post->ID, 'sh_testimonial_author', $single = true);
                $sh_testimonials_display_date = get_post_meta($post->ID, 'sh_testimonial_display_date', $single = true);
            ?>
            <div class="item">
                <div class="servicesbox lazyOwl">
                    <div class="row">
                        <div class="col-lg-4">
                            <?php the_post_thumbnail( 'full', ['class' => 'img-responsive'] );  ?>
                        </div><!-- end images -->
                        <div class="col-lg-6">
                            <div class="servicesbox_content">
                                <div class="rowevent">
                                    <div class="titleEvent"><?php echo get_the_title();?></div>
                                    <div class="descEvent"><?php echo get_the_content();?></div>
                                </div>
                                <div class="authorName"><?php echo $sh_testimonials_author;?></div>
                                <div class="rowOrangeDate"><?php echo $sh_testimonials_display_date;?> <span class="rowOrangeLocation"> // <?php echo $sh_testimonials_location;?></span></div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
            endwhile; 
            ?>
        </div>
    </div>
</div>