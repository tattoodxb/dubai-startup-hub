<?php
$template_url = get_template_directory_uri();
?><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_IN/sdk.js#xfbml=1&version=v2.10&appId=513928072133333";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="white-wrapper">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
                <h3 class="titleHeader">Connect with us on social media</h3>
                <div class="social_boxes removeLeftpadding">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="latestSocialcolumn">
                          <a class="twitter-timeline" data-width="307" data-height="600" data-theme="light" data-link-color="#0035E6" href="https://twitter.com/DubaiStartupHub?ref_src=twsrc%5Etfw">Tweets by DubaiStartupHub</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>                            
                            </div>
                        </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="latestSocialcolumn">
                          <div class="fb-page" data-href="https://www.facebook.com/dubaistartuphub/" data-tabs="timeline" data-width="307" data-height="600" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/dubaistartuphub/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dubaistartuphub/">Dubai Startup Hub</a></blockquote></div>                            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="latestSocialcolumn">
                            <div class="latestSocialTitle">
                                Follow us on LinkedIn <a href="https://www.linkedin.com/company/dubai-startup-hub" target="_blank"><span><i class="fa fa-linkedin socialIcon"></i></span></a>
                            </div>
                            <div class="row_twitter firstrow removeborder">

                                <div class="row_twitter_desc">
                                    <a href="https://www.linkedin.com/company/dubai-startup-hub" target="_blank">
                                    Dubai startup hub is the official, online hub of the Dubai startup and technology ecosystem</a>
                                </div>
                            </div>                            
                        </div>
                        <div class="latestSocialcolumn">
                            <div class="latestSocialTitle">
                                Follow us on Instagram <a href="https://www.instagram.com/dubaistartuphub/" target="_blank"><span><i class="fa fa-instagram socialIcon"></i></span></a>
                            </div>
                            <div class="row_twitter firstrow removeborder">
                                <div class="row_twitter_desc">
                                    <a href="https://www.instagram.com/dubaistartuphub/" target="_blank">Dubai startup hub is the official, online hub of the Dubai startup and technology ecosystem</a>
                                </div>
                                <div><a href="https://www.instagram.com/dubaistartuphub/" target="_blank"><img class="img-responsive" src="<?php echo $template_url;?>/images/instagramDummy.jpg" /></a></div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
        </div>
    </div>