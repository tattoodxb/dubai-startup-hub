<?php
$template_url = get_template_directory_uri();
if(isset($_GET["date"]))
{
    $date = $_GET["date"];
}
if(isset($_GET["tag"]))
{
    $tag = $_GET["tag"];
}
?><div class="aboutContentBanner">
    <div class="row reorder-xs">
        <?php
            $catID = get_the_category();
            $current_category = single_cat_title("", false);
            $current_desc = explode('</b>', category_description($catID[0]));
            $cur_title = str_replace('<b>', '', $current_desc[0]);
            $cur_desc = $current_desc[1];
        ?>
        <div class="col-lg-6 col-md-12  col-sm-12 flipFormobile">
            <div class="aboutCopyleft"><?php echo strip_tags($cur_title);?></div>
            <div class="questionCopy"><?php echo strip_tags($cur_desc);?></div>
            <div class="AboutBtn"><?php echo $current_category;?></div>
            <a href="<?php echo home_url();?>" class="menuReturn">Return to homepage <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i></a>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 pull-right mobileBoxes">
            <div class="widget JoinConversation">
                <div class="titleOrangeBox">
                    <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Join the conversation 
                </div>
                <div class="rowOrangeBox firstrow no-border">
                    <div class="rowOrangeDate signupTitle">Sign up / Sign in to use of the return  </div>
                    <div class="inpuNewsletterHolder">
                        <a href="<?php echo home_url('forum');?>" class="menuReturn">Go to forum <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 pull-right mobileBoxes">
            <div class="widget  RefineSearch">
                <div class="titleOrangeBox">
                    <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Refine your search
                </div>
                <div class="rowOrangeBox firstrow no-border">
                    <div class="rowOrangeDate signupTitle">Filter by keywords</div>
                    <div class="inpuNewsletterHolder midsize">
                        <input name="refine_keyword" style="width:95%" id="tags" class="form-control NewsletterInput refine_keyword" placeholder="Keywords" type="text" value="<?php echo $tag;?>">
                        <div class="submitOk refine_keyword_search">OK</div>
                    </div>
                    <div class="rowOrangeDate signupTitle">Browse by date</div>
                    <div class="inpuNewsletterHolder">
                        <input name="browse_date" id="dateFilter" class="form-control NewsletterInput browse_date" placeholder="Date" type="text" value="<?php echo $date;?>">
                        <div class="submitOk searchConversation datefield"><i class="fa fa-chevron-down"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>