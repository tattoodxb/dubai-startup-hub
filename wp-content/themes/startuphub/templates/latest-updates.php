<?php
$template_url = get_template_directory_uri();
?><section class="parallax latestupdate" style="background-image: url('<?php echo $template_url;?>/images/sectionbanner.jpg');">
        <div class="overlay">
            <div class="container">            
            <div class="titleHeader">Latest updates</div>
                <div class="latestupdate_boxes removeLeftpadding">
                    <?php
                        $args = array(
                            'post_type'=> 'news',
                            'post_status' => 'publish',
                            'orderby'=> 'menu_order',
                            'order'    => 'ASC',
                            'posts_per_page' => 4,
                            'paged' => get_query_var( 'paged' )
                        );
                        $wp_query = new WP_Query($args);
                        $i=1;
                        while (have_posts()) : the_post();
                        $sh_news_location = get_post_meta($post->ID, 'sh_news_location', $single = true);
                        $sh_news_display_date = get_post_meta($post->ID, 'sh_news_display_date', $single = true);
                        ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="latestupdatecolumn">         
                            <div class="latestupdateImageHolder"><a href="<?php echo the_permalink();?>" class="ls-modal"><?php if (class_exists('MultiPostThumbnails')) :
                            MultiPostThumbnails::the_post_thumbnail(
                                get_post_type(),
                                'news-overview-image' , null, 'post-thumbnail', array('class' => 'img-responsive' )
                            );
                        endif; ?></a></div>
                            <div class="col-lg-12 latestupdateContentBox">
                                <div class="latestupdateDate"><span class="latestupdatelocation" ><?php echo $sh_news_display_date;?> // <?php echo $sh_news_location;?></span></div>
                                <div class="latestupdateTitle"><?php echo get_the_title();?></div>
                                <div class="latestupdateDesc"><?php echo get_the_excerpt();?></div>
                                <span>...</span>
                                    <a class="readmoreRightcolumn ls-modal" href="<?php echo the_permalink();?>">Read more</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php 
                    endwhile; 
                    ?>
                    <div class="clearfix"></div>

                    <div class="col-lg-12">
                        <a href="<?php echo home_url('news');?>" class="menuReturn latestLoadmore">Load more <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div> 
            </div>
        </div>
</section>