<?php
$template_url = get_template_directory_uri();
if ( is_user_logged_in() ) {
}
else
{
    wp_redirect(home_url());
    exit;
}
$current_user = wp_get_current_user();
$registered_date = $current_user->user_registered;
$registered_date = date( "d F Y", strtotime( $registered_date ));
?><div class="aboutContentBanner">
    <div class="col-lg-3 col-md-12 col-sm-12">
        <div class="BannerCopy_1">
          Welcome back <br />
            <?php echo $current_user->first_name;?>
        </div>
        <div class="AboutBtn">Profile</div>
        <a href="<?php echo home_url();?>" class="menuReturn">Return to homepage <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i></a>
    </div>
    <div class="col-lg-4 col-md-12 col-sm-12 pull-right mobileBoxes">
        <div class="widget ProfileBox">
            <div style="height:200px;background: center url('<?php echo shub_user_avatar($current_user->ID); ?>');background-size:cover;"></div>
            <div class="ProfileCopyholder">
                <div class="titleEventbold">Member since<span class="titleEventNormal">  //  </span><span class="titleEventNormal"><?php echo $registered_date;?></span></div>
                <div class="ProfileName ">
                   <?php echo $current_user->first_name;?>
                </div>  
                <div style="color:#FFF">
                    <?php echo $current_user->Company;?><br>
                    Profile Status: 
                    <?php 
                    if($current_user->is_complete=="1")
                    {
                        $profile_status = 'Complete';
                    }
                    else
                    {
                        $profile_status = 'Not Complete';   
                    }
                    echo $profile_status;?>
                    </div>
                </div>
        </div>
           </div>
</div>