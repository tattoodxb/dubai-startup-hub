<div class="aboutContentBanner">

<div class="aboutleftcontainer col-lg-4 col-md-12 col-sm-12">
<div class="aboutCopyleft">
    Clarity and direction in your startup journey: <span class="colorAboutLight">become part of Dubai Startup Hub and you will be empowered to turn your idea into tangible reality.</span>
</div>
<div class="AboutBtn">About us</div>

<a href="<?php echo home_url('');?>" class="menuReturn">Return to homepage <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i></a>
</div>
<div class="col-lg-1"></div>
<div class="col-lg-7 col-md-12 col-sm-12">
<div class="AboutCopyRight contactContentRight">
    <div class="col-lg-12" style="padding-bottom:10px;">
        Feel free to contact us! Any questions or need more information, please fill in the contact below.

    </div>

    <div class="contactform">
    <form id="contactform" action="contact" name="contactform" method="post">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="error_label family_name" style="display: none">Please enter your family name</div>
            <input type="text" name="family_name" id="family_name" class="form-control" placeholder="Family name...">
            
              </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="error_label first_name" style="display: none">Please enter your first name</div>
            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First name">
                     </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="error_label subject" style="display: none">Please enter your subject</div>
            <input type="text" name="subject" id="subject" class="form-control" placeholder="Concerning... ">
            </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="error_label email" style="display: none">Please enter your valid email</div>
            <input type="text" name="email" id="email" class="form-control" placeholder="Email address... ">
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <textarea class="form-control" name="comments" id="comments" rows="6" placeholder="Message"></textarea>
          <div class="send btn-send">Send</div></div>
          </form>  
        </div>
        <div class="thankyou text-center" style="display: none"><br><br><br>Thank you</div>
</div>
</div>
</div>