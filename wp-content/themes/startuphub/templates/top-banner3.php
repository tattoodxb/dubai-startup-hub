<div class="surveyform copyHolder">
                <div class="BannerCopy_1">What is the most important question for you as a startup?</div>
                <div data-id="1" class="BannerCopy_3">What do I need to launch and run my startup in Dubai?</div>
                <div data-id="2" class="BannerCopy_3 NonFirst">Where can I find funding and investors for my startup?</div>
                <div data-id="3" class="BannerCopy_3 NonFirst">How do I grow my startup business in Dubai and where can I find clients?</div>
                <div data-id="4" class="BannerCopy_3 NonFirst">How do I build my startup team and where can I find talent?</div>
                <div data-id="5" class="BannerCopy_3 NonFirst">Where do I learn about entrepreneurship, the industry and the market?</div>
                <div data-id="6" class="BannerCopy_3 NonFirst">Where can I find examples and inspiration for a successful startup?</div>
                </div>
                <div class="thankyou copyHolder" style="display: none">
                <div class="BannerCopy_1">Thank you</div>
                <div class="s_form">
                  <div><br><br><br><input type="text" name="email" id="email" class="form-control" placeholder="Email address... "><div class="error_label email" style="display: none">Please enter your valid email</div></div>
                  <input type="hidden" name="question_id" id="question_id" value="">
                  <input type="hidden" name="question" id="question" value="">
                  <div><input type="button" class="btn btn-primary survey_btn" value="Send"></div>
                </div>            
                <div class="t_form" style="display: none;color:#FFF"><br><br><br>Your survey has been saved.</div>
                </div>