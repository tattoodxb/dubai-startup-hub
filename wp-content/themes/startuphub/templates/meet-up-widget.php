<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="widget OrangeColumn">
                        <div class="titleOrangeBox"><i class="fa fa-chevron-right nextBlue" aria-hidden="true"></i>Or meet us here
</div>
                    <?php
                        $args = array(
                            'post_type'=> 'meetup',
                            'post_status' => 'publish',
                            'orderby'=> 'menu_order',
                            'order'    => 'ASC',
                            'posts_per_page' => 4,
                            'paged' => get_query_var( 'paged' )
                        );
                        $wp_query = new WP_Query($args);
                        $x=1;
                        while (have_posts()) : the_post();
                          if($x==1)
                          {
                            $firstrow = 'firstrow';
                          }
                          else
                          {
                            $firstrow = ''; 
                          }
                        $sh_meetup_location = get_post_meta($post->ID, 'sh_meetup_location', $single = true);
                        $sh_meetup_display_date = get_post_meta($post->ID, 'sh_meetup_display_date', $single = true);
                        ?>                      
                          <div class="rowOrangeBox <?php echo $firstrow?>">
                              <div class="rowOrangeDate"><?php echo $sh_meetup_display_date;?> <span class="rowOrangeLocation"> // <?php echo $sh_meetup_location;?></span></div>
                              <div class="rowOrangeTitle"><?php echo get_the_title();?> </div>
                              <div><?php echo get_the_excerpt();?></div>

                          </div>
                          <?php
                          $x++;
                      endwhile; 
                    ?>

                          <div>
                              <a href="<?php echo home_url('meet-up');?>" class="menuReturn loadmoreBtn orangeSection">More meet-ups <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                          </div>
                      </div><!-- end widget -->
                  </div><!-- end col-lg-6 -->



              