<?php
$template_url = get_template_directory_uri();
?>
<script src="<?php echo $template_url;?>/js/jquery.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.scrollbar.js"></script>
<script>
    $(function () {
        $(".PopupContainer").scrollbar();

        $('.social_link').click(function (e) {
	        e.preventDefault();
	        var strURL = $(this).attr("href");
	        var LeftPosition = (screen.width) ? (screen.width-520)/2 : 0;
	        var TopPosition = (screen.height) ? (screen.height-300)/2 : 0;
	        window.open(strURL ,"popup","width=680,height=460,toolbar=yes,top="+TopPosition+", left="+LeftPosition+"");          
	        return false;
	    });
    });
</script>
</body>
</html>