<?php
$template_url = get_template_directory_uri();
$current_user = wp_get_current_user();
$pro_status = $current_user->is_complete;
global $wp;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <title><?php wp_title(); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php wp_head(); ?>
    <!-- Support for HTML5 -->
      <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <style>
      .PopupContainer
      {
        overflow: unset;
        height: auto;
      }
      </style>
      <script>
        var siteURL = '<?php echo home_url('/');?>';
        var currentURL = '<?php echo home_url( $wp->request );?>';
        var pro_status = '<?php echo $pro_status;?>';
      </script>
	</head>
  <body <?php body_class(isset($class) ? $class : ''); ?>>
    <div id="startupModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="startupModal">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body"></div>
        </div>
      </div>
    </div>

    <div id="alertmodal" class="modal fade bs-example-modal-sm <?php if (dshub_has_notices('error')) echo ' show-alert shub-notice-error '; ?> <?php if (dshub_has_notices('success')) echo ' show-alert shub-notice-success '; ?>" role="dialog" >
      <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Alert</h4>
          </div>
          <div class="modal-body text-center">
            <?php if (dshub_has_notices('error')) dshub_show_notices('error'); ?>
            <?php if (dshub_has_notices('success')) dshub_show_notices('success'); ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

<section class="blue-wrapper">
    <div class="overlay_blue">
        <header id="header-style-1">
            <div class="container">
                <div class="navbar yamm navbar-default">
                    <div class="navbar-header col-lg-4 col-md-8 col-sm-6 col-xs-6">               
                        <a href="<?php echo home_url('');?>" class="navbar-brand"></a>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 alignmenuRight" >
                        <div class="burgermenu"></div>
                                    <?php
                            if ( is_user_logged_in() ) {
                                ?>
                                <div class="user_profile">
                                    <div class="col-lg-9 no-margin">
                                        <div class="welcome ">Welcome</div>
                                        <div class="name"><?php loggedUser(); ?></div><br>                                        
                                    </div>
                                    <div class="col-lg-3 paddingno">
                                        <div class="profile_pic user-controls" style="background: center url(<?php echo shub_user_avatar($user_ID); ?>);background-size: cover;cursor: pointer;"></div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                </div>
                             <?php
                            } else { ?>
                                <div class="loginBtn">Login</div>
                             <?php
                                }
                                $keyword = "";
                                if(isset($_GET["keyword"]))
                                {
                                    $keyword = $_GET["keyword"];
                                }
                            ?>
                        
                        <div class="SearchHolder">
                            <input id="top_search" name="top_search" type="text" placeholder="Search" class="searchInput" value="<?php echo $keyword;?>" />
                            <div class="looptosearch top_search_button"></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="menu_Holder">
            <div class="container">
                <div class="closeMenu"><i class="fa fa-chevron-up"></i></div>
                <div class="menuRow">
                        <div class="menu col-lg-6 col-md-12 col-sm-12">
                            <?php strip_tags(wp_nav_menu( array('theme_location' => 'main_menu', 'depth'=> 0, 'items_wrap' => '%3$s', 'container'=> false)), '<a>' ); ?>
                            <div class="pull-left">
                                <div class="socialmediaIcons"><a href="https://twitter.com/DubaiStartupHub" target="_blank"><img src="<?php echo $template_url;?>/images/twitter_footer_logo.png"></a></div>
                                <div class="socialmediaIcons"><a href="https://www.facebook.com/dubaistartuphub/" target="_blank"><img src="<?php echo $template_url;?>/images/facebook_footer_logo.png"></a></div>
                                <div class="socialmediaIcons"><a href="https://www.linkedin.com/company/dubai-startup-hub" target="_blank"><img src="<?php echo $template_url;?>/images/linkedin_footer_logo.png"></a></div>
                                <div class="socialmediaIcons"><a href="https://www.instagram.com/dubaistartuphub/" target="_blank"><img src="<?php echo $template_url;?>/images/instagram_footer_logo.png"></a></div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 pull-right mobileBoxes">

                            <div class="widget OrangeColumn menuColumn col-lg-12 col-md-6 col-sm-6">
                                <div class="titleOrangeBox">
                                    <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Keep me up to date
                                </div>
                                <div class="rowOrangeBox firstrow no-border">
                                    <div class="rowOrangeDate signupTitle">Sign up for our newsletter </div>
                                    <div class="inpuNewsletterHolder">
                                        <input name="emailNewsletter" id="EmailNewsletter" class="form-control NewsletterInput" placeholder="Email address..." type="email">
                                        <div class="submitOk">OK</div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget JoinConversation col-lg-12 col-md-6 col-sm-6">
                                <div class="titleOrangeBox">
                                    <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Join the conversation
                                </div>
                                <div class="rowOrangeBox firstrow no-border">
                                    <div class="rowOrangeDate signupTitle">See latest discussions on the forum</div>
                                    <div class="inpuNewsletterHolder">
                                        <a href="<?php echo home_url('forum');?>" class="menuReturn">Go to forum <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 quickLinksBox pull-right mobileBoxes">
                            <div class="titleOrangeBox">
                                <i class="fa fa-chevron-right nextBlue" aria-hidden="true"></i>Quick links
                            </div>
                            <div class="smallblueLink">Quickly browse through the main topics</div>
                            <?php strip_tags(wp_nav_menu( array('theme_location' => 'category_menu', 'depth'=> 0, 'items_wrap' => '%3$s', 'container'=> false, 'before' => '<i class="fa fa-chevron-right nextBlue"></i>')), '<a>' ); ?>
                            <div class="clearfix"></div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div>
        <div class="login_Holder">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alignmenuRight margin-top-big">
                        <form id="loginfrm" name="loginfrm" autocomplete="off">
                            <?php if ( !is_user_logged_in() ) : ?>
                            <div class="RegisterBtn">Register</div>
                            <?php else: ?>
                            <div class="login-btn"><a href="<?php echo Wp_logout_url() ?>">Logout</a></div>                  
                            <div class="EditBtn"><a href="<?php echo home_url('dashboard') ?>">Edit profile</a> <i class="fa fa-cog"></i></div>
                            <?php endif; ?>

                            <?php if ( !is_user_logged_in() ) : ?>
                            <div class="loginFieldHolder col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <input name="sh_username" id="sh_username" placeholder="Username..." class="LoginFields" type="text">
                            </div>
                            <div class="loginFieldHolder col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <input name="sh_password" id="sh_password" placeholder="Password..." class="LoginFields" type="password">
                            </div>
                            <div class="loginBtnArrows login-submit"><i class="fa fa-chevron-right coloringOrange"></i><i class="fa fa-chevron-right coloringOrange"></i><i class="fa fa-chevron-right coloringOrange"></i></div>                        
                            <?php endif; ?>
                            <div class="clearfix"></div>
                            <?php if ( !is_user_logged_in() ) { ?>
                            <div class="rowLoginSocialmedia">
                                <div class="socialmediaCopy pull-right">
                                    <div style="padding-top:6px; float:left">
                                        Or sign in with your social media account
                                    </div>
                                    <div class="pull-right">
                                        <?php do_action( 'wordpress_social_login' ); ?> 
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="registrationbox">
                        <form id="registerfrm" name="registerfrm" method="post" enctype="multipart/form-data">
                            <div class="titleOrangeBox registerBox registerBoxTitle">
                                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Register
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-left">
                                    <?php
                                    if(isset($_GET["register"]))
                                    {
                                        if(isset($_GET["msg"]))
                                        {
                                            $msg = html_entity_decode($_GET["msg"]);
                                            if($msg == "Sorry,thatusernamealreadyexists!")
                                            {
                                                $msg = "Sorry, that username already exists!";
                                            }
                                            if($msg == "Sorry,thatemailaddressisalreadyused!")
                                            {
                                                $msg = "Sorry, that email address is already used!";   
                                            }
                                        }
                                        else
                                        {
                                            $msg = "Please check the fields";
                                        }                                    
                                    ?>
                                    <div class="error">Registration Failed: <?php echo $msg;?></div>
                                    <?php
                                    }
                                    ?>
                                    <input name="Full_name" id="Full_name" class="form-control" placeholder="Full name" type="text">
                                    <div class="user_label error"></div>
                                    <input name="Username" id="Username" class="form-control" placeholder="User name" type="text">                                    
                                    <div class="pass_label error"></div>
                                    <input name="Password" id="Password" class="form-control" placeholder="Password" type="password">                                    
                                    <input name="CPassword" id="CPassword" class="form-control" placeholder="Confirm Password" type="password">
                                    <input name="Company" id="Company" class="form-control" placeholder="Company" type="text">
                                    <div class="email_label error"></div>
                                    <input name="Email" id="Email" class="form-control" placeholder="Email" type="text">
                                    <select name="Industry" id="Industry" class="form-control">
                                        <option value="">Industry</option>
                                        <?php
                                        global $wpdb;
                                        $table_name = $wpdb->prefix . 'industry';
                                        $query = $wpdb->get_results("SELECT id, industry_name FROM $table_name");
                                        foreach($query as $row)
                                        {
                                            echo '<option value="'.$row->industry_name.'">'.$row->industry_name.'</option>';
                                        }
                                        ?>
                                    </select>
                                    <div class="inpuNewsletterHolder ">
                                     <div>
                                         <label><input type="checkbox" class="form-checkbox" name="newsletter" value="Yes"> <span class="coloringOrange signupCheck">Sign me up for the newsletter</span></label><br>
                                     </div>
                                    </div>                            
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <input type="file" class="thefilecss" name="avatar_file" id="avatar_file" >
                                <div class="imageHolderUpload">Upload profile picture</div>
                                <button type="submit" class="uploadbtn">Upload</button>
                                <input type="hidden" name="action" value="register_user" />
                                <?php wp_nonce_field( 'user_register'); ?>
                                <input type="button" class="AboutBtn register-submit" value="Submit">
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                    </div>
        </div>

        <div class="container">
            <?php do_action( 'bbp_template_notices' ); ?>
                <?php
                    $post_id = get_the_ID();
                    $sh_topbanner_template = get_post_meta($post_id, 'sh_topbanner_template', true);
                    $post_type = get_post_type();
                    //echo $post_type;
                    if($sh_topbanner_template != "")
                    {
                        get_template_part( 'templates/'.$sh_topbanner_template );
                    }
                    else if(is_category())
                    {
                        get_template_part( 'templates/top-banner-category' );
                    }
                    else if($post_type == 'forum' || $post_type == 'topic')
                    {
                        get_template_part( 'templates/top-banner-forum');
                    }
                    else
                    {
                        get_template_part( 'templates/top-banner1' );
                    }
                ?>
        </div>
    </div>
</section>
<div class="scroll-DownBtn"><i class="fa fa-chevron-down"></i></div>