<?php

//Add thumbnail, automatic feed links and title tag support
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );

//Add menu support and register main menu
if ( function_exists( 'register_nav_menus' ) ) {
  	register_nav_menus(
  		array(
  		  'main_menu' => 'Main Menu',
        'category_menu' => 'Category Menu',
        'footer_menu' => 'Footer Menu',
        'user_menu' => 'User Menu',
  		)
  	);
}

// filter the Gravity Forms button type
add_filter('gform_submit_button', 'form_submit_button', 10, 2);
function form_submit_button($button, $form){
    return "<button class='button btn' id='gform_submit_button_{$form["id"]}'><span>{$form['button']['text']}</span></button>";
}

// Register sidebar
add_action('widgets_init', 'theme_register_sidebar');
function theme_register_sidebar() {
	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'id' => 'sidebar-1',
		    'before_widget' => '<div id="%1$s" class="widget %2$s">',
		    'after_widget' => '</div>',
		    'before_title' => '<h4>',
		    'after_title' => '</h4>',
		 ));
	}
}

function footer_widgets_init() {
  register_sidebar( array(
    'name'          => 'Footer Navigation Area',
    'id'            => 'footer_nav_1',
    'before_widget' => '<div class="col-md-3 col-xs-6">',
    'after_widget'  => '</div>',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>',
  ) );
}
add_action( 'widgets_init', 'footer_widgets_init' );

function events_widgets_init() {
  register_sidebar( array(
    'name'          => 'Events Listing Area',
    'id'            => 'event_listing_area',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => '',
  ) );
}
add_action( 'widgets_init', 'events_widgets_init' );

if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Overview Image',
            'id' => 'overview-image',
            'post_type' => 'page'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Overview Image',
            'id' => 'news-overview-image',
            'post_type' => 'news'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Overview Image',
            'id' => 'events-overview-image',
            'post_type' => 'events'
        )
    );

}

add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

// START THEME OPTIONS
// custom theme options for user in admin area - Appearance > Theme Options
function pu_theme_menu()
{
  add_theme_page( 'Theme Option', 'Theme Options', 'manage_options', 'pu_theme_options.php', 'pu_theme_page');
}
add_action('admin_menu', 'pu_theme_menu');

function pu_theme_page()
{
?>
    <div class="section panel">
      <h1>Custom Theme Options</h1>
      <form method="post" enctype="multipart/form-data" action="options.php">
      <hr>
        <?php 

          settings_fields('pu_theme_options'); 
        
          do_settings_sections('pu_theme_options.php');
          echo '<hr>';
        ?>
            <p class="submit">  
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />  
            </p>
      </form>
    </div>
    <?php
}

add_action( 'admin_init', 'pu_register_settings' );

function pu_display_section()
{

}
/**
 * Function to register the settings
 */
function pu_register_settings()
{
    // Register the settings with Validation callback
    register_setting( 'pu_theme_options', 'pu_theme_options' );

    // Add settings section
    add_settings_section( 'pu_text_section', 'Social Links', 'pu_display_section', 'pu_theme_options.php' );

    // Create textbox field
    $field_args = array(
      'type'      => 'text',
      'id'        => 'twitter_link',
      'name'      => 'twitter_link',
      'desc'      => 'Twitter Link - Example: http://twitter.com/username',
      'std'       => '',
      'label_for' => 'twitter_link',
      'class'     => 'css_class'
    );

    // Add twitter field
    add_settings_field( 'twitter_link', 'Twitter', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );   

    $field_args = array(
      'type'      => 'text',
      'id'        => 'facebook_link',
      'name'      => 'facebook_link',
      'desc'      => 'Facebook Link - Example: http://facebook.com/username',
      'std'       => '',
      'label_for' => 'facebook_link',
      'class'     => 'css_class'
    );

    // Add facebook field
    add_settings_field( 'facebook_link', 'Facebook', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    $field_args = array(
      'type'      => 'text',
      'id'        => 'gplus_link',
      'name'      => 'gplus_link',
      'desc'      => 'Google+ Link - Example: http://plus.google.com/user_id',
      'std'       => '',
      'label_for' => 'gplus_link',
      'class'     => 'css_class'
    );

    // Add Google+ field
    add_settings_field( 'gplus_link', 'Google+', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    $field_args = array(
      'type'      => 'text',
      'id'        => 'youtube_link',
      'name'      => 'youtube_link',
      'desc'      => 'Youtube Link - Example: https://www.youtube.com/channel/channel_id',
      'std'       => '',
      'label_for' => 'youtube_link',
      'class'     => 'css_class'
    );

    // Add youtube field
    add_settings_field( 'youtube_ink', 'Youtube', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    $field_args = array(
      'type'      => 'text',
      'id'        => 'linkedin_link',
      'name'      => 'linkedin_link',
      'desc'      => 'LinkedIn Link - Example: http://linkedin.com/in/username',
      'std'       => '',
      'label_for' => 'linkedin_link',
      'class'     => 'css_class'
    );

    // Add LinkedIn field
    add_settings_field( 'linkedin_link', 'LinkedIn', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    $field_args = array(
      'type'      => 'text',
      'id'        => 'instagram_link',
      'name'      => 'instagram_link',
      'desc'      => 'Instagram Link - Example: http://instagram.com/username',
      'std'       => '',
      'label_for' => 'instagram_link',
      'class'     => 'css_class'
    );

    // Add Instagram field
    add_settings_field( 'instagram_link', 'Instagram', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    // Add settings section title here
    add_settings_section( 'section_name_here', 'Section Title Here', 'pu_display_section', 'pu_theme_options.php' );
    
    // Create textarea field
    $field_args = array(
      'type'      => 'textarea',
      'id'        => 'settings_field_1',
      'name'      => 'settings_field_1',
      'desc'      => 'Setting Description Here',
      'std'       => '',
      'label_for' => 'settings_field_1'
    );

    // section_name should be same as section_name above (line 116)
    add_settings_field( 'settings_field_1', 'Setting Title Here', 'pu_display_setting', 'pu_theme_options.php', 'section_name_here', $field_args );   


    // Copy lines 118 through 129 to create additional field within that section
    // Copy line 116 for a new section and then 118-129 to create a field in that section
}


// allow wordpress post editor functions to be used in theme options
function pu_display_setting($args)
{
    extract( $args );

    $option_name = 'pu_theme_options';

    $options = get_option( $option_name );

    switch ( $type ) {  
          case 'text':  
              $options[$id] = stripslashes($options[$id]);  
              $options[$id] = esc_attr( $options[$id]);  
              echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[$id]' value='$options[$id]' />";  
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
          break;
          case 'textarea':  
              $options[$id] = stripslashes($options[$id]);  
              //$options[$id] = esc_attr( $options[$id]);
              $options[$id] = esc_html( $options[$id]); 

              printf(
                wp_editor($options[$id], $id, 
                  array('textarea_name' => $option_name . "[$id]",
                    'style' => 'width: 200px'
                    )) 
        );
              // echo "<textarea id='$id' name='" . $option_name . "[$id]' rows='10' cols='50'>".$options[$id]."</textarea>";  
              // echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break; 
    }
}

function pu_validate_settings($input)
{
  foreach($input as $k => $v)
  {
    $newinput[$k] = trim($v);
    
    // Check the input is a letter or a number
    if(!preg_match('/^[A-Z0-9 _]*$/i', $v)) {
      $newinput[$k] = '';
    }
  }

  return $newinput;
}

// Add custom styles to theme options area
add_action('admin_head', 'custom_style');

function custom_style() {
  echo '<style>
    .appearance_page_pu_theme_options .wp-editor-wrap {
      width: 75%;
    }
    .regular-textcss_class {
      width: 50%;
    }
    .appearance_page_pu_theme_options h3 {
      font-size: 2em;
      padding-top: 40px;
    }
  </style>';
}

// END THEME OPTIONS


/**
 * Load site scripts.
 */
function bootstrap_theme_enqueue_scripts() {
	$template_url = get_template_directory_uri();
	// jQuery.
	
  wp_enqueue_script(
        'custom-script',
        get_stylesheet_directory_uri() . '/js/ajax.js',
        array( 'jquery' ), false, false
    );

  $my_nonce = wp_create_nonce('csrf_check');
  wp_localize_script( 'custom-script', 'the_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'csrf_token' => $my_nonce ) );

	wp_enqueue_style( 'bootstrap-style', $template_url . '/css/bootstrap.css' );

  wp_enqueue_style( 'owl-carousel', $template_url . '/css/owl-carousel.css' );

  wp_enqueue_style( 'animate-style', $template_url . '/css/animate.min.css' );  

  wp_enqueue_style( 'jQuery-ui-style', $template_url . '/css/jquery-ui.css' ); 

  wp_enqueue_style( 'fancy-box-style', $template_url . '/css/jquery.fancybox.css' ); 

  wp_enqueue_style( 'custom-style', $template_url . '/css/styles.css' );
	//Main Style
	wp_enqueue_style( 'main-style', get_stylesheet_uri() );
	// Load Thread comments WordPress script.
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'bootstrap_theme_enqueue_scripts', 1 );


add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');

function myStartSession() {
    if(!session_id()) {
        session_start();
    }
}

function myEndSession() {
    session_destroy ();
}

function shub_user_register() {
    if($_POST['action'] == "register_user")
    {
        $retrieved_nonce = $_REQUEST['_wpnonce'];
        if (!wp_verify_nonce($retrieved_nonce, 'user_register' ))
        {
            die( 'Failed security check' );
        }
        else
        {
            $Username = trim($_POST['Username']);
            $Password = trim($_POST['Password']);
            $Full_name = trim($_POST['Full_name']);
            $Company = trim($_POST['Company']);
            $Email = trim($_POST['Email']);
            $Industry = trim($_POST['Industry']);

            $userdata = array(
              'user_pass' =>  $Password,
              'user_login' => $Username,
              'first_name' => $Full_name,
              'last_name' => '',
              'user_email' => $Email,
              'user_url' => '',
              'role' => get_option( 'default_role' )
            );
            $new_user = wp_insert_user( $userdata );
            if (!is_wp_error($new_user))
            {
                $user = wp_signon( array( 'user_login' => $Username, 'user_password' => $Password, 'remember' => "forever" ), false );       
                //wp_new_user_notification($new_user, $Password);                
                if ( !empty( $_POST['Company'] ) )
                {
                    update_user_meta( $new_user, 'Company', esc_attr($_POST['Company']));
                }
                if ( !empty( $_POST['Industry'] ) )
                {
                    update_user_meta( $new_user, 'Industry', esc_attr($_POST['Industry']) );
                    update_user_meta( $new_user, 'is_complete', '1');
                }
                if ( !empty( $_POST['newsletter'] ) )
                {
                  update_user_meta( $new_user, 'newsletter', esc_attr($_POST['newsletter']) );
                }
                if(isset($_FILES['avatar_file']))
                {
                  if (!function_exists( 'wp_handle_upload' ) ) {
                        require_once( ABSPATH . 'wp-admin/includes/file.php' );
                    }
                    $uploadedfile = $_FILES['avatar_file'];
                    $upload_overrides = array( 'test_form' => false );
                    $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                    if ( $movefile && ! isset( $movefile['error'] ) ) {
                        update_user_meta($new_user, 'wsl_current_user_image', $movefile['url']);
                    }
                }
                $url = home_url('dashboard');
                wp_redirect($url);
                exit;
            }
            else
            {
                $msg = $new_user->get_error_message();
                $url = home_url('?register=error&msg='.$msg);
                wp_redirect($url);
                exit;
            }
        }
    }
}
add_action( 'wp_loaded', 'shub_user_register' );

function user_update_form(){
    if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] )) {
      if($_POST['action'] == "updateuser")
      {
          $retrieved_nonce = $_REQUEST['_wpnonce'];
          if (!wp_verify_nonce($retrieved_nonce, 'update_user' ))
          {
              die( 'Failed security check' );
          }
          else
          {
              $current_user = wp_get_current_user();
              $user_id = $current_user->ID;

              $user_fields = array(
                  'ID'           => $user_id,
                  'first_name'   => esc_attr($_POST['up_Full_name']),
                  'user_email'   => esc_attr($_POST['up_Email'])
              );              

              if ( !empty( $_POST['up_Company'] ) )
              {
                 update_user_meta( $user_id, 'Company', esc_attr($_POST['up_Company']));
              }
              if ( !empty( $_POST['up_Industry'] ) )
              {
                  update_user_meta( $user_id, 'Industry', esc_attr($_POST['up_Industry']) );
                  update_user_meta( $user_id, 'is_complete', '1');
              }
              if(isset($_FILES['avatar_file']))
              {
                  if (!function_exists( 'wp_handle_upload' ) ) {
                        require_once( ABSPATH . 'wp-admin/includes/file.php' );
                  }
                  $uploadedfile = $_FILES['avatar_file'];
                  $upload_overrides = array( 'test_form' => false );
                  $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                  if ( $movefile && ! isset( $movefile['error'] ) ) {
                      update_user_meta($user_id, 'wsl_current_user_image', $movefile['url']);
                  }
              }

              wp_update_user($user_fields);

              $url = home_url('dashboard/?update=a');
              wp_redirect($url);
              exit();
          }
      }    
    }
}
add_action( 'wp_loaded', 'user_update_form' );

function user_update_pass_form(){
    if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] )) {
      if($_POST['action'] == "updatepassword")
      {
          $retrieved_nonce = $_REQUEST['_wpnonce'];
          if (!wp_verify_nonce($retrieved_nonce, 'update_user_password' ))
          {
              die( 'Failed security check' );
          }
          else
          {

              $pass = esc_attr($_POST['current_password']);
              $current_user = wp_get_current_user();
              $user_id = $current_user->ID;

              $user = get_user_by( 'ID', $user_id );

              if ( $user && wp_check_password( $pass, $user->data->user_pass, $user->ID) )
              {                
                  $password = esc_attr($_POST['new_password']);
                  wp_set_password( $password, $user_id );
                  $url = home_url('dashboard/?update=p');
                  wp_redirect($url);
                  exit();
              } 
              else
              {
                   wp_redirect(home_url('dashboard/?update=f'));
                   exit();
              }

              
          }
      }    
    }
}
add_action( 'wp_loaded', 'user_update_pass_form' );

function tml_registration_errors( $errors ) {
  if ( empty( $_POST['user_company'] ) )
    $errors->add( 'empty_user_company', '<strong>ERROR</strong>: Please enter your company.' );
  if ( empty( $_POST['user_industry'] ) )
    $errors->add( 'empty_user_industry', '<strong>ERROR</strong>: Please enter your industry.' );
  return $errors;
}
add_filter( 'registration_errors', 'tml_registration_errors');

function tml_user_register( $user_id ) {
  if($_POST['action'] == "register")
      {
          if ( !empty( $_POST['user_company'] ) )
          {
              update_user_meta( $user_id, 'Company', esc_attr($_POST['user_company']));
          }
          if ( !empty( $_POST['user_industry'] ) )
          {
              update_user_meta( $user_id, 'Industry', esc_attr($_POST['user_industry']) );
              update_user_meta( $user_id, 'is_complete', '1');
          }
    }
}
add_action( 'user_register', 'tml_user_register' );

function shub_user_avatar($user_id) {
    $default_avatar = site_url('wp-content/uploads/2017/default-profile.png');
    $avatar = get_user_meta($user_id, 'wsl_current_user_image', true);
    if (empty($avatar)) {
      
      $gravatar = get_avatar_url($user_id);
      
      if (strpos($gravatar, 'blank') === false)
        $avatar = $gravatar;
    }

    return (!empty($avatar)) ? $avatar : $default_avatar;
}

function loggedUser(){
   $user = wp_get_current_user();
   if($user->user_firstname)
   {
      echo $user->user_firstname . ' ' . $user->user_lastname;
   }
   else
   {
      echo __('User');
   }   
}

function getUserName($user_id){
   $user = get_userdata($user_id);  
   if($user->user_firstname)
   {
      echo $user->user_firstname . ' ' . $user->user_lastname;
   }
   else
   {
      echo __('User');
   }   
}

/**
 * Format the date
 *
 */
function shub_date_format( $time = null ) {
  $time = empty($time) ? time() : $time;

  return date('d M Y', $time);
}

function shub_time_format( $time = null ) {
  $time = empty($time) ? time() : $time;

  return date('h:s A', $time);
}

function shub_login_class() {
  return (is_user_logged_in()) ? '' : 'need-signin';
}

add_filter( 'body_class','halfhalf_body_class' );
function halfhalf_body_class( $classes ) {
 
    if ( is_page_template( 'forum.php' ) ) {
        $classes[] = 'forumPage';
    }
     
    return $classes;
     
}

function shub_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'shub_excerpt_length', 999 );


function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}
/*if( !wp_next_scheduled( 'feedevents_refresh' ) ) {
   wp_schedule_event( time(), 'daily', 'feedevents_refresh' );
}*/
 
//add_action( 'feedevents_refresh', 'update_events_feed' );
add_action( 'wp_loaded', 'update_events_feed' );
function update_events_feed() {
   if($_REQUEST["event_feed"]== "true")
   {
      $jsondata = file_get_contents('http://astrolabs.com/wp-json/tribe/events/v1/events?per_page=10');
      $jsondata = json_decode($jsondata);
      //echo '<pre>';
      //print_r($jsondata);
      foreach ($jsondata->events as $key => $value) {
        //echo $key;
        if($value->venue->city=='Dubai')
        {
            $args = array(
                'post_type'=> 'events',
                'meta_query' => array(
                      array(
                          'key' => 'sh_events_eid',
                          'value' => ''.$value->id.'',
                          'compare' => '='
                      )
                  ),
            );
            //var_dump($args);
            $wp_query = new WP_Query($args);
            $x=0;
            while ($wp_query->have_posts()) : $wp_query->the_post();
              $x = 1;
            endwhile;
            
            if($x==0)
            {

                $post_id = wp_insert_post(array (
                   'post_type' => 'events',
                   'post_title' => $value->title,
                   'post_content' => $value->description,
                   'post_status' => 'pending'
                ));
                if ($post_id) {
                   add_post_meta($post_id, 'sh_events_date', $value->start_date);
                   add_post_meta($post_id, 'sh_events_date_end', $value->end_date);
                   add_post_meta($post_id, 'sh_events_location', $value->venue->venue);
                   add_post_meta($post_id, 'sh_events_address', $value->venue->address);
                   add_post_meta($post_id, 'sh_events_city', $value->venue->city);
                   add_post_meta($post_id, 'sh_events_ticket', $value->website);
                   add_post_meta($post_id, 'sh_events_eid', $value->id);
                   foreach ($value->categories as $cat)
                    {
                      $tag_name[] = $cat->name;
                    }
                    $tag_names = implode (", ", $tag_name);
                    wp_set_post_tags( $post_id, $tag_names, true );
                    Generate_Featured_Image($value->image->url, $post_id);
                }
                /*echo $value->id.'<br>';
                echo $value->title.'<br>';
                echo $value->image->url.'<br>';
                echo 'Start Date:' . $value->start_date . '<br>';
                echo 'End Date:' . $value->end_date . '<br>';
                foreach ($value->categories as $cat)
                {
                  echo $cat->name.'<br>';
                }
                echo $value->venue->city;
                echo $value->website;*/
            }

            
        }
      }
      //wp_die();
      exit();
    }
}


add_action('wp_loaded', 'update_rss_feed');
function update_rss_feed() {
  if($_REQUEST["rss_feed"]== "true")
   {        
        if(function_exists('fetch_feed')){
            $uri = 'https://courses.potential.com/feed/';
            $feed = fetch_feed($uri);
        }
        if($feed) {
            foreach ($feed->get_items() as $item){
                $titlepost = $item->get_title();
                $content = $item->get_content();
                $description = $item->get_description();
                $itemdate = $item->get_date();
                $media_group = $item->get_item_tags('', 'enclosure');
                //echo $media_group;
                //$img = $media_group[0]['attribs']['']['url'];
                //$width = $media_group[0]['attribs']['']['width'];           
                // $latestItemDate = $feed->get_item()->get_date();

                global $wpdb;

                $query = $wpdb->prepare(
                    'SELECT ID FROM ' . $wpdb->posts . '
                    WHERE post_title = %s
                    AND post_type = \'post\'',
                    $titlepost
                );
                $wpdb->query( $query );

                if ( $wpdb->num_rows ) {
                    
                } else {
                    $post_information = array(
                        'post_title' => $titlepost,
                        'post_content' => $description,
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'post_category' => array('15'),
                        'post_date' => date('Y-m-d H:i:s', strtotime($itemdate)),
                    );
                    //var_dump($post_information);
                    wp_insert_post( $post_information ); 
                }
            }
        }
    //wp_die();
    exit();
  }
}