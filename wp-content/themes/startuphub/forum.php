<?php
/* Template Name: Forum Template */
$template_url = get_template_directory_uri();
get_header(); 

 $parent_post = bbp_get_forum_id();
 $forum_title = 'Recent discussions';
  $trig = false;
  if($parent_post == 0)
  {
    $parent_post = 'any';
  }
  else
  {
    $trig = true;
    $forum_title = 'Discussions: ' . bbp_get_forum_title($parent_post);
  }
$keyword = "";
if(isset($_GET['keyword']))
{
    $keyword = $_GET['keyword'];    
    $forum_title = "Forums";
}

?>
<div class="white-wrapper" id="mainContent">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
                <h3 class="titleHeader"><?php echo $forum_title;?></h3>
                <?php
                if($keyword!="")
                      {
                      echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for <em><b>'.$keyword.'</b></em><br><br></div>';
                    }
                ?>
            </div>
            <div class="alignCenter">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding ">
                    <div class="widget">

                      <?php
                      $args = array( 'author' => 0, 'show_stickies' => false, 'order' => 'DESC', 'post_parent' => $parent_post, 'posts_per_page' => 10 );
                      if($keyword!="")
                      {
                        $args['s'] = $keyword;
                      }
                      if ( bbp_has_topics( $args ) )
                        bbp_get_template_part( 'bbpress/loop', 'topics' );
                      ?>                
                    </div><!-- end widget -->
                </div><!-- end col-lg-10 -->
            </div>
        </div>
    </div>
<?php get_footer(); ?>
<script type="text/javascript">
    $(window).load(function() {
    <?php 
    if($trig)
    {

    ?>
        $('.scroll-DownBtn').trigger('click');

      <?php
        }
    ?>
    });
</script>