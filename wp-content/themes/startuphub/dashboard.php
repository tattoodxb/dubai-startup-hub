<?php
/* Template Name: Dashboard Template */
$template_url = get_template_directory_uri();
if ( is_user_logged_in() ) {
}
else
{
    wp_redirect(home_url());
    exit;
}

get_header();

 ?>


<div class="white-wrapper" id="mainContent">

        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
                <h3 class="titleHeader">Change your details</h3>
            </div>
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">

                <div class="">
                    <?php
                    $current_user = wp_get_current_user();
                    ?>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 removeLeftpadding">

                        <form name="updateuser" id="updateuser" action="" enctype="multipart/form-data" method="post">

                        <div class="Detailedbox">
                            <div class="titleOrangeBox registerBox registerBoxTitle">
                                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Your details as registered
                            </div>
                            <div class="rowField">
                                <input name="up_Full_name" id="up_Full_name" placeholder="*Full Name" class="form-control input-md" required type="text" value="<?php echo $current_user->user_firstname;?>">
                            </div>
                            <div class="rowField">
                                <input name="up_User_name" id="up_User_name" placeholder="User Name" class="form-control input-md disabled" required type="text" value="<?php echo $current_user->user_login;?>" disabled>
                            </div>                            
                            <div class="rowField">
                                <input name="up_Email" id="up_Email" placeholder="*Email" class="form-control input-md" required type="text" value="<?php echo $current_user->user_email;?>">
                            </div>
                            <div class="rowField">
                                <input name="up_Company" id="up_Company" placeholder="*Company" class="form-control input-md" required type="text" value="<?php echo $current_user->Company;?>">
                            </div>
                            <div class="rowField">
                                <select name="up_Industry" id="up_Industry" class="form-control">
                                    <option value="">Select Industry</option>
                                        <?php
                                        global $wpdb;
                                        $table_name = $wpdb->prefix . 'industry';
                                        $query = $wpdb->get_results("SELECT id, industry_name FROM $table_name");
                                        foreach($query as $row)
                                        {
                                            $selected = "";
                                            if($row->industry_name == $current_user->Industry)
                                            {
                                                $selected = "selected";
                                            }
                                            echo '<option value="'.$row->industry_name.'" '.$selected.'>'.$row->industry_name.'</option>';
                                        }
                                        ?>
                                    </select>
                            </div>
                            <div class="rowField">
                                <div class="imageInp">
                                    <span>Please upload image</span>
                                    <input name="avatar_file" id="avatar" class="input-file" type="file">
                                </div>
                            </div>

                            <input type="hidden" id="h_country" name="h_country" value="" />
                            <input type="hidden" name="action" value="updateuser" />
                            <?php wp_nonce_field( 'update_user'); ?>
                            <input type="button" name="wp-submit" value="Save" class="btn AboutBtn" onclick="return validate_accountsetting();" />                        </div>
                    </form>
                        </div>


                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 removeLeftpadding  ">
                        <div class="Detailedbox">
                            <form name="changepassword" id="changepassword" action="" method="post">
                            <div class="titleOrangeBox registerBox registerBoxTitle">
                                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Reset your password
                            </div>


                            <div class="rowField">
                                <input name="current_password" id="current_password" placeholder="*Current Password" class="form-control input-md" required type="password">

                            </div>
                            <div class="rowField">
                                <input name="new_password" id="new_password" placeholder="*Password" class="form-control input-md" required type="password">

                            </div>

                            <div class="rowField">
                                <input name="con_password" id="con_password" placeholder="*Confirm Password" class="form-control input-md" required type="password">

                            </div>
                            <div class="change-pass-error text-danger"></div>
                            <input type="hidden" name="action" value="updatepassword" />
                            <?php wp_nonce_field( 'update_user_password'); ?>
                            <input type="button" name="wp-c-submit" value="Confirm and save" class="btn AboutBtn enlargeBtn" onclick="return validate_passwordchange();" />

                        </form>
                        </div>


                    </div>
       

                    <div class="clearfix"></div>

                </div>


                </div>

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 orangeHolder deactivateaccount">
 

                <div class="widget JoinConversation hide">
                    <div class="titleOrangeBox">
                        <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Deactivate account
                    </div>
                    <div class="inpuNewsletterHolder margin-top-big">
                        <a href="forum.html" class="menuReturn">Confirm <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div><!-- end col-lg-6 -->

            <div class="clearfix"></div>



            </div>
    </div>


<script>
<?php
if(isset($_GET['update']))
{
	$update = $_GET['update'];
?>
jQuery( document ).ready(function() {

<?php
	if($update=="p")
	{
?>
    jQuery('#alertmodal .modal-body').html('You have successfully saved your password.');
    jQuery('#alertmodal').modal('show');
<?php
}
else if($update=="a")
{
?>
    jQuery('#alertmodal .modal-body').html('You have successfully saved your profile.');
    jQuery('#alertmodal').modal('show');
<?php
}
else if($update=="f")
{
?>
    jQuery('#alertmodal .modal-body').html('Your current password is wrong, please enter correct password.');
    jQuery('#alertmodal').modal('show');
<?php
}
?>

});
<?php
}
?>
</script>
<?php get_footer(); ?>