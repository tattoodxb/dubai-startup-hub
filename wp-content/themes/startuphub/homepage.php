<?php
/* Template Name: Homepage Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<?php get_template_part( 'templates/upcoming-events' ); ?>
<?php get_template_part( 'templates/latest-updates' ); ?>
<?php get_template_part( 'templates/social-media' ); ?>
<?php get_template_part( 'templates/testimonials' ); ?>
<?php get_template_part( 'templates/our-partners' ); ?>
<?php get_footer(); ?>