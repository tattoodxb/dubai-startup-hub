<?php
/* Template Name: Events Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<div class="white-wrapper" id="mainContent">
  <div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
			<h3 class="titleHeader">Upcoming and recent events</h3></div>
      <?php

          $current_date = date('Y-m-d H:i:s');
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $args = array(
              'post_type'=> 'events',
              'post_status' => 'publish',
              'meta_key' => 'sh_events_date',
              'meta_query' => array(
                  array(
                      'key' => 'sh_events_date',
                      'value' => $current_date,
                      'compare' => '>=',
                      'type' => 'DATE'
                  )
              ),
              'orderby'=> 'meta_value_num',
              'order'    => 'ASC',
              'posts_per_page' => 6,
              'paged' => $paged
          );

          if(isset($_GET["tag"]))
          {
            $tag = $_GET["tag"];
          }

          if($tag != "")
          {
            $args['tag'] = $tag;
            echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the keyword <em><b>'.$tag.'</b></em></div>';

          }

          if(isset($_GET["date"]))
          {
            $date = $_GET["date"];
          }

          if($date != "")
          {
              $date = date('Y-m-d', strtotime($date));
              $args['meta_query'] = array(
                array(
                  'key'     => 'sh_events_date',
                  'value'   => $date,
                  'compare' => '=',
                  'type' => 'DATE'
                ),
              );
              echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the events on <em><b>'.$date.'</b></em></div>';
          }


          $wp_query = new WP_Query($args);
          $i=1;
          while ($wp_query->have_posts()) : $wp_query->the_post();
          $sh_events_location = get_post_meta($post->ID, 'sh_events_location', $single = true);
          $sh_events_display_date = get_post_meta($post->ID, 'sh_events_display_date', $single = true);
          $start_date = get_post_meta($post->ID, 'sh_events_date', $single = true);
                        $end_date = get_post_meta($post->ID, 'sh_events_date', $single = true);

                        $start_date = date('F d, Y', strtotime($start_date));
                        $end_date = date('F d, Y', strtotime($end_date));


                      if($sh_events_display_date=="")
                      {
                          if($start_date == $end_date)
                          {
                              $sh_events_display_date = $start_date;  
                          }
                          else
                          {
                              $sh_events_display_date = $start_date . '-' .$end_date;
                          }
                          
                      }
          $post_tags = get_the_tags();
          ?>
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">                
          <div class="questionBox paddingno" >
              <div class="imageEvents"><?php 
                                 the_post_thumbnail('thumbnail', ['class' => 'img-responsive', 'title' => get_the_title()]);
                                 ?></div>
              <div class="eventBoxHolder">
                  <div class="titleEventbold"><?php echo $sh_events_display_date;?><span class="titleEventNormal">  //  </span><span class="titleEventNormal"><?php echo $sh_events_location;?></span></div>
                  <div class="titleEvent"><a href="<?php echo the_permalink();?>" class="ls-modal"><?php echo get_the_title();?></a></div>
                  <div class="descEvent"><?php echo strip_tags(get_the_excerpt());?></div>
                  <span class="readmoreRightcolumn"><a href="<?php echo the_permalink();?>" class="ls-modal">Read more</a></span>
                  <div class="tags">
                      <?php
                            $posttags = $post_tags;
                            if ($posttags) {
                                foreach($posttags as $tag) {
                        ?>
                        <span class="tagCopy"><?php echo $tag->name;?></span>
                        <?php
                                }
                            }
                        ?>
                  </div>
              </div>
          </div>
    </div>
    <?php
            $i++;
            endwhile; 
            ?>
            <?php
            if($i==1)
                {
                    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue"><br>No result found for your search, please refine your search</div>';
                }
            ?>
  </div>

  <div class="text-center"><?php 
        $pag_args = array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $wp_query->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'list',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => sprintf( '<i></i> %1$s', __( 'Prev', 'text-domain' ) ),
            'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
            'add_args'     => false,
            'add_fragment' => '',
            'before_page_number' => '',
            'after_page_number'  => ''
        );
        $return = paginate_links( $pag_args );
        echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return );
    ?>
  </div>
</div>
<?php get_footer(); ?>