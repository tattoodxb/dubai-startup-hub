<?php 
$template_url = get_template_directory_uri();
$popup = "";
if(isset($_GET["page"]))
{
	$popup = $_GET["page"];
}
if($popup!="popup")
{
	get_header(); 
}
else
{
    get_template_part( 'templates/popup-header' );
}
?>
<?php if(have_posts()) : ?>
<?php 
while(have_posts()) : the_post();
    $post_id = get_the_ID();
	$post_title = get_the_title();
    $post_content = get_the_content();
    $post_link = get_the_permalink();
    $post_tags = get_the_tags();
    $categories = get_the_category(); 
    if ( ! empty( $categories ) ) {
        $current_category = $categories[0]->name;
        $cat_slug = $categories[0]->slug;
    }
    if (class_exists('MultiPostThumbnails')) :
        $overview_img = MultiPostThumbnails::get_post_thumbnail_url(
            get_post_type(),
            'events-overview-image' , null, 'post-thumbnail'
        );
    endif;
endwhile;
endif;
?>
<div class="popuHeights" id="mainContent">
    <div class="PopupContainer scrollbar-inner">    
        <div class="row reorder-xs">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 blueColumnPopup paddingno">
                    <img src="<?php echo $overview_img;?>" class="img-responsive" style="width:100%;" />                 
                    <div class="col-lg-12">
                        <div class="authorName shareArticle">Share this article</div>
                        <div class="socialmediaIcons">
                            <a href="http://twitter.com/share?url=<?php echo $post_link;?>?page=popup&text=Dubai%20Startup%20Hub%20Networking%20Series%20at%201776%20" class="social_link"><img src="<?php echo $template_url;?>/images/twitter_footer_logo.png"></a>
                        </div>
                        <div class="socialmediaIcons">
                            <a href="http://www.facebook.com/sharer.php?u=<?php echo $post_link;?>?page=popup" class="social_link"><img src="<?php echo $template_url;?>/images/facebook_footer_logo.png"></a>
                        </div>
                        <div class="socialmediaIcons">
                            <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_link;?>?page=popup" class="social_link"><img src="<?php echo $template_url;?>/images/linkedin_footer_logo.png"></a>
                        </div>
                        <div class="socialmediaIcons">
                            <a href="https://plus.google.com/share?url=<?php echo $post_link;?>?page=popup" class="social_link"><img src="<?php echo $template_url;?>/images/gplus_logo.png"></a>
                        </div>
                        <div class="clearfix"></div>
                    <div class="titleOrangeBox titleArrowPopup">
                        <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i><a href="javascript:window.print()">Print page</a>
                    </div>
                    <!--div class="titleOrangeBox titleArrowPopup">
                        <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>You might also be interested in
                    </div>
                    <div class="copyOrangeBlueCol">
                        Profitability can be interpreted.<br />
                        Reducing the potential scale.<br />
                        Emphasis on monetization<br />

                    </div>

                    <div class="titleOrangeBox titleArrowPopup">
                        <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Information provided by
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-3 imageHolder">
                        <img src="<?php echo $template_url;?>/images/dsh_mastheadibm_about@2x.png" class="img-responsive" />
                    </div-->
                    <div class="clearfix"><br></div>
                    <a href="<?php echo home_url('articles') . '/' . $cat_slug ; ?>" class="menuReturn" target="_parent">Return to <?php echo $current_category?> <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i></a>
                    </div>
                    
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 rightColumnPopup">                    
                    <div class="rowevent">
                        <div class="titleEvent" style="margin-top:30px;"><?php echo $post_title;?></div>
                        <div class="descEvent">
                            <?php echo $post_content;?>
                        </div>
                    </div>
                    <div class="tags">
                        <?php
                            $posttags = $post_tags;
                            if ($posttags) {
                                foreach($posttags as $tag) {
                        ?>
                        <span class="tagCopy"><?php echo $tag->name;?></span>
                        <?php
                                }
                            }
                        ?>
                    </div>

                    <div class="titleOrangeBox rightcolumTitles">
                        <i class="fa fa-chevron-right nextBlue" aria-hidden="true"></i>Upcoming events
                    </div>
                    <?php
                        $args = array(
                            'post_type'=> 'events',
                            'post_status' => 'publish',
                            'orderby'=> 'menu_order',
                            'order'    => 'ASC',
                            'posts_per_page' => 1,
                            'post__not_in' => array($post_id),
                        );
                        $wp_query = new WP_Query($args);
                        while ($wp_query->have_posts()) : $wp_query->the_post();
                            $sh_events_location = get_post_meta(get_the_ID(), 'sh_events_location', $single = true);
                            $sh_events_display_date = get_post_meta(get_the_ID(), 'sh_events_display_date', $single = true);
                        ?>
                            <div class="titleEventbold"><?php echo $sh_events_display_date;?><span class="titleEventNormal">  //  </span><span class="titleEventNormal"><?php echo $sh_events_location;?></span></div>
                            <div class="titleEvent"><?php echo get_the_title();?></div>
                            <div class="descEvent"><?php echo get_the_excerpt();?></div>
                            <a href="<?php echo the_permalink();?>?page=popup" class="ls-modal">Read more</a>
                            </div>
                            <br>
                        <?php 
                            endwhile;
                        ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
</div>
<?php
if($popup!="popup")
{
	get_footer();
}
else
{
    get_template_part( 'templates/popup-footer' );
}
 ?>