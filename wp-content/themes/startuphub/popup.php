<?php 
/* Template Name: Popup Template */
$popup = "";
if(isset($_GET["page"]))
{
	$popup = $_GET["page"];
}
if($popup!="popup")
{
	get_header(); 
}
else
{
    get_template_part( 'templates/popup-header' );
}
?>
<?php if(have_posts()) : ?>
<?php 
while(have_posts()) : the_post();
	$post_title = get_the_title();
?>
<div class="popuHeights" id="mainContent">        
    <div class="PopupContainer scrollbar-inner">    
        <div class="row reorder-xs">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 blueColumnPopup">
            <div class="authorName shareArticle">Share this article</div>
            <div class="col-lg-12">
                <div class="socialmediaIcons">
                    <a href="" target="_blank"><img src="images/email.png"></a>
                </div>
                <div class="socialmediaIcons">
                    <a href="" target="_blank"><img src="images/twitter_footer_logo.png"></a>
                </div>
                <div class="socialmediaIcons">
                    <a href="" target="_blank"><img src="images/facebook_footer_logo.png"></a>
                </div>
                <div class="socialmediaIcons">
                    <a href="" target="_blank"><img src="images/linkedin_footer_logo.png"></a>
                </div>
                <div class="socialmediaIcons">
                    <a href="" target="_blank"><img src="images/instagram_footer_logo.png"></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="titleOrangeBox titleArrowPopup">
                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Print page
            </div>
            <div class="titleOrangeBox titleArrowPopup">
                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Get tickets
            </div>
      

            <div class="titleOrangeBox titleArrowPopup">
                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Submit proposal
            </div>

            <div class="clearfix"></div>
            <a href="index.html" class="menuReturn" style="margin-top:150px;">Return to events <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i></a>



        </div>

        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 rightColumnPopup">
            <div class="rowevent" style="padding-bottom:10PX!important;">
                <div class="titleEventbold">September 29, 2017<span class="titleEventNormal">  //  </span><span class="titleEventNormal">AstroLabs Dubai</span></div>
                <div class="titleEventNormal">Dubai, UAE</div>
            </div>
            <div class="rowevent">
                <div class="titleEvent" style="margin-top:30px;"><?php the_title(); ?></div>
                <div class="descEvent">
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="tags">
                <span class="tagCopy">Information</span>
                <span class="tagCopy">Information</span>
                <span class="tagCopy">Information</span>
                <span class="tagCopy">Information</span>
                <span class="tagCopy">Information</span>
                <span class="tagCopy">Information</span>
            </div>

            <div class="titleOrangeBox rightcolumTitles">
                <i class="fa fa-chevron-right nextBlue" aria-hidden="true"></i>Related events
            </div>
            <div class="titleEventbold">September 29, 2017<span class="titleEventNormal">  //  </span><span class="titleEventNormal">AstroLabs Dubai</span></div>
            <div class="titleEvent">OPCDE 2017(CALL FOR PAPERS)</div>

            <div class="descEvent">
                Digital Marketing is one of the most powerful skill sets in the toolchest of the modern professional, and like any constantly changing topic, can be hard to navigate. In order to solve this, AstroLabs Academy has built a 1 Week intensive Digital Marketing workshop track...

                <span class="readmoreRightcolumn ">Read more</span>
            </div>

        </div>
        <div class="clearfix"></div>
    </div>
        </div>
<div class="white-wrapper">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
			<h3 class="titleHeader"></h3>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 removeLeftpadding">
			
		</div>
	</div>
</div>
</div>
<?php endwhile; ?>
<?php
endif;
?>
<?php
if($popup!="popup")
{
	get_footer();
}
else
{
    get_template_part( 'templates/popup-footer' );
}
 ?>