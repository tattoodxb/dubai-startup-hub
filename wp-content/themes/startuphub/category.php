<?php 
get_header();
?>
<div class="white-wrapper" id="mainContent">
	<div class="container">

<?php
            $args = array(
                'post_type'=> 'post',
                'post_status' => 'publish',
                'orderby'=> 'publish',
                'order'    => 'DESC',
                'posts_per_page' => 10,
                'paged' => get_query_var( 'paged' )
            );

            if(isset($_GET["tag"]))
              {
                $tag = $_GET["tag"];
              }

              if($tag != "")
              {
                $args['tag'] = $tag;
                echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the keyword <em><b>'.$tag.'</b></em></div>';

              }

              if(isset($_GET["date"]))
              {
                $date = $_GET["date"];
              }

              if($date != "")
              {
                  $date = date('Y-m-d', strtotime($date));
                  $args['meta_query'] = array(
                    array(
                      'key'     => 'sh_news_date',
                      'value'   => $date,
                      'compare' => '=',
                      'type' => 'DATE'
                    ),
                  );
                  echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the events on <em><b>'.$date.'</b></em></div>';
              }

            $wp_query = new WP_Query($args);
            $i=1;
while(have_posts()) : the_post();
	$post_title = get_the_title();
	$post_tags = get_the_tags();
?>
<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
    <div class="questionBox">
        <div class="titleEvent"><a href="<?php echo the_permalink();?>" class="ls-modal"><?php the_title(); ?></a></div>
        <div class="descEvent"><?php the_content(); ?></div>
        <div class="tags">
            <?php
                            $posttags = $post_tags;
                            if ($posttags) {
                                foreach($posttags as $tag) {
                        ?>
                        <span class="tagCopy"><?php echo $tag->name;?></span>
                        <?php
                                }
                            }
                        ?>
        </div>
        <span class="readmoreRightcolumn"><a href="<?php echo the_permalink();?>" class="ls-modal">Read more</a></span>
    </div>
</div>
<?php 
$i++;
            endwhile; 
            ?>
            <?php
            if($i==1)
                {
                    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue"><br>No result found for your search, please refine your search</div>';
                }
            ?>

	</div>
</div>

<?php
get_footer();
?>