<?php
/* Template Name: Search Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<div class="white-wrapper" id="mainContent">
  <div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
			<h3 class="titleHeader">Search</h3></div>
      <?php
      if(isset($_GET['keyword']))
      {
            $keyword = $_GET['keyword'];

            if($keyword != "")
            {
            echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for <em><b>'.$keyword.'</b></em></div>';
            $args = array(
                'post_type'=> array('post','news','events','meetup','page'),
                's'=> $keyword,
                'post_status' => 'publish',
                'orderby'=> 'menu_order',
                'order'    => 'ASC',
                'posts_per_page' => 10,
                'paged' => get_query_var( 'paged' )
            );
            $wp_query = new WP_Query($args);
            $i=1;
            while (have_posts()) : the_post();
                $post_type = get_post_type();
                $post_type_name = ucfirst($post_type);
                $overview_image = $post_type.'-overview-image';
                if($post_type=='page')
                {
                    $class = '';
                }
                else
                {
                    $class = 'ls-modal';    
                }                
            ?>

             <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">               
                <div class="questionBox paddingno" >
                    <div class="imageEvents"><a href="<?php echo the_permalink();?>" class="ls-modal"><?php if (class_exists('MultiPostThumbnails')) :
                            MultiPostThumbnails::the_post_thumbnail(
                                get_post_type(),
                                $overview_image, null, 'post-thumbnail', array('class' => 'img-responsive')
                            );
                        endif; ?></a></div>                 
                    <div class="eventBoxHolder">
                        <div class="titleEventNormal coloringBlue">in</span> <span class="titleEventbold coloringBlue"><?php echo $post_type_name?></span></div>
                        <div class="titleEvent coloringOrange"><?php echo get_the_title();?></div>
                        <div class="descEvent"><?php echo get_the_excerpt();?></div>
                        <a href="<?php echo the_permalink();?>" class="<?php echo $class?>">Read more</a>
                    </div>

                </div>

            </div>
            <?php 
            $i++;
            endwhile; 
                if($i==1)
                {
                    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue"><br>No result found for your search, please refine your search</div>';
                }
            }
            else
            {
                echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have not searched for anything</div>';
            }

    }
?>
  </div>
</div>
<?php get_footer(); ?>