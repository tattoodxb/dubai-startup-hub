<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 pull-right">
    <div class="widget JoinConversation margin-top-big noBgd">
        <div class="titleOrangeBox">
            <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Trending topics
        </div>

        <?php foreach ( dshub_forum_today_main_trends() as $t_arr ) : ?>
        <div class="rowsconversation">
            <div class="rowsconversationTitle">
                <a href="<?php echo get_permalink( $t_arr->topic_id ) ?>" style="color:#fff"><?php bbp_topic_title( $t_arr->topic_id ) ?></a>
            </div>
            <div class="ConversationParticipant hide"><?php             
            bbp_topic_voice_count( $t_arr->topic_id ) ?> Participants</div>
            <!-- <div class="ParticipantOrange pull-right">3 Currently online</div> -->
            <div class="clearfix"></div>
        </div>
        <?php endforeach ?>
        <div class="inpuNewsletterHolder">
            <?php
            $keyword = "";
            if(isset($_GET['keyword']))
                {
                    $keyword = $_GET['keyword'];
                }
            ?>
            <input name="forum_search" id="forum_search" class="form-control NewsletterInput forum_search" placeholder="Search" type="text" value="<?php echo $keyword;?>">
            <div class="submitOk searchConversation noBgd forum_search_btn"><div class="looptosearch"></div></div>
        </div>

        <div class="startConversation <?php echo shub_login_class() ?>">Start your conversation
        <span class="pull-right"><i class="fa fa-chevron-right"></i></span>
        </div>
    </div>
</div>
<div class="clearfix"></div>