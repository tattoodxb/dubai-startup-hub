<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

?>
<div class="mostHelpfulRow">
	<?php
		$reply_author_id = bbp_get_reply_author_id();
	?>
	<div class="col-lg-1 paddingno">
	   <img src="<?php echo shub_user_avatar($reply_author_id); ?>" class="img-circle img-responsive">
	</div>
	<div class="col-lg-11">
       <div class="rowsconversation no-margin margin-left-bit paddingno commentHeader">
           <div class="ConversationParticipant coloringBlue"><?php echo getUserName($reply_author_id);?></div><br>
           <div class="titleEventbold smallCopy"><?php bbp_reply_post_date(); ?><span class="titleEventNormal smallCopy"></div>
       </div>
       <div class="margin-left-bit">
           <div class="descEvent "><?php do_action( 'bbp_theme_before_reply_content' ); ?>

		<?php bbp_reply_content(); ?>

		<?php do_action( 'bbp_theme_after_reply_content' ); ?></div>

           <div class="titleEventbold smallCopy margin-top  pull-left">
              Was this helpful?
              <span class="titleEventNormal smallCopy margin-left">
                <a class="shub-reply-like" data-mode="like" data-reply="<?php bbp_reply_id() ?>" href="#">Yes (<span class="shub-reply-like-count shub-reply-like-count-<?php bbp_reply_id() ?>"><?php echo dshub_forum_reply_like_count( bbp_get_reply_id(), 'like' ) ?></span>)</a> / 
                <a class="shub-reply-like" data-mode="dislike" data-reply="<?php bbp_reply_id() ?>" href="#">No (<span class="shub-reply-dislike-count shub-reply-dislike-count-<?php bbp_reply_id() ?>"><?php echo dshub_forum_reply_like_count( bbp_get_reply_id(), 'dislike' ) ?></span>)</a>
              </span>
            </div>
       </div>
   </div>
   <div class="clearfix"></div>
</div>
