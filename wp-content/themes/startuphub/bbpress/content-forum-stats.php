<div class="col-lg-6 col-md-12 col-sm-12 ">
    <?php if ( bbp_has_forums( array( 'post_parent' => 0 ) ) ) : ?>
    <div class="ForumRowsHolder">
        <div class="widget JoinConversation margin-top-big no-border" id="accordion2">
            <div class="titleOrangeBox">
                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Main forum topics
            </div>
            
            <?php while ( bbp_forums() ) : bbp_the_forum(); ?>
            <div class="rowsconversation">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php bbp_forum_id() ?>">
                    <div class="rowsconversationTitle">
                        <?php bbp_forum_title() ?>
                        <span class="arrowRight"><i class="fa fa-chevron-down"></i></span>
                    </div>
                </a>
                <div class="col-lg-3  ConversationParticipant removeLeftpadding"><?php bbp_forum_post_count(); ?> Discussions</div>
                <?php if (bbp_get_forum_post_count()) : ?>
                <div class="col-lg-6 ConversationParticipant ">Last post <?php bbp_forum_last_active_time() ?></div>
                <div class="ParticipantOrange pull-right">
                    <?php
                    $helpful_cat = dshub_forum_helpul_category(bbp_get_forum_id());
                    echo $helpful_cat;
                    ?> Helpful answers</div>
                <?php endif ?>
                <div class="clearfix"></div>

                <div id="collapse<?php echo bbp_forum_id() ?>" class="panel-collapse collapse col-lg-12 col-md-12 col-sm-12 margin-left ">
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <?php $subforums = bbp_forum_get_subforums( array( 'post_parent' => bbp_get_forum_id() ) ) ?>
                            <!-- <pre><?php print_r($subforums) ?></pre> -->
                            <?php if (sizeof($subforums)) : ?>
                                <?php foreach ($subforums as $child_forum) : ?>
                                <div class="panel-heading active">                 
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse<?php bbp_forum_id() ?>" href="#">
                                        <div class="rowsconversationTitle">
                                            <a href="<?php echo get_permalink($child_forum->ID) ?>" style="color:#FFF"><?php echo apply_filters( 'bbp_get_topic_title', $child_forum->post_title, $child_forum->ID ) ?></a>
                                        </div>
                                    </a>                                                
                                </div>
                                <?php endforeach ?>
                            <?php else: ?>
                    
                            <!-- <div id="collapseOne<?php echo $sub_parent_id;?>" class="panel-collapse collapse margin-left ">
                                <div class="panel-body">
                                    <div class="rowsconversationTitle subInnerdrop">
                                        <a href="<?php the_permalink();?>" style="color:#FFF">Sub forum 1</a>
                                    </div>
                                </div>
                            </div> -->
                     
                            <div class="text-center">Currently there is no sub forum</div>
                            <?php endif ?>
                        </div><!-- end panel -->
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php endwhile; ?>                     
        </div>
    </div>
    <?php endif; ?>
    <a href="<?php echo home_url();?>" class="menuReturn">Return to homepage <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i></a>
</div>