<?php $helpful_users = dshub_forum_helpul_users(); ?>

<?php if ( sizeof( $helpful_users ) ) : ?>
<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 pull-right">
    <div class="widget JoinConversation margin-top-big noBgd">
        <div class="titleOrangeBox">
            <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Most helpful community members
        </div>
        <?php foreach ( $helpful_users as $obj ) : ?>
        <div class="mostHelpfulRow">
            <div class="col-lg-3 paddingno">
                <div class="user_pic" style="background: center url(<?php echo shub_user_avatar($obj->ID); ?>);"></div>
            </div>
            <div class="col-lg-9 rowsconversation no-margin">

                <div class="ConversationParticipant"><?php echo getUserName( $obj->ID ) ?></div><br />
                <div class="ParticipantOrange "><?php echo $obj->replies ?> Times helpful</div>
            </div>
            <div class="clearfix"></div>
        </div>
        <?php endforeach ?>
    </div>
</div>
<?php endif ?>