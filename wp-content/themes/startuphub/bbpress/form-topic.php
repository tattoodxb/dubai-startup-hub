<?php if ( bbp_current_user_can_access_create_topic_form() ) : ?>
<div class="conversationBox" id="conversationBox">
    <div class="rowDiscussion bgdWhite">
        <div class="col-lg-3 ">
            <div style="margin-top:14px; margin-bottom:14px;">
                <div class="leftColDiscussion">
                    <div class="col-lg-6 forumImage">
                        <div class="user_pic" style="background: center url(<?php echo shub_user_avatar(get_current_user_id()); ?>);"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="DiscussionAuthor"><?php echo getUserName( get_current_user_id() ) ?></div>
                    <div class="tagCopy"><?php echo shub_date_format() ?></div>
                    <div class="DiscussionTitle"><?php echo shub_time_format() ?></div>
                </div>
            </div>
        </div>

        <div class="col-lg-8">
            <div style="margin-top:14px; margin-bottom:14px;">
                <div class="rightColDiscussion">
                    <form class="shub-validate" id="new_form_topic" action="<?php the_permalink(); ?>" name="new-forum-topic" method="post">
                        
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

                            <div class="inpuNewsletterHolder midsize form-group">
                                <?php
                                    bbp_dropdown( array(
                                        'show_none' => __( 'Select topic', 'bbpress' ),
                                        'selected'  => bbp_get_form_topic_forum()
                                    ) );
                                ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input name="bbp_topic_title" id="bbp_topic_title" class="form-control" placeholder="Define title " type="text" />
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <textarea class="form-control" name="bbp_topic_content" id="bbp_topic_content" rows="10" placeholder="Type your message.."></textarea>
                            <div class="send shub-form-submit">Post</div>
                        </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="redirect_to" value="<?php echo get_permalink() ?>" />
                        <input type="hidden" name="bbp_topic_status" value="pending" />
                        <?php bbp_topic_form_fields(); ?>
                    </form>
                </div>
            </div>
        </div>
        <div class="btnclosePopup" id="btnclosePopup"></div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<?php endif; ?>