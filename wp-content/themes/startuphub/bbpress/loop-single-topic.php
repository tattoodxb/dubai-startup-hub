<?php

/**
 * Topics Loop - Single
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div id="bbp-topic-<?php bbp_topic_id(); ?>"  class="rowDiscussion">
		<?php
		$author_id = bbp_get_topic_author_id();		
		?>
		<div class="col-lg-3 ">
           <div style="margin-top:14px; margin-bottom:14px;">
               <div class="leftColDiscussion">
                   <div class="col-lg-12 col-md-12 col-sm-3 col-xs-3  forumImage paddingno">
                       <img src="<?php echo shub_user_avatar($author_id); ?>" class="img-circle img-responsive col-lg-6 col-md-12 col-sm-8 col-xs-10">
                   </div>
                   <div class="col-lg-12 col-md-12 col-sm-9 col-xs-9">
                      <div class="DiscussionAuthor"><?php echo getUserName($author_id);?></div>
                       <div class="tagCopy"><?php echo date('d M Y', get_post_time());?></div>
                       <div class="DiscussionTitle"><?php echo date('h:s A', get_post_time());?></div>
                  </div>
                  <div class="clearfix"></div>                   
               </div>

           </div>
       </div>
       <div class="col-lg-9 ">
           <div style="margin-top:14px; margin-bottom:14px;">
           	<div class="tagCopy pull-left">Top: <?php echo bbp_get_forum_title( bbp_get_topic_forum_id() ) ?></div>
           	<div class="tagCopy pull-left"><?php bbp_show_lead_topic() ? bbp_topic_reply_count() : bbp_topic_post_count(); ?> <span class="lightFont">Reactions</span></div>
           	<div class="tagCopy pull-left">
              <?php if (bbp_get_topic_author_id() == bbp_get_current_user_id() && !dshub_forum_topic_has_resolved( bbp_get_topic_id() ) ) : ?>
           		<span class="lightFont">is the issue resolved? </span>
           		<a class="shub-topic-resolve" data-topic="<?php bbp_topic_id() ?>" href="#">Yes</a>
           		<?php else: ?>
           			<?php echo dshub_forum_topic_has_resolved( bbp_get_topic_id() ) ? 'This issue is resolved' : ''; ?>
           		<?php endif ?>
           	</div>
           	<div class="pull-right  tagCopy coloringBlue readmoreComments" id="Continue_reading<?php bbp_topic_id(); ?>" onclick="showChildComment('<?php bbp_topic_id(); ?>')">Continue reading</div>
           	<div class="clearfix"></div>
           	<div class="titleEvent">
           		<?php do_action( 'bbp_theme_before_topic_title' ); ?>
					<?php bbp_topic_title(); ?>
				<?php do_action( 'bbp_theme_after_topic_title' ); ?>
				<div class="rowComments" id="rowComments<?php bbp_topic_id(); ?>" style="font-size:14px;">
                   <div class="descEvent">
                   		<?php do_action( 'bbp_theme_before_forum_description' ); ?>
						<div class="bbp-forum-content"><?php bbp_forum_content(); ?></div>
						<?php do_action( 'bbp_theme_after_forum_description' ); ?>
                   </div>
                    <div class="coloringOrange commentTitle reply-btn <?php echo shub_login_class() ?>" data-id="<?php bbp_topic_id(); ?>">Comment</div>
                    	<?php
						bbp_has_replies(array('post_parent' => bbp_get_topic_id(),'post__not_in' => array(bbp_get_topic_id())));
						while ( bbp_replies() ) : bbp_the_reply(); ?>
							<?php bbp_get_template_part( 'loop', 'single-reply' ); ?>
						<?php endwhile; ?>					
					<div id="reply_box<?php bbp_topic_id(); ?>" style="display:none">
	                	<?php bbp_get_template_part( 'form', 'reply' ); ?>
	             	</div>
				</div>
           	</div>
			<?php if ( bbp_is_user_home() ) : ?>
				<?php if ( bbp_is_favorites() ) : ?>
					<span class="bbp-row-actions">
						<?php do_action( 'bbp_theme_before_topic_favorites_action' ); ?>
						<?php bbp_topic_favorite_link( array( 'before' => '', 'favorite' => '+', 'favorited' => '&times;' ) ); ?>
						<?php do_action( 'bbp_theme_after_topic_favorites_action' ); ?>
					</span>
				<?php elseif ( bbp_is_subscriptions() ) : ?>
					<span class="bbp-row-actions">
						<?php do_action( 'bbp_theme_before_topic_subscription_action' ); ?>
						<?php bbp_topic_subscription_link( array( 'before' => '', 'subscribe' => '+', 'unsubscribe' => '&times;' ) ); ?>
						<?php do_action( 'bbp_theme_after_topic_subscription_action' ); ?>
					</span>
				<?php endif; ?>
			<?php endif; ?>
		<?php bbp_topic_pagination(); ?>
		<?php bbp_topic_row_actions(); ?>
		</div>
       </div>
       <div class="clearfix"></div>
</div><!-- #bbp-topic-<?php bbp_topic_id(); ?> -->