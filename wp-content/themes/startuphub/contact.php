<?php
/* Template Name: Contact Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<div class="white-wrapper" id="mainContent">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
                  <h3 class="titleHeader">Find us here</h3></div>
                  <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
                      <div class="widget">                      
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 removeLeftpadding" >
                          <div class="latestupdatecolumn FindUsLeft">
                              <div class="latestupdateImageHolder">
                                  <img src="<?php echo $template_url;?>/images/contact-us-icon.jpg" class="img-responsive contactIcon">
                              </div>
                              <div class="col-lg-12 latestupdateContentBox">

                                  <div class="latestupdateDate">23 March 2017 </div>
                                  <div class="latestupdateTitle">Dubai Startup Hup</div>
                                  <div class="latestupdateDesc">
                                      Toll free: 800 CHAMBER<br />
                                      (800 242 6236)<br />
                                      Phone: (+971) 4 228 0000 <br />
                                      Fax: (+971) 4 202 8888 <br />
                                      <br /><br />
                                      <b>Email us</b><br />
                                      <a href="mailto:startuphub@dubaichamber.com">startuphub@dubaichamber.com</a><br />
                                      <br />
                                      <br />
                                      <!--<b>Data and economic research</b><br />
                                      <a href="mailto:info.dataresearch@dubaichamber.com">info.dataresearch@dubaichamber.com</a><br />

                                      <b>All other enquiries</b><br />
                                      <a href="mailto:customercare@dubaichamber.com">customercare@dubaichamber.com</a><br />
                                      <br />
                                      <br />
                                      Our working hours are from 08:00 am to 04:00 pm Sunday to Thursday, and our Commercial Services Department provide its service on Saturday 08:00 am to 01:00 pm.-->



                                  </div>



                              </div>
                              <div class="clearfix"></div>


                          </div>




                      </div>

                          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 removerightpadding">

                              <div class="mapBox">
                                  <div id="map"></div>

                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                      <div class="latestupdateDesc">
                                          Toll free: 800 CHAMBER<br>
                                          (800 242 6236)<br>
                                          Phone: (+971) 4 228 0000 <br>
                                          Fax: (+971) 4 202 8888 <br>
                                          <br><br>

                                          <b>Contact details</b><br>Baniyas Road , Dubai
<br /><br />


                                      </div>

                                  </div>

                                  <div class="clearfix"></div>

                                  </div>

                              </div>





 
                      </div><!-- end widget -->
                  </div><!-- end col-lg-6 -->

                  <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 orangeHolder">
                      <div class="widget OrangeColumn">
                        <div class="titleOrangeBox"><i class="fa fa-chevron-right nextBlue" aria-hidden="true"></i>Or meet us here
</div>
               <div class="rowOrangeBox firstrow no-border">
                   <div class="rowOrangeDate signupTitle">Sign up for our newsletter </div>
                
                 <div class="inpuNewsletterHolder"><input name="emailNewsletter" id="EmailNewsletter" class="form-control NewsletterInput" placeholder="Email address..." type="email">

<div class="submitOk">OK</div>
</div>
               </div>
                      
                     
                      </div><!-- end widget -->
                  </div><!-- end col-lg-6 -->


           
              </div>

    </div>
<?php get_footer(); ?>
<!-- Google Map -->
<script src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDRv6uf1hCqC4tXu1qXqWtIFEKIVqVBPdc"></script>    <script type="text/javascript">
	(function($) {
	  "use strict";
		var locations = [
		['<div class="infobox"><h3 class="title"><a href="">OUR  OFFICE</a></h3></div></div></div>', 25.2597508, 55.3151446, 2]
		];

		var map = new google.maps.Map(document.getElementById('map'), {
		  zoom: 15,
			scrollwheel: false,
			navigationControl: true,
			mapTypeControl: false,
			scaleControl: false,
			draggable: true,
			styles: [{ "stylers": [{ "hue": "#0035E6" },
                {gamma: 0.50} ] } ],
			center: new google.maps.LatLng(25.2601422, 55.3153859),
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		var infowindow = new google.maps.InfoWindow();

		var marker, i;

		for (i = 0; i < locations.length; i++) {

			marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map ,
			icon: 'images/marker.png'
			});


		  google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
			  infowindow.setContent(locations[i][0]);
			  infowindow.open(map, marker);
			}
		  })(marker, i));
		}
	})(jQuery);
    </script>