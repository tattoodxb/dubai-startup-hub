<?php
/* Template Name: Sitemap Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<?php if(have_posts()) : ?>
<?php 
while(have_posts()) : the_post();
	$post_title = get_the_title();
?>
<?php endwhile; ?>
<?php
endif;
?>
<article id="section-sitemap" class="section-sitemap section-feeds" role="region">
	<div class="bg-wr" style="background-image:url();">
		<div class="bg-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="events-articles-wr">

						<div class="dummy-article-wr fx" data-animate="fadeInUp">
							<header>
								<h1 class="main-title"><?php echo $post_title ?></h1>
							</header>
				
							<?php get_template_part( 'templates/sitemap-content' ); ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>