<?php 
$template_url = get_template_directory_uri();
$popup = "";
if(isset($_GET["page"]))
{
	$popup = $_GET["page"];
}
if($popup!="popup")
{
	get_header(); 
}
else
{
    get_template_part( 'templates/popup-header' );
}
?>
<?php if(have_posts()) : ?>
<?php 
while(have_posts()) : the_post();
    $post_id = get_the_ID();
	$post_title = get_the_title();
    $post_content = apply_filters( 'the_content', get_the_content() );
    $post_link = get_the_permalink();
    $sh_news_location = get_post_meta($post->ID, 'sh_news_location', $single = true);
    $sh_news_date = get_post_meta($post->ID, 'sh_news_date', true);
    $sh_news_display_date = ($sh_news_date) ? date('F d, Y', strtotime($sh_news_date)) : '';
    $post_tags = get_the_tags();
    if (class_exists('MultiPostThumbnails')) :
        $overview_img = MultiPostThumbnails::get_post_thumbnail_url(
            get_post_type(),
            'news-overview-image' , null, 'post-thumbnail'
        );
    endif;
endwhile;
endif;
?>
<main class="wrapper" role="main">
<h1 class="text-indent"><?php echo $post_title;?></h1>
            <article id="section-events" class="section-events section-feeds single-event" role="region">                
                <div class="bg-wr" style="background-image:url();">
                    <div class="bg-overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="events-articles-wr">                                
                                    <div class="dummy-article-wr " >
                                        <div class="row">                                        
                                            <div class="col-sm-5">
                                                <div class="article-action-wr">
                                                    <figure>
                                                        <img src="<?php echo $overview_img;?>" class="img-responsive">
                                                        <figcaption class="text-indent"><?php echo $post_title;?></figcaption>
                                                    </figure>
                                                    <div class="article-action-btn-wr">
                                                        <h5 class="text-primary action-label">Share this article</h5>
                                                        <aside class="social-article-share-wr">
                                                            <h4 class="text-indent">Social Media Share</h4>
                                                            <ul>
                                                                <li>  
                                                                    <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=<?php echo urlencode($post_link);?>&via=DubaiStartupHub" title="Follow us on Twitter" class="social_link" target="_blank">
                                                                        <i class="fa fa-twitter"></i>
                                                                        <span class="sr-only">Follow us on Twitter</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="http://www.facebook.com/sharer.php?u=<?php echo $post_link;?>" title="Connect with our Facebook" class="social_link">
                                                                        <i class="fa fa-facebook"></i>
                                                                        <span class="sr-only">Follow us on Facebook</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_link;?>" title="Connect with our LinkedIn" class="social_link">
                                                                        <i class="fa fa-linkedin"></i>
                                                                        <span class="sr-only">Follow us on LinkedIn</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://plus.google.com/share?url=<?php echo $post_link;?>" title="Follow us on Instagram" class="social_link">
                                                                        <i class="fa fa-google-plus"></i>
                                                                        <span class="sr-only">Follow us on Google+</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                                
                                                        </aside>
                                                        <nav class="article-main-action-wr">
                                                            <h5 class="text-indent">Article Action</h3>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <a href="<?php echo $post_link?>#print" target="_blank" class="text-link-light accent-icon link-block" aria-label="Print Article">
                                                                        <i class="fa fa-print"></i>
                                                                        <span class="btn-text">Print Article</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </nav>
                                                        <div class="return-wr">
                                                            <a href="<?php echo home_url('articles') . '/' . $cat_slug ; ?>" class="btn btn-primary" aria-label="return" >
                                                                <span class="btn-text xspaddingright"> Return to News</span> 
                                                                <span class=" icon-carets icon-carets-left"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-7 article-content-wr- scrollbar-inner">
                                                <div class="main-article-wr">
                                                    <div class="dummy-header">
                                                        <div class="event-info-wr">
                                                            <time datetime="2017-09-29 01:00"><?php echo $sh_news_display_date;?></time>
                                                            <span class="event-separator">//</span>
                                                            <address><?php echo $sh_news_location;?></address>
                                                        </div>
                                                        <p class="dummy-label"><?php echo $post_title;?></p>
                                                    </div>
                                                    <div class="single-event-content-wr">
                                                        <?php echo $post_content;?>
                                                    </div>
                                                    <footer class="article-tags-wr" itemscope itemtype="http://schema.org/keywords">
                                                        <h6 class="tags-label">Tags:</h6>
                                                        <ul class="article-tags">
                                                            <?php
                                                                $posttags = $post_tags;
                                                                if ($posttags) {
                                                                    foreach($posttags as $tag) {
                                                            ?>
                                                            <li><?php echo $tag->name;?></li>
                                                            <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </ul>
                                                    </footer>
                                                    <aside class="primary-wr">
                                                        <h3 class="aside-title">Related News</h3>
                                                        <?php
                                                $args = array(
                                                    'post_type'=> 'news',
                                                    'post_status' => 'publish',
                                                    'orderby'=> 'menu_order',
                                                    'order'    => 'ASC',
                                                    'posts_per_page' => 1,
                                                    'post__not_in' => array($post_id),
                                                );
                                                $wp_query = new WP_Query($args);
                                                while ($wp_query->have_posts()) : $wp_query->the_post();
                                                    $sh_news_location = get_post_meta(get_the_ID(), 'sh_news_location', $single = true);
                                                    $sh_news_display_date = get_post_meta(get_the_ID(), 'sh_news_display_date', $single = true);
                                                ?>
                                                <article>
                                                            <header>
                                                                <div class="event-info-wr">
                                                                    <time datetime="2017-09-29 01:00"><?php echo $sh_news_display_date;?></time>
                                                                    <span class="event-separator">//</span>
                                                                    <address><?php echo $sh_news_location;?></address>
                                                                </div>
                                                                <h3><?php echo get_the_title();?></h3>
                                                            </header>
                                                            <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                                                            <p>
                                                                <a href="<?php echo the_permalink();?>" title="Read more" data-rel="modal:open" class="btn btn-outline-light btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
                                                            </p>
                                                        </article>
                                                <?php 
                                                    endwhile;
                                                ?>
                                                    </aside>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </article>
</main>
<?php
if($popup!="popup")
{
	get_footer();
}
else
{
    get_template_part( 'templates/popup-footer' );
}
 ?>