<?php
/* Template Name: Events Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<section id="section-events" class="section-updates section-feeds section-feeds-cols section-feeds-cols-3" role="region">
        <div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
              <h1 class="section-title fx main-title" data-animate="fadeInUp"><?php the_title(); ?></h1></div>
              <div class="same-height-all">              
                <div class="col-sm-12">
                  <div class="updates-articles-wr">                    
                    <?php
                    $current_date = date('Y-m-d H:i:s');
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $args = array(
              'post_type'=> 'events',
              'post_status' => 'publish',
              'meta_key' => 'sh_events_date',
              'meta_query' => array(
                  array(
                      'key' => 'sh_events_date',
                      'value' => $current_date,
                      'compare' => '>=',
                      'type' => 'DATE'
                  )
              ),
              'orderby'=> 'meta_value_num',
              'order'    => 'ASC',
              'posts_per_page' => 6,
              'paged' => $paged
          );

          if(isset($_GET["tag"]))
          {
            $tag = $_GET["tag"];
          }

          if($tag != "")
          {
            $args['tag'] = $tag;
            $tag = str_replace('-', ' ', $tag);
            $tag = str_replace(',', ', ', $tag);
            echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the keyword <em><b style="text-transform:capitalize">'.$tag.'</b></em><br><br></div>';

          }

          if(isset($_GET["date"]))
          {
            $date = $_GET["date"];
          }

          if($date != "")
          {
              $date = date('Y-m-d', strtotime($date));
              $args['meta_query'] = array(
                array(
                  'key'     => 'sh_events_date',
                  'value'   => $date,
                  'compare' => '=',
                  'type' => 'DATE'
                ),
              );
              echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the events on <em><b>'.$date.'</b></em><br><br></div>';
          }
                    ?>
                    <div class="row same-height-all">

                      <?php          
          $wp_query = new WP_Query($args);
          $i=1;
          while ($wp_query->have_posts()) : $wp_query->the_post();
          $sh_events_location = get_post_meta($post->ID, 'sh_events_location', $single = true);
          $sh_events_display_date = get_post_meta($post->ID, 'sh_events_display_date', $single = true);
          $start_date = get_post_meta($post->ID, 'sh_events_date', $single = true);
                        $end_date = get_post_meta($post->ID, 'sh_events_date', $single = true);

                        $start_date = date('F d, Y', strtotime($start_date));
                        $end_date = date('F d, Y', strtotime($end_date));


                      if($sh_events_display_date=="")
                      {
                          if($start_date == $end_date)
                          {
                              $sh_events_display_date = $start_date;  
                          }
                          else
                          {
                              $sh_events_display_date = $start_date . '-' .$end_date;
                          }
                          
                      }
          $post_tags = get_the_tags();
          ?>
                    
                      <div class="col-sm-4 fx " data-animate="fadeInUp">
                        <article>
                          <div class="row">
                            <div class="col-sm-12">
                              <figure>
                                <?php 
                                 the_post_thumbnail('thumbnail', ['class' => 'img-responsive width100p', 'title' => get_the_title()]);
                                 ?>
                                <figcaption class="text-indent"><?php echo get_the_title();?></figcaption>
                              </figure>
                            </div>
                            <div class="col-sm-12">
                              <div class="updates-info-wr">
                                <header>
                                  <div class="update-info-wr">
                                    <time datetime="<?php echo $start_date;?>"><?php echo $sh_events_display_date?></time>
                                    <span class="update-separator">//</span>
                                    <address><?php echo $sh_events_location;?></address>
                                  </div>
                                  <h3 class="same-height-text"><?php echo get_the_title();?></h3>
                                </header>
                                <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                                <p>
                                  <a href="<?php echo the_permalink();?>" title="Read more" data-rel="modal:open" class="btn btn-outline-primary-full btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
                                </p>
                                <footer class="tags-wr">
                                  <ul>  
									  <?php
                                        $posttags = $post_tags;
                                        if ($posttags) {
											$ctr = 0;
                                            foreach($posttags as $tag) {
                                    			if($ctr <= 10){
									
											    ?>
												<li><?php echo $tag->name;?></li>
												<?php
													
												}
												$ctr++;
                                            }
                                        }
                                    ?>
                                  </ul>
                                </footer>
                              </div>
                            </div>
                          </div>
                        </article>
                      </div>

                      <?php
            $i++;
            endwhile; 
            ?>
            <?php
            if($i==1)
                {
                    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue"><br>No result found for your search, please refine your search</div>';
                }
            ?>
                      
                    </div>
                    
                    <div class="row">
                      <div class="col-sm-4 col-centered fx" data-animate="fadeInUp">
                        <div class="text-center"><?php 
                                  $pag_args = array(
                                      'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                      'total'        => $wp_query->max_num_pages,
                                      'current'      => max( 1, get_query_var( 'paged' ) ),
                                      'format'       => '?paged=%#%',
                                      'show_all'     => false,
                                      'type'         => 'list',
                                      'end_size'     => 2,
                                      'mid_size'     => 1,
                                      'prev_next'    => true,
                                      'prev_text'    => sprintf( '<i></i> %1$s', __( 'Prev', 'text-domain' ) ),
                                      'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
                                      'add_args'     => false,
                                      'add_fragment' => '',
                                      'before_page_number' => '',
                                      'after_page_number'  => ''
                                  );
                                  $return = paginate_links( $pag_args );
                                  echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return );
                              ?>
                          </div>
                      </div>
                    </div>
                    
                  </div>
                </div>
                
                
            </div>
              
              </div>
          </div>
        </div>
      </section>
<?php get_footer(); ?>