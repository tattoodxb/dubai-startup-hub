<?php

/* Template Name: Education Template */
$template_url = get_template_directory_uri();
get_header();
?>
<section id="section-events" class="section-events section-feeds section-feeds-cols single-event market-access education" role="region">
	<?php the_content(); ?>
	<div class="col-sm-12 content">
  <!--
<h2 style="text-align: center;">BUYER’S <strong>PROFILE</strong></h2>
<h3 style="text-align: center;">PURCHASE OPPORTUNITIES</h3>
-->
<div class="updates-articles-wr member-details-wr startup-collapsable">
<div class="row same-height-all mdmargintop">

<?php
	$args = array(
	    'post_type'=> 'page',
	    'post_parent' => get_the_ID(),
	    'post_status' => 'publish',
	    'orderby'=> 'menu_order',
	    'order'    => 'ASC');
	$wp_query = new WP_Query($args);
	$x = 1;
	while (have_posts()) : the_post();
		if (class_exists('MultiPostThumbnails')) :
        $page_image_src = MultiPostThumbnails::get_post_thumbnail_url(
            get_post_type(),
            'page-overview-image' , null, 'full'
        );
        endif;
		$sub_post_title = get_the_title();
		$sub_post_excerpt = get_post_meta($post->ID, 'sh_overview_text', $single = true);
		if($post->ID == 883 || $post->ID == 4057)
		{
			$sub_post_title = $sub_post_excerpt;
		}
		if($x==1)
		{
			$cls = "col-md-offset-1";
			//$cls = "";
		}
		else
		{
			$cls = "";
		}
	 
	?>


<div class="col-sm-2 fx animated fadeInUp education <?php echo $cls;?>" data-animate="fadeInUp" style="margin-bottom: 20px">
<article>
<div class="row">
<div class="col-sm-12">
<figure><a href="<?php the_permalink(); ?>"><img class="img-responsive" src="<?php echo $page_image_src;?>" sizes="(max-width: 560px) 100vw, 560px" srcset="<?php echo $page_image_src;?> 560w, <?php echo $page_image_src;?> 300w" alt="" width="560" height="364"></a><figcaption class="text-indent"><?php echo $sub_post_title;?></figcaption></figure>
</div>
<div class="col-sm-12">
<div class="updates-info-wr text-center">
	<div class="text"><a href="<?php the_permalink(); ?>"><?php echo $sub_post_title;?></a></div>
	<div class="readmore"><a class="btn btn-outline-primary-full btn-sm" title="Read more" href="<?php the_permalink(); ?>" data-rel="modal:open">Read More <i class="fa fa-caret-right fa-fw"></i></a></div>
</div>
</div>
</div>
</article>
</div>
<?php
$x++;
endwhile;
?>


</div>
</div>
</div>
</section>
<?php get_footer(); ?>