<?php

/* Template Name: Co Founder Inner */
$template_url = get_template_directory_uri();
get_header();

?>

<section id="section-events" class="section-events section-feeds section-feeds-cols single-event co-founder" role="region">
	<?php the_content(); ?>
</section>
<?php get_footer(); ?>