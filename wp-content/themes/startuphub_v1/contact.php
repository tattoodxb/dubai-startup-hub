<?php
/* Template Name: Contact Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<section id="section-upcoming" class="section-feeds section-contactus" role="region">
	<div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
		<div class="bg-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="section-title fx main-title" data-animate="fadeInUp"><?php the_title() ?></h1>
				</div>

				<div class="col-sm-4 fx col-contact-details" data-animate="fadeInUp">
					<article class="border-primary">
						<div class="row">
							<div class="col-sm-12">
								<figure>
									<img class="img-responsive" src="../images/contact-feat-thumb.jpg" alt="Startuphub office building" />
									<figcaption class="text-indent">Startuphub office building</figcaption>
								</figure>
							</div>
							<div class="col-sm-12">
								<div class="updates-info-wr">
									<header>
										<h3 class="sub-title">Contact Details</h3>
										<h2 class="title text-primary">Dubai Startup Hub</h2>
									</header>

									<ul class="list-unstyled">
										<li>
											<span class="list-label">
												Toll free:
											</span>
											<span class="list-val">
												800 CHAMBER
											</span>
											<span class="list-val">
												(800 242 6236)
											</span>
										</li>
										<li>
											<span class="list-label">
												Phone:
											</span>
											<span class="list-val">
												(+971) 4 228 0000 
											</span>
										</li>
										<li>
											<span class="list-label">
												Fax:
											</span>
											<span class="list-val">
												(+971) 4 202 8888
											</span>
										</li>
									</ul>

									<address>
									<h5 class="child-title">Email Us</h5>
									<a href="mailto:startuphub@dubaichamber.com">startuphub@dubaichamber.com</a>  

									</address>

								</div>
							</div>
						</div>
					</article>
				</div>

				<div class="col-sm-8 col-contact-map-details fx" data-animate="fadeInUp">
					<div class="border-primary">
						<div class="map-wr">
							<img src="../images/map.jpg" class="map img-responsive" alt="Duabi Startup Hub Map Location">
							<div class="view-gmap-wr">
								<div class="vertical-full-wr">
									<div class="vertical-middle">
										<a target="_blank" href="https://www.google.ae/maps/place/Dubai+Chamber+Of+Commerce+%26+Industry/@25.2598175,55.3155055,18.5z/data=!4m13!1m7!3m6!1s0x3e5f5d270e8c04d1:0x81527d2f36afffeb!2sBaniyas+Rd+-+Dubai!3b1!8m2!3d25.2601986!4d55.3164721!3m4!1s0x3e5f5ccd43a579ef:0x5cfdfe960734b570!8m2!3d25.2601422!4d55.3153856?hl=en" class="btn btn-primary" title="Open in Google Map">
											View in Google Map
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="space-wr">
							<h3 class="title text-primary">Dubai Chamber of Commerce & Industry</h3>
							<p>
								Baniyas Road, Deira<bR>
								P.O. Box 1457 – Dubai, U.A.E<br>
								Makani: 3035894820
							</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
