
<?php $helpful_users = dshub_forum_helpul_users(); ?>

<?php if ( sizeof( $helpful_users ) ) : ?>
	<div class="col-lg-3 col-md-4 col-sm-3">
		<section class="outline-primary-box outline-box vertical-wr user-helpful" style="height: 133px;">
			<div class="vertical-middle">
				<h3 class="tool-title">Most helpful community members</h3>
				
				<table class="user-list">
					<?php foreach ( $helpful_users as $obj ) : ?>
					<?php $user_img = shub_user_avatar($obj->ID); ?>
					<tr>
						<td>
							<figure style="background-image:url('<?php echo $user_img ?>')" width="50" height="50" class="fig-bg">
							  <img src="<?php echo $user_img ?>" alt="<?php getUserName( $obj->ID ); ?>">
							</figure>
						</td>
						
						<td><?php  getUserName( $obj->ID ); ?></td>
						<td><?php echo $obj->replies ?> Times helpful</td>
					</tr>
					<?php endforeach ?>
				</table>
				<div class="clearfix"></div>
			</div>
		</section>
	</div>
<?php endif ?>