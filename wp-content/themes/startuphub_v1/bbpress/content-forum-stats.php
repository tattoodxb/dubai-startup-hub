<div class="col-lg-6 col-md-5 col-sm-6">
	<?php if ( bbp_has_forums( array( 'post_parent' => 0 ) ) ) : ?>
	<section class="outline-secondary-box outline-box forum-list">
		<h3 class="tool-title">Forum topics</h3>
		<div class="panel-group" id="accordion">
			<?php while ( bbp_forums() ) : bbp_the_forum(); ?>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a href="<?php bbp_topic_permalink(); ?>" class="collapsed">
							<?php bbp_forum_title() ?>
							<small class="desc">
							
							<?php echo bbp_get_forum_content(); ?>
							</small>
							<!-- <span class="icon-caret icon-caret-down"></span> -->
						</a>
					</h4>
				 </div>
				<?php /*
				<div id="collapse<?php bbp_forum_id() ?>" class="panel-collapse collapse">
					<ul class="list-group">
						<?php $subforums = bbp_forum_get_subforums( array( 'post_parent' => bbp_get_forum_id() ) ) ?>
							<!-- <pre><?php //print_r($subforums) ?></pre> -->
						   
							<?php if (sizeof($subforums)) : ?>
								<?php foreach ($subforums as $child_forum) : ?>
									
									<li class="list-group-item">
										<h4 class="post-list-title">
											<a class="scroll-to-trigger" href="<?php echo get_permalink($child_forum->ID).'#section-forum'; ?>">
												<span class="row block">
													<span class="col-sm-6 col-xs-12">
														<?php echo apply_filters( 'bbp_get_topic_title', $child_forum->post_title, $child_forum->ID ) ?>
														<small class="desc"><?php echo $child_forum->post_excerpt ?></small>
													</span>
													<span class="col-sm-3 col-xs-6 post-time post-info">
														<small>
															Last post <?php bbp_forum_last_active_time() ?>
														</small>
													</span>
													<span class="col-sm-3 col-xs-6 post-replay post-info">
														<small>
															Replies<br>
															<?php echo bbp_topic_reply_count()?>
														</small>
													</span>
												</span>
											</a>
										</h4>
									</li>	
							 	<?php endforeach ?>
                            <?php else: ?>
								<li class="list-group-item">
									<div class="text-center">Currently there is no sub forum</div>
								</li>
							<?php endif ?>
					</ul>
				</div>
				*/ ?>
			</div>
			<?php endwhile; ?>
		</div>	



	</section>
    <?php endif; ?>
</div>