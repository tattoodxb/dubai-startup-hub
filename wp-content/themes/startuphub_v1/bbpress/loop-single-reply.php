<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

?>
<div class="media">
	<?php
		$reply_author_id = bbp_get_reply_author_id();
	
        $reply_author_img    = shub_user_avatar($reply_author_id); 
	?>
	
	<div class="media-left">
		<figure style="background-image:url('<?php echo $reply_author_img ?>')" class="fig-bg">
		  <img class="media-object" src="<?php echo $reply_author_img ?>" alt="<?php getUserName($reply_author_id); ?>">
		</figure>
	</div>
	<div class="media-body">
	<div class="commentor-info">
		<h4 class="media-heading"><?php getUserName($reply_author_id); ?></h4>
		<p><small><i>Posted on <?php bbp_reply_post_date(); ?></i></small></p>
	</div>

	<?php
	/************************************************
					   REPLY CONTENT
	*************************************************/
	?>
	<div class="comment-content-wr ellip">
	<?php do_action( 'bbp_theme_before_reply_content' ); ?>

	<?php bbp_reply_content(); ?>

	<?php do_action( 'bbp_theme_after_reply_content' ); ?></div>
	</div>

	<div class="comment-feedback-wr">
		<span class="help-label">Was this helpful? </span>
		<a class="shub-reply-like yes" data-mode="like" data-reply="<?php bbp_reply_id() ?>" href="#">
			Yes (<span class="shub-reply-like-count shub-reply-like-count-<?php bbp_reply_id() ?>">
					<?php echo dshub_forum_reply_like_count( bbp_get_reply_id(), 'like' ) ?>
				</span>)
		</a>
		<span class="sep">/</span>
		<a class="shub-reply-like no" data-mode="dislike" data-reply="<?php bbp_reply_id() ?>" href="#">
			No (<span class="shub-reply-dislike-count shub-reply-dislike-count-<?php bbp_reply_id() ?>">
				<?php echo dshub_forum_reply_like_count( bbp_get_reply_id(), 'dislike' ) ?>
				</span>)
		</a>
	</div>
</div>
