<div class="col-sm-6 col-md-3">
	<section class="outline-primary-box outline-box vertical-wr" style="height: 133px;">
		<div class="vertical-middle">
			<h3 class="tool-title">Trending topics</h3>
			<?php foreach ( dshub_forum_today_main_trends() as $t_arr ) : ?>
			<ul class="trend-topics">
				<li class="">
					<a href="<?php echo get_permalink( $t_arr->topic_id ) ?>" style="color:#fff"><?php bbp_topic_title( $t_arr->topic_id ) ?></a>
				</li>
				
				<div class="clearfix"></div>
			</ul>
			<?php endforeach ?>
			
			<form class="nopaddingall mdmargintop newsletter-form" role="Subscribe" novalidate="novalidate">

				<div class="form-group">
					<div class="input-group">
						<label class="sr-only" for="search-core">Enter search</label>
						<?php
						$keyword = "";
						if(isset($_GET['keyword']))
							{
								$keyword = $_GET['keyword'];
							}
						?>
						<input id="forum_search" class="form-control forum_search" placeholder="Enter search" id="search-core" name="forum_search" type="text" value="<?php echo $keyword;?>">
				
						<div class="input-group-btn">
							<button class="btn btn-outline-primary submitOk searchConversation noBgd forum_search_btn" type="submit" aria-label="toggle search"><i class="fa fa-search"></i></button>
							<div class="looptosearch"></div>
						</div>
					</div>
				</div>



			</form>
			<h3 class="tool-title">Start a Conversation</h3>
			<div class="form-group nomarginbottom">
				<a class="btn btn-primary btn-block new-convo startConversation <?php echo shub_login_class() ?>" href="#new-convo" >
					Start your conversation 
					<i class="fa fa-caret-right fa-fw"></i>
				</a>
			</div>
		</div>
	</section>
</div>