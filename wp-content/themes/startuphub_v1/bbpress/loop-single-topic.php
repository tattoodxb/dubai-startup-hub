<?php

/**
 * Topics Loop - Single
 *
 * @package bbPress
 * @subpackage Theme
 */

?>
<!-- start row-->	
<?php
	$author_id  = bbp_get_topic_author_id();
	$u_topic_id = wp_generate_password(10, false, false); // generate random string for unique id
    $target     = '#'. strtolower(str_replace(" ","", bbp_get_forum_title( bbp_get_topic_forum_id() )));
?>
<div id="bbp-topic-<?php echo $u_topic_id ?>" data-target="<?php echo $target?>" class="media fx" data-animate="fadeInUp">
	
	<div class="row nomarginall">
		<div class="col-sm-2">
			<div class="media-left media-main">
				<div class="media-inner-wr">
					<?php $author_img    = shub_user_avatar($author_id);  ?>
					<figure style="background-image:url('<?php echo $author_img ?>')" class="fig-bg">
					  <img class="media-object" src="<?php echo $author_img ?>" alt="<?php getUserName($author_id); ?>">
					</figure>
					
					<h4 class="media-heading">
						<span><?php getUserName($author_id); ?></span>
					</h4>
					<p>
						<small>Posted on <?php echo date('d M Y', get_post_time());?></small>
						<small><?php echo date('h:s A', get_post_time());?></small>
					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-10 forum-post-content">
			<div class="media-body">
				<div class="post-action-wr">
					<div class="row">
						<div class="col-sm-6">
							<span class="category">
								Category: <span class="cat-val"><?php echo bbp_get_forum_title( bbp_get_topic_forum_id() ) ?></span>
							</span>
							<span class="replies">
								Replies: <span class="rep-val"><?php bbp_topic_reply_count(); ?></span>
							</span>
						</div>
						<div class="col-sm-6">
							<?php 
								$readmore_hdn = "hidden";
								if(bbp_get_topic_reply_count() > 0)
								{
									$readmore_hdn = "";
								}
							
							?>
							<button type="button" class="<?php echo $readmore_hdn; ?> btn btn-xs btn-primary-full pull-right forum-more" data-toggle="collapse" data-parent="#bbp-topic-<?php echo $u_topic_id ?>" data-target="#form-post-<?php echo $u_topic_id ?>">
								<span class="txt-read">Read More </span>
								<span class="circle">
									<span class="horizontal"></span>
									<span class="vertical"></span>
								</span>
							</button>
							
						</div>
					</div>
				</div>
				
				<?php do_action( 'bbp_theme_before_topic_title' ); ?>
				<?php
				/************************************************
							      FORUM TITLE
				*************************************************/
				?>
				<h3 class="forum-post-title"><?php bbp_topic_title(); ?></h3>
				<?php do_action( 'bbp_theme_after_topic_title' ); ?>
				<?php
				/************************************************
							     FORUM CONTENT
				*************************************************/
				?>
				<?php do_action( 'bbp_theme_before_forum_description' ); ?>
				<div class="ellipsis">
					<p><?php bbp_forum_content(bbp_get_topic_id()); ?></p>	
				</div>
 				<?php do_action( 'bbp_theme_after_forum_description' ); ?>
				
				<?php
				/************************************************
								ADD COMMENT
				*************************************************/
				?>
				
				<div class="new-comment">
					<div class="row">
						<div class="col-xs-12">

							<button type="button" class="btn btn-xs btn-primary-full pull-right btn-add-comment">
							Add Comment
							<span class="circle">
								<span class="horizontal"></span>
								<span class="vertical"></span>
							</span>
						</button>


						</div>
					</div>
				</div>
				
				<div class="first-reply-wr">
					<?php
						$bbp_reply_first_id = 0;
						bbp_has_replies(array('post_parent' => bbp_get_topic_id(), 'posts_per_page' => '1','post__not_in' => array(bbp_get_topic_id())));
							while ( bbp_replies() ) : bbp_the_reply(); 
					 		$bbp_reply_first_id = bbp_get_reply_id();
							bbp_get_template_part( 'loop', 'single-reply' ); 
						endwhile; 
					?>
				</div>
				
				<div id="form-post-<?php echo $u_topic_id ?>" class="collapse collapse-comments">
					
					<?php
					/************************************************
								       REPLIES
					*************************************************/
					?>
	
					<?php
						if($bbp_reply_first_id):
							bbp_has_replies(array('post_parent' => bbp_get_topic_id(),'post__not_in' => array(bbp_get_topic_id(),$bbp_reply_first_id )));
								while ( bbp_replies() ) : bbp_the_reply(); 
								bbp_get_template_part( 'loop', 'single-reply' ); 
							endwhile; 
					 	endif;
					?>	
						
					<div class="add-comment-textarea-wr">
						<?php bbp_get_template_part( 'form', 'reply' ); ?>
					</div>

				</div>

			</div>

		</div>
	</div>
</div>
<!-- end row-->
												