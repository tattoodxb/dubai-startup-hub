
<?php if ( bbp_current_user_can_access_create_topic_form() ) : ?>
<aside id="conversationBox" class="modal modal-convo">
	<div class="row">
		<div class="col-sm-2">
			<div class="media-left media-main">
				<div class="media-inner-wr">
					<img class="media-object" src="<?php echo shub_user_avatar(get_current_user_id()); ?>">
					<h4 class="media-heading">
						<span><?php echo getUserName( get_current_user_id() ) ?></span>
					</h4>
					<p>
						<small>Posted on <?php echo shub_date_format() ?></small>
						<small><?php echo shub_time_format() ?></small>
					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-10">
			<form class="shub-validate contact-form" id="new_form_topic" action="<?php the_permalink(); ?>" name="new-forum-topic" method="post">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group ">
								<div class="bb-select-opt">
									<?php if ( bbp_has_forums( array( 'post_parent' => 0 , 'orderby'=> 'order') ) ) : ?>
										<select id="bbp_forum_id" name="bbp_forum_id" class="form-control">
											<option value="">Select Topic</option>
										<?php while ( bbp_forums() ) : bbp_the_forum(); ?>
											<option value="<?php bbp_forum_id() ?>"><?php bbp_forum_title() ?></option>
										<?php endwhile; ?>
										</select>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php /*
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group ">
								<div class="bb-select-opt">
										<select id="bbp_forum_id" name="bbp_forum_id" class="form-control">
											<option value="">Select sub topic</option>
										</select>
								</div>
							</div>
						</div>
					</div>
					*/ ?>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="sr-only" for="topic-title">Conversation Title</label>
								<input type="text" name="bbp_topic_title" id="bbp_topic_title" class="form-control" placeholder="Enter Conversation Title"  />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<textarea class="form-control textarea" rows="3" name="bbp_topic_content" id="bbp_topic_content" placeholder="Type your message.." style="width:100%;"></textarea>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-primary-full pull-right send shub-form-submit">Submit</button>
						</div>
					</div>
					<input type="hidden" name="redirect_to" value="<?php echo get_permalink() ?>" />
					<input type="hidden" name="bbp_topic_status" value="pending" />
					<?php bbp_topic_form_fields(); ?>
			</form>
		</div>
	</div>
</aside>
<?php endif; ?>