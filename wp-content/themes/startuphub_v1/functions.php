<?php

//Add thumbnail, automatic feed links and title tag support
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );

// Template tags
require get_template_directory() . '/template-tags.php';

//Add menu support and register main menu
if ( function_exists( 'register_nav_menus' ) ) {
  	register_nav_menus(
  		array(
  		  'main_menu' => 'Main Menu',
        'category_menu' => 'Category Menu',
        'footer_menu' => 'Footer Menu',
        'user_menu' => 'User Menu',
  		)
  	);
}

// filter the Gravity Forms button type
add_filter('gform_submit_button', 'form_submit_button', 10, 2);
function form_submit_button($button, $form){
    return "<button class='button btn' id='gform_submit_button_{$form["id"]}'><span>{$form['button']['text']}</span></button>";
}

// Register sidebar
add_action('widgets_init', 'theme_register_sidebar');
function theme_register_sidebar() {
	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'id' => 'sidebar-1',
		    'before_widget' => '<div id="%1$s" class="widget %2$s">',
		    'after_widget' => '</div>',
		    'before_title' => '<h4>',
		    'after_title' => '</h4>',
		 ));
	}
}

function footer_widgets_init() {
  register_sidebar( array(
    'name'          => 'Footer Navigation Area',
    'id'            => 'footer_nav_1',
    'before_widget' => '<div class="col-md-3 col-xs-6">',
    'after_widget'  => '</div>',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>',
  ) );
}
add_action( 'widgets_init', 'footer_widgets_init' );

function events_widgets_init() {
  register_sidebar( array(
    'name'          => 'Events Listing Area',
    'id'            => 'event_listing_area',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => '',
  ) );
}
add_action( 'widgets_init', 'events_widgets_init' );

if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Overview Image',
            'id' => 'overview-image',
            'post_type' => 'page'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Overview Image',
            'id' => 'news-overview-image',
            'post_type' => 'news'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Overview Image',
            'id' => 'events-overview-image',
            'post_type' => 'events'
        )
    );

}
add_image_size( 'news-listing-thumb', 261, 174, true ); 
add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

// START THEME OPTIONS
// custom theme options for user in admin area - Appearance > Theme Options
function pu_theme_menu()
{
  add_theme_page( 'Theme Option', 'Theme Options', 'manage_options', 'pu_theme_options.php', 'pu_theme_page');
}
add_action('admin_menu', 'pu_theme_menu');

function pu_theme_page()
{
?>
    <div class="section panel">
      <h1>Custom Theme Options</h1>
      <form method="post" enctype="multipart/form-data" action="options.php">
      <hr>
        <?php 

          settings_fields('pu_theme_options'); 
        
          do_settings_sections('pu_theme_options.php');
          echo '<hr>';
        ?>
            <p class="submit">  
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />  
            </p>
      </form>
    </div>
    <?php
}

add_action( 'admin_init', 'pu_register_settings' );

function pu_display_section()
{

}
/**
 * Function to register the settings
 */
function pu_register_settings()
{
    // Register the settings with Validation callback
    register_setting( 'pu_theme_options', 'pu_theme_options' );

    // Add settings section
    add_settings_section( 'pu_text_section', 'Social Links', 'pu_display_section', 'pu_theme_options.php' );

    // Create textbox field
    $field_args = array(
      'type'      => 'text',
      'id'        => 'twitter_link',
      'name'      => 'twitter_link',
      'desc'      => 'Twitter Link - Example: http://twitter.com/username',
      'std'       => '',
      'label_for' => 'twitter_link',
      'class'     => 'css_class'
    );

    // Add twitter field
    add_settings_field( 'twitter_link', 'Twitter', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );   

    $field_args = array(
      'type'      => 'text',
      'id'        => 'facebook_link',
      'name'      => 'facebook_link',
      'desc'      => 'Facebook Link - Example: http://facebook.com/username',
      'std'       => '',
      'label_for' => 'facebook_link',
      'class'     => 'css_class'
    );

    // Add facebook field
    add_settings_field( 'facebook_link', 'Facebook', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    $field_args = array(
      'type'      => 'text',
      'id'        => 'gplus_link',
      'name'      => 'gplus_link',
      'desc'      => 'Google+ Link - Example: http://plus.google.com/user_id',
      'std'       => '',
      'label_for' => 'gplus_link',
      'class'     => 'css_class'
    );

    // Add Google+ field
    add_settings_field( 'gplus_link', 'Google+', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    $field_args = array(
      'type'      => 'text',
      'id'        => 'youtube_link',
      'name'      => 'youtube_link',
      'desc'      => 'Youtube Link - Example: https://www.youtube.com/channel/channel_id',
      'std'       => '',
      'label_for' => 'youtube_link',
      'class'     => 'css_class'
    );

    // Add youtube field
    add_settings_field( 'youtube_ink', 'Youtube', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    $field_args = array(
      'type'      => 'text',
      'id'        => 'linkedin_link',
      'name'      => 'linkedin_link',
      'desc'      => 'LinkedIn Link - Example: http://linkedin.com/in/username',
      'std'       => '',
      'label_for' => 'linkedin_link',
      'class'     => 'css_class'
    );

    // Add LinkedIn field
    add_settings_field( 'linkedin_link', 'LinkedIn', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    $field_args = array(
      'type'      => 'text',
      'id'        => 'instagram_link',
      'name'      => 'instagram_link',
      'desc'      => 'Instagram Link - Example: http://instagram.com/username',
      'std'       => '',
      'label_for' => 'instagram_link',
      'class'     => 'css_class'
    );

    // Add Instagram field
    add_settings_field( 'instagram_link', 'Instagram', 'pu_display_setting', 'pu_theme_options.php', 'pu_text_section', $field_args );

    // Add settings section title here
    add_settings_section( 'section_name_here', 'Section Title Here', 'pu_display_section', 'pu_theme_options.php' );
    
    // Create textarea field
    $field_args = array(
      'type'      => 'textarea',
      'id'        => 'settings_field_1',
      'name'      => 'settings_field_1',
      'desc'      => 'Setting Description Here',
      'std'       => '',
      'label_for' => 'settings_field_1'
    );

    // section_name should be same as section_name above (line 116)
    add_settings_field( 'settings_field_1', 'Setting Title Here', 'pu_display_setting', 'pu_theme_options.php', 'section_name_here', $field_args );   


    // Copy lines 118 through 129 to create additional field within that section
    // Copy line 116 for a new section and then 118-129 to create a field in that section
}


// allow wordpress post editor functions to be used in theme options
function pu_display_setting($args)
{
    extract( $args );

    $option_name = 'pu_theme_options';

    $options = get_option( $option_name );

    switch ( $type ) {  
          case 'text':  
              $options[$id] = stripslashes($options[$id]);  
              $options[$id] = esc_attr( $options[$id]);  
              echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[$id]' value='$options[$id]' />";  
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
          break;
          case 'textarea':  
              $options[$id] = stripslashes($options[$id]);  
              //$options[$id] = esc_attr( $options[$id]);
              $options[$id] = esc_html( $options[$id]); 

              printf(
                wp_editor($options[$id], $id, 
                  array('textarea_name' => $option_name . "[$id]",
                    'style' => 'width: 200px'
                    )) 
        );
              // echo "<textarea id='$id' name='" . $option_name . "[$id]' rows='10' cols='50'>".$options[$id]."</textarea>";  
              // echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break; 
    }
}

function pu_validate_settings($input)
{
  foreach($input as $k => $v)
  {
    $newinput[$k] = trim($v);
    
    // Check the input is a letter or a number
    if(!preg_match('/^[A-Z0-9 _]*$/i', $v)) {
      $newinput[$k] = '';
    }
  }

  return $newinput;
}

// Add custom styles to theme options area
add_action('admin_head', 'custom_style');

function custom_style() {
  echo '<style>
    .appearance_page_pu_theme_options .wp-editor-wrap {
      width: 75%;
    }
    .regular-textcss_class {
      width: 50%;
    }
    .appearance_page_pu_theme_options h3 {
      font-size: 2em;
      padding-top: 40px;
    }
  </style>';
}

// END THEME OPTIONS


/**
 * Load site scripts.
 */
function bootstrap_theme_enqueue_scripts() {
	$template_url = get_template_directory_uri();
	// jQuery.
	
  wp_enqueue_script(
        'custom-script',
        get_stylesheet_directory_uri() . '/js/ajax.js',
        array( 'jquery' ), false, false
    );

	
	$my_nonce = wp_create_nonce('csrf_check');
 				  wp_localize_script( 'custom-script', 'startup_services_script', array('shubservices_forms' => plugins_url('startuphub/ajax/startup-forms-ajax.php'), 'shubservices_url' => plugins_url('startuphub/'), 'csrf_token' => $my_nonce ) );

	// Load Thread comments WordPress script.
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
}

add_action( 'wp_enqueue_scripts', 'bootstrap_theme_enqueue_scripts', 1 );


add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');

function myStartSession() {
    if(!session_id()) {
        session_start();
    }
}

function myEndSession() {
    session_destroy ();
}

function shub_user_register() {
    if($_POST['action'] == "register_user")
    {
        $retrieved_nonce = $_REQUEST['_wpnonce'];
        if (!wp_verify_nonce($retrieved_nonce, 'user_register' ))
        {
            die( 'Failed security check' );
        }
        else
        {
            $Password = trim($_POST['Password']); 
			$first_name = trim($_POST['first_name']);
			$last_name = trim($_POST['last_name']);
			$Full_name = trim($first_name . ' ' . $last_name);
            $Company = trim($_POST['Company']);
            $Email = trim($_POST['Email']);
            $Industry = trim($_POST['Industry']);

            $userdata = array(
              'user_pass' =>  $Password,
              'user_login' => $Email,
              'first_name' => $first_name,
              'last_name' => $last_name,
              'user_email' => $Email,
              'user_url' => '',
              'role' => get_option( 'default_role' )
            );
            $new_user = wp_insert_user( $userdata );
            if (!is_wp_error($new_user))
            {
                $user = wp_signon( array( 'user_login' => $Email, 'user_password' => $Password, 'remember' => "forever" ), false );       
                //wp_new_user_notification($new_user, $Password);                
                if ( !empty( $_POST['Company'] ) )
                {
                    update_user_meta( $new_user, 'Company', esc_attr($_POST['Company']));
                }
                if ( !empty( $_POST['Country'] ) )
                {
                    update_user_meta( $new_user, 'Country', esc_attr($_POST['Country']) );
                    update_user_meta( $new_user, 'is_complete', '1');
                }
                if ( !empty( $_POST['OrganisationType'] ) )
                {
                    update_user_meta( $new_user, 'OrganisationType', esc_attr($_POST['OrganisationType']));
                }
                if ( !empty( $_POST['Industry'] ) )
                {
                    update_user_meta( $new_user, 'Industry', esc_attr($_POST['Industry']) );
                }
                if ( !empty( $_POST['newsletter'] ) )
                {
                  update_user_meta( $new_user, 'newsletter', esc_attr($_POST['newsletter']) );
                }
                
				if(isset($_FILES['avatar_file']))
                {
				  if(!is_executable($_FILES['avatar_file']['name'])){
					  
					  if (!function_exists( 'wp_handle_upload' ) ) {
							require_once( ABSPATH . 'wp-admin/includes/file.php' );
						}
						$uploadedfile = $_FILES['avatar_file'];
						$upload_overrides = array( 'test_form' => false );
						$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
						if ( $movefile && ! isset( $movefile['error'] ) ) {
							update_user_meta($new_user, 'wsl_current_user_image', $movefile['url']);
						}
				  } 
                }
				
				
				// Add fullname to $_POST
				$_POST['Full_name'] = $Full_name;
				
                // Push to CRM
                dshub_user_to_crm( $_POST );

                $url = home_url('dashboard');
                //$url = get_permalink();
                wp_redirect($url);
                exit;
            }
            else
            {
                $msg = $new_user->get_error_message();
                $url = home_url('?register=error&msg='.$msg);
                wp_redirect($url);
                exit;
            }
        }
    }

    if($_POST['action'] == "save_memberform")
    {
        $retrieved_nonce = $_REQUEST['_wpnonce'];
        if (!wp_verify_nonce($retrieved_nonce, 'member_register' ))
        {
            die( 'Failed security check' );
        }
        else
        {
            $firstname = sanitize_text_field(trim($_POST['firstname']));
            $lastname = sanitize_text_field(trim($_POST['lastname']));
            $email = sanitize_text_field(trim($_POST['email']));
            $mobilenumber = sanitize_text_field(trim($_POST['mobilenumber']));
            $nationality = sanitize_text_field(trim($_POST['nationality']));
            $source = sanitize_text_field(trim($_POST['source']));
            $companyname = sanitize_text_field(trim($_POST['companyname']));
            $jobtitle = sanitize_text_field(trim($_POST['jobtitle']));
            $website = sanitize_text_field(trim($_POST['website']));
            $industrysector = sanitize_text_field(trim($_POST['industrysector']));
            $country = sanitize_text_field(trim($_POST['country']));
            $overview = sanitize_text_field(trim($_POST['overview']));
            $tradelicense_authority = sanitize_text_field(trim($_POST['tradelicense_authority']));
            $tradelicense_number = sanitize_text_field(trim($_POST['tradelicense_number']));
            $year_of_incoporation = sanitize_text_field(trim($_POST['year_of_incoporation']));

            if(isset($_FILES['tradelicense_file']))
            {
              if(!is_executable($_FILES['tradelicense_file']['name'])){

                if (!function_exists( 'wp_handle_upload' ) ) {
                  require_once( ABSPATH . 'wp-admin/includes/file.php' );
                }
                $uploadedfile = $_FILES['tradelicense_file'];
                $upload_overrides = array( 'test_form' => false, 'mimes' => get_allowed_mime_types());
                $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                if ( $movefile && ! isset( $movefile['error'] ) ) {
                    $tradelicense_file = $movefile['url'];
                }
              }
            }

            $created = date('Y-m-d H:i:s');
            global $wpdb;
            $table = $wpdb->prefix . "members";
            $members_data = array(
                              'firstname' => $firstname,
                              'lastname' => $lastname,
                              'email' => $email,
                              'mobilenumber' => $mobilenumber,
                              'nationality' => $nationality,
                              'source' => $source,
                              'companyname' => $companyname,
                              'jobtitle' => $jobtitle,
                              'website' => $website,
                              'industrysector' => $industrysector,
                              'country' => $country,
                              'overview' => $overview,
                              'tradelicense_authority' => $tradelicense_authority,
                              'tradelicense_number' => $tradelicense_number,
                              'tradelicense_file' => $tradelicense_file,
                              'year_of_incoporation' => $year_of_incoporation,
                              'date_created' => $created
                            );
            $res = $wpdb->insert($table, $members_data);
            if ($res) {
              $members_data['id'] = $wpdb->insert_id;
              $crm_id = dshub_user_to_crm($members_data);
              $wpdb->update('shub_members', array('crm_id' => $crm_id), array('id' => $members_data['id']));
              $headers = array('Content-Type: text/html; charset=UTF-8');

                $message = "<html>
<head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>

<h3>MEMBERSHIP APPLICATION</h3>

<table>
  <tr>
    <td>FIRST NAME:</td>
    <td>$firstname</td>
  </tr>
  <tr>
    <td>LAST NAME:</td>
    <td>$lastname</td>
  </tr>
  <tr>
    <td>EMAIL:</td>
    <td>$email</td>
  </tr>
  <tr>
    <td>MOBILE:</td>
  <td>$mobilenumber</td>
  </tr>
  <tr>
    <td>NATIONALITY:</td>
  <td>$nationality</td>
  </tr> 
  <tr>
    <td>HOW DID YOU HEAR ABOUT US?:</td>
    <td>$source</td>
  </tr>
  <tr> 
    <td>COMPANY NAME:</td>
  <td>$companyname</td>
  </tr>
  <tr>
    <td>JOB TITLE:</td>
    <td>$jobtitle</td>  
  </tr>
<tr>  
    <td>INDUSTRY SECTOR:</td>
  <td>$industrysector</td>
  </tr>
  <tr>  
    <td>COUNTRY:</td>
  <td>$country</td>
  </tr>
  <tr>
    <td>WEBSITE:</td>
    <td>$website</td>
  </tr>
  <tr>
    <td>TRADE LICENSE:</td>
  <td>$tradelicense_file</td>
  </tr>
  <tr>
    <td>YEAR OF INCOPORATION:</td>
    <td>$year_of_incoporation</td>
  </tr> 
  <tr>
    <td>BRIEF OVERVIEW OF YOUR BUSINESS:</td>
    <td>$overview</td>
  </tr>
  

</table>
  
<hr>
<p>This e-mail was sent from a Membership Form on Dubai Startup Hub</p>
</body>
</html>";

                $subject = "Dubai Startup Hub Membership";

                $to = "Kimberly.James@Dubaichamber.com,dubaistartuphub@dubaichamber.com";
                if (!wp_mail($to, $subject, $message, $headers)) {
                    
                }
              dshub_add_notices('We are excited to receive your membership application! Our team will be in touch within the next two weeks.', 'success');
              $url = home_url('membership/thank-you');
              wp_redirect($url);
            } else {
              dshub_add_notices('There was error processing your data. Please check your fields', 'error');
              $url = home_url('membership');
              wp_redirect($url);
            }
            exit();
        }
    }
}
add_action( 'wp_loaded', 'shub_user_register' );

/**
 * User CRM integration
 *
 */
function dshub_user_to_crm( $data ) {

  if($data['year_of_incoporation']!='')
  {
      $year_of_incoporation = '01/01/' . $data['year_of_incoporation'];
  }
  else
  {
      $year_of_incoporation = '';
  }

  // Attachments
  $attachments = '';
  if ( $data['attachments'] ) {
    foreach ( $data['attachments'] as $key => $path ) {

      if ( file_exists( $path ) ) {
        $path_info = pathinfo($path);

        $attachments .= '
          <dcs:DcStartupProfAttachment> 
             <dcs:Id></dcs:Id>
             <dcs:ContactFileExt>'. $path_info['extension'] .'</dcs:ContactFileExt>
             <dcs:ContactFileName>'. $key .'</dcs:ContactFileName>
             <dcs:ContactFileBuffer>'. base64_encode( file_get_contents( $path ) ) .'</dcs:ContactFileBuffer>
          </dcs:DcStartupProfAttachment>
        ';
      }
    }
  }

  if ( $attachments )
    $attachments = '<dcs:ListOfDcStartupProfAttachment>'. $attachments .'</dcs:ListOfDcStartupProfAttachment>';
	
  $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="http://siebel.com/CustomUI" xmlns:dcs="http://www.siebel.com/xml/DCStartupPrf">
  <soap:Header xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/07/secext" soap:mustUnderstand="0">
<wsse:UsernameToken>
<wsse:Username>EAIUSERPROD</wsse:Username>
<wsse:Password Type="wsse:PasswordText">EA1US3RPR0D</wsse:Password>
<wsse:SessionType xmlns="http://siebel.com/webservices">ServerDetermine</wsse:SessionType>
</wsse:UsernameToken>
</wsse:Security>
</soap:Header>
   <soapenv:Body>
      <cus:CreateStartupProfile_Input>
         <cus:Process_spcInstance_spcId>?</cus:Process_spcInstance_spcId>
         <dcs:ListOfDcstartupprf>
            <!--Zero or more repetitions:-->
            <dcs:DcStartupProfileBc>
               <dcs:DCYearIncorporation>'. $year_of_incoporation .'</dcs:DCYearIncorporation>
               <dcs:DCNationality>' . $data['nationality'] . '</dcs:DCNationality>
               <dcs:Id>'. $data['id'] .'</dcs:Id>              
               <dcs:DCCountry>'. $data['country'] .'</dcs:DCCountry>             
               <dcs:DCDCCIMemberFlag></dcs:DCDCCIMemberFlag>
               <dcs:DCEmail>'. $data['email'] .'</dcs:DCEmail>
               <dcs:DCEmirates></dcs:DCEmirates>               
               <dcs:DCEntreprenurName>'. str_replace('&','and',$data['companyname']) .'</dcs:DCEntreprenurName>              
               <dcs:DCFacebook></dcs:DCFacebook>               
               <dcs:DCFirstName>'. $data['firstname'] .'</dcs:DCFirstName>              
               <dcs:DCIndustrySector>'. $data['industrysector'] .'</dcs:DCIndustrySector>               
               <dcs:DCInstagram></dcs:DCInstagram>               
               <dcs:DCInvestmentSize></dcs:DCInvestmentSize>              
               <dcs:DCJobtitle>'. str_replace('&','and',$data['jobtitle']) .'</dcs:DCJobtitle>               
               <dcs:DCKnownAboutUs>'. $data['source'] .'</dcs:DCKnownAboutUs>              
               <dcs:DCLastName>'. $data['lastname'] .'</dcs:DCLastName>               
               <dcs:DCLicenseIssueAuth>'. $data['tradelicense_authority'] .'</dcs:DCLicenseIssueAuth>               
               <dcs:DCLicenseNumber>'. $data['tradelicense_number'] .'</dcs:DCLicenseNumber>             
               <dcs:DCLicenseType></dcs:DCLicenseType>              
               <dcs:DCLocation></dcs:DCLocation>               
               <dcs:DCMemberAccelatorFlag></dcs:DCMemberAccelatorFlag>              
               <dcs:DCMemberType></dcs:DCMemberType>               
               <dcs:DCMiddleName></dcs:DCMiddleName>               
               <dcs:DCMobilePhone>'. $data['mobilenumber'] .'</dcs:DCMobilePhone>               
               <dcs:DCNatureofBusiness></dcs:DCNatureofBusiness>               
               <dcs:DCOrgType></dcs:DCOrgType>               
               <dcs:DCOthers>'. str_replace('&','and',$data['overview']) .'</dcs:DCOthers>              
               <dcs:DCPoBox></dcs:DCPoBox>               
               <dcs:DCStartupName>'. str_replace('&','and',$data['companyname']) .'</dcs:DCStartupName>               
               <dcs:DCTags></dcs:DCTags>               
               <dcs:DCTitle>'. $data['title'] .'</dcs:DCTitle>              
               <dcs:DCTwitter></dcs:DCTwitter>              
               <dcs:DCWebsite>'. $data['website'] .'</dcs:DCWebsite>
               '. $attachments .'               
            </dcs:DcStartupProfileBc>
         </dcs:ListOfDcstartupprf>
         <cus:Object_spcId></cus:Object_spcId>
         <cus:Siebel_spcOperation_spcObject_spcId>?</cus:Siebel_spcOperation_spcObject_spcId>
         <cus:Error_spcCode></cus:Error_spcCode>
         <cus:Error_spcMessage></cus:Error_spcMessage>
      </cus:CreateStartupProfile_Input>
   </soapenv:Body>
</soapenv:Envelope>';

  $location = 'https://dcsoamw.dubaichamber.com/STARTUPHUB/ProxyService/DC_Create_Startup_Profile_WF_PRXY-pipeline';

  $action = 'http://siebel.com/CustomUI';

  $version = 1;

  $client = new SoapClient(NULL, array(
    'location' => $location,
    'uri'      => "",
    'trace'    => 1,
    'exceptions'=> 1,
  ));

  /*
 echo '<textarea style="border:1px solid yellow; width:100%;height:300px">'.$request.'</textarea>';
  try {
    $response = $client->__doRequest($request, $location, $action, $version);
  } catch (Exception $e) {
    var_dump($e->faultcode);
    var_dump($e->getMessage());
    var_dump($client->__getLastRequest());
    var_dump($client->__getLastResponse());
    exit;
  }
  echo '<textarea  style="border:1px solid red;width:100%;height:300px">'.$response.'</textarea>';
  exit;
  */


  $response = $client->__doRequest($request, $location, $action, $version);
  $clean_xml = str_ireplace(['SOAP-ENV:', 'env:', 'SOAP:', 'ns:'], '', $response);
  $r_array = json_decode(json_encode((array)simplexml_load_string($clean_xml)),1);
  return $r_array['Body']['CreateStartupProfile_Output']['Process_spcInstance_spcId'];
}

function user_update_form(){
    if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] )) {
      if($_POST['action'] == "updateuser")
      {
          $retrieved_nonce = $_REQUEST['_wpnonce'];
          if (!wp_verify_nonce($retrieved_nonce, 'update_user' ))
          {
              die( 'Failed security check' );
          }
          else
          {
              $current_user = wp_get_current_user();
              $user_id = $current_user->ID;

              $user_fields = array(
                  'ID'           => $user_id,
                  'first_name'   => esc_attr($_POST['up_First_name']),
				  'last_name'    => esc_attr($_POST['up_Last_name'])
              );              

              if ( !empty( $_POST['up_Company'] ) )
              {
                  update_user_meta( $user_id, 'Company', esc_attr($_POST['up_Company']));
              }
              if ( !empty( $_POST['up_Country'] ) )
              {
                  update_user_meta( $user_id, 'Country', esc_attr($_POST['up_Country']) );
                  update_user_meta( $user_id, 'is_complete', '1');
              }
              if ( !empty( $_POST['up_OrganisationType'] ) )
              {
                  update_user_meta( $user_id, 'OrganisationType', esc_attr($_POST['up_OrganisationType']));
              }
              if ( !empty( $_POST['up_Industry'] ) )
              {
                  update_user_meta( $user_id, 'Industry', esc_attr($_POST['up_Industry']) );
              }
              if(isset($_FILES['avatar_file']))
              {
                  if (!function_exists( 'wp_handle_upload' ) ) {
                        require_once( ABSPATH . 'wp-admin/includes/file.php' );
                  }
                  $uploadedfile = $_FILES['avatar_file'];
                  $upload_overrides = array( 'test_form' => false );
                  $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                  if ( $movefile && ! isset( $movefile['error'] ) ) {
                      update_user_meta($user_id, 'wsl_current_user_image', $movefile['url']);
                  }
              }

              wp_update_user($user_fields);

              // Push to CRM
              $updatePOST = array(
                              'Country' => $_POST['up_Country'],
                              'Company' => $_POST['up_Company'],
                              'Email' => $current_user->user_email,
                              'Full_name' => trim($_POST['up_First_name'] . ' ' . $_POST['up_Last_name']),
				  			  'first_name' => $_POST['up_First_name'],
				  			  'last_name'  => $_POST['up_Last_name'],
                              'Industry' => $_POST['up_Industry'],
                              'Update' => 'Update',
                              'OrganisationType' => $_POST['up_OrganisationType'],
              );
              dshub_user_to_crm( $updatePOST );

              dshub_add_notices('You have successfully saved your profile.', 'success');
              $url = home_url('dashboard');
              wp_redirect($url);
              exit();
          }
      }    
    }
}
add_action( 'wp_loaded', 'user_update_form' );

function user_update_pass_form(){
    if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] )) {
      if($_POST['action'] == "updatepassword")
      {
          $retrieved_nonce = $_REQUEST['_wpnonce'];
          if (!wp_verify_nonce($retrieved_nonce, 'update_user_password' ))
          {
              die( 'Failed security check' );
          }
          else
          {

              $pass = esc_attr($_POST['current_password']);
              $current_user = wp_get_current_user();
              $user_id = $current_user->ID;

              $user = get_user_by( 'ID', $user_id );

              if ( $user && wp_check_password( $pass, $user->data->user_pass, $user->ID) )
              {                
                  $password = esc_attr($_POST['new_password']);
                  wp_set_password( $password, $user_id );
                  $url = home_url('dashboard/?update=p');
                  wp_redirect($url);
                  exit();
              } 
              else
              {
                   wp_redirect(home_url('dashboard/?update=f'));
                   exit();
              }

              
          }
      }    
    }
}
add_action( 'wp_loaded', 'user_update_pass_form' );

function tml_registration_errors( $errors ) {
  if ( empty( $_POST['user_company'] ) )
    $errors->add( 'empty_user_company', '<strong>ERROR</strong>: Please enter your company.' );
  if ( empty( $_POST['user_org'] ) )
    $errors->add( 'empty_user_org', '<strong>ERROR</strong>: Please select your organisation type.' );
  return $errors;
}
add_filter( 'registration_errors', 'tml_registration_errors');

function tml_user_register( $user_id ) {
  if( in_array( $_POST['action'], array( 'register', 'wordpress_social_account_linking' ) ) )
      {
          if ( !empty( $_POST['user_company'] ) )
          {
              update_user_meta( $user_id, 'Company', esc_attr($_POST['user_company']));
          }
          if ( !empty( $_POST['user_country'] ) )
          {
              update_user_meta( $user_id, 'Country', esc_attr($_POST['user_country']));
              update_user_meta( $user_id, 'is_complete', '1');
          }
          if ( !empty( $_POST['user_org'] ) )
          {
              update_user_meta( $user_id, 'OrganisationType', esc_attr($_POST['user_org']));
          }
          if ( !empty( $_POST['user_industry'] ) )
          {
              update_user_meta( $user_id, 'Industry', esc_attr($_POST['user_industry']) );              
          }
	    
	  	  $user_fields = array(
                  'ID'           => $user_id,
                  'first_name'   => esc_attr($_POST['first_name']),
				  'last_name'    => esc_attr($_POST['last_name'])
              ); 
	  
	  	  wp_update_user($user_fields);
	  
          // Push to CRM
            $updatePOST = array(
                'Country' => $_POST['user_country'],
                'Company' => $_POST['user_company'],
                'Email' => $_POST['user_email'],
                'Full_name' => $_POST['user_email'],
        				'first_name' => $_POST['first_name'],
        				'last_name'  => $_POST['last_name'],
                'Industry' => $_POST['user_industry'],
                'Update' => 'Update',
                'OrganisationType' => $_POST['user_org'],
            );
            dshub_user_to_crm( $updatePOST );
    }
}
add_action( 'user_register', 'tml_user_register' );

function shub_user_avatar($user_id) {
    $default_avatar = site_url('wp-content/uploads/2017/default-profile.png');
    $avatar = get_user_meta($user_id, 'wsl_current_user_image', true);
    if (empty($avatar)) {
      
      $gravatar = get_avatar_url($user_id);
		
      if (strpos($gravatar, 'blank') === false)
        $avatar = $gravatar;
    }
	
	if(!empty($avatar))
	{
		$filename_from_url = parse_url($avatar);
		$ext = pathinfo($filename_from_url['path'], PATHINFO_EXTENSION);
		$allowed_ext = array("jpeg", "jpg", "png");
		if(!in_array(strtolower($ext), $allowed_ext))
		{
			$avatar = $default_avatar;
		}
	}
	
    return (!empty($avatar)) ? $avatar : $default_avatar;
}

function loggedUser(){
   $user = wp_get_current_user();
   if($user->user_firstname)
   {
      echo $user->user_firstname . ' ' . $user->user_lastname;
   }
   else
   {
      echo __('User');
   }   
}

function getUserName($user_id){
   $user = get_userdata($user_id);  
   if($user->user_firstname)
   {
      echo $user->user_firstname . ' ' . $user->user_lastname;
   }
   else
   {
      echo __('User');
   }   
}

/**
 * Format the date
 *
 */
function shub_date_format( $time = null ) {
  $time = empty($time) ? time() : $time;

  return date('d M Y', $time);
}

function shub_time_format( $time = null ) {
  $time = empty($time) ? time() : $time;

  return date('h:s A', $time);
}

function shub_login_class() {
  return (is_user_logged_in()) ? '' : 'need-signin';
}

add_filter( 'body_class','halfhalf_body_class' );
function halfhalf_body_class( $classes ) {
 
    if ( is_page_template( 'forum.php' ) ) {
        $classes[] = 'forumPage';
    }
     
    return $classes;
     
}

function shub_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'shub_excerpt_length', 999 );


function Generate_Featured_Image( $image_url, $post_id, $ssl_image = false ){


    $upload_dir = wp_upload_dir();
    if($ssl_image == true)
    {
      $arrContextOptions=array(
          "ssl"=>array(
              "verify_peer"=>false,
              "verify_peer_name"=>false,
          ),
      );  
      $image_data = file_get_contents($image_url, false, stream_context_create($arrContextOptions));
    }
    else
    {
      $image_data = file_get_contents($image_url);
    }    
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}

if( !wp_next_scheduled( 'feedevents_refresh' ) ) {
   wp_schedule_event( time(), 'daily', 'feedevents_refresh' );
}
 
add_action( 'feedevents_refresh', 'update_events_feed' );
add_action( 'wp_loaded', 'update_events_feed' );
function update_events_feed() {  
   if($_REQUEST["event_feed"]== "true")
   {     
      $jsondata = file_get_contents('http://community.dtec.ae/wp-json/tribe/events/v1/events');
      $jsondata = json_decode($jsondata);
      // echo '<pre>';
      // print_r($jsondata);
      // exit;
      foreach ($jsondata->events as $key => $value) {      
        
        if(true)
        {

            $post_type = 'events';

            foreach ($value->categories as $cat)
             {
                if(in_array($cat->slug, array('dtec-internal-events', 'external-events')))
                {
                    $post_type = 'events';
                }
             }

            $args = array(
                'post_type'=> $post_type,
                'meta_query' => array(
                      array(
                          'key' => 'sh_events_eid',
                          'value' => ''.$value->id.'',
                          'compare' => '='
                      )
                  ),
            );
           
            //var_dump($args);
            $wp_query = new WP_Query($args);
            $x=0;
            while ($wp_query->have_posts()) : $wp_query->the_post();
              $x = 1;
            endwhile;
            
            if($x==0)
            {

                $post_args = array (
                   'post_type' => $post_type,
                   'post_title' => $value->title,
                   'post_content' => $value->description,
                   'post_status' => 'pending'
                );                
                if($post_type == 'post')
                {
                    $post_args['post_category'] = array('15');
                }
                $post_id = wp_insert_post($post_args);
                if ($post_id) {
                   add_post_meta($post_id, 'sh_events_date', $value->start_date);
                   add_post_meta($post_id, 'sh_events_date_end', $value->end_date);
                   add_post_meta($post_id, 'sh_events_location', $value->venue->venue);
                   add_post_meta($post_id, 'sh_events_address', $value->venue->address);
                   add_post_meta($post_id, 'sh_events_city', $value->venue->city);
                   add_post_meta($post_id, 'sh_events_ticket', $value->website);
                   add_post_meta($post_id, 'sh_events_eid', $value->id);
                   unset($tag_name);
                   $tag_name = array();
                   $tag_names = "";
                   foreach ($value->categories as $cat)
                   {
                      if($cat->name!="zzzhidden")
                      {
                        $tag_name[] = $cat->name;
                      }
                   }
                    $tag_names = implode (", ", $tag_name);
                    wp_set_post_tags( $post_id, $tag_names, true );
                    Generate_Featured_Image($value->image->url, $post_id);
                }
                /*echo $value->id.'<br>';
                echo $value->title.'<br>';
                echo $value->image->url.'<br>';
                echo 'Start Date:' . $value->start_date . '<br>';
                echo 'End Date:' . $value->end_date . '<br>';
                foreach ($value->categories as $cat)
                {
                  echo $cat->name.'<br>';
                }
                echo $value->venue->city;
                echo $value->website;*/

                echo $value->title .'<br />';
            }

            
        }
      }
      //wp_die();
      exit();
    }
}

if( !wp_next_scheduled( 'feedrss_refresh' ) ) {
   wp_schedule_event( time(), 'daily', 'feedrss_refresh' );
}
 
add_action( 'feedrss_refresh', 'update_rss_feed' );

add_action('wp_loaded', 'update_rss_feed');
function update_rss_feed() {
  if($_REQUEST["rss_feed"]== "true")
   {
	  //ini_set('display_errors', 1);
        if(function_exists('fetch_feed')){
            $uri = 'https://courses.potential.com/feed/';
            $feed = fetch_feed($uri);
        }
        if (! is_wp_error( $feed ))  {
            foreach ($feed->get_items() as $item){
                $titlepost = $item->get_title();
                $content = $item->get_content();
                $description = $item->get_description();
                $itemdate = $item->get_date();
                $media_group = $item->get_item_tags('', 'enclosure');
                //echo $media_group;
                //$img = $media_group[0]['attribs']['']['url'];
                //$width = $media_group[0]['attribs']['']['width'];           
                // $latestItemDate = $feed->get_item()->get_date();

                global $wpdb;

                $query = $wpdb->prepare(
                    'SELECT ID FROM ' . $wpdb->posts . '
                    WHERE post_title = %s
                    AND post_type = \'post\'',
                    $titlepost
                );
                $wpdb->query( $query );

                if ( $wpdb->num_rows ) {
                    
                } else {
                    $post_information = array(
                        'post_title' => $titlepost,
                        'post_content' => $content,
                        'post_type' => 'post',
                        'post_status' => 'pending',
                        'post_category' => array('15'),
                        'post_date' => date('Y-m-d H:i:s', strtotime($itemdate)),
                    );
                    //var_dump($post_information);
                    $post_id = wp_insert_post( $post_information ); 

                    // Import image
                    if($post_id)
                    {
                      unset($tag_name);
                     $tag_name = array();
                     $tag_names = "";
                     foreach ($item->get_categories() as $cat)
                     {
                        $tag_name[] = $cat->term;
                     }
                      $tag_names = implode (", ", $tag_name);
                      wp_set_post_tags( $post_id, $tag_names, true );
                      if ( $enclosure = $item->get_enclosure() ) {
                        Generate_Featured_Image( $enclosure->get_thumbnail(), $post_id, true );
                      }
                    }
                    

                    echo $titlepost .'<br />';
                }
            }
        }
    //wp_die();
    exit();
  }
}

// add css class if page is forum
add_action('wp', 'is_shub_forum_page');
function is_shub_forum_page()
{
	$page = get_page(get_the_ID());
	if(($page->post_name == 'forum' && $page->post_type == 'page') || $page->post_type == 'forum'){
		return "section-page-hero-forum";
	}
	else{
		return "";
	}
	
}

function shub_add_twitter_meta() {
	
	$post_type = get_post_type(get_the_ID());
	$post_arr  = array('post', 'events', 'news');
	
	if(in_array($post_type, $post_arr ))
	{
		if(have_posts()) {

			while(have_posts()) : the_post();

				$post_id    = get_the_ID();
				$post_title = get_the_title();
				$excerpt    = get_the_excerpt();
				$thumbnail  = get_the_post_thumbnail_url($post_id ,'medium');

				$template = '<meta name="twitter:card" content="summary">
							 <meta name="twitter:site" content="@DubaiStartupHub">
							 <meta name="twitter:title" content="'.$post_title.'">
							 <meta name="twitter:description" content="'.$excerpt.'">
							 <meta name="twitter:image" content="'.$thumbnail.'">';

				echo $template;

			endwhile;
	   }
	}
	
}
add_action('wp_head', 'shub_add_twitter_meta');

/**
 * Save the contact 7 form details with the user meta
 *
 */
function shub_save_membership( $contact_form ) {
  global $current_user;

  $user_id = apply_filters( 'determine_current_user', false );

  if (!$user_id)
    return;

  $title = $contact_form->title;
  $submission = WPCF7_Submission::get_instance();

  if( $submission && $title == 'Membership Application' ) {
    $data = $submission->get_posted_data();

    $membership_details = array(
      'id' => $user_id,
      'is_member' => ( $data['is-member'][0] == 'yes' ) ? 'Y' : 'N',
      'city' => $data['city'],
      'first_name' => $data['firstname'],
      'middle_name' => $data['middlename'],
      'last_name' => $data['lastname'],
      'job_title' => $data['job-title'],
      'known_source' => $data['source'][0],
      'license_authority' => $data['trade-license-authority'],
      'license_number' => $data['trade-license-number'],
      'license_type' => $data['trade-license-type'],
      'is_inc_member' => ( $data['is-inc-member'][0] == 'yes' ) ? 'Y' : 'N',
      'member_type' => $data['member-type'],
      'mobile_number' => $data['mobile-number'],
      'title' => $data['title'],
      'po_box' => $data['po-box'],
      'website' => $data['website'],
      'others' => $data['overview'],

      'Country' => ucwords( strtolower( $data['nationality'] ) ),
      'Company' => $data['companyname'],
      'Email' => $data['email'],
      'Full_name' => $data['firstname'] .' '. $data['middlename'] .' '. $data['lastname'],
      'Industry' => $data['industry-sector'],
      'Update' => 'Update',
      'attachments' => $submission->uploaded_files()
    );

    $exclude_meta = array('id', 'first_name', 'last_name', 'Email', 'Full_name', 'Update');
    $req_meta = array_diff_key( $membership_details, array_flip( $exclude_meta ) );

    foreach ( $req_meta as $key => $val )
      update_user_meta( $membership_details['id'], $key, $val );

    update_user_meta( $membership_details['id'], 'membership_completed', 1 );

    // Send to the CRM
    dshub_user_to_crm( $membership_details );
  }
} 
add_action( 'wpcf7_mail_sent', 'shub_save_membership' );

/**
 * Members import from the mail HTML
 *
 */
function shub_members_import() {

  if ( !isset( $_GET['_mems_import'] ) )
    return;

  $keys = array(
    'member_type',
    'title',
    'first_name',
    'middle_name',
    'last_name',
    'Company',
    'job_title',
    'Industry',
    'website',
    'license_file',
    'Email',
    'mobile_number',
    'license_authority',
    'license_type',
    'license_number',
    'po_box',
    'Country',
    'city',
    'others',
    'profile',
    'is_inc_member',
    'is_member',
    'known_source'
  );

  $path = ABSPATH .'wp-content/uploads/members';

  foreach( scandir( $path ) as $r_path ) {
    $path_info = pathinfo( $r_path );

    if ( $path_info['extension'] == 'htm' ) {

      $content = file_get_contents( $path .'/'. $path_info['basename'] );

      $content = str_replace('<o:p>', '', $content);
      $content = str_replace('</o:p>', '', $content);

      // load into DOM
      $dom = new DOMDocument();
      $dom->loadHTML($content);

      // make xpath-able
      $xpath = new DOMXPath($dom);

      // search for the first td of each tr, where its content is $id
      $query = "//tr/td[position()=2]";
      $elements = $xpath->query($query);
      /*if ($elements->length != 1) {
          // not exactly 1 result as expected? return number of hits
          return $elements->length;
      }*/

      $new_keys = $keys;
      if ( $elements->length == 21 )
        $arr = array_splice( $new_keys, 12, 2 );

      // our td was found
      $member = array();
      for( $i = 0; $i < $elements->length; $i++ ) {
        $element = $elements->item($i);

        $member[$new_keys[$i]] = trim( strip_tags( $element->textContent ) );
      }
      $user = get_user_by('email', $member['Email']);
      if ( !$user )
        continue;

      $attachments = array();
      if ( $member['profile'] ) {
        $profile_info = pathinfo( $member['profile'] );
        $profile_path = $path .'/images/'. $path_info['filename'] .'.'. $profile_info['extension'];
        $attachments['profile-photo'] = $profile_path;
      }

      if ( $member['license_file'] ) {
        $license_info = pathinfo( $member['license_file'] );
        $license_path = $path .'/licenses/'. $path_info['filename'] .'.'. $license_info['extension'];
        $attachments['trade-license-file'] = $license_path;
      }

      $member['attachments'] = $attachments;
      $member['Update'] = 'Update';
      $member['Full_name'] = $member['first_name'] .' '. $member['middle_name'] .' '. $member['last_name'];

      $exclude_meta = array('id', 'first_name', 'last_name', 'Email', 'Full_name', 'Update', 'profile', 'license_file');
      $req_meta = array_diff_key( $member, array_flip( $exclude_meta ) );

      
      echo $member['Email'];
      echo '<br>';
      //continue;

      foreach ( $req_meta as $key => $val )
        update_user_meta( $user->ID, $key, $val );

      update_user_meta( $user->ID, 'membership_completed', 1 );

      // Send to the CRM
      dshub_user_to_crm( $member );
    }
  }

  exit;
}
add_action( 'init', 'shub_members_import' );

/**
 * Import all users to the CRM
 */
function shub_users_import() {

  if ( isset( $_GET['_users_import'] ) ) {
    //return;
    set_time_limit(0);

    //$users = get_users();
    global $wpdb;
    $query = $wpdb->get_results("select * from shub_members where crm_id='' or crm_id IS NULL LIMIT 5");
    $type_arr = array();
    $ind_arr = array();
    //foreach ($users as $user) {
    foreach ($query as $row) {
      $import_arr = array(
                            'id' => $row->id,
                            'firstname' => $row->firstname,
                            'lastname' => $row->lastname,
                            'email' => $row->email,
                            'mobilenumber' => $row->mobilenumber,
                            'nationality' => $row->nationality,
                            'source' => $row->source,
                            'companyname' => $row->companyname,
                            'jobtitle' => $row->jobtitle,
                            'website' => $row->website,
                            'industrysector' => $row->industrysector,
                            'country' => $row->country,
                            'overview' => $row->overview,
                            'tradelicense_authority' => $row->tradelicense_authority,
                            'tradelicense_number' => $row->tradelicense_number,
                            'tradelicense_file' => $row->tradelicense_file,
                            'year_of_incoporation' => $row->year_of_incoporation
                          );
      
      $tradelicense_file = $row->tradelicense_file;
      $tradelicense_path = str_replace(home_url('/'), ABSPATH, $tradelicense_file);
      if ( $tradelicense_file ) {
        $import_arr['attachments'] = array(
          'tradelicense_file' => $tradelicense_path
        );
      }
        // echo '<pre>';
        // echo $row->email;
        // $crm_id = dshub_user_to_crm( $import_arr );
        // echo $crm_id .'<br>';
        // $wpdb->update('shub_members', array('crm_id' => $crm_id), array('id' => $row->id));
        // echo '</pre>';
        // continue;
      //}
    }
    exit;
  }
}
add_action( 'init', 'shub_users_import' );

function res_fromemail($email) {
    $wpfrom = get_option('admin_email');
    return $wpfrom;
}
 
function res_fromname($email){
    $wpfrom = get_option('blogname');
    return $wpfrom;
}

add_filter('wp_mail_from', 'res_fromemail');
add_filter('wp_mail_from_name', 'res_fromname');