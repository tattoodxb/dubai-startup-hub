<?php 
$template_url = get_template_directory_uri();
$popup = "";
if(isset($_GET["page"]))
{
	$popup = $_GET["page"];
}
if($popup!="popup")
{
	get_header(); 
}
else
{
    get_template_part( 'templates/popup-header' );
}
?>
<?php if(have_posts()) : ?>
<?php 
while(have_posts()) : the_post();
    $post_id = get_the_ID();
	$post_title = get_the_title();
    $post_content = apply_filters( 'the_content', get_the_content() );
    $post_link = get_the_permalink();
    $post_tags = get_the_tags();
    $categories = get_the_category(); 
    $sh_events_location = get_post_meta($post->ID, 'sh_events_location', $single = true);
    $sh_events_display_date = get_post_meta($post->ID, 'sh_events_display_date', $single = true);
    $start_date = get_post_meta($post->ID, 'sh_events_date', $single = true);
    $end_date = get_post_meta($post->ID, 'sh_events_date', $single = true);
    $register_enable = get_post_meta($post->ID, 'sh_event_enable', true);

    $start_date = ($start_date) ? date('F d, Y', strtotime($start_date)) : '';
    $end_date = ($end_date) ? date('F d, Y', strtotime($end_date)) : '';


      if($sh_events_display_date=="")
      {
          if($start_date == $end_date)
          {
              $sh_events_display_date = $start_date;  
          }
          else
          {
              $sh_events_display_date = $start_date . '-' .$end_date;
          }
          
      }
    $sh_events_ticket = get_post_meta($post->ID, 'sh_events_ticket', $single = true);
    if ( ! empty( $categories ) ) {
        $current_category = $categories[0]->name;
        $cat_slug = $categories[0]->slug;
    }
    if (class_exists('MultiPostThumbnails')) :
        $overview_img = MultiPostThumbnails::get_post_thumbnail_url(
            get_post_type(),
            'events-overview-image' , null, 'post-thumbnail'
        );
    endif;
endwhile;
endif;
?>
<main class="wrapper" role="main">
<h1 class="text-indent"><?php echo $post_title;?></h1>
            <article id="section-events" class="section-events section-feeds single-event" role="region">                
                <div class="bg-wr" style="background-image:url();">
                    <div class="bg-overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="events-articles-wr">                                
                                    <div class="dummy-article-wr " >
                                        <div class="row">                                        
                                            <div class="col-sm-5">
                                                <div class="article-action-wr">
                                                    <figure>
                                                        <?php 
                     the_post_thumbnail('thumbnail', ['class' => 'img-responsive', 'style' => 'width:100%', 'title' => get_the_title()]);
                     ?>
                                                        <figcaption class="text-indent"><?php echo $post_title;?></figcaption>
                                                    </figure>
                                                    <div class="article-action-btn-wr">
                                                        <h5 class="text-primary action-label">Share this article</h5>
                                                        <aside class="social-article-share-wr">
                                                            <h4 class="text-indent">Social Media Share</h4>
                                                            <ul>
                                                                <li>
																	<a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=<?php echo urlencode($post_link);?>&via=DubaiStartupHub" title="Follow us on Twitter" class="social_link" target="_blank">
                                                                        <i class="fa fa-twitter"></i>
                                                                        <span class="sr-only">Follow us on Twitter</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="http://www.facebook.com/sharer.php?u=<?php echo $post_link;?>" title="Connect with our Facebook" class="social_link" target="_blank">
                                                                        <i class="fa fa-facebook"></i>
                                                                        <span class="sr-only">Follow us on Facebook</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_link;?>" title="Connect with our LinkedIn" class="social_link" target="_blank">
                                                                        <i class="fa fa-linkedin"></i>
                                                                        <span class="sr-only">Follow us on LinkedIn</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://plus.google.com/share?url=<?php echo $post_link;?>" title="Follow us on Google+" class="social_link" target="_blank">
                                                                        <i class="fa fa-google-plus"></i>
                                                                        <span class="sr-only">Follow us on Google+</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                                
                                                        </aside>
                                                        <nav class="article-main-action-wr">
                                                            <h5 class="text-indent">Article Action</h3>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <a href="<?php echo $post_link?>#print" target="_blank" class="text-link-light accent-icon link-block" aria-label="Print Article">
                                                                        <i class="fa fa-print"></i>
                                                                        <span class="btn-text">Print Article</span>
                                                                    </a>
                                                                </div>
                                                                <?php if($sh_events_ticket!=""){?>
                                                                <div class="col-sm-12">
                                                                    <a href="<?php echo $sh_events_ticket?>" class="text-link-light accent-icon link-block" aria-label="Print Article">
                                                                        <i class="fa fa-ticket"></i>
                                                                        <span class="btn-text">Get tickets</span>
                                                                    </a>
                                                                </div>
                                                                <?php } ?>
                                                                <?php
                                                                if(!$register_enable)
                                                                    {
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                if ( is_user_logged_in() ) {  
                                                                ?>
                                                                <div class="col-sm-12">
                                                                    <a data-id="<?php echo get_the_ID();?>" href="javascript:void(0);" class="text-link-light accent-icon link-block request-attend" aria-label="Request to attend">
                                                                        <i class="fa fa-ticket"></i>
                                                                        <span class="btn-text">Book your seat</span>
                                                                    </a>
                                                                </div>
                                                                <?php 
                                                                    
                                                                    
                                                                } else { ?>
                                                                 <div class="col-sm-12">
                                                                    <a href="javascript:void(0);" class="text-link-light accent-icon link-block request-attend-login" aria-label="Request to attend">
                                                                        <i class="fa fa-ticket"></i>
                                                                        <span class="btn-text">Book your seat</span>
                                                                    </a>
                                                                </div>
                                                                 <?php 
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </nav>
                                                        <div class="return-wr">
                                                            <a href="<?php echo home_url('events') . '/' . $cat_slug ; ?>" class="btn btn-primary" aria-label="return" >
                                                                <span class="btn-text xspaddingright"> Return to Events</span> 
                                                                <span class=" icon-carets icon-carets-left"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-7 article-content-wr- scrollbar-inner">
                                                <div class="main-article-wr">
                                                    <div class="dummy-header">
                                                        <div class="event-info-wr">
                                                            <time datetime="2017-09-29 01:00"><?php echo $sh_events_display_date;?></time>
                                                            <span class="event-separator">//</span>
                                                            <address><?php echo $sh_events_location;?></address>
                                                        </div>
                                                        <p class="dummy-label"><?php echo $post_title;?></p>
                                                    </div>
                                                    <div class="single-event-content-wr">
                                                        <?php echo $post_content;?>
                                                    </div>
                                                    <footer class="article-tags-wr" itemscope itemtype="http://schema.org/keywords">
                                                        <h6 class="tags-label">Tags:</h6>
                                                        <ul class="article-tags">
                                                            <?php
                                                                $posttags = $post_tags;
                                                                if ($posttags) {
                                                                    foreach($posttags as $tag) {
                                                            ?>
                                                            <li><?php echo $tag->name;?></li>
                                                            <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </ul>
                                                    </footer>
                                                    <aside class="primary-wr">
                                                        <h3 class="aside-title">Related Events</h3>
                                                        <?php
                                                $args = array(
                                                    'post_type'=> 'events',
                                                    'post_status' => 'publish',
                                                    'orderby'=> 'menu_order',
                                                    'order'    => 'ASC',
                                                    'posts_per_page' => 1,
                                                    'post__not_in' => array($post_id),
                                                );
                                                $wp_query = new WP_Query($args);
                                                while ($wp_query->have_posts()) : $wp_query->the_post();
                                                    $sh_events_location = get_post_meta(get_the_ID(), 'sh_events_location', $single = true);
                                                    $sh_events_display_date = get_post_meta(get_the_ID(), 'sh_events_display_date', $single = true);
                                                ?>
                                                <article>
                                                            <header>
                                                                <div class="event-info-wr">
                                                                    <time datetime="2017-09-29 01:00"><?php echo $sh_events_display_date;?></time>
                                                                    <span class="event-separator">//</span>
                                                                    <address><?php echo $sh_events_location;?></address>
                                                                </div>
                                                                <h3><?php echo get_the_title();?></h3>
                                                            </header>
                                                            <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                                                            <p>
                                                                <a href="<?php echo the_permalink();?>" title="Read more" data-rel="modal:open" class="btn btn-outline-light btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
                                                            </p>
                                                        </article>
                                                <?php 
                                                    endwhile;
                                                ?>
                                                    </aside>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </article>
</main>
<?php
if($popup!="popup")
{
	get_footer();
}
else
{
    get_template_part( 'templates/popup-footer' );
}
 ?>