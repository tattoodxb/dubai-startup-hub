<?php

/* Template Name: Education Inner Page*/
$template_url = get_template_directory_uri();
get_header();
$post_id = get_the_ID();
$post_type = get_post_type();
$post_name = get_post($post_id)->post_name;
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Login/Register</h4>
      </div>
      <div class="modal-body">
        Interested in being a guest speaker? (Login or Register)
      </div>
    </div>
  </div>
</div>
<section id="section-events" class="section-events section-programmes single-event market-access" role="region">
	<div class="container">
		<div class="row">
			<?php the_content(); ?>			
		</div>
	</div>
	<div class="container">
		<br><br>
		
		<?php
		if($post_type == 'page' && $post_name == 'university-campaigns')
		{
		?>
		<div class="row">
			<div class="col-sm-12 col-xs-12"><h2 style="text-align: center;"><strong>Become involved</strong></h2></div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<div class="panel panel-secondary lgmargintop">
				<div class="panel-heading"><br>Startups<br><p style="text-transform:none;">Join as guest speaker</p></div>
				<div class="panel-body">
					<?php
                            if ( is_user_logged_in() ) {
                                ?>
					<form id="ma_form_enquiry" class="col-sm-11 col-centered" action="" method="post" novalidate="novalidate">
						<p><button class="btn col-centered block btn-block btn-primary register-interest" type="submit" data-id="<?php echo get_the_ID();?>" data-action="save_guest_speaker">Click here</button><br></p>
					</form>
				<div id="ma_return_msg"></div>
				<?php
                            } else { ?>
                            	<p><button class="btn col-centered block btn-block btn-primary" type="submit" data-toggle="modal" data-target="#myModal">Click here</button><br></p>
                            <?php
                            } ?>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="panel panel-secondary lgmargintop">
				<div class="panel-heading"><br>Universities<p style="text-transform:none;">Wishing to host us?</p></div>
				<div class="panel-body">
					<form class="col-sm-11 col-centered" action="mailto:dubaistartuphub@dubaichamber.com?subject=Universities wishing to host us" method="post">
					<p><button class="btn col-centered block btn-block btn-primary" type="submit">Click here</button><br></p>
					</form>
				</div>
			</div>
			</div>
		</div>
		<?php
		}
		?>
		<?php
		if($post_type == 'page' && $post_name == 'competitions')
		{
		?>
		<div class="row">
			<div class="col-sm-12 col-xs-12"><h2 style="text-align: center;"><strong>Become involved</strong></h2></div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="panel panel-secondary lgmargintop">
				<div class="panel-heading"><br>Startups<br><p style="text-transform:none;">Interested in hearing more on upcoming Competitions</p></div>
				<div class="panel-body">
					<?php
                            if ( is_user_logged_in() ) {
                                ?>
					<form id="ma_form_enquiry" class="col-sm-11 col-centered" action="" method="post" novalidate="novalidate">
						<p><button class="btn col-centered block btn-block btn-primary register-interest" type="submit" data-id="<?php echo get_the_ID();?>" data-action="save_competition">Click here</button><br></p>
					</form>
				<div id="ma_return_msg"></div>
				<?php
                            } else { ?>
                            	<p><button class="btn col-centered block btn-block btn-primary" type="submit" data-toggle="modal" data-target="#myModal">Click here</button><br></p>
                            <?php
                            } ?>
				</div>
			</div>
		</div>
		</div>
		<?php
		}
		?>
		<?php
		if($post_type == 'page' && $post_name == 'expo2020')
		{
		?>
		<div class="row">
			<div class="col-sm-12 col-xs-12"><h2 style="text-align: center;"><strong>Learn more about EXPO</strong></h2></div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="panel panel-secondary lgmargintop">
				<div class="panel-heading"><br>Expo 2020<br><p style="text-transform:none;">20 October, 2020 – 10 April, 2021</p></div>
				<div class="panel-body">
					
                            	<p><a href="https://www.expo2020dubai.com/" target="_blank" class="btn col-centered block btn-block btn-primary">Click here</a><br></p>
                            
				</div>
			</div>
		</div>
		</div>
		<?php
		}
		?>
		<?php
		if($post_type == 'page' && $post_name == 'dubai-startup-hub-workshops-trainings')
		{
		?>
		<div class="row">
			<div class="col-sm-12 col-xs-12"><h2 style="text-align: center;"><strong>Become involved</strong></h2></div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="panel panel-secondary lgmargintop">
				<div class="panel-heading"><br>Entrepreneurs and Students<br><p style="text-transform:none;">looking to attend our workshops and trainings</p></div>
				<div class="panel-body">
					<form class="col-sm-11 col-centered" action="mailto:dubaistartuphub@dubaichamber.com?subject=Entrepreneurs and Students looking to attend our workshops and trainings" method="post">
					<p><button class="btn col-centered block btn-block btn-primary" type="submit">Click here</button><br></p>
					</form>
                            
				</div>
			</div>
		</div>
		</div>
		<?php
		}
		?>
	</div>
</section>
<div class="page-hero-nav">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-xs-6" style="margin-bottom:15px;">                                          
				<a href="<?php echo home_url('entrepreneur-education');?>" class="btn btn-primary dynamic-home-link-" aria-label="Return to Education">
					<span class="btn-text xspaddingright">Return to Education</span> 
					<span class=" icon-carets icon-carets-left"></span>
				</a>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php get_footer(); ?>