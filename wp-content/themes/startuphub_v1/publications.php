<?php
/* Template Name: Publications Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<section id="section-events" class="section-updates section-feeds section-feeds-cols section-feeds-cols-3" role="region">
        <div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
              <h2 class="section-title fx main-title text-center" data-animate="fadeInUp">View our library below</h2></div>
              <div class="same-height-all">              
                <div class="col-sm-12">
                  <div class="updates-articles-wr">                    
                    <?php
                    $current_date = date('Y-m-d H:i:s');
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $args = array(
              'post_type'=> array('publications'),
              'post_status' => 'publish',
              'orderby'=> 'post_date',
              'order'    => 'DESC',
              'posts_per_page' => 9,
              'paged' => $paged
          );

                    ?>
                    <div class="row same-height-all">

                      <?php          
          $wp_query = new WP_Query($args);
          $i=1;
          while ($wp_query->have_posts()) : $wp_query->the_post();
		 
          if ( !regUserOnly_can_see( $post->ID ) ) continue;
          ?>
                      
                      <div class="col-sm-4 fx " data-animate="fadeInUp" >
                        <article>
                          <div class="row">
                            <div class="col-sm-12">
                              <figure>
                                <?php 
                                 the_post_thumbnail('medium', ['class' => 'img-responsive width100p', 'title' => get_the_title()]);
                                 ?>
                                <figcaption class="text-indent"><?php echo get_the_title();?></figcaption>
                              </figure>
                            </div>
                            <div class="col-sm-12">
                              <div class="updates-info-wr">
                                <header>                                  
                                  <h3 class="same-height-text"><?php echo get_the_title();?></h3>
                                </header>
                                <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                                <p>
                                  <a href="<?php echo pdf_file_url();?>" target="_blank" title="Read more" data-rel="modal:open" class="btn btn-outline-primary-full btn-sm">Read more - English  <i class="fa fa-caret-right fa-fw"></i></a> 
                                  <?php
                                  if(pdf_file_ar_url() != "" && pdf_file_ar_url() != "SELECT pdf FILE")
                                  {
                                  ?>
                                  <a href="<?php echo pdf_file_ar_url();?>" target="_blank" title="Read more" data-rel="modal:open" class="btn btn-outline-primary-full btn-sm">Read more - Arabic  <i class="fa fa-caret-right fa-fw"></i></a> 
                                  <?php
                                  }
                                  ?>
                                </p>
                              </div>
                            </div>
                          </div>
                        </article>
                      </div>

                      <?php
            $i++;
					echo '<!-- test:'.$i.'-->';
            endwhile; 
            ?>
            <?php
            if($i==1)
                {
                    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue"><br>No result found for your search, please refine your search</div>';
                }
            ?>
                      
                    </div>
                    
                    <div class="row">
                      <div class="col-sm-4 col-centered fx" data-animate="fadeInUp">
                        <div class="text-center"><?php 
                                  $pag_args = array(
                                      'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                      'total'        => $wp_query->max_num_pages,
                                      'current'      => max( 1, get_query_var( 'paged' ) ),
                                      'format'       => '?paged=%#%',
                                      'show_all'     => false,
                                      'type'         => 'list',
                                      'end_size'     => 2,
                                      'mid_size'     => 1,
                                      'prev_next'    => true,
                                      'prev_text'    => sprintf( '<i></i> %1$s', __( 'Prev', 'text-domain' ) ),
                                      'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
                                      'add_args'     => false,
                                      'add_fragment' => '',
                                      'before_page_number' => '',
                                      'after_page_number'  => ''
                                  );
                                  $return = paginate_links( $pag_args );
                                  echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return );
                              ?>
                          </div>
                      </div>
                    </div>
                    
                  </div>
                </div>
                
                
            </div>
              
              </div>
          </div>
        </div>
      </section>
<?php get_footer(); ?>