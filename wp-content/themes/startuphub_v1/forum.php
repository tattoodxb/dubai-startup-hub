<?php
/* Template Name: Forum Template */
$template_url = get_template_directory_uri();
get_header(); 

 //var_dump(bbp_get_forum_id()); exit;
 $parent_post = bbp_get_forum_id();
 $forum_title = 'Recent discussions';
  $trig = false;
  if($parent_post == 0)
  {
    $parent_post = 'any';
  }
  else
  {
    $trig = true;
    $forum_title = 'Discussions: ' . bbp_get_forum_title($parent_post);
  }
$keyword = "";
if(isset($_GET['keyword']))
{
    $keyword = $_GET['keyword'];    
    $forum_title = "Forums";
}

?>

<section id="section-forum" class="section-forum section-feeds " role="region">
	<div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
		<div class="bg-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="section-title fx" data-animate="fadeInUp"><?php echo $forum_title;?></h2>

				</div>
				<div class="col-sm-12">
					<div class="forum-post" id="forum-post">

						<?php
						  if ( is_user_logged_in() ) {
							 $post_per_page = 10;
						  }
						  else {
							  $post_per_page = 4;
						  }
						  $args = array( 'author' => 0, 'show_stickies' => false, 'order' => 'DESC', 'post_parent' => $parent_post, 'posts_per_page' => $post_per_page );
						  if($keyword!="")
						  {
							$args['s'] = $keyword;
						  }
						  if ( bbp_has_topics( $args ) )
							bbp_get_template_part( 'bbpress/loop', 'topics' );
						 ?>                

					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>

<script type="text/javascript">
    $(window).load(function() {
    <?php 
    if($trig)
    {

    ?>
        $('.scroll-DownBtn').trigger('click');

      <?php
        }
    ?>
    });
</script>