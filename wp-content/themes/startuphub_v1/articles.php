<?php 
/* Template Name: Articles Template */
get_header();

?>
<section id="section-events" class="section-updates section-feeds section-feeds-cols section-feeds-cols-3" role="region">
        <div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
              <h1 class="section-title fx main-title" data-animate="fadeInUp">Articles</h1></div>
              <div class="">              
                <div class="col-sm-12">
                  <div class="updates-articles-wr same-height-article">                                        

                    <div class="row">


                      <?php  
          $template_url = get_template_directory_uri();              
          $categories = get_categories(array('parent'  => 0));
          foreach($categories as $category) {

          ?>
                    
                      <div class="col-sm-4 fx " data-animate="fadeInUp">
                        <article>
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="updates-info-wr">
                                <header>
                                 <span class="svg-wr">
                                   <img class="logo-svg svg-img" data-src="<?php echo $template_url;?>/images/category/icon-<?php echo str_replace(" ", "-", strtolower($category->name));?>.svg" src="<?php echo $template_url;?>/images/empty.png" onerror="this.src='<?php echo $template_url;?>/images/empty.png'" alt="Dubai Startup Hub">
                                 </span>
                                  <h3 class="same-height-text"><?php echo $category->name;?></h3>
                                </header>
                                <p><?php echo $category->description;?></p>
                                <p>
                                  <a href="<?php echo get_category_link($category->term_id);?>" title="Read more" data-rel="modal:open" class="btn btn-outline-primary-full btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
                                </p>
                              </div>
                            </div>
                          </div>
                        </article>
                      </div>

                      <?php
                    }
            ?>
                      
                    </div>
                    
                  </div>
                </div>
                
                
            </div>
              
              </div>
          </div>
        </div>
      </section>
<?php get_footer(); ?>