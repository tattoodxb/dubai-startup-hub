<?php 
$template_url = get_template_directory_uri();
wp_footer(); 
?>
</main>

<?php if(shub_show_return_home_button()): ?>
<div class="page-hero-nav">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-xs-6" style="margin-bottom:15px;">                                          
				<a href="<?php echo home_url('');?>" class="btn btn-primary dynamic-home-link-" aria-label="Return to Home">
					<span class="btn-text xspaddingright">Return to Home</span> 
					<span class=" icon-carets icon-carets-left"></span>
				</a>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php endif; ?>
<footer class="site-footer">
    <div class="bg-wr bg-wr-secondary-full" style="background-image:url();">
        <div class="bg-overlay bg-overlay-secondary-full"></div>
        <div class="container">
            <div class="row">            
                <div class="col-sm-4 fx" data-animate="fadeInUp">
                    <figure>
                        <span class="svg-wr footer-logo">
                            <img class="logo-svg svg-img" data-src="<?php echo $template_url;?>/images/logo.svg" src="<?php echo $template_url;?>/images/empty.png" onerror="this.src='<?php echo $template_url;?>/images/logo.png'" alt="Dubai Startup Hub">
                        </span>
                        <figcaption class="">Dubai Startup Hub is a semi government initiative rooted in the Dubai Chamber</figcaption>
                    </figure>                    
                    <aside class="social-links-wr">
                        <h4 class="text-indent">Social Media Links</h4>
                        <?php $pu_theme_options = get_option('pu_theme_options'); ?>
                        <ul>
                            <li>
                                <a href="<?php echo $pu_theme_options['twitter_link'];?>" title="Follow us on Twitter" target="_blank">
                                    <i class="fa fa-twitter"></i>
                                    <span class="sr-only">Follow us on Twitter</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $pu_theme_options['facebook_link'];?>" title="Connect with our Facebook" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                    <span class="sr-only">Connect with our Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $pu_theme_options['linkedin_link'];?>" title="Connect with our LinkedIn" target="_blank">
                                    <i class="fa fa-linkedin"></i>
                                    <span class="sr-only">Connect with our LinkedIn</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $pu_theme_options['instagram_link'];?>" title="Follow us on Instagram" target="_blank">
                                    <i class="fa fa-instagram"></i>
                                    <span class="sr-only">Follow us on Instagram</span>
                                </a>
                            </li>
                        </ul>
                            
                    </aside>
                </div>
                <div class="col-sm-4 col-xs-6 fx" data-animate="fadeInUp">
                        <h4>Sitemap</h4>
                        <ul>
                            <?php strip_tags(wp_nav_menu( array('theme_location' => 'main_menu', 'depth'=> 0, 'items_wrap' => '%3$s', 'container'=> false)), '<a>' ); ?>
                        </ul>
                </div>
                <div class="col-sm-4 col-xs-6 fx" data-animate="fadeInUp">
                    <aside class="quick-links hide">
                        <h4 class="title">Quick Links</h4>
                        <ul>
                            <?php strip_tags(wp_nav_menu( array('theme_location' => 'category_menu', 'depth'=> 0, 'items_wrap' => '%3$s', 'container'=> false, 'before' => '')), '<a>' ); ?>
                        </ul>
                    </aside>
                </div>
            
            </div>
        </div>
    </div>
</footer>
<aside class="modal-wr"></aside>
<aside id="alertmodal" class="modal modal-error <?php if (dshub_has_notices('error')) echo ' show-alert shub-notice-error '; ?> <?php if (dshub_has_notices('success')) echo ' show-alert shub-notice-success '; ?>">
    <div class="alert <?php if (dshub_has_notices('error')) echo 'alert-danger'; ?> <?php if (dshub_has_notices('success')) echo 'alert-success'; ?>">
        <?php if (dshub_has_notices('error')) dshub_show_notices('error'); ?>
        <?php if (dshub_has_notices('success')) dshub_show_notices('success'); ?>
    </div>
</aside>

<?php 
    if ( !is_user_logged_in() ) {
      //get_template_part( 'templates/signup-footer' );
    }
?>

<?php //shub_show_forum_live(); ?>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo $template_url;?>/js/jquery.min.js"></script>
<script src="<?php echo $template_url;?>/js/bootstrap-select.min.js"></script>
<script src="<?php echo $template_url;?>/js/bootstrap.min.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.visible.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.validate.min.js"></script>
<script src="<?php echo $template_url;?>/js/jquery-validate.bootstrap-tooltip.min.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.scrollbar.min.js"></script>
<script src="<?php echo $template_url;?>/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo $template_url;?>/js/locales/bootstrap-datepicker.en-GB.min.js"></script>
<script src="<?php echo $template_url;?>/js/bootstrap-datepicker.min.js"></script>


<script src="<?php echo $template_url;?>/js/jquery.matchHeight-min.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.dotdotdot.min.js"></script>
<script src="<?php echo $template_url;?>/js/jquery.modal.min.js"></script>
<script src="<?php echo $template_url;?>/js/slick.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo $template_url;?>/js/ie10-viewport-bug-workaround.js"></script>

<script src="<?php echo $template_url;?>/js/main.js?v=<?php echo date('YmdHis')?>"></script>
<script src="<?php echo $template_url;?>/js/custom.js?v=<?php echo date('YmdHis')?>"></script>
<script type="text/javascript">
$(window).load(function() {
<?php 
if(isset($_GET["register"]))
{
    $register = $_GET["register"];

    if($register == "error")
    {

?>
    $(".registrationbox").show();
<?php
    }
    }
?>
<?php 
if(isset($_GET["tag"]))
{
    $tag = $_GET["tag"];
    $tag=explode(",", $tag);
    $tag_list=implode("','", $tag);

?>
    $('.selectpicker').selectpicker('val', ['<?php echo $tag_list?>']);
<?php
    }    
?>
});
</script>
</body>
</html>