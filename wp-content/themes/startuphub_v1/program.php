<?php

/* Template Name: Program Template */
$template_url = get_template_directory_uri();
/*if ( is_user_logged_in() ) {
}
else
{
    wp_redirect(home_url());
    exit;
}*/
get_header();

?>

	<section id="section-programmes" class="section-updates section-feeds section-feeds-cols section-feeds-cols-3" role="region">

		<div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
			<div class="bg-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="section-title fx main-title" data-animate="fadeInUp">
							Membership Details
						</h2>
						<p class="fx main-desc" data-animate="fadeInUp">If you are an Entrepreneur, with an idea or operating business and would like to learn more on how our membership can benefit you, please read below</p>
					</div>
					<div class="col-sm-12">

						<div class="updates-articles-wr member-details-wr startup-collapsable">

							<div class="row same-height-all">

								<div class="col-sm-12 fx  animated fadeInUp" data-animate="fadeInUp">
									<article>
										<div class="row">
											<div class="col-sm-12 hide">
												<figure>
													
													<img width="560" height="364" src="http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-entrepreneur.jpg"
														    class="img-responsive" alt="" srcset="http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-entrepreneur.jpg 560w, http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-entrepreneur.jpg 300w"
														    sizes="(max-width: 560px) 100vw, 560px">
													
													<figcaption class="text-indent">I’m an Entrepreneur</figcaption>
												</figure>
											</div>
											<div class="col-sm-12">
												<div class="updates-info-wr">
													<header>
														<h3 class="same-height-text">I’m an Entrepreneur</h3>
													</header>
													<h6><strong>Definition</strong></h6>
													<p>A UAE-based individual (UAE national, expat) interested in an entrepreneurship path and has a business idea/ operating business within one of the UAE priority sectors, Education | Electronic Security | Food Manufacturing | Fabricated Materials | Healthcare | ICT (Tech and Digital Media) | Professional Services | Sustainability | Environment | Climate Change | Transportation | Logistics | Aerospace</p>
													<h6><strong>Introduction</strong></h6>
													<p>Do you want to turn your big idea into a reality? Are you facing challenges in making this happen? Whether you are looking for role models, financing, talent, clients, specific expertise or detailed information about regulations, Dubai Startup Hub is here to support you</p>
													<h6>Member Benefits</h6>
													<ul>
														<li>Mentorship sessions with Dubai Startup Hub and leading industry experts</li>
														<li>Complimentary pass to Dubai Startup Hub events</li>
														<li>Discount to Dubai Chamber paid events</li>
														<li>MarketAccess program</li>
														<li>Investor Pitching</li>
														<li>UAE Overseas Missions</li>
														<li>Talent program</li>
														<li>Support in exhibiting at selected by Dubai Startup Hub events</li>
													</ul>
												</div>
											</div>
										</div>
									</article>
								</div>
								<!--
								<div class="col-sm-6 fx  animated fadeInUp" data-animate="fadeInUp">
									<article>
										<div class="row">
											<div class="col-sm-12">
												<figure>
													
														<img width="560" height="364" src="http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-startup-use-this.jpg" class="img-responsive"
														    alt="" srcset="http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-startup-use-this.jpg 560w, http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-startup-use-this.jpg 300w"
														    sizes="(max-width: 560px) 100vw, 560px">
													
													<figcaption class="text-indent">I’m a Startup</figcaption>
												</figure>
											</div>
											<div class="col-sm-12">
												<div class="updates-info-wr">
													<header>
														<h3 class="same-height-text">I’m a Startup</h3>
													</header>
													<h6>Definition</h6>

													<p>A technology enabled or other innovative enterprise, registered in the UAE with high potential of growth, under
														3 years’ old</p>

													<h6><strong>Introduction</strong></h6>
													<p>Your business is set up, you have a license in hand, now it’s time to grow your team and customers, both locally
														and internationally. </p>

													<h6>Package benefits</h6>
													<ul>
														<li>Mentorship sessions with Dubai Startup Hub and leading industry experts</li>
														<li>Complimentary pass to Dubai Startup Hub events</li>
														<li>Discount to Dubai Chamber paid-events</li>
														<li>MarketAccess program</li>
														<li>Investor Pitching</li>
														<li>UAE Overseas Missions</li>
														<li>Internship program</li>
														<li>Support in exhibiting at selected by Dubai Startup Hub events</li>
													</ul>
												</div>
											</div>
										</div>
									</article>
								</div>

								 <div class="col-sm-4 fx  animated fadeInUp" data-animate="fadeInUp">
									<article>
										<div class="row">
											<div class="col-sm-12">
												<figure>
													
														<img width="560" height="364" src="http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-corporate.jpg" class="img-responsive"
														    alt="" srcset="http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-corporate.jpg 560w, http://www.dubaichamber.com/dubaistartuphub/wp-content/uploads/2018/03/im-a-corporate.jpg 300w"
														    sizes="(max-width: 560px) 100vw, 560px">
													
													<figcaption class="text-indent">I’m a Corporate</figcaption>
												</figure>
											</div>
											<div class="col-sm-12">
												<div class="updates-info-wr">
													<header>
														<h3 class="same-height-text">I’m a Corporate</h3>
													</header>
													<h6>Definition</h6>
													<p>A UAE-based organization (for international organisations, local legal representative is required) interested in enhancing its business model, to grow market by collaborating with start-ups</p>

													<h6>Introduction</h6>
													<p>Developing a world-class startup ecosystem in Dubai is dependent on corporates being active in this space, we need to continuously strive for Dubai to be a role model for entrepreneurial leadership, creating an attractive business climate for startups to set up.
Working with a startup is practical and makes economic sense, providing a short cut to the latest innovations available in the market.
</p>

													<h6>Package <strong>benefits</strong></h6>
													<ul>
														<li>MarketAccess program </li>
														<li>Investor Pitching</li>
														<li>Access to the Dubai Startup Hub innovative Startup community</li>
														<li>Guest speaker opportunities at Dubai Startup Hub event</li>
														<li>Complimentary seats at Dubai Startup Hub paid events</li>
														<li>Panel / judge participation </li>
													</ul>
												</div>
											</div>
										</div>
									</article>
								</div> -->

							</div>
						</div>


						

					</div>
				</div>
			</div>
		</div>

		<div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
			<div class="bg-overlay-secondary-light"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="section-title fx main-title" data-animate="fadeInUp">
							Apply Now
						</h2>
						<p class="fx main-desc" data-animate="fadeInUp">Please complete the below form to submit your application request</p>
					</div>
					<div class="col-sm-12">
					<div class="apply-here-wr">
						<?php the_content(); ?>						
						<div role="form" lang="en-US" dir="ltr">
						<div class="screen-reader-response"></div>
						<form action="<?php echo home_url('membership');?>" id="membership-form" class="membership-form" method="post" enctype="multipart/form-data" novalidate="novalidate">
							<div class="col-sm-12 apply-form-wr">
								<div class="row">
									<div class="col-sm-12"><h3>Personal information</h3><hr size="1"></div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group"> 
											<label class="control-label col-sm-4" for="firstname">FIRST NAME<span class="required">*</span></label>
											<div class="col-sm-8"> <span class=" firstname"><input type="text" name="firstname" value="" size="40" class="form-control" id="firstname" aria-required="true" aria-invalid="false"></span> </div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group"> <label class="control-label col-sm-4" for="lastname">LAST NAME<span class="required">*</span></label><div class="col-sm-8"> <span class=" lastname"><input type="text" name="lastname" value="" size="40" class="form-control" id="lastname" aria-required="true" aria-invalid="false"></span> </div></div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group"> <label class="control-label col-sm-4" for="email">EMAIL<span class="required">*</span></label><div class="col-sm-8"> <span class=" email"><input type="email" name="email" value="" size="40" class=" wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="email" aria-required="true" aria-invalid="false"></span> </div></div>
									</div>
									<div class="col-sm-6">
										<div class="form-group"> <label class="control-label col-sm-4" for="mobile-number">MOBILE NUMBER<span class="required">*</span></label><div class="col-sm-8"><span class="mobile-number"><input type="text" name="mobilenumber" value="" size="40" class="form-control" id="mobilenumber" aria-required="true" aria-invalid="false" placeholder="+971 xx xxx xxxx"></span> </div></div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group"> <label class="control-label col-sm-4" for="po-box">NATIONALITY<span class="required">*</span></label>
										<div class="col-sm-8"><span class=" nationality"><select name="nationality" class="  form-control" id="nationality" aria-required="true" aria-invalid="false"><option value="">---</option><?php
                                            global $wpdb;
                                            $table_name = $wpdb->prefix . 'country_list';
                                            $query = $wpdb->get_results("SELECT id, country FROM $table_name ORDER BY sort_order");
                                            foreach($query as $row)
                                            {
                                                echo '<option value="'.$row->country.'">'.$row->country.'</option>';
                                            }
                                            ?></select></span> </div>
										</div>
									</div>
									
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group checkbox-group-wr"> <label class="control-label col-sm-3" for="source">HOW DID YOU HEAR ABOUT US?<span class="required">*</span></label>
										<div class="col-sm-9"><span class=" source"><span class=" wpcf7-radio radio-group"><span class="wpcf7-list-item first"><label><input type="radio" name="source" value="Social Media"><span class="wpcf7-list-item-label">Social Media</span></label></span><span class="wpcf7-list-item"><label><input type="radio" name="source" value="Word of Mouth"><span class="wpcf7-list-item-label">Word of Mouth</span></label></span><span class="wpcf7-list-item"><label><input type="radio" name="source" value="Newsletter"><span class="wpcf7-list-item-label">Newsletter</span></label></span><span class="wpcf7-list-item"><label><input type="radio" name="source" value="Direct Mail"><span class="wpcf7-list-item-label">Direct Mail</span></label></span><span class="wpcf7-list-item last"><label><input type="radio" name="source" value="Other" checked="checked"><span class="wpcf7-list-item-label">Other</span></label></span></span></span> </div>
										</div>
									</div>									
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
										<div class="col-sm-12"> <label for="overview">BRIEF OVERVIEW OF YOUR BUSINESS<span class="required">*</span></label> <span class=" overview"><textarea name="overview" cols="40" rows="6" class=" wpcf7-textarea wpcf7-validates-as-required form-control" id="overview" aria-required="true" aria-invalid="false" placeholder="In less than 100 words"></textarea></span></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group checkbox-group-wr"> <label class="control-label col-sm-3" for="trade_licence">DO YOU HAVE A TRADE LICENSE?<span class="required">*</span></label>
										<div class="col-sm-9"><span class="trade_licence"><span class="wpcf7-radio radio-group"><span class="wpcf7-list-item first"><label><input type="radio" name="trade_licence" value="Yes"><span class="wpcf7-list-item-label">Yes</span></label></span><span class="wpcf7-list-item"><label><input type="radio" name="trade_licence" value="No"><span class="wpcf7-list-item-label">No</span></label></span></span></span> </div>
										</div>
									</div>									
								</div>
								<div id="companyinfo" class="ignore" style="display: none;">
									<div class="row">
										<div class="col-sm-12"><h3>Startup Information</h3><hr size="1"></div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="companyname">COMPANY NAME<span class="required">*</span></label><div class="col-sm-8"><span class=" companyname"><input type="text" name="companyname" value="" size="40" class="form-control" id="companyname" aria-required="true" aria-invalid="false"></span> </div></div>
										</div>
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="job-title">JOB TITLE<span class="required">*</span></label><div class="col-sm-8"> <span class="jobtitle"><input type="text" name="jobtitle" value="" size="40" class="form-control" id="jobtitle" aria-required="true" aria-invalid="false"></span> </div></div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="website">WEBSITE<span class="required">*</span></label>
											<div class="col-sm-8"> <span class=" website"><input type="text" name="website" value="" size="40" class="form-control" id="website" aria-required="true" aria-invalid="false"></span> </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="industry-sector">INDUSTRY SECTOR<span class="required">*</span></label><div class="col-sm-8"> <span class=" industry-sector"><select name="industrysector" class="  form-control" id="industrysector" aria-required="true" aria-invalid="false"><option value="">---</option><?php
	                                            global $wpdb;
	                                            $table_name = $wpdb->prefix . 'industry';
	                                            $query = $wpdb->get_results("SELECT id, industry_name FROM $table_name");
	                                            foreach($query as $row)
	                                            {
	                                                echo '<option value="'.$row->industry_name.'">'.$row->industry_name.'</option>';
	                                            }
	                                            ?></select></span> </div></div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="country">COUNTRY<span class="required">*</span></label>
											<div class="col-sm-8"> <span class=" country"><select name="country" class="  form-control" id="country" aria-required="true" aria-invalid="false"><option value="">---</option><?php
	                                            global $wpdb;
	                                            $table_name = $wpdb->prefix . 'country_list';
	                                            $query = $wpdb->get_results("SELECT id, country FROM $table_name");
	                                            foreach($query as $row)
	                                            {
	                                                echo '<option value="'.$row->country.'">'.$row->country.'</option>';
	                                            }
	                                            ?></select></span> </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="trade-license-file">TRADE LICENSE</label>
											<div class="col-sm-8"> <span class="tradelicense_file"><input type="file" name="tradelicense_file" size="40" class=" wpcf7-file upload-btn" accept=".pdf,.doc,.jpg" aria-invalid="false"></span> </div>
											</div>
											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="year_of_incoporation">YEAR OF INCORPORATION<span class="required">*</span></label>
											<div class="col-sm-8"> <span class="year_of_incoporation"><input type="text" name="year_of_incoporation" value="" size="40" class="form-control" id="year_of_incoporation" placeholder="e.g. 1979" aria-required="true" aria-invalid="false"></span> </div>
											</div>
										</div>
										<div class="col-sm-6">
											
										</div>
									</div>
									<div class="row hide">
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="tradelicense_number">TRADE LICENSE #</label>
											<div class="col-sm-8"> <span class=" trade-license-number"><input type="text" name="tradelicense_number" value="" size="40" class="form-control" id="tradelicense_number" aria-invalid="false"></span> </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group"> <label class="control-label col-sm-4" for="trade-license-authority">TRADE LICENSE AUTHORITY</label>
											<div class="col-sm-8"> <span class="tradelicense_authority"><select name="tradelicense_authority" class="form-control" id="tradelicense_authority" aria-invalid="false"><option value="">---</option><option value="Al Aweer Free Zone - DUCAMZ">Al Aweer Free Zone - DUCAMZ</option><option value="Al Awir Free Zone - Textile City">Al Awir Free Zone - Textile City</option><option value="Department of Economic Development">Department of Economic Development</option><option value="e-Traders License">e-Traders License</option><option value="Deptartment of Tourism &amp; Commercial Promotion">Deptartment of Tourism &amp; Commercial Promotion</option><option value="Dubai Airport Free Zone Authority">Dubai Airport Free Zone Authority</option><option value="Dubai Aviation City Corporation">Dubai Aviation City Corporation</option><option value="Dubai Healthcare City">Dubai Healthcare City</option><option value="Dubai International Financial Centre">Dubai International Financial Centre</option><option value="Dubai Maritime City Authority">Dubai Maritime City Authority</option><option value="Dubai Maritime City Authority - Free Zone">Dubai Maritime City Authority - Free Zone</option><option value="Dubai Multi Commodities Centre">Dubai Multi Commodities Centre</option><option value="Dubai Silicon Oasis">Dubai Silicon Oasis</option><option value="Dubai Technology and Media Free Zone Authority">Dubai Technology and Media Free Zone Authority</option><option value="DUBAI WORLD TRADE CENTRE">DUBAI WORLD TRADE CENTRE</option><option value="Dubai World Central">Dubai World Central</option><option value="Government of Dubai - Techno Park">Government of Dubai - Techno Park</option><option value="International Humanitarian City Authority FZ">International Humanitarian City Authority FZ</option><option value="Jebel Ali Free Zone Authority">Jebel Ali Free Zone Authority</option><option value="Jebel Ali Free Zone Authority - Ducamz">Jebel Ali Free Zone Authority - Ducamz</option><option value="Jebel Ali Free Zone Authority - International City">Jebel Ali Free Zone Authority - International City</option><option value="Jumeirah Lakes Towers">Jumeirah Lakes Towers</option><option value="Meydan Freezone">Meydan Freezone</option><option value="Ministry of Agriculture and Fisheries">Ministry of Agriculture and Fisheries</option><option value="Ministry of Finance and Industry">Ministry of Finance and Industry</option><option value="TARAKHEES-Ports,Customs and Free Zone Corporation">TARAKHEES-Ports,Customs and Free Zone Corporation</option><option value="Tourism Trakhees Local">Tourism Trakhees Local</option><option value="Trakhees – Located Dubai">Trakhees – Located Dubai</option><option value="Dubai Creative Clusters Authority">Dubai Creative Clusters Authority</option><option value="Without License">Without License</option><option value="National Industries Park">National Industries Park</option></select></span> </div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10 text-right">
											<input type="hidden" name="action" value="save_memberform" />
                                    		<?php wp_nonce_field( 'member_register'); ?>
											<input type="submit" value="Submit Application" class="btn btn-secondary btn-member"><span class="ajax-loader"></span> </div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</form></div>
					</div>
					</div>
				</div>
			</div>
		</div>

	</section>
	<?php get_footer(); ?>