<?php
$template_url = get_template_directory_uri();
$current_user = wp_get_current_user();
$pu_theme_options = get_option('pu_theme_options');
$pro_status = $current_user->is_complete;
global $wp;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <?php /* ?> <title><?php wp_title(); ?></title> <?php */ ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php wp_head(); ?>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Chrome, Firefox OS, Opera and Vivaldi -->
    <meta name="theme-color" content="#0e2498" />
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#0e2498" />
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#0e2498" />
    <meta name="description" content="">
    <meta name="author" content="">
	 
    <link rel="icon" href="<?php echo $template_url;?>/images/favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo $template_url;?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/jquery.modal.min.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/megamenu.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/slick.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/slick-theme.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/bootstrap-datepicker3.min.css" rel="stylesheet">

    <link href="<?php echo $template_url;?>/css/animate.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo $template_url;?>/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo $template_url;?>/css/stock.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/styles.css?v=<?php echo date('YmdHis')?>" rel="stylesheet">
    <link href="<?php echo $template_url;?>/style.css" rel="stylesheet">
    <link href="<?php echo $template_url;?>/css/responsive.css?v=<?php echo date('YmdHis')?>" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo $template_url;?>/js/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      <script>
        var siteURL = '<?php echo home_url('');?>';
        var currentURL = '<?php echo home_url( $wp->request );?>';
        var templateURL = '<?php echo $template_url?>/';
        var pro_status = '<?php echo $pro_status;?>';
      </script>
      <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NHWWPG');</script>
        <!-- End Google Tag Manager -->
	  
	  <!-- Global site tag (gtag.js) - Google Ads: 779076327 -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=AW-779076327"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'AW-779076327');
		</script>
	  
	</head>
  <body <?php body_class(isset($class) ? $class : ''); ?> >
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHWWPG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header >
            <!-- navbar start-->
            <div class="navbar megamenu navbar-default navbar-fixed-top-">
                <div class="container">
                    
                    <div class="row">
                        <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-main" class="navbar-toggle fx" data-animate="fadeInUp"  aria-expanded="false" aria-label="Main Menu collapsible">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <?php
                            if ( is_user_logged_in() ) {
                                ?>
                                <div class="btn btn-outline-primary navbar-btn navbar-toggle fx welcome-btn">
                                <a href="<?php echo home_url('dashboard');?>">Welcome</a>
                            </div>
                             <?php
                            } else { ?>
                                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-login" id="hdr-btn-loginhdr-btn-login" class="btn btn-outline-primary navbar-btn navbar-toggle fx" data-animate="fadeInUp" aria-expanded="false" aria-label="Login Menu collapsible ">Login/ Register</button>
                             <?php
                                }
                                $keyword = "";
                                if(isset($_GET["keyword"]))
                                {
                                    $keyword = $_GET["keyword"];
                                }
                            ?>
                            
                            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-search" class="btn btn-outline-primary navbar-btn navbar-toggle fx" data-animate="fadeInUp" aria-expanded="false" aria-label="Search Menu collapsible ">
                                <i class="fa fa-search"></i>
                            </button>
                            <h2 class="logo-wr fx" data-animate="flipInY">
                                <a href="<?php echo home_url();?>" class="navbar-brand logo-link">
                                    <span class="svg-wr">
                                        <img class="logo-svg svg-img" data-src="<?php echo $template_url;?>/images/logo.svg" src="<?php echo $template_url;?>/images/empty.png" onerror="this.src='<?php echo $template_url;?>/images/logo.png'" alt="Dubai Startup Hub">
                                    </span>
                                </a>
                            </h2>
                        </div>
                        
                        <!-- main menu -->
                        <nav id="navbar-collapse-main" class="navbar-collapse collapse nav-main " role="navigation">
                            <h2 class="text-indent">Navigations</h2>
                            <ul class="nav navbar-nav navbar-right-">
                                <li class="dropdown megamenu-fw">
                                    <button type="button" class="tcon tcon-menu--xcross navbar-toggle dropdown-toggle navbar-toggle-show btn-main-dropdown" data-toggle="dropdown" aria-label="Main Menu show">
										<span class="tcon-menu__lines" aria-hidden="true"></span>
										<span class="tcon-visuallyhidden">toggle menu</span>
									</button>
                                    
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="row">
                                                <div class="same-height-all-menu">
                                                    <div class="col-sm-4 main-menu-outer-wr">
                                                        <div class="main-menu-wr">
                                                            <nav class="main-menu" role="navigation">
                                                                <h3 class="text-indent">Main Navigation</h3>
                                                                <ul>
                                                                    <?php strip_tags(wp_nav_menu( array('theme_location' => 'main_menu', 'depth'=> 0, 'items_wrap' => '%3$s', 'container'=> false)), '<a>' ); ?>
                                                                </ul>
                                                                <aside class="social-links-wr">
                                                                    <h4 class="text-indent">Social Media Links</h4>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="<?php echo $pu_theme_options['twitter_link'];?>" title="Follow us on Twitter">
                                                                                <i class="fa fa-twitter"></i>
                                                                                <span class="sr-only">Follow us on Twitter</span>
                                                                            </a>
																			
																			
                                                                        </li>
                                                                        <li>
                                                                            <a href="<?php echo $pu_theme_options['facebook_link'];?>" title="Connect with our Facebook">
                                                                                <i class="fa fa-facebook"></i>
                                                                                <span class="sr-only">Follow us on Facebook</span>
                                                                            </a>
														
                                                                        </li>
                                                                        <li>
                                                                            <a href="<?php echo $pu_theme_options['linkedin_link'];?>" title="Connect with our LinkedIn">
                                                                                <i class="fa fa-linkedin"></i>
                                                                                <span class="sr-only">Follow us on LinkedIn</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="<?php echo $pu_theme_options['instagram_link'];?>" title="Follow us on Instagram">
                                                                                <i class="fa fa-instagram"></i>
                                                                                <span class="sr-only">Follow us on Instagram</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                        
                                                                </aside>
                                                            </nav>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-4 quick-links-outer-wr">
                                                        <!-- <div class="primary-wr group-nav-wr rounded"> -->
														<div class="outline-primary-box group-nav-wr rounded hide">
                                                            <div class="secondary-menu-wr">
                                                                <nav class="secondary-menu" role="navigation">
                                                                    <div class="title-wr">
                                                                        <h3 class="title">Quick Links</h3>
                                                                        <p>Quickly browse through the main topics</p>
                                                                    </div>
                                                                    <ul>
                                                                        <?php strip_tags(wp_nav_menu( array('theme_location' => 'category_menu', 'depth'=> 0, 'items_wrap' => '%3$s', 'container'=> false, 'before' => '')), '<a>' ); ?>
                                                                    </ul>
                                                                </nav>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-4 outline-boxes-outer-wr">
                                                        <aside class="tool-boxes-wr ">
                                                            <h3 class="text-indent">Additional tools</h3>
                                                            <div class="same-height-all-menu">
																<section class="outline-primary-box outline-box vertical-wr">
                                                                    <div class="vertical-middle">
                                                                            <h3 class="title">Dubai Startup Hub Membership</h3>
                                                                            <p class="nomarginbottom">Join the community</p>
                                                                        	<div class="input-group-btn  text-right">
                                                                            	<a href="<?php echo home_url('');?>/membership/" class="mdmargintop btn btn-outline-primary"><b>Apply Now</b></a> 
                                                                    		</div>
                                                                </section>
                                                                <?php /*
                                                                <section class="outline-primary-box outline-box vertical-wr">
                                                                    <div class="vertical-middle">
                                                                            <h3 class="title">Keep me up to date</h3>
                                                                            <p class="nomarginbottom">Sign up for our newsletter</p>
                                                                        <form class="nopaddingall mdmargintop newsletter-form" role="Subscribe">
                                                                            <div class="input-group">
                                                                                <label class="sr-only" for="email-newsletter">Email address:</label>
                                                                                <input type="email" class="form-control" placeholder="Email address" id="email-newsletter" name="email" />
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-outline-primary" type="submit" >Subscribe</button>
                                                                                </div>
                                                                            </div>
																			<div class="response-msg text-center" style="display:none;margin-top:10px;">
																				Thank you for your subscription
																			</div>
                                                                        </form>
                                                                    </div>
                                                                </section>
																
																
																 
                                                                <section class="outline-secondary-box outline-box vertical-wr">
                                                                    <div class="vertical-middle">
                                                                        <h3 class="title">Join the conversation</h3>
                                                                        <p class="nomarginbottom">See latest discussions on the forum</p>
                                                                        <button class="btn btn-outline-primary forum_btn mdmargintop" type="button" role="button">
                                                                            Go to forum
                                                                            <span class="icon-carets smmarginleft"></span>
                                                                        </button>
                                                                    </div>
                                                                </section>
																*/ ?>
																<div class="home-btn-wr">
																	<a href="<?php echo home_url('');?>" class="btn btn-primary" aria-label="Return to Home">
																		<span class="btn-text xspaddingright">Return to Home</span> 
																		<span class=" icon-carets icon-carets-left"></span>
																	</a>
																</div>
                                                            </div>
                                                        </aside>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        
                        <!--login-->
                        <nav id="navbar-collapse-login" class="navbar-collapse collapse nav-login " >
                            <h2 class="text-indent">Login</h2>
                            <ul class="nav navbar-nav navbar-right- navbar-btn-wr-">
                                <li class="dropdown contain-menu-right">
                                    <?php
                            if ( is_user_logged_in() ) {
                                ?>
                                <div class="user_profile">
                                    <div class="col-lg-9">
                                        <div class="welcome ">Welcome</div>
                                        <div class="name"><?php loggedUser(); ?></div><br>                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <?php /* <div class="profile_pic user-controls" style="background: center url(<?php echo shub_user_avatar($user_ID); ?>);background-size: cover;cursor: pointer;"></div> */ ?>
										
										<div class="dropdown profile_pic user-controls" style="background: center url(<?php echo shub_user_avatar($user_ID); ?>);background-size: cover;cursor: pointer;">
  											<button class="btn btn-primary dropdown-toggle btn-dropdown" type="button" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											  <ul class="dropdown-menu">
												<li><a href="<?php echo home_url('dashboard');?>">Profile</a></li>
												<li><a href="<?php echo wp_logout_url(home_url()); ?>">Logout</a></li>
											  </ul>
										</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                             <?php
                            } else { ?>
                                <button data-toggle="dropdown" type="button" id="hdr-btn-login" class="btn btn-outline-primary navbar-btn dropdown-toggle btn-login-dropdown" role="button">Subscribe/Register</button>
                                <ul class="dropdown-menu login-register-wr col-sm-6">
                                        <li>
                                            <aside>
                                                <h3 class="text-indent">Subscribe/Register</h3>
                                                <div class="row">
                                                    <div class="col-sm-6 login-wr">
                                                        <section class="login-inner-wr">
                                                            <h3>Newsletter Sign-up</h3>
                                                            <form id="newsletterfrm" name="newsletterfrm" class="form-horizontal newsletter-form" role="Subscribe">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label class="sr-only">First Name:</label>
                                                                        <input type="textbox" class="form-control" id="newsletter_firstname" name="firstname" placeholder="First Name">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label class="sr-only">Last Name:</label>
                                                                        <input type="textbox" class="form-control" id="newsletter_lastname" name="lastname" placeholder="Last Name">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label class="sr-only" for="email-login">Email:</label>
                                                                        <input type="email" class="form-control" id="newsletter_email" name="email" placeholder="Email">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group"> 
                                                                    <div class="col-sm-12 ">
                                                                        <button class="btn btn-outline-primary" type="submit" ><b>Subscribe</b></button>
                                                                    </div>
                                                                </div>
                                                                <div class="response-msg text-center" style="display:none;margin-top:10px;">
                                                                                Thank you for your subscription
                                                                            </div>
                                                            </form>
                                                        </section>
                                                    </div>
                                                    <div class="col-sm-6 register-wr">
                                                        <section class="register-inner-wr">
                                                            <h3>Dubai Startup Hub Membership</h3>
                                                            <div class="row"> 
                                                                <div class="col-sm-12 ">
                                                                    <p class="nomarginbottom">Join the community</p>
                                                                    <a href="<?php echo home_url('');?>/membership/" class="mdmargintop btn btn-outline-primary" ><b>Apply Now</b></a> 
                                                                </div>
                                                            </div>
                                                            <?php
                                                            /*
                                                            ?>
                                                            <section>
                                                                <h4 class="text-indent">Social Media Sign-up/Login</h4>
                                                                <h5 class="text-indent">Sign in direcly with your social media account</h5>
                                                                <div class="row">
                                                                    <div class="social-separator-wr">
                                                                        <div class="col-sm-12">
                                                                            <hr class="separator" role="separator">
                                                                            <span class="or-text">or sign-in with</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <?php if ( !is_user_logged_in() ) { ?>
                            
                                                                            <?php do_action( 'wordpress_social_login' ); ?> 
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            <?php
                                                            */
                                                            ?>
                                                        </section>
                                                    </div>
                                                </div>
                                            </aside>
                                        </li>
                                    </ul>
                                    <?php
                                }
                                $keyword = "";
                                if(isset($_GET["keyword"]))
                                {
                                    $keyword = $_GET["keyword"];
                                }
                            ?>
                                </li>
                            </ul>
                        </nav>                        
                        <!--search-->
                        <div id="navbar-collapse-search" class="navbar-collapse collapse pull-right menu-search-wr col-sm-3 col-md-3 " >
                            <div class="menu-search-inner-wr">
                                <form class="navbar-form nopaddingall search-menu-form" role="search">
                                    <div class="input-group">
                                        <label class="sr-only" for="input-search">Search:</label>
                                        <input type="text" class="form-control" placeholder="Search" id="top_search" name="top_search" value="<?php echo $keyword;?>" >
                                        <div class="input-group-btn">
                                            <button class="btn btn-default top_search_button" type="submit" aria-label="search" ><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
					</div>
                </div>
            </div>
            <!--register-->
            <?php
            if (!is_user_logged_in() ) {
            ?>
            <div class="registrationbox" style="display: none;">
                    <a href="javascript:void(0)" class="close-modal reg-close" style="float: right;"><span class="close-text">X</span></a>
                        <form id="registerfrm" name="registerfrm" method="post" enctype="multipart/form-data">
                            <div class="titleOrangeBox registerBox registerBoxTitle">
                                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Register
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-left">
                                    <?php
                                    if(isset($_GET["register"]))
                                    {
                                        if(isset($_GET["msg"]))
                                        {
                                            $msg = html_entity_decode($_GET["msg"]);
                                            if($msg == "Sorry,thatusernamealreadyexists!")
                                            {
                                                $msg = "Sorry, that username already exists!";
                                            }
                                            if($msg == "Sorry,thatemailaddressisalreadyused!")
                                            {
                                                $msg = "Sorry, that email address is already used!";   
                                            }
                                        }
                                        else
                                        {
                                            $msg = "Please check the fields";
                                        }                                    
                                    ?>
                                    <div class="error">Registration Failed: <?php echo $msg;?></div>
                                    <?php
                                    }
                                    ?>
                                    <?php /*<div class="rowField">
										<div class="user_label error"></div>
                                        <input name="Full_name" id="Full_name" class="form-control" placeholder="Full name" type="text">
                                    </div> */ ?>
									<div class="rowField">
										<div class="user_label error"></div>
                                        <input name="first_name" id="first_name" class="form-control" placeholder="First name" type="text">
                                    </div>
									<div class="rowField">
										<div class="userlast_label error"></div>
                                        <input name="last_name" id="last_name" class="form-control" placeholder="Last name" type="text">
                                    </div>
                                    <div class="rowField">
                                        <div class="email_label error"></div>
                                        <input name="Email" id="Email" class="form-control" placeholder="Email" type="text">
                                    </div>
                                    <div class="rowField">
										<small class="text-primary">Note: Password should consist of at least 6 characters and 1 uppercase letter.</small>
                                        <div class="pass_label error"></div>
                                        <input name="Password" id="Password" class="form-control" placeholder="Password" type="password">
                                    </div>
                                    <div class="rowField">
                                        <input name="CPassword" id="CPassword" class="form-control" placeholder="Confirm Password" type="password">
                                    </div>
                                    <div class="rowField">
                                        <input name="Company" id="Company" class="form-control" placeholder="Company" type="text">
                                    </div>
                                    <div class="rowField">
                                        <select name="Country" id="Country" class="form-control">
                                            <option value="">Country</option>
                                            <?php
                                            global $wpdb;
                                            $table_name = $wpdb->prefix . 'country_list';
                                            $query = $wpdb->get_results("SELECT id, country FROM $table_name ORDER BY sort_order");
                                            foreach($query as $row)
                                            {
                                                echo '<option value="'.$row->country.'">'.$row->country.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="rowField">
                                        <select name="OrganisationType" id="OrganisationType" class="form-control organisation-type">
                                            <option value="">Your organisation type</option>
                                            <option value="Startup">Startup</option>
                                            <option value="Co-working Space">Co-working Space</option>
                                        </select>
                                    </div>
                                    <div class="rowField startup-market" style="display: none">
                                        <select name="Industry" id="Industry" class="form-control field-start-market">
                                            <option value="">Your startup market</option>
                                            <?php
                                            global $wpdb;
                                            $table_name = $wpdb->prefix . 'industry';
                                            $query = $wpdb->get_results("SELECT id, industry_name FROM $table_name");
                                            foreach($query as $row)
                                            {
                                                echo '<option value="'.$row->industry_name.'">'.$row->industry_name.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="inpuNewsletterHolder">
                                     <div>
                                         <label><input type="checkbox" class="form-checkbox" name="newsletter" value="Yes"> <span class="coloringOrange signupCheck">Sign me up for the newsletter</span></label><br>
                                     </div>
                                    </div>                            
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?php /*
								<div class="form-group">
                                    <input type="file" class="thefilecss" name="avatar_file" id="avatar_file" >
                                    <div class="imageHolderUpload">Upload profile picture</div>
                                    <button type="submit" class="uploadbtn">Upload</button>
                                </div>
								*/ ?>
								<div class="form-group img-upload-wr">
									<span>Profile Image</span>
									<p class="sub-info"><small>(Maximum upload size is up to 300KB)</small></p>
									<div class="clearfix"></div>
                                    <div class="profile-pic-wrapper">
										 <input type="file" class="" name="avatar_file" id="avatar_file" accept="image/jpeg,image/png">
										 <img id="imageHoldr" class="img-holder-upload" src="#" alt="Your Image" />
										 <button type="button" class="delete-profile-pic btn btn-xs">Delete Photo</button>
									</div>
                                    <button type="submit" class="uploadbtn">Upload</button>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="action" value="register_user" />
                                    <?php wp_nonce_field( 'user_register'); ?>
                                    <input type="button" class="btn btn-primary register-submit" value="Submit">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                    <?php
                    }
                    $post_id = get_the_ID();
                    $sh_topbanner_template = get_post_meta($post_id, 'sh_topbanner_template', true);
                    $post_type = get_post_type();
                    $post_name = get_post($post_id)->post_name;

                    if($post_name == "entrepreneur-education")
                    {
                        $header_banner = "education-header.jpg";
                    }
                    else
                    {
                        $header_banner = "banner02.jpg";
                    }

                    ?>
            <!-- navbar end -->
        </header>        
        <main class="wrapper" role="main">        
            <section class="section-hero section-page-hero <?php echo is_shub_forum_page() ?>" role="region">
                <div class="bg-wr" >
                    <div class="responsive-slider banner-slider">
                        <?php do_action( 'bbp_template_notices' ); ?>
                        <?php
                            $template_part = '';
                            if(is_404())
                            {
                                $template_part = 'templates/top-banner-404';
                            }
                            else if($sh_topbanner_template != "")
                            {
                                $template_part = 'templates/'.$sh_topbanner_template;
                            }
                            else if($post_type == 'page' && ($post_name == 'membership' || $post_name == 'thank-you'))
                            {
                                $template_part = 'templates/top-banner-membership';
                            }
                            else if($post_type == 'page' && (in_array($post_name, array('emirates-nbd, pcfc, sonafi, nikai, market-access')) ) )
                            {
                                $template_part = 'templates/top-banner-marketaccess';
                            }
                            else if(is_category())
                            {
                                $template_part = 'templates/top-banner-category';
                            }
                            else if($post_type == 'forum' || $post_type == 'topic')
                            {
                                $template_part = 'templates/top-banner-forum';
                            }
                            else
                            {
                                $template_part = 'templates/top-banner1';
                            }
                        ?>
                        <?php
                        if($template_part == 'templates/top-banner1')
                        {
                            get_template_part( $template_part );
                        }
                        else
                        {
                        ?>
                        <div class="section-page-hero-content" style="background-image:url(<?php echo $template_url;?>/images/banners/<?php echo $header_banner;?>);">
                        <div class="bg-overlay-secondary"></div>
                        <div class="vertical-full-wr fx-" data-animate="fadeInUp-">
                            <div class="vertical-middle">
                                <div class="container">
                                        <?php get_template_part( $template_part ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    
                    <div class="banner-scroll-to-wr">
						<div class="banner-scroll-to-inner-wr fx" data-animate="bounceInDown">
							<a class="scroll-to" href="#section-events" >
								<span class="sr-only">Scroll down</span>
								<span class="tcon-indicator" aria-label="scroll" aria-hidden="true">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="tcon-svgchevron" viewBox="0 0 30 36">
										<path class="a3" d="M0,0l15,16L30,0"></path>
										<path class="a2" d="M0,10l15,16l15-16"></path>
										<path class="a1" d="M0,20l15,16l15-16"></path>
									</svg>
								</span>
							</a>
						</div>
					</div>
					
                </div>
            </section>