<div class="col-lg-3 col-md-4 col-sm-3 meet-us-wr">
  <aside class="primary-wr fx" data-animate="fadeInRight">
    <h3 class="title fx" data-animate="fadeInUp">Or meet us here</h3>
    <?php
        $args = array(
            'post_type'=> 'meetup',
            'post_status' => 'publish',
            'orderby'=> 'menu_order',
            'order'    => 'ASC',
            'posts_per_page' => 4,
            'paged' => get_query_var( 'paged' ),
            'meta_key' => 'sh_meetup_date',
            'meta_query' => array(
                array(
                    'key' => 'sh_meetup_date',
                    'value' => date('Y-m-d H:i:s'),
                    'compare' => '>=',
                    'type' => 'DATE'
                )
            )
        );
        $wp_query = new WP_Query($args);
        $x=1;
        while (have_posts()) : the_post();
          if($x==1)
          {
            $firstrow = 'firstrow';
          }
          else
          {
            $firstrow = ''; 
          }
        $sh_meetup_location = get_post_meta($post->ID, 'sh_meetup_location', $single = true);
        $sh_meetup_date = get_post_meta($post->ID, 'sh_meetup_date', $single = true);
        $sh_meetup_display_date = ($sh_meetup_date) ? date('F d, Y', strtotime($sh_meetup_date)) : '';
        ?>
    <article class="fx" data-animate="fadeInUp">
      <div class="row">
        <div class="col-sm-12">
          <header>
            <div class="meet-info-wr">
              <time datetime="2017-09-29 01:00"><?php echo $sh_meetup_display_date;?></time>
              <span class="event-separator">//</span>
              <address><?php echo $sh_meetup_location;?></address>
            </div>
            <h3><?php echo get_the_title();?></h3>
          </header>
          <p class="ellipsis"><?php echo get_the_excerpt();?></p>
          <p>
            <a href="<?php echo the_permalink();?>" data-rel="modal:open" title="Read more" class="btn btn-outline-light btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
          </p>
        </div>
      </div>
    </article>
    <?php
          $x++;
      endwhile; 
    ?>
    <a href="<?php echo home_url('meet-up');?>" class="btn btn-outline-light lgmargintop btn-lg btn-block text-center fx fx2" data-animate="fadeInUp">
      More Meet-ups
      <span class="icon-carets smmarginleft"></span>
    </a>
    
  </aside>
</div>