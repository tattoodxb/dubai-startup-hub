<?php
$template_url = get_template_directory_uri();
?>
<section class="section-feeds section-feeds section-feeds-cols">
  <div class="bg-wr" style="background-image:url();">
    <div class="bg-overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="section-title fx" data-animate="fadeInUp">
            <a href="<?php echo home_url('entrepreneur-education/publications-reports-library');?>" class="title-link"><span>Dubai Ecosystem - Key Info</span><i class="fa fa-caret-right fa-fw"></i></a>
          </h2>
        </div>
        <div class=" same-height-all">              
                <div class="col-sm-12">
                  <div class="updates-articles-wr">     
        <?php
                    $current_date = date('Y-m-d H:i:s');
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $args = array(
              'post_type'=> array('publications'),
              'post_status' => 'publish',
              'orderby'=> 'post_date',
              'order'    => 'DESC',
              'posts_per_page' => 3,
              'paged' => $paged
          );

                    ?>
                    <div class="row same-height-all">

                      <?php          
          $wp_query = new WP_Query($args);
          $i=1;
          while ($wp_query->have_posts()) : $wp_query->the_post();
     
          if ( !regUserOnly_can_see( $post->ID ) ) continue;
          ?>
                      
                      <div class="col-sm-4 fx " data-animate="fadeInUp" >
                        <article>
                          <div class="row">
                            <div class="col-sm-12">
                              <figure>
                                <?php 
                                 the_post_thumbnail('medium', ['class' => 'img-responsive width100p', 'title' => get_the_title()]);
                                 ?>
                                <figcaption class="text-indent"><?php echo get_the_title();?></figcaption>
                              </figure>
                            </div>
                            <div class="col-sm-12">
                              <div class="updates-info-wr">
                                <header>                                  
                                  <h3><?php echo get_the_title();?></h3>
                                </header>
                                <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                                <p>
                                  <a href="<?php echo pdf_file_url();?>" target="_blank" title="Read more" data-rel="modal:open" class="btn btn-outline-primary-full btn-sm">Read more - English  <i class="fa fa-caret-right fa-fw"></i></a> 
                                  <?php
                                  if(pdf_file_ar_url() != "" && pdf_file_ar_url() != "SELECT pdf FILE")
                                  {
                                  ?>
                                  <a href="<?php echo pdf_file_ar_url();?>" target="_blank" title="Read more" data-rel="modal:open" class="btn btn-outline-primary-full btn-sm">Read more - Arabic  <i class="fa fa-caret-right fa-fw"></i></a> 
                                  <?php
                                  }
                                  ?>
                                </p>
                              </div>
                            </div>
                          </div>
                        </article>
                      </div>

                      <?php
            $i++;
            endwhile; 
            ?>
                      
                    </div></div></div></div>
      </div>
      <hr class="sep-hr" />
    </div>
  </div>
</section>