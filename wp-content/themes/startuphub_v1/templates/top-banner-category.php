<?php
$template_url = get_template_directory_uri();
if(isset($_GET["date"]))
{
    $date = $_GET["date"];
}
if(isset($_GET["tag"]))
{
    $tag = $_GET["tag"];
}
?>
<?php

	$catObj = get_queried_object();
    //echo '<!--'.json_encode($catObj->term_id).'-->';

	$catID = $catObj->term_id;
	$current_category = single_cat_title("", false);
	$current_desc = explode('</b>', category_description($catID[0]));
	$cur_title = str_replace('<b>', '', $current_desc[0]);
	$cur_desc = $current_desc[1];     
	
	/*
    global $post;
	$catID = get_the_category($post->ID);
	$current_category = single_cat_title("", false);
	$current_desc = explode('</b>', category_description($catID[0]));
	$cur_title = str_replace('<b>', '', $current_desc[0]);
	$cur_desc = $current_desc[1];   
	*/
?>
<div class="col-lg-6 col-md-5 col-sm-6 col-xs-12 ">
<h2 class="title"><?php echo strip_tags($cur_title);?></h2>
<p><?php echo strip_tags($cur_desc);?></p>
<div class="row">
    <div class="page-hero-nav">
		<?php /*
        <div class="col-sm-4 col-xs-6">                                          
            <a class="btn btn-primary btn-block" ><?php echo $current_category;?></a>    
        </div>
		
        <div class="col-sm-4 col-xs-6">                                          
            <a href="<?php echo home_url();?>" class="btn btn-primary" aria-label="Return to Home">
                <span class="btn-text xspaddingright">Return to Home</span> 
                <span class=" icon-carets icon-carets-left"></span>
            </a>
        </div> */ ?>
    </div>
</div>                                      
</div>
<div class="col-lg-3 col-md-4 col-sm-3">
	<section class="outline-primary-box outline-box vertical-wr" style="height: 133px;">
		<div class="vertical-middle">
				<h3 class="tool-title">Refine your search</h3>
			<form class="nopaddingall mdmargintop newsletter-form" role="Subscribe" novalidate="novalidate">

				<div class="form-group">
					<label class="sr-only-" for="search-tags">Filter by keywords</label>
					<div class="input-group">                    
						<select id="search-tags" name="selectoption form-control" class="selectpicker" data-style="btn-primary" data-live-search="true" data-size="15" data-width="100%" title="Search keywords" multiple  data-selected-text-format="count > 1">
							<?php
						$tags = get_tags();
						$items = array();
						foreach ( $tags as $tag ) {
						  //$items[$tag->slug] = $tag->name;
						?>
							<option value="<?php echo $tag->slug;?>"><?php echo $tag->name?></option>
							<?php
						}
							?>
						</select>
						<div class="input-group-btn">
							<button class="btn btn-outline-primary refine_keyword_search" type="submit">Search</button>
						</div>
					</div>
				</div>            
				<div class="form-group" style="display:none;">
					<label class="-sr-only" for="refine-date">Browse by date</label>
					<div class="input-group date">

						<input type="text" class="form-control btn-primary browse_date" id="refine-date" placeholder="Select Date" value="<?php echo $date;?>">
						<span class="input-group-addon btn btn-outline-primary">
							<i class="fa fa-calendar"></i>
						</span>
					</div>

				</div>

			</form>
		</div>
	</section>
	<div class="home-btn-wr">
		<a href="<?php echo home_url('');?>" class="btn btn-primary" aria-label="Return to Home">
			<span class="btn-text xspaddingright">Return to Home</span> 
			<span class=" icon-carets icon-carets-left"></span>
		</a>
	</div>
</div>

<div class="col-sm-6 col-md-3">
	<section class="outline-secondary-box outline-box vertical-wr" style="height: 133px;display: none">
		<div class="vertical-middle">
			<h3 class="tool-title">Join the conversation</h3>
			<p class="nomarginbottom">Sign up / Sign in to our forum</p>
			<button class="btn btn-outline-primary mdmargintop forum_btn" type="button" role="button">
				Go to forum
				<span class="icon-carets smmarginleft"></span>
			</button>
		</div>
	</section>
</div>