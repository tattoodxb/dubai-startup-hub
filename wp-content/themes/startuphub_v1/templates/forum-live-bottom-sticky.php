<div id="forum-live-wr">
	<div class="forum-live-inner-wr">
		<a href="<?php echo home_url('forum') ?>" class="fx btn-live btn btn-primary" data-animate="fadeInUp">
			<span class="tcon-indicator l-arrow">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="tcon-svgchevron" viewBox="0 0 30 36">
					<path class="a3" d="M0,0l15,16L30,0"></path>
					<path class="a2" d="M0,10l15,16l15-16"></path>
					<path class="a1" d="M0,20l15,16l15-16"></path>
				</svg>
			</span>
			Join the Live Forum Session Now
			<span class="tcon-indicator r-arrow">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="tcon-svgchevron" viewBox="0 0 30 36">
					<path class="a3" d="M0,0l15,16L30,0"></path>
					<path class="a2" d="M0,10l15,16l15-16"></path>
					<path class="a1" d="M0,20l15,16l15-16"></path>
				</svg>
			</span>
		</a>
	</div>
</div>