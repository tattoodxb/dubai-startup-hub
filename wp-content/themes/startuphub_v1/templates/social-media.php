<?php
$template_url = get_template_directory_uri();
?><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_IN/sdk.js#xfbml=1&version=v2.10&appId=513928072133333";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<aside id="section-socialmedia" class="section-socialmedia section-feeds" role="region">
            <div class="bg-wr" style="background-image:url();">
                <div class="bg-overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12"><h2 class="section-title fx" data-animate="fadeInUp"><span class="title-link"><span>Connect with us on social media</span></span></h2></div>                        
                        <div class="same-height-all-">                        
                            <div class="col-sm-4">
                                <section class="fx" data-animate="fadeInUp">
                                    <div class="primary-outline-wr">
                                        <h3 class="title">Follow us on Twitter</h3>
                                        <div class="social-feed-wr social-feed-twitter-wr">
                                        </div>                                            
                                    </div>
                                </section>
                            </div>                            
                            <div class="col-sm-4">
                                <section class="fx" data-animate="fadeInUp">
                                    <div class="primary-outline-wr">
                                        <h3 class="title">Follow us on Facebook</h3>
                                        <div class="social-feed-wr social-feed-fb-wr">
                                            <div class="fb-page" data-href="https://www.facebook.com/dubaistartuphub/" data-tabs="timeline" data-width="400" data-height="605" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/dubaistartuphub/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dubaistartuphub/">Dubai Startup Hub</a></blockquote></div>
                                        </div>
                                    </div>
                                </section>
                            </div>                            
                            <div class="col-sm-4">
                                <div class="same-col-social">
                                    <section class="fx" data-animate="fadeInUp">
                                        <div class="primary-outline-wr">
                                            <h3 class="title"><a href="https://www.linkedin.com/company/dubai-startup-hub" target="_blank">Follow us on LinkedIn</a></h3>
                                            <div class="social-feed-wr social-feed-linkedin-wr">
                                            </div>
                                        </div>
                                    </section>
                                    <section class="fx" data-animate="fadeInUp">
                                        <div class="primary-outline-wr">
                                            <h3 class="title">Follow us on Instagram</h3>      
                                            <div><blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BbwaogKDO9M/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by @dubaistartuphub</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-11-21T12:00:09+00:00">Nov 21, 2017 at 4:00am PST</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script></div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </aside>
