<?php
$template_url = get_template_directory_uri();
?><aside id="section-testimonials" class="section-testimonials section-slider" role="region">
<div class="bg-wr bg-wr-primary-light" style="background-image:url();">
<div class="bg-overlay-primary-light"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="text-indent test-title">Our Testimonials</h2>
        </div>
        
        <div class="col-sm-12 fx" data-animate="fadeInUp">
            <div class="responsive-slider slider-testimonials">
                <?php
$args = array(
'post_type'=> 'testimonials',
'post_status' => 'publish',
'orderby'=> 'menu_order',
'order'    => 'ASC',
'posts_per_page' => 10,
'paged' => get_query_var( 'paged' )
);
$wp_query = new WP_Query($args);
$i=1;
while (have_posts()) : the_post();
$sh_testimonials_location = get_post_meta($post->ID, 'sh_testimonial_location', $single = true);
$sh_testimonials_author = get_post_meta($post->ID, 'sh_testimonial_author', $single = true);
$sh_testimonials_display_date = get_post_meta($post->ID, 'sh_testimonial_display_date', $single = true);
?>
                <figure>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php the_post_thumbnail( 'full', ['class' => 'img-responsive'] );  ?>
                        </div>
                        <div class="col-sm-8">
                            <blockquote cite="#">
                                <h3><?php echo get_the_title();?></h3>
                                <p><?php echo get_the_content();?></p>
								<div class="r-more"></div>
                                <footer>
                                    <cite><?php echo $sh_testimonials_author;?></cite>
                                    <div class="testimonials-info-wr">
                                        <time datetime="2017-09-29 01:00"><?php echo $sh_testimonials_display_date;?></time>
                                        <span class="testimonials-separator">//</span>
                                        <address><?php echo $sh_testimonials_location;?></address>
                                    </div>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                </figure>
                <?php 
endwhile; 
?>
            </div>
            
            
            <div class="slider-nav-wr"></div>
        </div>
        
    </div>
</div>
</div>
</aside>