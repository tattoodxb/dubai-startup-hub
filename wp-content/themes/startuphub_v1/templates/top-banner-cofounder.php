<?php
$template_url = get_template_directory_uri();
global $post;
?>
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-centered text-center cofounder-banner">	
	<div><?php the_post_thumbnail('medium') ?></div>
	<p><br><?php echo get_the_excerpt() ?></p>
</div>