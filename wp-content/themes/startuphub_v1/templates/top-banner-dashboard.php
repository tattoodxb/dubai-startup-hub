<?php
$template_url = get_template_directory_uri();
if ( is_user_logged_in() ) {
}
else
{
    wp_redirect(home_url());
    exit;
}
$current_user = wp_get_current_user();
$registered_date = $current_user->user_registered;
$registered_date = date( "d F Y", strtotime( $registered_date ));
?>
<div class="flex-center">
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<h1 class="title" style="margin-bottom:0;">Welcome</h1>
<h2 class="sub-title" style="margin-bottom:30px;"><?php echo $current_user->first_name;?></h2>
<div class="home-btn-wr">
	<a href="<?php echo home_url(); ?>/forum/" class="btn btn-primary" aria-label="Enter the Forum now!" style="display: none">
		<span class="btn-text xspaddingright">Enter the Forum now!&nbsp;</span> 
		<span class=" icon-carets icon-carets-right"></span>
	</a>
	
	<div class="clearfix"></div>
	<a href="<?php echo home_url(); ?>/membership/" class="btn btn-primary" aria-label="Enter the Forum now!" style="margin-top:10px;">
		<span class="btn-text xspaddingright apply-member-cta">Apply for Membership</span> 
		<span class=" icon-carets icon-carets-right"></span>
	</a>
</div>
<?php /* 
<div class="row">
    <div class="page-hero-nav">
        <div class="col-sm-4 col-xs-6">                                          
            <a class="btn btn-primary btn-block" >Profile</a>    
        </div>
        <div class="col-sm-4 col-xs-6">
            <a href="<?php echo home_url();?>" class="btn btn-primary" aria-label="Return to Home">
                <span class="btn-text xspaddingright">Return to Home</span> 
                <span class=" icon-carets icon-carets-left"></span>
            </a>
        </div>
    </div>
</div>  
*/ ?>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right mobileBoxes">
    <div class="widget ProfileBox">
        <div style="height:200px;background: center url('<?php echo shub_user_avatar($current_user->ID); ?>');background-size:cover;"></div>
        <div class="ProfileCopyholder">
            <div class="titleEventbold">Member since<span class="titleEventNormal">  //  </span><span class="titleEventNormal"><?php echo $registered_date;?></span></div>
            <div class="ProfileName ">
               <?php echo $current_user->first_name;?>
            </div>  
            <div style="color:#FFF">
                <?php echo $current_user->Company;?><br>
                Profile Status: 
                <?php 
                if($current_user->is_complete=="1")
                {
                    $profile_status = 'Complete';
                }
                else
                {
                    $profile_status = 'Not Complete';   
                }
                echo $profile_status;?><br><br>

                </div>
                <a href="<?php echo wp_logout_url(home_url()); ?>" class="btn btn-primary" aria-label="Return to Home">
                <span class="btn-text xspaddingright">Logout</span> 
                <span class=" icon-carets icon-carets-left"></span>
            </a>
            </div>
    </div>
</div>	
</div>