<?php
$template_url = get_template_directory_uri();
?>
<div class="row">
	<div class="col-sm-10 col-xs-12 col-centered">

			<header>
				<div class="row">
					<div class="col-sm-4 text-right">
						<span class="error-404">404</span>
					</div>
					<div class="col-sm-8">
					 <h1 class="title">Oops! Page not found</h1>
			  		 <h2 class="sub-title">The page you are looking for can not be found.</h2></div>
				</div>

			</header>

	</div>
</div>
