<?php
$template_url = get_template_directory_uri();
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<!-- Chrome, Firefox OS, Opera and Vivaldi -->
		<meta name="theme-color" content="#0e2498" />
		<!-- Windows Phone -->
		<meta name="msapplication-navbutton-color" content="#0e2498" />
		<!-- iOS Safari -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#0e2498" />
		
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="images/favicon.ico">

		<title>Dubai Startup Hub | Event </title>

	</head>

	<body class="en">