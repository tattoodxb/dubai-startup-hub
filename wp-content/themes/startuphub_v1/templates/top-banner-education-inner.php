<?php
$template_url = get_template_directory_uri();
global $post;
?>
<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-centered text-center education-inner">
	<h1 class="title"><?php the_title() ?></h1>
	<div><?php the_post_thumbnail('medium') ?></div>
	<div><?php echo get_the_excerpt() ?></div>
</div>