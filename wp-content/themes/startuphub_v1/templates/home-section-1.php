<?php
$template_url = get_template_directory_uri();
?>
<section class="section-feeds section-feeds">
  <div class="bg-wr" style="background-image:url();">
    <div class="bg-overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="section-title fx" data-animate="fadeInUp">
            <span class="title-link"><span>We Serve</span></span>
          </h2>
        </div>
        <div class="blue-blocks">
          <div class="col-xs-12 col-sm-4">
            <div class="box fx" data-animate="fadeInUp">
              <span class="svg-wr">
                <img class="svg-img" data-src="<?php echo $template_url;?>/images/icons/icon-graph.svg"
                  src="<?php echo $template_url;?>/images/empty.png"
                  onerror="this.src='<?php echo $template_url;?>/images/logo.png'" alt="">
              </span>
              <p class="desc">Entrepreneurs with business idea or early-stage company (under 5 years old)</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="box fx" data-animate="fadeInUp">
              <span class="svg-wr">
                <img class="svg-img" data-src="<?php echo $template_url;?>/images/icons/icon-deal.svg"
                  src="<?php echo $template_url;?>/images/empty.png"
                  onerror="this.src='<?php echo $template_url;?>/images/logo.png'" alt="">
              </span>
              <p class="desc">Corporates looking to collaborate and work with startups </p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="box fx" data-animate="fadeInUp">
              <span class="svg-wr">
                <img class="svg-img" data-src="<?php echo $template_url;?>/images/icons/icon-economy.svg"
                  src="<?php echo $template_url;?>/images/empty.png"
                  onerror="this.src='<?php echo $template_url;?>/images/logo.png'" alt="">
              </span>
              <p class="desc">Investors interested in Dubai ventures</p>
            </div>
          </div>
        </div>
      </div>
      <hr class="sep-hr" />
    </div>
  </div>
</section>