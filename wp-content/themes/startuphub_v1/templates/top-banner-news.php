<?php
$template_url = get_template_directory_uri();
if(isset($_GET["date"]))
{
    $date = $_GET["date"];
}
if(isset($_GET["tag"]))
{
    $tag = $_GET["tag"];
}
?>
<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 ">
<h2 class="title">Clarity and direction in your startup journey.</h2>
<h3 class="sub-title">Become part of Dubai Startup Hub and you will be empowered to turn your idea into a tangible reality.</h3>
<div class="row">
    <div class="page-hero-nav">
		<?php /*
        <div class="col-sm-4">                                          
            <a class="btn btn-primary btn-block" ><?php echo get_the_title();?></a>    
        </div> 
        <div class="col-sm-4">                                          
            <a href="<?php echo home_url();?>" class="btn btn-primary" aria-label="Return to Home">
                <span class="btn-text xspaddingright">Return to Home</span> 
                <span class=" icon-carets icon-carets-left"></span>
            </a>
        </div> */ ?>
    </div>
</div>                                      
</div>

<?php /*
<div class="col-sm-3">
<section class="outline-primary-box outline-box vertical-wr" style="height: 133px;">
    <div class="vertical-middle">
            <h3 class="tool-title">Refine your search</h3>
        <form class="nopaddingall mdmargintop newsletter-form" role="Subscribe" novalidate="novalidate">
        
            <div class="form-group">
                <label class="sr-only-" for="search-tags">Filter by keywords</label>
                <div class="input-group">                    
                    <select id="search-tags" name="selectoption form-control" class="selectpicker" data-style="btn-primary" data-live-search="true" data-width="100%" title="Search keywords" multiple  data-selected-text-format="count > 1" >
                        <?php
                    $tags = get_tags();
                    $items = array();
                    foreach ( $tags as $tag ) {
                      //$items[$tag->slug] = $tag->name;
                    ?>

                        <option value="<?php echo $tag->slug;?>"><?php echo $tag->name?></option>
                        <?php
                    }
                        ?>
                    </select>
                    <div class="input-group-btn">
                        <button class="btn btn-outline-primary refine_keyword_search" type="submit">Search</button>
                    </div>
                </div>
            </div>
            
            <div class="form-group" style="display:none;">
                <label class="-sr-only" for="refine-date">Browse by date</label>
                <div class="input-group date">
                    
                    <input type="text" class="form-control btn-primary browse_date" id="refine-date" placeholder="Select Date" value="<?php echo $date;?>">
                    <span class="input-group-addon btn btn-outline-primary">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
                
            </div>
            
        </form>
    </div>
</section>
</div>
*/ ?>

<div class="col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">
<section class="outline-secondary-box outline-box vertical-wr" style="height: 133px;display: none">
    <div class="vertical-middle">
        <h3 class="tool-title">Join the conversation</h3>
        <p class="nomarginbottom">Sign up / Sign in to our forum</p>
        <button class="btn btn-outline-primary mdmargintop forum_btn" type="button" role="button">
            Go to forum
            <span class="icon-carets smmarginleft"></span>
        </button>
    </div>
</section>
</div>