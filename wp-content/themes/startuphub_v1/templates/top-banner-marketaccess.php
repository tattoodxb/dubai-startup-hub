<?php
$template_url = get_template_directory_uri();
global $post;
?>

  <div class="col-lg-6 col-md-5 col-sm-6 col-xs-12 col-centered text-center">
    <h1 class="title"><?php the_title() ?></h1>
    <h2 class="sub-title smmarginbottom"><?php echo get_the_excerpt() ?></h2>
    <?php the_post_thumbnail() ?>
	<?php if ( ( is_page() && !($post->post_parent > 0) ) ) { ?>
	  <p>The first event in the UAE where innovative startups and large corporations can interact and collaborate effortlessly
      </p>
	<?php } ?>
  </div>
