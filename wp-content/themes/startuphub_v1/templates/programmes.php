<?php
$template_url = get_template_directory_uri();
?>
<section id="section-programmes" class="section-programmes section-feeds" role="region">
        <div class="bg-wr" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12"><h2 class="section-title fx" data-animate="fadeInUp">
            <span class="title-link"><span>Dubai Startup Hub Programmes</span></span>
            <div class="title-content">Become a Dubai Startup Hub Member and you'll be empowered to turn your idea into a tangible reality or accelerate the growth of your already existing business. Dubai Startup Hub offers a wide range of value added services and benefits to its members.</div>
          </h2>
        </div>              
                         
                <div class="col-xs-12">
                  <div class="events-articles-wr-">
                    <?php
                        $current_date = date('Y-m-d H:i:s');
                        $args = array(
                            'post_type'=> 'programmes',
                            'post_status' => 'publish',
							'orderby'=> 'post_date',
              				'order'    => 'ASC',
                        );
                        
                        $wp_query = new WP_Query($args);
                        $i=1;
                        while (have_posts()) : the_post();
                        $sh_programme_link = get_post_meta($post->ID, 'sh_programme_link', $single = true);
                        $sh_programme_label = get_post_meta($post->ID, 'sh_programme_label', $single = true);

                       
                        ?>
                    <article class="fx" data-animate="fadeInUp">
                      <div class="row padrow">
                        <div class="col-md-9 col-sm-8">
                          <header>
                             <h3><?php echo get_the_title();?></h3>
                          </header>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <a href="<?php echo $sh_programme_link ?>" title="<?php echo $sh_programme_label ?>" class="btn btn-secondary btn-sm"> <?php echo $sh_programme_label ?> <i class="fa fa-chevron-right fa-fw"></i></a>
                        </div>
                      </div>
                    </article>
                    <?php 
                    endwhile; 
                    ?>
                    
                  </div>
                </div>
           	  
            </div>
          </div>
        </div>
      </section>