<?php $s_posts = sthub_category_stickies(); ?>

<?php if ( sizeof( $s_posts ) ) : ?>
  <h3 class="fx" data-animate="fadeInUp">Pinned Posts</h3>
  <div class="row same-height-all">
  <?php foreach ( $s_posts as $p ) : setup_postdata( $p ); 
  $event_img = get_the_post_thumbnail_url($p->ID, 'thumbnail');
  ?>
  <div class="col-sm-4 fx " data-animate="fadeInUp">
    <article>
      <div class="row">
        <div class="col-sm-12">
          <figure>
            <img src="<?php echo home_url();?>/timthumb.php?src=<?php echo $event_img?>&w=300&h=195" class="img-responsive width100p wp-post-image" />
            <figcaption class="text-indent"><?php echo get_the_title( $p->ID ) ?></figcaption>
          </figure>
        </div>
        <div class="col-sm-12">
          <div class="updates-info-wr">
            <header>
              <h3 class="same-height-text"><?php echo get_the_title( $p->ID ) ?></h3>
            </header>
            <p class="ellipsis"><?php echo get_the_excerpt( $p->ID ) ?></p>
            <p>
              <a href="<?php echo get_the_permalink( $p->ID ) ?>" data-rel="modal:open" title="Read more" class="btn btn-outline-primary-full btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
            </p>
            <footer class="tags-wr">
              <ul>
                <?php $p_tags = get_the_tags( $p->ID ) ?>
                <?php if ( $p_tags ) : ?>
                  <?php foreach ( $p_tags as $p_tag ) : ?>
                  <li><?php echo $p_tag->name ?></li>
                  <?php endforeach ?>
                <?php endif ?>
              </ul>
            </footer>
          </div>
        </div>
      </div>
    </article>
  </div>
  <?php endforeach; ?>
  <?php wp_reset_postdata(); ?>
  </div>
  <hr />
<?php endif; ?>