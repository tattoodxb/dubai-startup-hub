<?php
$template_url = get_template_directory_uri();
?>
<section id="section-events" class="section-events section-feeds" role="region">
        <div class="bg-wr" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12"><h2 class="section-title fx" data-animate="fadeInUp">Upcoming events</h2></div>              
              <div class="same-height-all-">              
                <div class="col-lg-9 col-md-8 col-sm-9">
                  <div class="events-articles-wr">
                    <?php
                        $current_date = date('Y-m-d H:i:s');
                        $args = array(
                            'post_type'=> 'events',
                            'post_status' => 'publish',
                            'meta_key' => 'sh_events_date',
                            'meta_query' => array(
                                'relation' => 'AND',
                                array(
                                    'key' => 'sh_events_date',
                                    'value' => $current_date,
                                    'compare' => '>=',
                                    'type' => 'DATE'
                                ),
                                array(
                                    'key' => 'sh_event_sticky',
                                    'value' => 'true',
                                ),
                            ),
                            'tax_query' => array(
                                array(
                                  'taxonomy' => 'category',
                                  'field'    => 'term_id',
                                  'terms'    => array( 18 ),
                                  'operator' => 'NOT IN',
                                ),
                              ),
                            'orderby'=> 'meta_value_num',
                            'order'    => 'ASC',
                            'posts_per_page' => 3,
                            'paged' => get_query_var( 'paged' )
                        );


                        
                        $wp_query = new WP_Query($args);
                        $i=1;
                        while (have_posts()) : the_post();
                        $sh_events_location = get_post_meta($post->ID, 'sh_events_location', $single = true);
                        $sh_events_display_date = get_post_meta($post->ID, 'sh_events_display_date', $single = true);

                        $start_date = get_post_meta($post->ID, 'sh_events_date', $single = true);
                        $end_date = get_post_meta($post->ID, 'sh_events_date', $single = true);

                        $start_date = date('F d, Y', strtotime($start_date));
                        $end_date = date('F d, Y', strtotime($end_date));


                      if($sh_events_display_date=="")
                      {
                          if($start_date == $end_date)
                          {
                              $sh_events_display_date = $start_date;  
                          }
                          else
                          {
                              $sh_events_display_date = $start_date . '-' .$end_date;
                          }
                          
                      }
                        ?>
                    <article class="fx" data-animate="fadeInUp">
                      <div class="row">
                        <div class="col-sm-4">
                          <figure>
                            <a href="<?php echo the_permalink();?>" class="ls-modal"><?php 
                                 the_post_thumbnail('thumbnail', ['class' => 'img-responsive', 'alt' => get_the_title()]);
                                 ?></a>                            
                            <figcaption class="text-indent"><?php echo get_the_title();?></figcaption>
                          </figure>
                        </div>
                        <div class="col-sm-8">
                          <header>
                            <div class="event-info-wr">
                              <time datetime="2017-09-29 01:00"><?php echo $sh_events_display_date;?></time>
                              <span class="event-separator">//</span>
                              <address><?php echo $sh_events_location;?></address>
                            </div>
                            <h3><?php echo get_the_title();?></h3>
                          </header>
                          <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                          <p>
                            <a href="<?php echo the_permalink();?>" title="Read more" class="btn btn-outline-secondary btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
                          </p>
                        </div>
                      </div>
                    </article>
                    <?php 
                    endwhile; 
                    ?>
                    
                    <div class="row">
                      <div class="col-lg-4 col-md-5 col-sm-6">
                        <a href="<?php echo home_url('event');?>" class="btn btn-outline-secondary  btn-lg btn-block text-center fx fx2" data-animate="fadeInUp">
                          View More Events
                          <span class="icon-carets smmarginleft"></span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <?php 
                  get_template_part( 'templates/meet-up-widget' );
                  ?>
              
            </div>
              
              </div>
          </div>
        </div>
      </section>