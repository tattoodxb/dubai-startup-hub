<?php
$template_url = get_template_directory_uri();
?><section id="section-updates" class="section-updates section-feeds section-feeds-cols" role="region">
<div class="bg-wr bg-wr-secondary no-bg-img" style="background-image:url();">
<div class="bg-overlay-secondary"></div>
<div class="container">
<div class="row">
    <div class="col-sm-12"><h2 class="section-title fx" data-animate="fadeInUp">Latest updates</h2></div>
    
    <div class="same-height-all">
    
        <div class="col-sm-12">
            <div class="updates-articles-wr">
                
                <div class="row same-height-all">
                    <?php
$current_date = date('Y-m-d H:i:s');
$args = array(
    'post_type'=> 'news',
    'post_status' => 'publish',
    'meta_key' => 'sh_news_date',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'sh_news_date',            
            'type' => 'DATE'
        ),
        array(
            'key' => 'sh_news_sticky',
            'value' => 'true',
        ),
    ),
    'orderby'=> 'meta_value',
    'order'    => 'DESC',
    'posts_per_page' => 4,
    'paged' => get_query_var( 'paged' )
);
$wp_query = new WP_Query($args);
$i=1;
while (have_posts()) : the_post();

if ( !regUserOnly_can_see( $post->ID ) ) continue;

$sh_news_location = get_post_meta($post->ID, 'sh_news_location', $single = true);
$sh_news_display_date = get_post_meta($post->ID, 'sh_news_display_date', $single = true);
?>
                    <div class="col-sm-3 fx " data-animate="fadeInUp">
                        <article>
                            <div class="row">
                                <div class="col-sm-12">
                                    <figure>
                                        <a href="<?php echo the_permalink();?>" data-rel="modal:open"><?php if (class_exists('MultiPostThumbnails')) :
                                            MultiPostThumbnails::the_post_thumbnail(
                                                get_post_type(),
                                                'news-overview-image' , null, 'news-listing-thumb', array('class' => 'img-responsive' )
                                            );
                                        endif; ?></a>
                                        <figcaption class="text-indent">AstroLabs Dubai Skyline</figcaption>
                                    </figure>
                                </div>
                                <div class="col-sm-12">
                                    <div class="updates-info-wr">
                                        <header>
                                            <div class="update-info-wr">
                                                <time datetime="2017-09-29 01:00"><?php echo $sh_news_display_date;?></time>
                                                <span class="update-separator">//</span>
                                                <address><?php echo $sh_news_location;?></address>
                                            </div>
                                            <h3 class="same-height-text"><?php echo get_the_title();?></h3>
                                        </header>
                                        <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                                        <p>
                                            <a href="<?php echo the_permalink();?>" data-rel="modal:open" title="Read more" class="btn btn-outline-primary-full btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <?php 
endwhile; 
?>
                   
                </div>
                
                <div class="row">
                    <div class="col-sm-4 col-centered fx" data-animate="fadeInUp">
                        <a href="<?php echo home_url('news');?>" class="btn btn-outline-light-primary lgmargintop btn-lg btn-block text-center">
                            View More News
                            <span class="icon-carets smmarginleft"></span>
                        </a>
                    </div>
                </div>
                
            </div>
        </div>
        
        
</div>
    
    </div>
</div>
</div>
</section>