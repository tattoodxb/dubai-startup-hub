
<div class="row same-height-all- sitemap-nav-wr">

	<div class="col-sm-4">
		<nav>
			<h3 class="sitemap-nav-title">Pages</h3>
			<?php 	
				wp_nav_menu(array('menu_class'=> 'list-unstyled')); 
			?>
		</nav>
	</div>

	<div class="col-sm-4">
		<nav>
			<h3 class="sitemap-nav-title">Quick Links</h3>
			<ul class="list-unstyled">
				<?php 
					$args = array('hide_empty' => FALSE, 'title_li'=>'');
					wp_list_categories($args); 
				?>
			</ul>
		</nav>
	</div>


	<div class="col-sm-4">
		<nav>
			<h3 class="sitemap-nav-title">Meetups</h3>
			<?php
				$args = array(
					'numberposts' => 10,
					'offset' => 0,
					'category' => 0,
					'orderby' => 'post_date',
					'order' => 'DESC',
					'include' => '',
					'exclude' => '',
					'meta_key' => '',
					'meta_value' =>'',
					'post_type' => 'meetup',
					'post_status' => 'publish',
					'suppress_filters' => true
				);

				$recent_posts = wp_get_recent_posts( $args, OBJECT );

				if(!empty($recent_posts)){
			?>
				<ul class="list-unstyled">
					<?php foreach($recent_posts as $post){ ?>	
					<li>
						<a href=""><?php echo $post->post_title ?></a>	
					</li>
					<?php } ?>
				</ul>
			<?php
				}

			?>
		</nav>
	</div>

	<div class="col-sm-4">
		<nav>
			<h3 class="sitemap-nav-title">News</h3>
			<?php
				$args = array(
					'numberposts' => 10,
					'offset' => 0,
					'category' => 0,
					'orderby' => 'post_date',
					'order' => 'DESC',
					'include' => '',
					'exclude' => '',
					'meta_key' => '',
					'meta_value' =>'',
					'post_type' => 'news',
					'post_status' => 'publish',
					'suppress_filters' => true
				);

				$recent_posts = wp_get_recent_posts( $args, OBJECT );

				if(!empty($recent_posts)){
			?>
				<ul class="list-unstyled">
					<?php foreach($recent_posts as $post){ ?>	
					<li>
						<a href="<?php echo $post->guid?>"><?php echo $post->post_title ?></a>	
					</li>
					<?php } ?>
				</ul>
			<?php
				}
				//echo '<pre>'; print_r($recent_posts );
			?>
		</nav>
	</div>

	<div class="col-sm-4">
		<nav>
			<h3 class="sitemap-nav-title">Events</h3>
			<?php
				$args = array(
					'numberposts' => 10,
					'offset' => 0,
					'category' => 0,
					'orderby' => 'post_date',
					'order' => 'DESC',
					'include' => '',
					'exclude' => '',
					'meta_key' => '',
					'meta_value' =>'',
					'post_type' => 'events',
					'post_status' => 'publish',
					'suppress_filters' => true
				);

				$recent_posts = wp_get_recent_posts( $args, OBJECT );

				if(!empty($recent_posts)){
			?>
				<ul class="list-unstyled">
					<?php foreach($recent_posts as $post){ ?>	
					<li>
						<a href=""><?php echo $post->post_title ?></a>	
					</li>
					<?php } ?>
				</ul>
			<?php
				}

			?>
		</nav>
	</div>

</div>
						