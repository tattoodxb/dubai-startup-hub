<?php
$template_url = get_template_directory_uri();
?>
<div class="section-page-hero-content" style="background-image:url(<?php echo $template_url;?>/images/banners/banner02.jpg);">
    <div class="bg-overlay-secondary"></div>
    <div class="vertical-full-wr fx-" data-animate="fadeInUp-">
        <div class="vertical-middle">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <header>
                            <h1 class="title">Clarity and direction in your startup journey.</h1>
                            <h2 class="sub-title">Become part of Dubai Startup Hub and you will be
                                empowered to turn your idea into a tangible reality.</h2>
                        </header>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/*
?><div class="section-page-hero-content" style="background-image:url(<?php echo $template_url;?>/images/banners/banner-main.jpg);">
    <div class="bg-overlay-secondary"></div>
    <div class="vertical-full-wr fx-" data-animate="fadeInUp-">
        <div class="vertical-middle">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <header>
                            <h1 class="title">Startup journey.</h1>
                            <h2 class="sub-title">Will be
                                empowered to turn your idea into a tangible reality.</h2>
                        </header>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-page-hero-content" style="background-image:url(<?php echo $template_url;?>/images/banners/education-header.jpg);">
    <div class="bg-overlay-secondary"></div>
    <div class="vertical-full-wr fx-" data-animate="fadeInUp-">
        <div class="vertical-middle">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <header>
                            <h1 class="title">Startup journey.</h1>
                            <h2 class="sub-title">Will be
                                empowered to turn your idea into a tangible reality.</h2>
                        </header>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
*/
?>