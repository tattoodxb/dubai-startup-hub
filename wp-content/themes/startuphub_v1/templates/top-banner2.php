
<div class="col-sm-6">

   <h2 class="title">Clarity and direction in your startup journey.</h2>
   <h3 class="sub-title">Become part of Dubai Startup Hub and you will be empowered to turn your idea into a tangible reality.</h3>

	<div class="row">
		<div class="page-hero-nav">
			<div class="col-lg-4 col-md-5 col-sm-7 col-xs-6 xs-xspaddingleft">										
				<a href="<?php echo home_url('');?>" class="btn btn-primary btn-block" aria-label="Return to Home">
					<span class="btn-text xspaddingright">Return to Home</span> 
					<span class=" icon-carets icon-carets-left"></span>
				</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>							
</div>
<div class="col-sm-6 col-contactus">
	<h3 class="tool-title light">Feel free to contact us if you have any questions or need more information, please fill in the contact form below.</h3>
	<form id="contactform" action="contact" name="contactform" method="post" class="form-contactus">
		<div class="row condensed">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="sr-only" for="first_name">First name</label>
					<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="sr-only" for="family_name">Last Name:</label>
					<input type="text" class="form-control" name="family_name" id="family_name" placeholder="Last Name">
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="sr-only" for="email">Email address:</label>
					<input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="form-group">
					<label class="sr-only" for="subject">Subject:</label>
					<input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="sr-only" for="comments">Comment:</label>
					<textarea class="form-control" rows="3" id="comments" name="comments" placeholder="Message"></textarea>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block send btn-send">Send</button>
				</div>
			</div>
			<div class="clearfix"></div>
			<!--<div class="col-lg-12" style="height:50px;">
				<div class="response-msg text-center" style="display: none"></div>
			</div>-->
		</div>
	</form>
</div>
