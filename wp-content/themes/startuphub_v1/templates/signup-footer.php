<aside class="signup-toolbox">
	<a class="close-sign-up" href="#" role="button"><span class=""><i class="fa fa-times"></i></span></a>
	<div class="container">
		<div class="row">	
				<div class="signup-toolbox-content-rw">
					<div class="col-md-8 col-sm-6">
						<h3 class="">Register Now</h3>
						<p class="nomarginbottom">Greetings! Register with us today and keep updated on all our workshops, trainings and events via our monthly newsletter. Registration will also enable you to apply for Dubai Startup Hub Membership.</p>
					</div>

					<div class="col-md-4 col-sm-6 btn-group-wr">
						<button type="button" class="btn btn-primary btn-register" aria-label="Sign up">
							<span class="btn-text xspaddingright">Register</span> 
						</button>
						<a href="javascript:;" class="btn-login-ft" aria-label="Login">
							<span class="btn-text-login-ft">Login</span> 
						</a	>
					</div>
				</div>
			
			</div>
		</div>
</aside>