<?php 
$popup = "";
if(isset($_GET["page"]))
{
	$popup = $_GET["page"];
}
if($popup!="popup")
{
	get_header(); 
}
?>
<?php if(have_posts()) : ?>
<?php 
while(have_posts()) : the_post();
	$post_title = get_the_title();
?>
<div class="white-wrapper" id="mainContent">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">
			<h3 class="titleHeader"><?php the_title(); ?></h3>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 removeLeftpadding">
			<?php the_content(); ?>
		</div>
	</div>
</div>
<?php endwhile; ?>
<?php
endif;
?>
<?php
if($popup!="popup")
{
	get_footer();
}
 ?>