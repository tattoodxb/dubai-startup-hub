<?php
/* Template Name: About Template */
$template_url = get_template_directory_uri();
get_header(); ?>

<section class="section- about section-feeds" id="section-about" role="region">
	<div class="bg-wr" style="background-image:url();">
	  <div class="bg-overlay-primary-light"></div>
	  <div class="container">
		<div class="row">
		  <div class="col-sm-12">
		   <h1 class="section-title main-title fx animated fadeInUp" data-animate="fadeInUp">
			<?php the_title(); ?></h1>
		   <?php the_content(); ?>
		  </div>
		</div>
	  </div>
	</div>
</section>

<section id="section-events" class="section-events section-feeds section-feeds-cols-3 meet-team-wr" role="region">
        <div class="bg-wr" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
		
            <div class="row">
              <div class="col-sm-12"><h2 class="section-title fx" data-animate="fadeInUp">Meet the team</h2></div>
              
              <div class="same-height-all-">
              
                <div class="col-lg-9 col-md-8 col-sm-9">
                  <div class="section-feeds-cols">
                    <?php
                        $current_date = date('Y-m-d H:i:s');
                        $args = array(
                            'post_type'=> 'team',
                            'post_status' => 'publish',
                            'orderby'=> 'post_date',
                            'order'    => 'ASC',
                            'posts_per_page' => 10,
                            'paged' => get_query_var( 'paged' )
                        );
                        
                        $wp_query = new WP_Query($args);
                        $i=1;
                        while (have_posts()) : the_post();                        
                        ?>
                        <div class="col-sm-4 fx " data-animate="fadeInUp">
                        <article>
                            <div class="row">
                                <div class="col-sm-12">
                                    <figure>
                                    <?php 
                                        the_post_thumbnail('thumbnail', ['class' => 'img-responsive', 'alt' => get_the_title()]);
                                 ?>
                                        <figcaption class="text-indent"><?php echo get_the_title();?></figcaption>
                                    </figure>
                                </div>
                                <div class="col-sm-12">
                                    <div class="updates-info-wr">
                                        <header>
                                            <h3 class=""><?php echo get_the_title();?></h3>
                                        </header>
                                        <p class=""><?php echo get_the_content();?></p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <?php 
                    endwhile; 
                    ?>
                  </div>
                </div>
                <?php 
                  get_template_part( 'templates/meet-up-widget' );
                  ?>
              
            </div>
              
              </div>
          </div>
        </div>
      </section>
<?php //get_template_part( 'templates/our-initiatives' ); ?>    
<?php get_template_part( 'templates/latest-updates' ); ?>
<?php get_template_part( 'templates/social-media' ); ?>
<?php get_template_part( 'templates/our-partners' ); ?>
<?php get_footer(); ?>