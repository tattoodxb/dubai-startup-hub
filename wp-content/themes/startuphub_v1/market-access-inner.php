<?php

/* Template Name: Market Access Inner */
$template_url = get_template_directory_uri();
get_header();

?>

<section id="section-events" class="section-events section-feeds section-feeds-cols single-event market-access" role="region">
	<?php the_content(); ?>
</section>
<?php get_footer(); ?>