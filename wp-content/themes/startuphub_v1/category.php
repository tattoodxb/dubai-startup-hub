<?php 
get_header();
$categories = get_category(get_query_var('cat'), false);
$current_category = single_cat_title("", false);

$category_id = $categories->cat_ID;

?>
<section id="section-events" class="section-updates section-feeds section-feeds-cols section-feeds-cols-3" role="region">
        <div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
              <div style="padding:5px;"><a href="<?php echo home_url();?>/articles/">Articles</a> | <?php echo $current_category;?></div>
              <h1 class="section-title fx main-title" data-animate="fadeInUp"><?php echo $current_category;?></h1></div>
              <div class="same-height-all">              
                <div class="col-sm-12">
                  <div class="updates-articles-wr">                    
                    <?php
                    $current_date = date('Y-m-d H:i:s');
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          
          $args = array(
              'post_type'=> 'post',
              'post_status' => 'publish',
              'cat' => $category_id,
              'orderby'=> 'publish_date',
              'order'    => 'DESC',
              'posts_per_page' => get_option('posts_per_page'),
              'paged' => $paged,
              'post__not_in' => get_option( 'sticky_posts' )
          );
          
          if(isset($_GET["tag"]))
          {
            $tag = $_GET["tag"];
          }

          if($tag != "")
          {
            $args['tag'] = $tag;
            $tag = str_replace('-', ' ', $tag);
            $tag = str_replace(',', ', ', $tag);
            echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the keyword <em><b style="text-transform:capitalize">'.$tag.'</b></em><br><br></div>';

          }

          if(isset($_GET["date"]))
          {
            $date = $_GET["date"];
          }

          if($date != "")
          {
              $date = date('Y-m-d', strtotime($date));
              $args['meta_query'] = array(
                array(
                  'key'     => 'publish_date',
                  'value'   => $date,
                  'compare' => '=',
                  'type' => 'DATE'
                ),
              );
              echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for the events on <em><b>'.$date.'</b></em><br><br></div>';
          }
                    ?>

                    <?php get_template_part( 'templates/category-sticky' ); ?>

                    <div class="row same-height-all">

                      <?php          
          $wp_query = new WP_Query($args);
          $i=1;
          while ($wp_query->have_posts()) : $wp_query->the_post();
          $post_tags = get_the_tags();
          $post_id = get_the_ID();
          $event_img = get_the_post_thumbnail_url($post_id, 'thumbnail');

          if ( !regUserOnly_can_see( $post_id ) ) continue;
          ?>
                    
                      <div class="col-sm-4 fx " data-animate="fadeInUp">
                        <article>
                          <div class="row">
                            <div class="col-sm-12">
                              <figure>
                                 <img src="<?php echo home_url();?>/timthumb.php?src=<?php echo $event_img?>&w=300&h=195" class="img-responsive width100p wp-post-image" />
                                <figcaption class="text-indent"><?php echo get_the_title();?></figcaption>
                              </figure>
                            </div>
                            <div class="col-sm-12">
                              <div class="updates-info-wr">
                                <header>
                                  <h3 class="same-height-text"><?php echo get_the_title();?></h3>
                                </header>
                                <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                                <p>
                                  <a href="<?php echo the_permalink();?>" title="Read more" data-rel="modal:open" class="btn btn-outline-primary-full btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
                                </p>
                                <footer class="tags-wr">
                                  <ul>
                                    <?php
                                        $posttags = $post_tags;
                                        if ($posttags) {
											$ctr = 0;
                                            foreach($posttags as $tag) {
                                    			if($ctr <= 10){
									
											    ?>
												<li><?php echo $tag->name;?></li>
												<?php
													
												}
												$ctr++;
                                            }
                                        }
                                    ?>
                                  </ul>
                                </footer>
                              </div>
                            </div>
                          </div>
                        </article>
                      </div>

                      <?php
            $i++;
            endwhile; 
            ?>
            <?php
            if($i==1)
                {
                    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue"><br>No result found for your search, please refine your search</div>';
                }
            ?>
                      
                    </div>
                    
                    <div class="row">
                      <div class="col-sm-4 col-centered fx" data-animate="fadeInUp">
                        <div class="text-center"><?php 
                                  $pag_args = array(
                                      'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                      'total'        => $wp_query->max_num_pages,
                                      'current'      => max( 1, get_query_var( 'paged' ) ),
                                      'format'       => '?paged=%#%',
                                      'show_all'     => false,
                                      'type'         => 'list',
                                      'end_size'     => 2,
                                      'mid_size'     => 1,
                                      'prev_next'    => true,
                                      'prev_text'    => sprintf( '<i></i> %1$s', __( 'Prev', 'text-domain' ) ),
                                      'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
                                      'add_args'     => false,
                                      'add_fragment' => '',
                                      'before_page_number' => '',
                                      'after_page_number'  => ''
                                  );
                                  $return = paginate_links( $pag_args );
                                  echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return );
                              ?>
                          </div>
                      </div>
                    </div>
                    
                  </div>
                </div>
                
                
            </div>
              
              </div>
          </div>
        </div>
      </section>
<?php 
	if($categories->slug == "inspiration") {
		get_template_part( 'templates/testimonials' ); 
	}
?>
<?php get_footer(); ?>