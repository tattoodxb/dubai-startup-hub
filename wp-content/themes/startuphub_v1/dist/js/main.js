//slidefade
$.fn.slideFadeToggle  = function(speed, easing, callback) {
    return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
};

$.fn.slideFadeIn  = function(speed, easing, callback) {
    return this.animate({opacity: 'show', height: 'show'}, speed, easing, callback);
};

$.fn.slideFadeOut  = function(speed, easing, callback) {
    return this.animate({opacity: 'hide', height: 'hide'}, speed, easing, callback);
};

//mega menu
$(function() {
	//window.prettyPrint && prettyPrint()
	$(document).on('click', '.megamenu .dropdown-menu', function(e) {
	  e.stopPropagation()
	});
	$('.megamenu .dropdown .dropdown-toggle').click(function(e){
		e.stopPropagation();
		
		var parent = $(this).parent(".dropdown");
		var target = parent.find(">.dropdown-menu");
		var currentOpenParent = $('.megamenu .dropdown.open-menu');
		var currentOpen = $('.megamenu .dropdown.open-menu > .dropdown-menu');
		var currentOpenCount = $('.megamenu .dropdown.open-menu > .dropdown-menu').length;
		
		if(parent.hasClass("open-menu")){
			console.log("if");
			parent.removeClass("open-menu");	
			target.stop().clearQueue().slideFadeOut();
		}else{
			if(currentOpenCount>0){
				console.log("else if ");
				currentOpenParent.removeClass("open-menu");	
				currentOpen.stop().clearQueue().slideFadeToggle();
				parent.addClass("open-menu");
				target.stop().clearQueue().slideFadeIn();
			}else{
				console.log("else else");
				parent.addClass("open-menu");
				target.stop().clearQueue().slideFadeIn();
				SameHeight();
			}
		}
		SameHeight();
	});
	$(document).click(function(event) { 
		if(!$(event.target).closest('.megamenu .dropdown .dropdown-toggle').length) {
			$('.dropdown-menu:visible').slideFadeToggle();
			$('.megamenu .dropdown.open-menu').removeClass("open-menu");
		}        
	});
})

//same maxheight
setTimeout(function() {
    var partnerH = jQuery('.finance-overview-navtabs-wr .stage-overview').map(function() {
        return jQuery(this).height();
    }).get();
    var partnermaxH = Math.max.apply(null, partnerH);
    jQuery('.finance-overview-navtabs-wr .stage-overview').height(partnermaxH);
 }, 500);



function scrollToAnchor(){
	console.log("scroll to anchor");
	var thisEl = jQuery(location).attr('href');
	var anchor = thisEl.split('#')[1];
	var anchoredid = "#" + anchor;
	
	//add selected to anchored
	jQuery('a[href$="'+thisEl+'"]').parent().addClass("selected");
	
	if(jQuery(anchoredid).length>0){
		console.log("scroll to anchor now");
		var anchoroffset = jQuery(anchoredid).offset().top;
		var navH = jQuery("header").outerHeight()*2;
		var offsetT = anchoroffset - navH;
		console.log(anchoroffset);
		console.log(navH);
		console.log(offsetT);
		jQuery('html, body').animate({scrollTop: offsetT}, 800);
	}else{
		console.log("nothing to scroll");
	}
}

//Convert SVG image to xml
function SVGtoIMG(){
	 //svg image to xml
	 console.log("svg convert started");
	 
	var count = $('.svg-img').length;
	var current = 0;
	$('.svg-img').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('data-src');
		if(imgURL){
			var req = $.get(imgURL, function(data) {
				current++;
				
				// Get the SVG tag, ignore the rest
				var $svg = $(data).find('svg');

				// Add replaced image's ID to the new SVG
				if(typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				// Add replaced image's classes to the new SVG
				if(typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass+' replaced-svg');
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Check if the viewport is set, if the viewport is not set the SVG wont't scale.
				if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
					$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
				}

				// Replace image with new SVG
				$img.replaceWith($svg);

			}, 'xml').done(function(data) {
				if(current>=count){
					console.log("svg Convertion stopped");
					req.abort();
					/*$(".loader-wr").fadeOut( function() {
						FxAnimate();
						adjustGridIconSize();
						GridInit();
					});*/
				}
			});
		}
    });
    
 }

function SameHeight(){
	console.log("same height called");
	$('.same-height-all').sameHeight({
		//this option by default is false so elements on the same row can have
		//different height. If set to true all elements will have the same height
		//regardless of whether they are on the same row or not.
		oneHeightForAll: true,

		//this option by default is false. If set to true css height will be
		//used instead of min-height to change height of elements.
		useCSSHeight: true,

		//this function will be called every time height is adjusted
		callback: function() {
			//do something here...
		}
	});
	$('.same-height-row').sameHeight({
		oneHeightForAll: false,
		useCSSHeight: true,
		callback: function() {
			//do something here...
		}
	});
}
 
$(window).load(function(){
	console.log("loaded");
	var windowW = parseInt(jQuery(window).width());
	
	SVGtoIMG();
	SameHeight();
	
	//move modal to bottom
	$(".modal").detach().appendTo('body');
	
	//add class to direct downlaods
	$(".direct-download-wr a").each(function() {
		$(this).addClass("direct-download");
	});
	
	//fade page on link click
	$("a:not(.scroll-to)").click(function(){
		var thisEl = $(this).hasClass('direct-download');
		console.log(thisEl);
		if(thisEl==true) {
			console.log("dont go out");
			window.onbeforeunload = function(){};
		} else {
			console.log("before going out");
			window.onbeforeunload = function(){
				console.log("going out");
				$(".pageWrapper").addClass("fade-out");
			};
		}
	});
	
	//click scroll to anchor
	$(".scroll-to").click(function(e){
		console.log("scroll to");
		e.preventDefault();
		
		var thisEl = $(this);
		var anchor = thisEl.attr('href').split('#')[1];
		var anchoredid = "#" + anchor;
		
		//add active
		thisEl.parent().parent().find("li").removeClass("selected");
		thisEl.parent().addClass("selected");
		
		if($(anchoredid).length>0){
			var anchoroffset = $(anchoredid).offset().top;
			var navH = $("header").outerHeight();
			var offsetT = anchoroffset - navH;
			console.log(anchoroffset);
			console.log(navH);
			console.log(offsetT);
			$('html, body').animate({scrollTop: offsetT}, 800);
		}
	});	
	
	//scrollto anchor after load
	scrollToAnchor();

	
	
    
}); 


var PrevWindowW = parseInt($(window).width()); 
$(window).on('load resize', function () {
	var CurrentWindowW = parseInt($(window).width()); //current width
	var windowH = parseInt($(window).height());
    var windowW = parseInt($(window).width());

    var headerH = parseInt($(".top-head").height());
    var footerH = parseInt($("#footWrapper").height());
    var usedH = footerH + headerH;
	var availH = windowH-usedH;
	
    console.log("loaded resize");
	
	setTimeout(function() {
		if(windowW<415){
			console.log("415");
		}else if(windowW<740){	
			console.log("740");
		}else if(windowW<993){
			console.log("993");
		}else{
		}
	}, 100);
	
	setTimeout(function() {
		if(PrevWindowW != CurrentWindowW){
			
        }
    }, 700);
    
});


$(document).ready(function () {
    $('.selectpicker').selectpicker({
	  style: 'btn-info',
	  size: 4
	});

   
});
