<?php
/* Template Name: Search Template */
$template_url = get_template_directory_uri();
get_header(); ?>
<section id="section-events" class="section-updates section-feeds section-feeds-cols section-feeds-cols-3" role="region">
        <div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
              <h2 class="section-title fx" data-animate="fadeInUp">Search</h2></div>
              <div class="same-height-all">              
                <div class="col-sm-12">
                  <div class="updates-articles-wr"> 
      <?php
      if(isset($_GET['keyword']))
      {
            $keyword = $_GET['keyword'];

            if($keyword != "")
            {
				
            echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have searched for <em><b>'.$keyword.'</b></em></div>';
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            //echo get_query_var( 'paged' );
            $args = array(
                'post_type'=> array('post','news','events','meetup','page'),
                's'=> $keyword,
                'post_status' => 'publish',
                'orderby'=> 'menu_order',
                'order'    => 'ASC',
                'posts_per_page' => 9,
				'offset' => 0,
                'paged' => 2,
            );
            $wp_query = new WP_Query($args);
			//echo '<pre>'; print_r($wpdb); exit;
            $i=1;
            while (have_posts()) : the_post();
                $post_type = get_post_type();
                $post_type_name = ucfirst($post_type);
                $overview_image = $post_type.'-overview-image';
                if($post_type=='page')
                {
                    $class = '';
                }
                else
                {
                    $class = 'ls-modal';    
                }                
            ?>
            <div class="col-sm-4 fx " data-animate="fadeInUp">
                <article>
                  <div class="row">
                    <div class="col-sm-12">
                      <figure>
                        <?php 
                         the_post_thumbnail('thumbnail', ['class' => 'img-responsive width100p', 'title' => get_the_title()]);
                         ?>
                        <figcaption class="text-indent"><?php echo get_the_title();?></figcaption>
                      </figure>
                    </div>
                    <div class="col-sm-12">
                      <div class="updates-info-wr">
                        <div class="titleEventNormal coloringBlue">in</span> <span class="titleEventbold coloringBlue"><?php echo $post_type_name?></span></div>
                        <header>
                          <h3 class="same-height-text"><?php echo get_the_title();?></h3>
                        </header>
                        <p class="ellipsis"><?php echo get_the_excerpt();?></p>
                        <p>
                          <a href="<?php echo the_permalink();?>" data-rel="modal:open" title="Read more" class="btn btn-outline-primary-full btn-sm">Read More  <i class="fa fa-caret-right fa-fw"></i></a>
                        </p>
                      </div>
                    </div>
                  </div>
                </article>
              </div>
            <?php 
            $i++;
            endwhile; 
                if($i==1)
                {
                    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue"><br>No result found for your search, please refine your search</div>';
                }
            }
            else
            {
                echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have not searched for anything</div>';
            }

    }
    else
            {
                echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coloringBlue">You have not searched for anything</div>';
            }
?>
</div>  </div>    </div>          
                    <div class="row">
                      <div class="col-sm-4 col-centered fx" data-animate="fadeInUp">
                        <div class="text-center"><?php 
                                  $pag_args = array(
                                      'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                      'total'        => $wp_query->max_num_pages,
                                      'current'      => max( 1, get_query_var( 'paged' ) ),
                                      'format'       => '?paged=%#%',
                                      'show_all'     => false,
                                      'type'         => 'list',
                                      'end_size'     => 2,
                                      'mid_size'     => 1,
                                      'prev_next'    => true,
                                      'prev_text'    => sprintf( '<i></i> %1$s', __( 'Prev', 'text-domain' ) ),
                                      'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
                                      'add_args'     => false,
                                      'add_fragment' => '',
                                      'before_page_number' => '',
                                      'after_page_number'  => ''
                                  );
                                  $return = paginate_links( $pag_args );
                                  echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return );
                              ?>
                          </div>
                      </div>
                    </div>
                
                
            </div>
              
              </div>
      </section>
<?php get_footer(); ?>