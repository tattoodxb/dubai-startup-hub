jQuery( document ).ready(function($) {
    $('.btn_article').on('click', function(e){
		e.preventDefault();
		var strCategory = $('#select-startup-opt option:selected').val();
		document.location.href = siteURL + '/articles/' + strCategory;
	});

     $(".btn-register").click(function () {
        $(".login-register-wr").hide();
        $(".registrationbox").show();
     });

    $(".reg-close").click(function () {
        $(".registrationbox").hide();
     });

    $('.top_search_button').on('click', function(e){
        e.preventDefault();
        var keyword = $('#top_search').val();
        document.location.href = siteURL + '/search/?keyword=' + keyword;
    });

    $('.forum_search_btn').on('click', function(e){
        e.preventDefault();
        var keyword = $('#forum_search').val();
        document.location.href = siteURL + '/forum/?keyword=' + keyword;
    });

    $('.forum_btn').on('click', function(e){
        e.preventDefault();
        document.location.href = siteURL + '/forum';
    });

    $('.profile_pic').on('click', function(e){
        //e.preventDefault();
        //document.location.href = siteURL + '/dashboard';
    });

    $('#top_search').keyup(function(e){
        //console.log(e.keyCode);
        if(e.keyCode == 13)
        {
            $('.top_search_button').trigger("click");
        }
    });

    $('#forum_search').keyup(function(e){
        //console.log(e.keyCode);
        if(e.keyCode == 13)
        {
            $('.forum_search_btn').trigger("click");
        }
    });

    $('.refine_keyword_search').on('click', function(e){
        e.preventDefault();
        var keyword = $('.selectpicker').selectpicker('val');        
        document.location.href = currentURL + '?tag=' + keyword;
    });

    $('.refine_keyword').keyup(function(e){
        //console.log(e.keyCode);
        if(e.keyCode == 13)
        {
            $('.refine_keyword_search').trigger("click");
        }
    });

    $('.browse_date').change(function(e){
        //console.log(e.keyCode);
        var search_date = $('.browse_date').val();
        document.location.href = currentURL + '?date=' + search_date; 
    });

    $('.organisation-type').change(function(e){
        var organisation_type = $('option:selected', this).val();
        if(organisation_type == "Startup")
        {
            $('.startup-market').show();
        }
        else
        {
            $('.startup-market').hide();
            $('.field-start-market').val("");
        }
    });

    
    
    /*var cache = {};
    $( "#tags" ).autocomplete({
      source: function( request, response ) {
        var term = request.term;
        if ( term in cache ) {
          response( cache[ term ] );
          return;
        }
        var requestdata = {
            term: request.term,
            action: 'get_tags_response',
            csrf_token: startup_services_script.csrf_token,
        };
        $.getJSON( startup_services_script.shubservices_forms, requestdata, function( data, status, xhr ) {
          cache[ term ] = data;
          response( data );
        });
      }
    });
    /*$( "#tags" ).autocomplete({
        source: function( request, response ) {
        $.ajax( {
              url: startup_services_script.shubservices_forms,
              dataType: "jsonp",
              data: {
                term: request.term,
                action: 'get_tags_response',
                csrf_token: startup_services_script.csrf_token,
              },
              success: function( data ) {
                    response( data );
              }
            } );
          }
    });*/

    //$("#dateFilter").datepicker();

    $('.social_link').click(function (e) {
        e.preventDefault();
        var strURL = $(this).attr("href");
        var LeftPosition = (screen.width) ? (screen.width-520)/2 : 0;
        var TopPosition = (screen.height) ? (screen.height-300)/2 : 0;
        window.open(strURL ,"popup","width=680,height=460,toolbar=yes,top="+TopPosition+", left="+LeftPosition+"");          
        return false;
    });    

    $('.reply-btn').click(function (e) {
        var data_id = $(this).data('id');
        $('#reply_box'+data_id).toggle();
    });

    $('.login-submit').click(function (e) {
        e.preventDefault();
        var user_name = $('#sh_username');
        var user_pass = $('#sh_password');
        var redirectTo = $('#login_redirect_to').val();
        var err = 0;
        if(user_name.val()=="")
        {
            user_name.addClass('has-error');
            err = 1;
        }
        else if( !isValidEmailAddress( user_name.val() ) ) 
        {
            user_name.addClass('has-error');
            //email_label.html('Invalid email address');
            err = 1;
        }
        else
        {
            user_name.removeClass('has-error');
        }
        if(user_pass.val()=="")
        {
            user_pass.addClass('has-error');
            err = 1;
        }
        else
        {
            user_pass.removeClass('has-error');
        }

        if(err == 1)
        {
            return false;
        }
        else
        {
            var data = {
                action: 'user_valid_response',
                user_name: user_name.val(),
                user_pass: user_pass.val(),
                csrf_token: startup_services_script.csrf_token,
            };
            $.post(startup_services_script.shubservices_forms, data, function(response) {
				//debugger;ery-modal",  //
                if(response!="success")
                {
                    var $modal = $('#alertmodal > div');
					$modal.removeClass('alert-success');
					$modal.addClass('alert-danger');
					$modal.html(response);

                    $('<a href="#close-modal" rel="modal:close" class="close-modal"><span class="close-text">X</span>').appendTo('.modal-error');
                    $('.modal-error').modal({
                        blockerClass: "jquery-modal",  // CSS class added to the overlay (blocker).
                        modalClass: "modal",    // CSS class added to the element being displayed in the modal.
                        showClose: false, 
                        fadeDuration: 400    // Number of milliseconds the fade transition takes (null means no transition)
                    });
                }
                if(response=="success")
                {
                    document.location.href = redirectTo;
                }

            });
            return false;
        }
    });

    $('.register-submit').click(function (e) {
        e.preventDefault();
        var first_name = $('#first_name');
        var last_name = $('#last_name');
        var Password = $('#Password');
        var CPassword = $('#CPassword');
        var Company = $('#Company');
        var Country = $('#Country');
        var Email = $('#Email');
        var Industry = $('#Industry');
        var OrganisationType = $('#OrganisationType');
        var email_label = $('.email_label');
        var pass_label = $('.pass_label');
        var user_label = $('.user_label');
		var userlast_label = $('.userlast_label');
		var avatarFile = $('#avatar_file');
        var err = 0;
        if(first_name.val()=="")
        {
            first_name.addClass('has-error');
            err = 1;
        }
		else if(first_name.val().length<2)
        {
            first_name.addClass('has-error');
            user_label.html("First name must be at least 2 characters.");
            err = 1;
        }
        else
        {
            first_name.removeClass('has-error');
			user_label.html("");
        }

        if(last_name.val()=="")
        {
            last_name.addClass('has-error');
            err = 1;
        }
		else if(last_name.val().length<2)
        {
            last_name.addClass('has-error');
            userlast_label.html("Last name must be at least 2 characters.");
            err = 1;
        }
        else
        {
            last_name.removeClass('has-error');
			userlast_label.html("");
        }

        if(Email.val()=="")
        {
            Email.addClass('has-error');
            err = 1;
        }
        else if( !isValidEmailAddress( Email.val() ) ) 
        {
            Email.addClass('has-error');
            email_label.html('Invalid email address');
            err = 1;
        }
        else
        {
            Email.removeClass('has-error');
            email_label.html('');
        }

        if(Password.val()=="")
        {
            Password.addClass('has-error');
            err = 1;
        }
        else if(Password.val().length < 6)
        {
            Password.addClass('has-error');
            pass_label.html("Password must be at least 6 characters.");
            err = 1;
        }
		else if(isWeakPassword(Password.val()))
        {
			Password.addClass('has-error');
            pass_label.html("Password complexity failed");
			 err = 1;
			// validate strong password
        }
        else
        {
            Password.removeClass('has-error');
            pass_label.html("");
        }

        if(CPassword.val()=="")
        {
            CPassword.addClass('has-error');
            err = 1;
        }
        else if(Password.val()!=CPassword.val())
        {
            CPassword.addClass('has-error');
            err = 1;
        }
        else
        {
            CPassword.removeClass('has-error');
        }


        if(Company.val()=="")
        {
            Company.addClass('has-error');
            err = 1;
        }
        else
        {
            Company.removeClass('has-error');
        }

        if(Country.val()=="")
        {
            Country.addClass('has-error');
            err = 1;
        }
        else
        {
            Country.removeClass('has-error');
        }
        
        if(OrganisationType.val()=="")
        {
            OrganisationType.addClass('has-error');
            err = 1;
        }
        else if(OrganisationType.val()=="Startup")
        {
            if(Industry.val()=="")
            {
                Industry.addClass('has-error');
                err = 1;
            }
            else
            {
                Industry.removeClass('has-error');
            }
        }
        else
        {
            OrganisationType.removeClass('has-error');
        }   
		
		// check if avatar is not empty
		/*if(!$('.profile-pic-wrapper .hasImage').length)
		{
			if(!$('#avatar_file').get(0).files.length)
			{
				err = 1;
				$('#avatar_file').addClass('err-file');
			}
		}
		else 
		{
			$('#avatar_file').removeClass('err-file');
		}*/
		

        if(err == 1)
        {
            return false;
        }
        else
        {
            $('#registerfrm').submit();
        }
    });
    
    $(".membership-form").validate({
        ignore: '.ignore',
        rules: {
            firstname: "required",
            lastname: "required",
            email: {
                required: true,
                email: true
            },
            mobilenumber: "required",
            nationality: "required",
            year_of_incoporation: {
                required: true,
                number: true,
                minlength: 4,
                maxlength: 4
            },
            companyname: "required",
            jobtitle: "required",
            website: {
                required: true,
                validUrl: true
            },
            industrysector: "required",
            city: "required",
            overview: "required"            
        },
        messages: {
            //firstname: "Please enter your firstname"
        },
        tooltip_options: {
            //firstname: {placement: 'bottom', html: true}
        },

        submitHandler: function () {
            return true;
        }
    });
	
	$(".form-contactus").validate({
		rules: {
			first_name: {
				required: true,
			},
			family_name: {
				required: true,
			},
			email: {
				required: true,
				email: true
			},
			subject: {
				required: true,
			},
			comments: {
				required: true,
			},
		},
		messages: {
			first_name: {
				required: "Please enter your first name",
			},
			family_name: {
				required: "Please enter your last name",
			},
			email: {
				required: "Please enter your email",
				email: "Please enter a valid email address",
			},
			subject: {
				required: "Please enter a subject",
			},
			comments: {
				required: "Please enter your message",
			},
		},
		tooltip_options: {
			first_name: {placement:'top',html:true},
	 		family_name: {placement:'top',html:true},
	 		email: {placement:'top',html:true},
			subject: {placement:'top',html:true},
			comments: {placement:'top',html:true},
		},
		
		submitHandler:function(){
			//console.log("submitHandler success");
	        var data = {
                action: 'save_enquiry',
                csrf_token: startup_services_script.csrf_token,
				first_name: $('#first_name').val(),
				family_name: $('#family_name').val(),
				email: $('#email').val(),
				subject: $('#subject').val(),
				comments: $('#comments').val()
            };
			
			$.post(startup_services_script.shubservices_forms, data, function(response) {
			   
				var modal = $('#alertmodal > div');
				modal.removeClass('alert-danger');
				modal.removeClass('alert-success');
				
				if(response == "success")
				{
					modal.addClass('alert-success');
					modal.html('Thank you for contacting us, we will get back to you regarding your questions as soon as we can.');
					
					/* reset fields */
					$('#first_name').val('');
					$('#family_name').val('');
					$('#email').val('');
					$('#subject').val('');
					$('#comments').val('');
					
				}
				else
				{   
					console.log(response);
				    modal.addClass('alert-danger');
					modal.html('Unable to process your request at the moment. Please, try again.');
				 	
				}
				
				$('#alertmodal').addClass('show-alert');
				showNotices();
				
			});
		
		}
	});

    jQuery('.request-attend').click(function () {
        var cur_e = jQuery(this);
        var event_id = cur_e.attr("data-id");
        var data = {
            action: 'event_attend_response',
            event_id: event_id,
            csrf_token: startup_services_script.csrf_token,
        };
        jQuery.post(startup_services_script.shubservices_forms, data, function(response) {
            var modal = $('#alertmodal > div');
            modal.removeClass('alert-danger');
            modal.removeClass('alert-success');            
            if(response=="present")
            {
                modal.addClass('alert-danger');
                modal.html('You have already requested to attend this event.<br><br>We will be in touch with you shortly to let you know the status of your participation.');                
            }
            if(response=="added")
            {
                modal.addClass('alert-success');
                modal.html('Thank you for registering in our event and we hope to see you there.');                
            }
            $('#alertmodal').addClass('show-alert');
            showNotices();
        });
        return false;
    });

    jQuery('.request-attend-login').click(function () {

        var modal = $('#alertmodal > div');
        modal.removeClass('alert-danger');
        modal.removeClass('alert-success');

        modal.addClass('alert-danger');
        modal.html('You need to be registered member of the site to access this feature. Please login/register.');

        $('#alertmodal').addClass('show-alert');
        showNotices();
        return false;
    });

    jQuery('.trade_licence input').click(function () {
        var trade_licence = $(this).val();
        if(trade_licence == 'Yes')
        {
            $('#companyinfo').find('.form-control').removeClass('ignore');
            $('#companyinfo').show();
        }
        else
        {
            $('#companyinfo').find('.form-control').addClass('ignore');
            $('#companyinfo').hide();   
        }
    });
	
    $.validator.addMethod('validUrl', function(value, element) {
        var url = $.validator.methods.url.bind(this);
        return url(value, element) || url('http://' + value, element);
      }, 'Please enter a valid URL');
	
	$(".newsletter-form").validate({
		rules: {
			email: {
				required: true,
				email: true
			},
            firstname: "required",
            lastname: "required"
		},
		messages: {
			email: {
				required: "Please enter your email",
				email: "Please enter a valid email address",
			},
            firstname: {
                required: "Please enter your first name",
            },
            lastname: {
                required: "Please enter your last name",
            }
		},
		tooltip_options: {
           email: {placement:'bottom',html:true},
           firstname: {placement:'bottom',html:true},
           lastname: {placement:'bottom',html:true}
        },
		
		submitHandler:function(){
			//console.log("submitHandler success");
			 var data = {
                action: 'save_subscriber',
                csrf_token: startup_services_script.csrf_token,
				email: $('#newsletter_email').val(),
                firstname: $('#newsletter_firstname').val(),
                lastname: $('#newsletter_lastname').val(),
            };
			
			var modal = $('.response-msg');
			
			$.post(startup_services_script.shubservices_forms, data, function(response) {
			    if(response == "success")
				{
					modal.show();
					modal.html('Thank you for subscribing to our news letter!');					
				}
				else
				{   console.log(response);				 
				 	modal.show();
				 	if(response == 'duplicate'){
					   modal.html('Your email already exists.'); 
				    }
				    else{
					   modal.html('Unable to process your request at the moment. Please, try again.');
					}
				}
				  //showNotices();
			});
		
		}
	});
});

$(function() {
    $('.shub-form-submit').on('click', function(e) {
        $(this).parents('form').submit();
    });

    // New forum topic validation
    $('#new_form_topic').validate({
        rules: {
			
            bbp_forum_id: {
                required: true
            },
			/*bbp_parent_forum_id: {
                required: true
            },*/
            bbp_topic_title: {
                required: true
            },
           /* bbp_topic_content: {
                required: true
            }, */
        },
        messages: {
			
			bbp_forum_id: {
                required: 'Please select the topic'
            },
			
            /* bbp_parent_forum_id: {
                required: 'Please select the topic'
            },*/
            bbp_topic_title: {
                required: 'Please enter the title'
            },
           /* bbp_topic_content: {
                required: 'Please enter the content'
            },*/
        },
        errorPlacement: function(error, element) {
            error.insertBefore(element);
        }
    });

    $('.need-signin').click(function(e) {        
        $("html, body").animate({ scrollTop: 0 }, "slow", function() {
            if (!$('#loginfrm').is(':visible'))
                $('.btn-login-dropdown').trigger('click');
            
            $('#sh_username').focus();
        });
    });

    $(".startConversation").click(function () {
        if(pro_status == "1")
        {
            $(".conversationBox").show();
        }
        else
        {
            if(!$(this).hasClass('need-signin'))
            {
                    jQuery('.modal-error .alert-danger').html('You have not completed your profile yet. <a href="'+siteURL+'/dashboard">click here to complete the profile</a>');
                    $('<a href="#close-modal" rel="modal:close" class="close-modal"><span class="close-text">X</span>').appendTo('.modal-error');
                    $('.modal-error').modal({
                        blockerClass: "jquery-modal",  // CSS class added to the overlay (blocker).
                        modalClass: "modal",    // CSS class added to the element being displayed in the modal.
                        showClose: false, 
                        fadeDuration: 400    // Number of milliseconds the fade transition takes (null means no transition)
                    });
            }           
            
            //jQuery('#alertmodal .modal-body').html('You have not completed your profile yet. <a href="'+siteURL+'/dashboard">click here to complete the profile</a>');
            //jQuery('#alertmodal').modal('show');
        }
     });

    // Forum topic resolve
    $('.shub-topic-resolve').on('click', function() {
        var ele = $(this);
        var opt = confirm('Are you sure?');
        if (opt) {
            var data = {
                topic_id: ele.data('topic'),
                action: 'shub_topic_resolve',
                csrf_token: startup_services_script.csrf_token
            }
            $.post(startup_services_script.shubservices_forms, data, function(res) {
                if (res.success)
                    ele.replaceWith('Yes');
            });
        }

        return false;
    });
	/*
    $('#bbp_parent_forum_id').on('change', function() {
        var parent_id = $("#bbp_parent_forum_id option:selected").val();
		
		if(parent_id == ""){
			$('#bbp_forum_id').empty();
			$('#bbp_forum_id').append( $('<option></option>').val('').html('Select sub topic'));
			return false;
		}
		
        var data = {
            parent_id: parent_id,
            action: 'shub_forum_child',
            csrf_token: startup_services_script.csrf_token
        }
        $.post(startup_services_script.shubservices_forms, data, function(res) {
            if (res.success) {

                var forum_sub_topics = res.data.message;
                console.log(forum_sub_topics);
                if(forum_sub_topics!=null){
                    $('#bbp_forum_id').empty();
                    $('#bbp_forum_id').append( $('<option></option>').val('').html('Select sub topic'));
                    $.each(forum_sub_topics, function(val, text) {
                        $('#bbp_forum_id').append( $('<option></option>').val(val).html(text));
                    });
                }
                else
                {
					$('#bbp_forum_id').empty();
					$('#bbp_forum_id').append( $('<option></option>').val('').html('Other'));   
                }
            }
        });
        return false;
    });
	*/
    // Forum reply mark as like
    $('.shub-reply-like').on('click', function() {
        var ele = $(this);

        var data = {
            reply_id: ele.data('reply'),
            action: 'shub_reply_like',
            mode: ele.data('mode'),
            csrf_token: startup_services_script.csrf_token
        }
        $.post(startup_services_script.shubservices_forms, data, function(res) {
            if (res.success) {
                ele.find('span').html(res.data.new_count);
            }
        });

        return false;
    });

    $('.user-controls').on('click', function() {
        $(".login_Holder").slideToggle('1000');
        return false;
    });

    showNotices();
});

// Short notices
function showNotices() {
    if ($('#alertmodal').hasClass('show-alert')){
        $('<a href="#close-modal" rel="modal:close" class="close-modal"><span class="close-text">X</span>').appendTo('.modal-error');
        $('.modal-error').modal({
            blockerClass: "jquery-modal",  // CSS class added to the overlay (blocker).
            modalClass: "modal",    // CSS class added to the element being displayed in the modal.
            showClose: false, 
            fadeDuration: 400    // Number of milliseconds the fade transition takes (null means no transition)
        });
        $('#alertmodal').removeClass('show-alert');
    }
}

$(window).load(function () {
    function equaliseIt() {
        $('.latestupdate_boxes').each(function () {
            var highestBox = 0;
            $('.latestupdatecolumn', this).each(function () {
                if ($(this).height() > highestBox)
                    highestBox = $(this).height();
            });

            $('.latestupdatecolumn', this).height(highestBox);
        });
    }
    //call the function at page load
    equaliseIt();
});

/*
function showChildComment(id) {
    if ($("#Continue_reading" + id).text() == "Continue reading") {
        $("#rowComments" + id).show("normal");
        $("#Continue_reading" + id).text("Close");            
    }
    else {
        $("#rowComments" + id).hide("normal");
        $("#Continue_reading" + id).text("Continue reading");
    }
}
*/

function validate_accountsetting()
{    
    var up_Full_name = jQuery('#up_Full_name');
    var up_Company = jQuery('#up_Company');
    var up_Email = jQuery('#up_Email');
    var Country = $('#up_Country');
    var Industry = $('#up_Industry');
    var OrganisationType = $('#up_OrganisationType');
	var avatarFile = $('#avatar_file');
	var up_user_label = $('.up_user_label');
	
    var err = 0;
    
    if(up_Full_name.val()=="")
    {
        up_Full_name.addClass('has-error');
        err = 1;
    }
	else if(up_Full_name.val().length<6)
	{
		up_Full_name.addClass('has-error');
		up_user_label.html("Full name must be at least 6 characters.");
		err = 1;
	}
	else
	{
		up_Full_name.removeClass('has-error');
		up_user_label.html("");
	}

    if(up_Company.val()=="")
    {
        up_Company.addClass('has-error');
        err = 1;
    }
    else
    {
        up_Company.removeClass('has-error');
    }

    if(up_Email.val()=="")
    {
        up_Email.addClass('has-error');
        err = 1;
    }
    else if( !isValidEmailAddress( up_Email.val() ) ) 
    { 
        up_Email.addClass('has-error');
        err = 1;
    }
    else
    {
        up_Email.removeClass('has-error');
    }

    if(Country.val()=="")
    {
        Country.addClass('has-error');
        err = 1;
    }
    else
    {
        Country.removeClass('has-error');
    }
    
    if(OrganisationType.val()=="")
    {
        OrganisationType.addClass('has-error');
        err = 1;
    }
    else if(OrganisationType.val()=="Startup")
    {
        if(Industry.val()=="")
        {
            Industry.addClass('has-error');
            err = 1;
        }
        else
        {
            Industry.removeClass('has-error');
        }
    }
    else
    {
        OrganisationType.removeClass('has-error');
    }
	
	// check if avatar is not empty
	if(!$('.profile-pic-wrapper .hasImage').length)
	{
		if(!$('#avatar_file').get(0).files.length)
		{
			err = 1;
			$('#avatar_file').addClass('err-file');
		}
	}
	else 
	{
		$('#avatar_file').removeClass('err-file');
	}
	
    if(err == 1)
    {
        return false;
    }
    else
    {
        document.updateuser.submit();
    }
}

function validate_passwordchange()
{
    var current_password = jQuery('#current_password');
    var password = jQuery('#new_password');
    var cpassword = jQuery('#con_password');

    
    var err = 0;
    if(current_password.val()=="")
    {
        current_password.addClass('error');
        err = 1;
    }
    else
    {
        current_password.removeClass('error');
    }

    var err = 0;
    if(password.val()=="")
    {
        password.addClass('error');
        err = 1;
    }
    else
    {
        password.removeClass('error');
    }

    if(password.val().length < 6)
    {
        password.addClass('error');
        jQuery('.change-pass-error').html("Password should be atleast 6 characters");
        err = 1;
    }
	else if(isWeakPassword(password.val()))
	{
		password.addClass('error');
        jQuery('.change-pass-error').html("Password complexity failed");
        err = 1;	
	}
    else
    {
        jQuery('.change-pass-error').html("");
    }

    if(cpassword.val()=="")
    {
        cpassword.addClass('error');
        err = 1;
    }
    else
    {
        cpassword.removeClass('error');
    }

    if(password.val()!=cpassword.val())
    {
        cpassword.addClass('error');
        jQuery('.change-pass-error').html("Password don't match");
        err = 1;
    }

    if(err == 1)
    {
        return false;
    }
    else
    {
        document.changepassword.submit();
    }

}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function isWeakPassword(pword){
	
    var variations = {
        //digits: /\d/.test(pword),
        lower: /[a-z]/.test(pword),
        upper: /[A-Z]/.test(pword),
        //nonWords: /\W/.test(pword),
    }

    variationCount = 0;
    for (var check in variations) {
		variationCount += (variations[check] == true) ? 0 : 1;	
    }
	// password is weak if the return is greater than 0
	return variationCount; 	
}



$('#ma_form_enquiry').validate({
		rules: {
            ma_name: {
                required: true,
				minlength: 3,
				maxlength: 30,
            },
            ma_email: {
                required: true,
				minlength: 4,
				email:true,
				maxlength: 50,
            },
            ma_contact: {
                required: true,
				minlength: 6,
				maxlength: 30,
            },
        },
        messages: {
            ma_name: {
                required: 'Please enter your name'
            },
            ma_email: {
                required: "Please enter your email",
				email: "Please enter a valid email",
            },
            ma_contact: {
                required: 'Please enter your contact number'
            },
        },
		submitHandler:function(form){
		
			var data = {
                action: $(form).find('input[name="action"]').val(),
                ma_name: $(form).find('input[name="ma_name"]').val(),
				ma_email: $(form).find('input[name="ma_email"]').val(),
                ma_contact: $(form).find('input[name="ma_contact"]').val(),
                ma_message: $(form).find('textarea[name="ma_message"]').val(),
                csrf_token: startup_services_script.csrf_token,
            };
			
			$.post(startup_services_script.shubservices_forms, data, function(response) {
				
				if(response == "success")
				 {					
					$('#ma_return_msg').html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your contact details have been received and our team representative will be in touch with you shortly.</div>');
					$(form).find('input[name="ma_name"]').val('');
					$(form).find('input[name="ma_email"]').val('');
					$(form).find('input[name="ma_contact"]').val('');
                    $(form).find('textarea[name="ma_message"]').val('');
					return false;	
				 }
				 else
				 {
					$('#ma_return_msg').html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Unable to save your enquiry, please reload the page and try again.</div>');
					return false;	 
				 }

			});
		}
});
$('#cf_form_enquiry').validate({
		rules: {
            ma_name: {
                required: true,
				minlength: 3,
				maxlength: 30,
            },
            ma_email: {
                required: true,
				minlength: 4,
				email:true,
				maxlength: 50,
            },
            ma_contact: {
                required: true,
				minlength: 6,
				maxlength: 30,
            },
        },
        messages: {
            ma_name: {
                required: 'Please enter your name'
            },
            ma_email: {
                required: "Please enter your email",
				email: "Please enter a valid email",
            },
            ma_contact: {
                required: 'Please enter your contact number'
            },
        },
		submitHandler:function(form){
		
			var data = {
                action: 'cf_save_enquiry',
                ma_name: $(form).find('input[name="ma_name"]').val(),
				ma_email: $(form).find('input[name="ma_email"]').val(),
                ma_contact: $(form).find('input[name="ma_contact"]').val(),
                csrf_token: startup_services_script.csrf_token,
            };
			
			$.post(startup_services_script.shubservices_forms, data, function(response) {
				
				if(response == "success")
				 {					
					$('#ma_return_msg').html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your contact details have been received and our team representative will be in touch with you shortly.</div>');
					$(form).find('input[name="ma_name"]').val('');
					$(form).find('input[name="ma_email"]').val('');
					$(form).find('input[name="ma_contact"]').val('');
					return false;	
				 }
				 else
				 {
					$('#ma_return_msg').html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Unable to save your enquiry, please reload the page and try again.</div>');
					return false;	 
				 }

			});
		}
});
jQuery('.register-interest').click(function () {
    var cur_e = jQuery(this);
    var page_id = cur_e.attr("data-id");
    var data_action = cur_e.attr("data-action");
    var data = {
        action: data_action,
        page_id: page_id,
        csrf_token: startup_services_script.csrf_token,
    };
    jQuery.post(startup_services_script.shubservices_forms, data, function(response) {        
        if(response=="added")
        {
            if(data_action=='save_guest_speaker')
            {
                $('#ma_return_msg').html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Thank you for registering an interest, a member of our team will be in touch soon!</div>');
            }
            if(data_action=='save_competition')
            {
                $('#ma_return_msg').html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Thank you for registering an interest, we will keep you posted on future roadshow dates!</div>');
            }
            return false;
        }
        else
        {
            $('#ma_return_msg').html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Unable to save your enquiry, please reload the page and try again.</div>');
            return false;
        }
    });
    return false;
});

