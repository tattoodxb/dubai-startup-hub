//slidefade
$.fn.slideFadeToggle = function (speed, easing, callback) {
    return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
};

$.fn.slideFadeIn = function (speed, easing, callback) {
    return this.animate({opacity: 'show', height: 'show'}, speed, easing, callback);
};

$.fn.slideFadeOut = function (speed, easing, callback) {
    return this.animate({opacity: 'hide', height: 'hide'}, speed, easing, callback);
};

//mega menu
$(function () {
    //window.prettyPrint && prettyPrint()

    $('.profile_pic .dropdown-menu li a').click(function (e) {
        window.location.href = $(this).attr('href');
    });

    $(document).on('click', '.megamenu .dropdown-menu', function (e) {
        e.stopPropagation()
    });
    var tiggered = 1;
    $('.megamenu .dropdown .dropdown-toggle').click(function (e) {
        e.stopPropagation();

        var parent = $(this).parent(".dropdown");
        var target = parent.find(">.dropdown-menu");
        var currentOpenParent = $('.megamenu .dropdown.open-menu');
        var currentOpen = $('.megamenu .dropdown.open-menu > .dropdown-menu');
        var currentOpenCount = $('.megamenu .dropdown.open-menu > .dropdown-menu').length;

        if (parent.hasClass("open-menu")) {
            console.log("if");
            parent.removeClass("open-menu");
            target.stop().clearQueue().slideFadeOut();
        } else {
            if (currentOpenCount > 0) {
                console.log("else if ");
                currentOpenParent.removeClass("open-menu");
                currentOpen.stop().clearQueue().slideFadeToggle();
                parent.addClass("open-menu");
                target.stop().clearQueue().slideFadeIn();
            } else {
                console.log("else else");
                parent.addClass("open-menu");
                target.stop().clearQueue().slideFadeIn(function () {

                });
                if (tiggered == 1) {
                    SameHeightMenu();
                    tiggered = 0;
                }
            }
        }
    });
    $(document).click(function (event) {
        var target = $(event.target);
        //console.log(target.context.className);
        //console.log(!$(event.target).closest('.megamenu .dropdown .dropdown-toggle').length);
        var className = target.context.className;
        if (!$(event.target).closest('.megamenu .dropdown .dropdown-toggle').length) {
            if (target.hasClass("close-modal") || target.hasClass("close-text") || target.hasClass("jquery-modal") || className == "btn-text-login-ft") {
                // console.log("dont close menu");
            } else {
                console.log("close menu");
                if ($('.megamenu .dropdown-menu').is(":visible")) {
                    $('.megamenu .dropdown-menu:visible').slideFadeToggle();
                    $('.megamenu .dropdown.open-menu').removeClass("open-menu");
                    $(".tcon").toggleClass("tcon-transform");
                }
            }
        }
    });
})

//same maxheight
setTimeout(function () {
    var partnerH = jQuery('.same-height-text').map(function () {
        return jQuery(this).height();
    }).get();
    var partnermaxH = Math.max.apply(null, partnerH);
    jQuery('.same-height-text').height(partnermaxH);
}, 500);

setTimeout(function () {
    var partnerH = jQuery('.member-details-wr .updates-info-wr').map(function () {
        return jQuery(this).height();
    }).get();
    var partnermaxH = Math.max.apply(null, partnerH);
    jQuery('.member-details-wr .updates-info-wr').height(partnermaxH);
}, 500);

setTimeout(function () {
    var partnerH = jQuery('.section-feeds-cols article .tags-wr').map(function () {
        return jQuery(this).height();
    }).get();
    var partnermaxH = Math.max.apply(null, partnerH);
    jQuery('.section-feeds-cols-3 article .tags-wr').height(partnermaxH);
}, 500);


function MatchHPartners() {
    setTimeout(function () {
        var partnerH = jQuery('.slider-partners .slick-track .col-sm-2').map(function () {
            return jQuery(this).height();
        }).get();
        var partnermaxH = Math.max.apply(null, partnerH);
        jQuery('.slider-partners .slick-track .col-sm-2').height(partnermaxH);
    }, 500);

}

function dropdownSelectCut() {
    var $select = $('select');

    // Init selectpicker.
    $select.selectpicker();

    // Loop through all of the option links in the dropdown menu.
    $select.data('selectpicker').$menu.find('li a').each(function () {
        var $link = $(this)
                , $text = $link.find('span.text')
                ;

        // Bind to mouseenter.
        $link.on('mouseenter', function () {
            // Clone the text & append it to the body so we can measure its natural width.
            var $clone = $text.clone().appendTo('body')
                    , diff = ($clone.width() - $text.width())
                    ;

            // Remove our dummy clone.
            $clone.remove();

            // If the text content is wider than the menu, animate the `text-indent`.
            if (diff > 0) {
                $text.stop(true).delay(250).animate({textIndent: '-' + diff + 'px'});
            }
        });

        // On mouseleave, animate the `text-indent` back to `0`.
        $link.on('mouseleave', function () {
            $text.stop(true).delay(250).animate({textIndent: 0});
        });
    });
}

function scrollToAnchor() {
    console.log("scroll to anchor");
    var thisEl = jQuery(location).attr('href');
    var anchor = thisEl.split('#')[1];
    var anchoredid = "#" + anchor;

    //add selected to anchored
    jQuery('a[href$="' + thisEl + '"]').parent().addClass("selected");

    if (jQuery(anchoredid).length > 0) {
        console.log("scroll to anchor now");
        var anchoroffset = jQuery(anchoredid).offset().top;
        var navH = jQuery("header").outerHeight() * 2;
        var offsetT = anchoroffset - navH;
        var offsetT = anchoroffset;
        console.log(anchoroffset);
        console.log(navH);
        console.log(offsetT);
        jQuery('html, body').animate({scrollTop: offsetT}, 800);
    } else {
        //console.log("nothing to scroll");
    }
}

function printNow() {
    var thisEl = jQuery(location).attr('href');
    var anchor = thisEl.split('#')[1];
    console.log(anchor);
    if (anchor == "print") {
        console.log("print now");
        setTimeout(function () {
            javascript:window.print();
        }, 1200);
    }
}

//Convert SVG image to xml
function SVGtoIMG() {
    //svg image to xml

    var count = $('.svg-img').length;
    var current = 0;
    $('.svg-img').each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('data-src');
        if (imgURL) {
            var req = $.get(imgURL, function (data) {
                current++;

                // Get the SVG tag, ignore the rest
                var $svg = $(data).find('svg');

                // Add replaced image's ID to the new SVG
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
                if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                    $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                }

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml').done(function (data) {
                if (current >= count) {
                    console.log("svg Convertion stopped");
                    req.abort();
                    /*$(".loader-wr").fadeOut( function() {
                     FxAnimate();
                     adjustGridIconSize();
                     GridInit();
                     });*/
                }
            });
        }
    });

}

function LogoSVGtoIMG() {
    //svg image to xml

    var count = $('.logo-svg').length;
    var current = 0;
    $('.logo-svg').each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('data-src');
        if (imgURL) {
            var req = $.get(imgURL, function (data) {
                current++;

                // Get the SVG tag, ignore the rest
                var $svg = $(data).find('svg');

                // Add replaced image's ID to the new SVG
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
                if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                    $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                }

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml').done(function (data) {
                if (current >= count) {
                    console.log("svg Convertion stopped");
                    req.abort();
                    logoFxAnimate();
                    /*$(".loader-wr").fadeOut( function() {
                     FxAnimate();
                     adjustGridIconSize();
                     GridInit();
                     });*/
                }
            });
        }
    });

}

//same height Menu
function SameHeightMenu() {
    console.log("same height Menu called");
    $('.same-height-all-menu').each(function () {
        var target = $(this).children();
        target.matchHeight({
            byRow: false,
            property: 'height',
            target: null,
            remove: false
        });
    });
}

//same height all
function SameHeightAll() {
    console.log("same height all called");
    $('.same-height-all').each(function () {
        var target = $(this).children();
        target.matchHeight({
            byRow: false,
            property: 'height',
            target: null,
            remove: false
        });
    });


}

function SameHeightArticle() {
    console.log("same height same-height-article called");
    $('.same-height-article').each(function () {
        var target = $(this).find("article");
        target.matchHeight({
            byRow: false,
            property: 'height',
            target: null,
            remove: false
        });
    });


}

//function cut text
function ellipsis() {
    $(".ellipsis").each(function () {
        var wrapper = $(this);

        wrapper.dotdotdot({

            callback: function (isTruncated) {},
            /* Function invoked after truncating the text.
             Inside this function, "this" refers to the wrapper. */

            ellipsis: "\u2026 ",
            /* The text to add as ellipsis. */

            height: null,
            /* The (max-)height for the wrapper:
             null: measure the CSS (max-)height ones;
             a number: sets a specific height in pixels;
             "watch": re-measures the CSS (max-)height in the "watch". */

            keep: null,
            /* jQuery-selector for elements to keep after the ellipsis. */

            tolerance: 0,
            /* Deviation for the measured wrapper height. */

            truncate: "letter",
            /* How to truncate the text: By "node", "word" or "letter". */

            watch: "window",
            /* Whether to update the ellipsis: 
             true: Monitors the wrapper width and height;
             "window": Monitors the window width and height. */

        });
    });

    $(".first-reply-wr .ellip").each(function () {
        var wrappr = $(this);
        wrappr.removeClass('autoheight');
        wrappr.dotdotdot({

            callback: function (isTruncated) {},
            /* Function invoked after truncating the text.
             Inside this function, "this" refers to the wrapper. */

            ellipsis: "\u2026 ",
            /* The text to add as ellipsis. */

            height: null,
            /* The (max-)height for the wrapper:
             null: measure the CSS (max-)height ones;
             a number: sets a specific height in pixels;
             "watch": re-measures the CSS (max-)height in the "watch". */

            keep: null,
            /* jQuery-selector for elements to keep after the ellipsis. */

            tolerance: 0,
            /* Deviation for the measured wrapper height. */

            truncate: "letter",
            /* How to truncate the text: By "node", "word" or "letter". */

            watch: "window",
            /* Whether to update the ellipsis: 
             true: Monitors the wrapper width and height;
             "window": Monitors the window width and height. */

        });
    });
}

function slider() {
    console.log("slider() called");
    $(".responsive-slider").each(function () {
        console.log("responsive-slider called");
        var thisSlider = $(this);
        if (thisSlider.hasClass("banner-slider")) {
            console.log("banner-slider");
            var autoplayOpt = true;
            var lgCount = 1;
            var mdCount = 1;
            var smCount = 1;
            var xsCount = 1;
            var lgtoscroll = 1;
            var mdtoscroll = 1;
            var smtoscroll = 1;
            var xstoscroll = 1;
            var dotsOpt = true;
            var arrowsOpt = true;
            var infiniteOpt = true;
            var centerModeOpt = false;
            var fade = false;
            var prevArrow = '<button type="button" class="slick-custom-arrow slick-prev-arrow"><svg height="13" viewBox="0 0 28 13" width="28" xmlns="http://www.w3.org/2000/svg"><path d="m20.3636364 0 7.6363636 6.18845316-7.6363636 6.18845314v-4.95076251h-20.3636364v-2.47538126h20.3636364z" fill="#ed6b06" fill-rule="evenodd" transform="matrix(-1 0 0 1 28 0)"/></svg></button>'
            var nextArrow = '<button type="button" class="slick-custom-arrow slick-next-arrow"><svg height="13" viewBox="0 0 28 13" width="28" xmlns="http://www.w3.org/2000/svg"><path d="m20.3636364 0 7.6363636 6.18845316-7.6363636 6.18845314v-4.95076251h-20.3636364v-2.47538126h20.3636364z" fill="#ed6b06" fill-rule="evenodd" transform=""/></svg></button>';
        }else if (thisSlider.hasClass("slider-testimonials")) {
            var autoplayOpt = true;
            var lgCount = 1;
            var mdCount = 1;
            var smCount = 1;
            var xsCount = 1;
            var lgtoscroll = 1;
            var mdtoscroll = 1;
            var smtoscroll = 1;
            var xstoscroll = 1;
            var dotsOpt = true;
            var arrowsOpt = true;
            var infiniteOpt = true;
            var centerModeOpt = false;
            var fade = false;
            var prevArrow = '<button type="button" class="slick-custom-arrow slick-prev-arrow"><img alt="Go to previous slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-left.svg" onerror="this.src=\'' + templateURL + 'images/caret-left.png\'"></button>'
            var nextArrow = '<button type="button" class="slick-custom-arrow slick-next-arrow"><img alt="Go to next slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-right.svg" onerror="this.src=\'' + templateURL + 'images/caret-right.png\'"></button>';
        } else if (thisSlider.hasClass("slider-partners")) {
            var autoplayOpt = true;
            var lgCount = 5;
            var mdCount = 5;
            var smCount = 3;
            var xsCount = 3;
            var lgtoscroll = 1;
            var mdtoscroll = 1;
            var smtoscroll = 1;
            var xstoscroll = 1;
            var dotsOpt = false;
            var arrowsOpt = false;
            var infiniteOpt = true;
            var xsinfiniteOpt = true;
            var centerModeOpt = true;
            var xscenterModeOpt = true;
            var fade = false;
            var prevArrow = '<button type="button" class="slick-custom-arrow slick-prev-arrow"><img alt="Go to previous slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-left.svg" onerror="this.src=\'' + templateURL + 'images/caret-left.png\'"></button>'
            var nextArrow = '<button type="button" class="slick-custom-arrow slick-next-arrow"><img alt="Go to next slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-right.svg" onerror="this.src=\'' + templateURL + 'images/caret-right.png\'"></button>';
        } else if (thisSlider.hasClass("floorplans-slider")) {
            var autoplayOpt = false;
            var lgCount = 2;
            var mdCount = 2;
            var smCount = 2;
            var xsCount = 1;
            var lgtoscroll = 1;
            var mdtoscroll = 1;
            var smtoscroll = 1;
            var xstoscroll = 1;
            var dotsOpt = true;
            var arrowsOpt = true;
            var infiniteOpt = true;
            var xsinfiniteOpt = true;
            var centerModeOpt = false;
            var xscenterModeOpt = true;
            var fade = false;
            var prevArrow = '<button type="button" class="slick-custom-arrow slick-prev-arrow"><img alt="Go to previous slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-left.svg" onerror="this.src=\'' + templateURL + 'images/caret-left.png\'"></button>'
            var nextArrow = '<button type="button" class="slick-custom-arrow slick-next-arrow"><img alt="Go to next slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-right.svg" onerror="this.src=\'' + templateURL + 'images/caret-right.png\'"></button>';
        } else if (thisSlider.hasClass("portfolio-slider")) {
            var autoplayOpt = false;
            var lgCount = 1;
            var mdCount = 1;
            var smCount = 1;
            var xsCount = 1;
            var lgtoscroll = 1;
            var mdtoscroll = 1;
            var smtoscroll = 1;
            var xstoscroll = 1;
            var dotsOpt = true;
            var arrowsOpt = false;
            var infiniteOpt = true;
            var xsinfiniteOpt = true;
            var centerModeOpt = false;
            var xscenterModeOpt = true;
            var fade = false;
            var prevArrow = '<button type="button" class="slick-custom-arrow slick-prev-arrow"><img alt="Go to previous slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-left.svg" onerror="this.src=\'' + templateURL + 'images/caret-left.png\'"></button>'
            var nextArrow = '<button type="button" class="slick-custom-arrow slick-next-arrow"><img alt="Go to next slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-right.svg" onerror="this.src=\'' + templateURL + 'images/caret-right.png\'"></button>';
        } else {
            var autoplayOpt = false;
            var lgCount = 3;
            var mdCount = 3;
            var smCount = 2;
            var xsCount = 2;
            var lgtoscroll = 1;
            var mdtoscroll = 1;
            var smtoscroll = 1;
            var xstoscroll = 1;
            var dotsOpt = true;
            var arrowsOpt = true;
            var infiniteOpt = true;
            var centerModeOpt = true;
            var fade = false;
            var prevArrow = '<button type="button" class="slick-custom-arrow slick-prev-arrow"><img alt="Go to previous slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-left.svg" onerror="this.src=\'' + templateURL + 'images/caret-left.png\'"></button>'
            var nextArrow = '<button type="button" class="slick-custom-arrow slick-next-arrow"><img alt="Go to next slide" class="carret-left svg-img img-responsive" src="' + templateURL + 'images/caret-right.svg" onerror="this.src=\'' + templateURL + 'images/caret-right.png\'"></button>';
        }

        /*thisSlider.on('init', function(){ 
         console.log("slider init");
         if(thisSlider.hasClass("slider-testimonials")){
         var dots = $(".slider-testimonials .slick-dots");
         console.log(dots);
         }
         
         if(thisSlider.hasClass("portfolio-slider")){
         //portfolio height adjsut
         if(adjustOnce==true){
         setTimeout(function() {	
         console.log("addjust H Portfolio");
         $('.portfolio-half').matchHeight({ property: 'height' });
         }, 500);
         adjustOnce = false;
         }
         }
         });*/

        thisSlider.on('init reInit', function (event, slick) {
            console.log("slider init");
            if (thisSlider.hasClass("slider-testimonials")) {
                var prev = thisSlider.find(".slick-prev-arrow");
                var list = thisSlider.find(".slick-dots");
                var next = thisSlider.find(".slick-next-arrow");
                var wrapper = thisSlider.parent().find(".slider-nav-wr");
                prev.appendTo(wrapper);
                list.appendTo(wrapper);
                next.appendTo(wrapper);
            } else if (thisSlider.hasClass("slider-partners")) {
                var prev = thisSlider.find(".slick-prev-arrow");
                var list = thisSlider.find(".slick-dots");
                var next = thisSlider.find(".slick-next-arrow");
                var wrapper = thisSlider.parent().find(".slider-nav-wr");
                prev.appendTo(wrapper);
                list.appendTo(wrapper);
                next.appendTo(wrapper);
            }
            MatchHPartners();
        });

        thisSlider.slick({
            dots: dotsOpt,
            infinite: infiniteOpt,
            centerMode: centerModeOpt,
            centerPadding: "0",
            lazyLoad: 'progressive',
            autoplay: autoplayOpt,
            autoplaySpeed: 3000,
            speed: 300,
            slidesToShow: lgCount,
            slidesToScroll: lgtoscroll,
            arrows: arrowsOpt,
            prevArrow: prevArrow,
            nextArrow: nextArrow,
            fade: fade,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: mdCount,
                        slidesToScroll: mdtoscroll,
                        infinite: true,
                        dots: dotsOpt
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: smCount,
                        slidesToScroll: smtoscroll
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: xsCount,
                        slidesToScroll: xstoscroll,
                        infinite: xsinfiniteOpt,
                        centerMode: xscenterModeOpt,
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });

}

function FxAnimate() {
    $('.fx').each(function (i) {
        var fx = $(this);
        var fxAnimate = fx.attr("data-animate");
        var visible = fx.visible(true, false, 'both');
        //console.log(fxAnimate);
        //console.log("======= " +i);
        //console.log("======= " +50*i);
        if (visible) {
            setTimeout(function () {
                fx.addClass('animated ' + fxAnimate);
            }, 50 * i);
        }


        if ($('.fx').length == i + 1) {
            //CurrentMenu();
        }
    });
}

function logoFxAnimate() {
    $('.logo-wr').each(function (i) {
        var fx = $(this);
        var fxAnimate = fx.attr("data-animate");
        var visible = fx.visible(true, false, 'both');
        //console.log(fxAnimate);
        //console.log("======= " +i);
        //console.log("======= " +50*i);
        if (visible) {
            setTimeout(function () {
                fx.addClass('animated ' + fxAnimate);
            }, 50 * i);
        }


        if ($('.logo-wr').length == i + 1) {
            //CurrentMenu();
        }
    });
}

function formValidate() {

    $(".startup-options").validate({
        //focusCleanup: true,
        /*onfocusout:function(element){
         this.element(element);
         },
         onkeyup:function(element){
         this.element(element);
         },
         ignore: "",*/
        rules: {
            selectoption: {
                required: true
            }
        },
        messages: {
            selectoption: "Just check the box<h5 class='text-danger'>You aren't going to read the EULA</h5>"
        },
        tooltip_options: {
            selectoption: {placement: 'top', html: true}
        },

        submitHandler: function () {
            /*console.log("submitHandler success");
             setTimeout(function(){
             
             //$("#request-form .alert").fadeIn();
             //$("#request-form input").val("");  
             //$(".select-theme-form-wr select.subcat").val("");
             //$(".select-theme-form-wr select.subcat").selectpicker('refresh');
             
             },10);*/

            return true;
        }
    });

    $(".comment-form").validate({
        rules: {
            bbp_reply_content: {
                required: true
            }
        },
        messages: {
            bbp_reply_content: "Please input your comment"
        },
        tooltip_options: {
            bbp_reply_content: {placement: 'bottom', html: true}
        },

        submitHandler: function () {
            return true;
        }
    });

    $(".search-menu-form").validate({
        rules: {
            search: {
                required: true
            }
        },
        messages: {
            search: "Please input a search key"
        },
        tooltip_options: {
            search: {placement: 'bottom', html: true}
        },

        submitHandler: function () {
            /*
             console.log("submitHandler success");
             setTimeout(function(){
             
             },10); */
            return true;
        }
    });

    /* AJAX REQUEST NEWSLETTER FORM WAS MOVED TO CUSTOM JS*/

    $(".menu-login-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: "required"
        },
        messages: {
            email: {
                required: "Please enter your email",
                email: "Please enter a valid email address",
            },
            password: "Please enter your password",

        },
        tooltip_options: {
            email: {placement: 'top', html: true},
            password: {placement: 'top', html: true}
        },

        submitHandler: function () {

            /*console.log("submitHandler success");
             setTimeout(function(){
             
             },10); */
            return true;
        }
    });

    $(".form-user-profile").validate({
        rules: {
            user_login: {
                required: true,
            },
            user_email: {
                required: true,
                email: true
            },
            pass1: {
                required: true,
            },
            pass2: {
                required: true,
                equalTo: "#pass1"
            },
            user_country: {
                required: true,
            },
            user_org: {
                required: true,
            },
            user_industry: {
                required: function (element) {
                    return $("#user_org").val() == "Startup";
                }
            }

        },
        messages: {
            user_login: {
                required: "Please enter your username",
            },
            user_email: {
                required: "Please enter your email",
                email: "Please enter a valid email address",
            },
            pass1: {
                required: "Please enter your password",
            },
            pass2: {
                required: "Please confirm your password",
                equalTo: "Password doesn't match",
            },
            user_country: {
                required: "Please enter your Country",
            },
            user_industry: {
                required: "Please select market",
            },
            user_org: {
                required: "Please select Organisation Type",
            },
        },
        tooltip_options: {
            user_login: {placement: 'top', html: true},
            user_email: {placement: 'top', html: true},
            pass1: {placement: 'top', html: true},
            pass2: {placement: 'top', html: true},
            user_country: {placement: 'top', html: true},
            user_industry: {placement: 'top', html: true},
            user_org: {placement: 'top', html: true},
        },

        submitHandler: function () {
            return true;
        }
    });

    /* AJAX REQUEST CONTACT US FORM WAS MOVED TO CUSTOM JS*/
    /*
     $(".form-contactus").validate({
     rules: {
     first_name: {
     required: true,
     },
     family_name: {
     required: true,
     },
     email: {
     required: true,
     email: true
     },
     subject: {
     required: true,
     },
     comments: {
     required: true,
     },
     },
     messages: {
     first_name: {
     required: "Please enter your first name",
     },
     family_name: {
     required: "Please enter your last name",
     },
     email: {
     required: "Please enter your email",
     email: "Please enter a valid email address",
     },
     subject: {
     required: "Please enter a subject",
     },
     comments: {
     required: "Please enter your message",
     },
     },
     tooltip_options: {
     first_name: {placement:'top',html:true},
     family_name: {placement:'top',html:true},
     email: {placement:'top',html:true},
     subject: {placement:'top',html:true},
     comments: {placement:'top',html:true},
     },
     
     submitHandler:function(){
     console.log("submitHandler success");
     var base_path = "2017/dubai-startup-hub";
     //var url = window.location.protocol + "//" + window.location.host + "/" + base_path + "/" + "my_action_callback";
     //console.log(url);
     $.get(the_ajax_script.ajaxurl, function(data){
     console.log(data);
     });
     
     setTimeout(function(){
     
     },10);
     }
     });
     */

}

function singleEventH() {
    var windowH = parseInt(jQuery(window).height());
    var singleEventH = windowH - 100;
    //$(".article-action-wr,.article-content-wr").css("height",singleEventH);
    var sourceH = $(".article-action-wr").outerHeight();
    $(".article-content-wr").css("height", sourceH)
}

function singleEventHPop() {
    var windowH = parseInt(jQuery(window).height());
    var singleEventH = windowH - 100;
    //$(".article-action-wr,.article-content-wr").css("height",singleEventH);
    var sourceH = $(".article-action-wr").outerHeight();
    $(".article-content-wr").css("height", sourceH);
    $("<style>.article-content-wr { max-height: " + sourceH + "px; }</style>").appendTo("head")
}

function modalError() {
    $('<a href="#close-modal" rel="modal:close" class="close-modal"><span class="close-text">X</span>').appendTo('.modal-error');
    $('.modal-error').modal({
        blockerClass: "jquery-modal", // CSS class added to the overlay (blocker).
        modalClass: "modal", // CSS class added to the element being displayed in the modal.
        showClose: false,
        fadeDuration: 5000    // Number of milliseconds the fade transition takes (null means no transition)
    });
}

function modalConvo() {
    $('.new-convo').click(function () {
        console.log("new convo");
        $('<a href="#close-modal" rel="modal:close" class="close-modal"><span class="close-text">X</span>').appendTo('.modal-convo');
        $('.modal-convo').modal({
            blockerClass: "jquery-modal", // CSS class added to the overlay (blocker).
            modalClass: "modal", // CSS class added to the element being displayed in the modal.
            showClose: false,
            fadeDuration: 400    // Number of milliseconds the fade transition takes (null means no transition)
        });
    });
}

function forumMore() {

    $(".forum-post-content .collapse").on('show.bs.collapse', function () {
        console.log('The collapsible content is about to be shown.');

        var element = $(this);
        var parent = $(this).parent(".media-body");
        var target = parent.find(".ellipsis");
        var ftarget = parent.find(".first-reply-wr .ellip");
        var triggerid = element.attr("id");
        var trigger = $('*[data-target="#' + triggerid + '"]');
        var forumMore = trigger.hasClass("forum-more");
        var actives = element.parents(".forum-post").find('.collapse.in');
        console.log(forumMore);
        console.log(trigger);
        console.log(triggerid);

        actives.collapse('hide');
        target.trigger('destroy');
        ftarget.trigger('destroy');
        ftarget.addClass('autoheight');

        trigger.removeClass("plus").addClass("minus");
        trigger.find('.txt-read').html('Read Less');

        setTimeout(function () {
            var outerHeight = 0;
            target.find("p").each(function () {
                outerHeight += $(this).outerHeight() + parseInt($(this).css("margin-bottom"));
            });
            console.log(outerHeight);

            target.animate({
                height: outerHeight
            }, {
                duration: 500,
                complete: function () {
                    run = 1;
                }
            });

        }, 100);

    });

    $(".forum-post-content .collapse").on('hide.bs.collapse', function () {

        var element = $(this);
        var parent = $(this).parent(".media-body");
        var target = parent.find(".ellipsis");
        var triggerid = element.attr("id");
        var trigger = $('*[data-target="#' + triggerid + '"]');
        var forumMore = trigger.hasClass("forum-more");


        trigger.find('.txt-read').html('Read More');
        trigger.removeClass("minus").addClass("plus");

        target.animate({
            height: "88px"
        }, {
            duration: 500,
            complete: function () {
                ellipsis();
            }
        });
    });



    $('.btn-add-comment').click(function () {
        var triggered = $(this);
        var parent = triggered.closest('.forum-post-content');
        var target = parent.find(".add-comment-textarea-wr");
        //var target = triggered.parents(".collapse-comments").find(".add-comment-textarea-wr");
        var loggedin = $("body").hasClass("logged-in");

        if (!loggedin) {
            $('html, body').animate({scrollTop: 0}, 500);
            setTimeout(function () {
                $(".btn-login-dropdown").trigger("click");
            }, 500);
        } else {

            if (!parent.find('.collapse-comments').hasClass('in'))
            {
                parent.find('.forum-more').trigger("click");
                target.slideToggle("fast");
            } else
            {
                target.slideToggle("fast");
            }

            setTimeout(function () {
                console.log("time done");
                var offset = $(target).offset().top;
                $('html, body').animate({scrollTop: offset}, 500);
            }, 100);
        }
    });
}

/* Will scroll to body after keyword search with on page load event */
function scrollToOnPageLoad()
{
    var url = window.location.href, expr = "?tag=";
    var keyword_expr = "?tag=";
    var profile_expr = "/login/";
    if (url.indexOf(keyword_expr) !== -1 && $('main section#section-events').length > 0) {
        $('html, body').animate({
            scrollTop: $('#section-events').offset().top
        }, 'slow');
    } else if (url.indexOf(profile_expr) !== -1 && $('main section#section-profile').length > 0) {
        $('html, body').animate({
            scrollTop: $('#section-profile').offset().top
        }, 'slow');
    }

}

function ChangeUrl(url) {
    if (void 0 !== history.pushState) {
        var o = {
            Title: 'Home - Dubai Startup Hub',
            Url: url
        };
        history.pushState(o, o.Title, o.Url)
    }
}

function getErrorMessageParams() {


    var currentLocation = window.location.href;
    var url = new URL(currentLocation);
    var param = url.searchParams.get("register");
    if (param == 'error') {
        var new_url = currentLocation.split("?");
        //console.log(new_url[0]);
        ChangeUrl(new_url[0]);
        $('.register-inner-wr .btn-register').trigger('click');
    }

}

function forumLiveAddClass() {
    if ($('.live-on').length)
    {
        $('.forum-menu-item > a').addClass('live');
    }
}

function scrollToAnchorById(anchoredid) {

    if ($(anchoredid).length > 0) {
        console.log("scroll to anchor now");
        var anchoroffset = $(anchoredid).offset().top;
        var navH = $("header").outerHeight() + $(".top-bar").outerHeight();
        var offsetT = anchoroffset - navH;
        $('html, body').animate({scrollTop: offsetT}, 800);
    } else {
        console.log("nothing to scroll");
    }
}


$(window).load(function () {

    scrollToOnPageLoad();

    console.log("loaded");
    var windowW = parseInt(jQuery(window).width());

    printNow();

    SVGtoIMG();
    modalConvo();

    FxAnimate();
    $(window).scroll(function () {
        FxAnimate();
    });

    forumMore();
    SameHeightAll();
    
    setTimeout(function () {
        SameHeightArticle();    
    }, 700);

    formValidate();
    ellipsis();

    $(".hover-fx").hover(function () {
        var animate = $(this).attr("data-hover-animate");
        var rmanimate = $(this).attr("data-animate");
        $(this).removeClass(rmanimate).addClass(animate);
    }, function () {
        var animate = $(this).attr("data-hover-animate");
        var rmanimate = $(this).attr("data-animate");
        $(this).removeClass(animate);
    });

    $(".tcon").click(function () {
        $(this).toggleClass("tcon-transform");
    });

    $(document).click(function (e) {
        //console.log(e.target);
        //console.log($(e.target).is('input'));

        if ($('.section-forum').length > 0) {
            //console.log("dont close forum");
        } else if ($(e.target).parents('.collapse.in').length > 0) {
            //console.log("dont close");
        } else if ($(e.target).is('.collapse.in')) {
            //console.log("dont close");
        } else if (!$(e.target).is('a') || !$(e.target).is('input')) {
            $('.collapse').collapse('hide');
        }
    });

    //move modal to bottom
    // was remove to resolve disappearing popup message
    //$(".modal").detach().appendTo('body');

    //add class to direct downlaods
    $(".direct-download-wr a").each(function () {
        $(this).addClass("direct-download");
    });

    //fade page on link click
    /*$("a").click(function(){
     var thisEl = $(this).hasClass('direct-download');
     console.log(thisEl);
     if(thisEl==true) {
     console.log("dont go out");
     window.onbeforeunload = function(){};
     } else {
     console.log("before going out");
     window.onbeforeunload = function(){
     console.log("going out");
     $(".pageWrapper").addClass("fade-out");
     };
     }
     });*/

    //click scroll to anchor
    $(".scroll-to").click(function (e) {
        console.log("scroll to");
        e.preventDefault();

        var thisEl = $(this);
        var anchor = thisEl.attr('href').split('#')[1];
        var anchoredid = "#" + anchor;

        //add active
        thisEl.parent().parent().find("li").removeClass("selected");
        thisEl.parent().addClass("selected");

        if ($(anchoredid).length > 0) {
            var anchoroffset = $(anchoredid).offset().top;
            var navH = $("header").outerHeight();
            var offsetT = anchoroffset - navH;
            var offsetT = anchoroffset;
            console.log(anchoroffset);
            console.log(navH);
            console.log(offsetT);
            $('html, body').animate({scrollTop: offsetT}, 800);
        }
    });

    //click scroll to anchor
    $(".scroll-to-trigger").click(function (e) {
        //console.log("scroll to focus");
        e.preventDefault();

        var thisEl = $(this);
        var anchor = thisEl.attr('href').split('#')[1];
        var anchoredid = "#" + anchor;
        var focus = $('[data-target="' + anchoredid + '"]');
        //console.log(anchoredid);
        //console.log(focus);

        window.location.href = thisEl.attr('href');
        /*
         if($(focus).length>0){
         var anchoroffset = $(focus).offset().top-30;
         $('html, body').animate({scrollTop: anchoroffset}, 500);
         }
         */


    });

    //scrollto anchor after load
    scrollToAnchor();

    if ($(".social-feed-twitter-wr").length) {
        console.log("social-feed-twitter-wr");
        $("body").append("<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = '//connect.facebook.net/en_IN/sdk.js#xfbml=1&version=v2.10&appId=513928072133333';fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>");

        $(".social-feed-twitter-wr").append('<a class="twitter-timeline" data-width="100%" data-height="600" data-tweet-limit="4" data-theme="light" data-link-color="#f37421" href="https://twitter.com/DubaiStartupHub?ref_src=twsrc%5Etfw">Tweets by DubaiStartupHub</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>');
    }

    if ($(".social-feed-fb-wr").length) {
        console.log("social-feed-fb-wr");
        //$(".social-feed-fb-wr").append('<div class="fb-page" data-href="https://www.facebook.com/dubaistartuphub/" data-tabs="timeline" data-width="400" data-height="605" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/dubaistartuphub/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dubaistartuphub/">Dubai Startup Hub</a></blockquote></div>');
    }

    if ($(".social-feed-linkedin-wr").length) {
        console.log("social-feed-linkedin-wr");
        if (windowW < 365) {
            var linkedin = '<script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/FollowCompany" data-id="10516438" data-format="inline" data-related="false" data-width="298"></script>'
        } else if (windowW < 380) {
            var linkedin = '<script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/FollowCompany" data-id="10516438" data-format="inline" data-related="false" data-width="313"></script>'
        } else if (windowW < 480) {
            var linkedin = '<script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/FollowCompany" data-id="10516438" data-format="inline" data-related="false" data-width="352"></script>'
        } else if (windowW < 1025) {
            var linkedin = '<script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/FollowCompany" data-id="10516438" data-format="inline" data-related="false" data-width="200"></script>'
        } else {
            var linkedin = '<script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/FollowCompany" data-id="10516438" data-format="inline" data-related="false" data-width="330"></script>'
        }
        $(".social-feed-linkedin-wr").append(linkedin);
    }

    $(".section-testimonials blockquote").each(function () {
        var target = $(this);
        var targetp = target.find("p");
        var lineH = parseInt(targetp.css("line-height"));
        var finalH = lineH * 3;
        var currH = parseInt(targetp.css("height"));

        if (currH > finalH) {
            ;
            targetp.addClass('ellips');
            //append button
            target.find('.r-more').append('<button type="button" class="btn btn-primary btn-sm">read more</button>');

        }

    });


    getErrorMessageParams();

    forumLiveAddClass();

});

var PrevWindowW = parseInt($(window).width());
$(window).on('load resize', function () {
    var CurrentWindowW = parseInt($(window).width()); //current width
    var windowH = parseInt($(window).height());
    var windowW = parseInt($(window).width());

    //console.log("loaded resize");

    setTimeout(function () {
        if (windowW < 480) {
            console.log("480");
            dropdownSelectCut();
        } else if (windowW < 740) {
            console.log("740");
        } else if (windowW < 993) {
            console.log("993");
        } else {
        }
    }, 100);

    setTimeout(function () {
        if (PrevWindowW != CurrentWindowW) {

        }
    }, 700);

});

// image upload preview on registration
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var maxSizeAllowed = 300000; // 300KB
        var err_msg = "";
        console.log('input.files[0]');
        console.log(input.files[0]);



        reader.onload = function (e) {

            var fileExtension = ['jpeg', 'jpg', 'png'];
            if ($.inArray(input.files[0]['name'].split('.').pop().toLowerCase(), fileExtension) == -1) {
                $('.delete-profile-pic').trigger('click');
                err_msg = "Only formats are allowed : " + fileExtension.join(', ');
                $('.profile-pic-wrapper').append('<p class="err_msg">' + err_msg + '</p>');
                console.log(err_msg);
                return false;
            } else if (input.files[0]['size'] > maxSizeAllowed)
            {
                $('.delete-profile-pic').trigger('click');
                err_msg = "Attachment size exceeds the allowable limit.";
                $('.profile-pic-wrapper').append('<p class="err_msg">' + err_msg + '</p>');
                console.log(err_msg);
                return false;
            } else
            {
                $('#imageHoldr').attr('src', e.target.result);
                $('#imageHoldr').show();
                $('#avatar_file').hide();
                $('.delete-profile-pic').show();
                $('p.err_msg').remove();
            }

        }

        reader.readAsDataURL(input.files[0]);

    }
}

function showLoginOrReg() {

    alert('Please login or register to apply for membership');
    $("html, body").animate({scrollTop: 0}, "slow");

    var windowW = parseInt(jQuery(window).width());
    if (windowW > 480) {

        if (!$("header .contain-menu-right").hasClass('open-menu')) {
            $('#hdr-btn-login').trigger('click');
        }
    } else {
        if (!$('.nav-login').hasClass('in')) {
            $('#hdr-btn-loginhdr-btn-login').trigger('click');
        }
    }

}

// registration upload image preview
$(".profile-pic-wrapper").on('change', '#avatar_file', function () {
    readURL(this);
});

$('.profile-pic-wrapper').on('click', '.delete-profile-pic', function () {
    var $html = '<input type="file" name="avatar_file" id="avatar_file" accept="image/jpeg,image/png">';
    $html += '<img  id="imageHoldr" class="img-holder-upload" src="#" alt="Your Image" />';
    $html += '<button type="button" class="delete-profile-pic btn btn-xs">Delete Photo</button>';
    $('.profile-pic-wrapper').html($html);
});


$(document).ready(function () {
    slider();

    LogoSVGtoIMG();

    singleEventH();

    $('[data-rel="modal:open"]').click(function (event) {

        /*$.ajax({
         
         url: $(this).attr('href') + '?page=popup',
         
         success: function(newHTML, textStatus, jqXHR) {
         $('<a href="#close-modal" rel="modal:close" class="close-modal "><span class="close-text">X</span>').appendTo('.modal-wr');
         $(newHTML).appendTo('.modal-wr');
         $('.modal-wr').modal({
         blockerClass: "jquery-modal",  // CSS class added to the overlay (blocker).
         modalClass: "modal",    // CSS class added to the element being displayed in the modal.
         showClose: false, 
         fadeDuration: 400    // Number of milliseconds the fade transition takes (null means no transition)
         });
         $('.modal-wr').on($.modal.AFTER_CLOSE , function(event, modal) {
         console.log("modal clsoe");
         
         $('.jquery-modal').remove();
         $('.modal-wr').remove();
         
         $(".site-footer").after("<aside class='modal-wr'></aside>");
         });
         
         $('.modal-wr').on($.modal.BEFORE_OPEN  , function(event, modal) {
         
         singleEventHPop();
         $(this).find('.article-content-wr').scrollbar();
         });
         
         $('.modal-wr').on($.modal.OPEN   , function(event, modal) {
         ellipsis();
         });
         },
         
         error: function(jqXHR, textStatus, errorThrown) {
         // Handle AJAX errors
         }
         
         // More AJAX customization goes here.
         
         });
         
         return false;*/
    });

    $('.input-group.date').datepicker({
        maxViewMode: 0,
        autoclose: true,
        todayHighlight: true,
        toggleActive: true
    });

    $('#select-startup-opt').on('changed.bs.select', function (e) {
        $('.btn_article').click();
    });

    $('.sitemap-nav-wr').each(function () {
        var target = $(this).children();
        target.matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    });

    $('.meet-team-wr .updates-info-wr').matchHeight({
        byRow: false,
        property: 'height',
        target: null,
        remove: false
    });

    // footer sign forum page login button click event
    $('.btn-login-ft').click(function () {

        $("html, body").animate({scrollTop: 0}, "slow");

        var windowW = parseInt(jQuery(window).width());
        if (windowW > 480) {

            if (!$("header .contain-menu-right").hasClass('open-menu')) {
                $('#hdr-btn-login').trigger('click');
            }
        } else {
            if (!$('.nav-login').hasClass('in')) {
                $('#hdr-btn-loginhdr-btn-login').trigger('click');
            }
        }


    });

    $('.signup-toolbox-content-rw .btn-register').click(function () {
        $("html, body").animate({scrollTop: 0}, "slow");
    });

    // footer sign up forum page close button click event
    $('.signup-toolbox .close-sign-up').click(function () {
        $('.signup-toolbox').slideFadeOut();

    });

    // testimonial read more click event
    $('.section-testimonials blockquote').on('click', '.r-more > .btn', function () {
        var $btn = $(this);
        $btn.closest('blockquote').find('.ellips').removeClass('ellips');
        $btn.hide();
    });

    $('.apply-here-wr #overview').keyup(function () {

        var $el = $(this);
        var wordCount = $el.val().split(" ").length;
        var maxLen = 100;
        var val_arr = $el.val().split(" ");
        console.log(val_arr);
        console.log('wordCount: ' + wordCount);
        if (wordCount > maxLen)
        {
            var trunCount = wordCount - maxLen;
            var newVal = "";
            for (i = wordCount; maxLen < i; i--)
            {
                console.log(i);
                var index = val_arr.indexOf(val_arr[i - 1]);
                console.log("index: " + index);
                if (index > -1) {
                    val_arr.splice(index, 1);
                }
            }

            $el.val(val_arr.join(" "));

        }

    });

    // For footer return to home link
    if ($('.dynamic-home-link').length > 0) {
        if (typeof dynamicHomeLink !== 'undefined')
            $('.dynamic-home-link').attr('href', dynamicHomeLink);

        if (typeof dynamicHomeLinkLabel !== 'undefined')
            $('.dynamic-home-link').html(dynamicHomeLinkLabel);
    }

    // To force to login
    $('.force-to-reg').click(function () {
        if ($('.btn-login-dropdown').length > 0) {
            var redirectTo = $(this).attr('data-redirect-to');
            if (redirectTo) {
                $('#login_redirect_to').val(siteURL + '/' + redirectTo);
            }

            showLoginOrReg();
            return false;
        }

        return true;
    });

    $('#myModal').on('hidden.bs.modal', function (e) {
        $("html, body").animate({scrollTop: 0}, "slow");
        var windowW = parseInt(jQuery(window).width());
        if (windowW > 480) {

            if (!$("header .contain-menu-right").hasClass('open-menu')) {
                $('#hdr-btn-login').trigger('click');
            }
        } else {
            if (!$('.nav-login').hasClass('in')) {
                $('#hdr-btn-loginhdr-btn-login').trigger('click');
            }
        }
    });


});
$(function () {
    var collapseInit = 0;
    $('#whoapply .card-header h5').on('click', function () {

        var thisH5 = $(this);
        var button = $(this).find("button");
        var target = button.attr("data-target");
        if (collapseInit === 0) {
            $(thisH5).find(".collapseicon").css("transform", "rotate(-180deg)");
        } else {
            if (button.hasClass("collapsed")) {
                $('#whoapply .collapseicon').css("transform", "rotate(0deg)");
                $(thisH5).find(".collapseicon").css("transform", "rotate(-180deg)");
            } else {
                $(thisH5).find(".collapseicon").css("transform", "rotate(0deg)");
            }
        }
        collapseInit = 1;
    })

})