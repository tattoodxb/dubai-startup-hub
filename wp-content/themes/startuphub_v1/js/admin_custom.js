jQuery( document ).ready(function($) {

	$('#forumlive_switch').change(function(){
		var $switch = $(this);
		var $value = ($switch.val() == 'off')? 'on' : 'off';
		$switch.val($value);
		
		$('#forum_live_status').val($value);
	
		$('#forumlive_form').submit();
	});
	
	
	$(window).load(function(){
		
		var $val = $('#forum_live_status').val();
		
		if($val == "on")
		{
			$('#forum_dashboard_widget .bullet').addClass('on');			
		}
		
	});

});
    