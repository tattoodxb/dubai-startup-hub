<?php 
/**
 * Sticky posts for the category page
 *
 */
function sthub_category_stickies() {
	$sticky = get_option( 'sticky_posts' );

	if ( sizeof( $sticky ) ) {
		return get_posts( array(
			'post_type'=> 'post',
			'post_status' => 'publish',
			'cat' => get_query_var('cat'),
			'post__in' => $sticky,
			'orderby'=> 'publish_date',
			'order'    => 'DESC',
			'posts_per_page' => 6
		) );
	} else {
		return array();
	}
}