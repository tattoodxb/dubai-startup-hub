<?php
/* Template Name: Dashboard Template */
$template_url = get_template_directory_uri();
if ( is_user_logged_in() ) {
}
else
{
    wp_redirect(home_url());
    exit;
}

get_header();

 ?>


<section id="section-events" class="section-updates section-feeds section-feeds-cols section-feeds-cols-3" role="region">
        <div class="bg-wr bg-wr-white no-bg-img" style="background-image:url();">
          <div class="bg-overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
              <h2 class="section-title fx" data-animate="fadeInUp">Change your details</h2></div>
              <div class="same-height-all">              
                <div class="col-sm-12">

            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 removeLeftpadding">

                <div class="">
                    <?php
                    $current_user = wp_get_current_user();
					
					//var_dump($current_user);
                    ?>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 removeLeftpadding">

                        <form name="updateuser" id="updateuser" action="" enctype="multipart/form-data" method="post">

                        <div class="Detailedbox">
                            <div class="titleOrangeBox registerBox registerBoxTitle">
                                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Your details as registered
                            </div>
                            <?php /*div class="rowField">
								<div class="up_user_label error"></div>
                                <input name="up_Full_name" id="up_Full_name" placeholder="*Full Name" class="form-control input-md" required type="text" value="<?php echo $current_user->user_firstname;?>">
                            </div> */?>
							
							<div class="rowField">
								<div class="up_user_label error"></div>
                                <input name="up_First_name" id="up_First_name" placeholder="*First Name" class="form-control input-md" required type="text" value="<?php echo $current_user->user_firstname;?>">
                            </div>
							<div class="rowField">
								<div class="up_user_label error"></div>
                                <input name="up_Last_name" id="up_Last_name" placeholder="*Last Name" class="form-control input-md" required type="text" value="<?php echo $current_user->user_lastname;?>">
                            </div>
                            <div class="rowField">
                                <input name="up_Email" id="up_Email" placeholder="*Email" class="form-control input-md" required type="text" value="<?php echo $current_user->user_email;?>" disabled>
                            </div>
                            <div class="rowField">
                                <input name="up_Company" id="up_Company" placeholder="*Company" class="form-control input-md" required type="text" value="<?php echo $current_user->Company;?>">
                            </div>
                            <div class="rowField">
                                <select name="up_Country" id="up_Country" class="form-control">
                                    <option value="">Country</option>
                                    <?php
                                    global $wpdb;
                                    $table_name = $wpdb->prefix . 'country_list';
                                    $query = $wpdb->get_results("SELECT id, country FROM $table_name ORDER BY sort_order");
                                    foreach($query as $row)
                                    {                                        
                                        if($current_user->Country == $row->country)
                                        {
                                            echo '<option value="'.$row->country.'" selected>'.$row->country.'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$row->country.'">'.$row->country.'</option>';
                                        }                                        
                                    }
                                    $org_type = $current_user->OrganisationType;
                                    ?>
                                </select>
                            </div>
                            <div class="rowField">
                                <select name="up_OrganisationType" id="up_OrganisationType" class="form-control organisation-type">
                                    <option value="">Your organisation type</option>
                                    <option value="Startup" <?php echo ($org_type == "Startup" ? "selected" : "");?>>Startup</option>
                                    <!-- <option value="Corporate/ Government" <?php echo ($org_type == "Corporate/ Government" ? "selected" : "");?>>Corporate/ Government</option>
                                    <option value="Investor" <?php echo ($org_type == "Investor" ? "selected" : "");?>>Investor</option> -->
                                    <option value="Co-working Space" <?php echo ($org_type == "Co-working Space" ? "selected" : "");?>>Co-working space</option>
                                </select>
                            </div>
                            <div class="rowField startup-market" style="display: <?php echo ($org_type == "Startup" ? "block" : "none");?>">
                                <select name="up_Industry" id="up_Industry" class="form-control field-start-market">
                                    <option value="">Your startup market</option>
                                        <?php
                                        global $wpdb;
                                        $table_name = $wpdb->prefix . 'industry';
                                        $query = $wpdb->get_results("SELECT id, industry_name FROM $table_name");
                                        foreach($query as $row)
                                        {
                                            $selected = "";
                                            if($row->industry_name == $current_user->Industry)
                                            {
                                                $selected = "selected";
                                            }
                                            echo '<option value="'.$row->industry_name.'" '.$selected.'>'.$row->industry_name.'</option>';
                                        }
                                        ?>
                                    </select>
                            </div>
                            <div class="rowField">
                                <div class="imageInp img-upload-wr">
									<span>Profile Image</span>
									<p class="sub-info"><small>(Maximum upload size is up to 300KB)</small></p>
									<div class="clearfix"></div>
									<div class="profile-pic-wrapper">
									<?php 
										$hasImage = shub_user_avatar($current_user->ID);
										if($hasImage) {
									?>  
										
										<img id="imageHoldr"  src="<?php echo $hasImage;?>" alt="Your Image" style="display:inline;">
										<input type="hidden" class="hasImage" value="1">
										<button type="button" class="delete-profile-pic btn btn-xs" style="display:inline-block;">Delete Photo</button>
									<?php
										}
										else {
									?>	
										 <input type="file" class="" name="avatar_file" id="avatar_file" accept="image/jpeg,image/png">
										 <img id="imageHoldr" class="img-holder-upload" src="#" alt="Your Image" />
										 <button type="button" class="delete-profile-pic btn btn-xs">Delete Photo</button>
										 
									<?php  
										} 
									?>
									</div> 		 
								</div>
                            </div>

                            <input type="hidden" id="h_country" name="h_country" value="" />
                            <input type="hidden" name="action" value="updateuser" />
                            <?php wp_nonce_field( 'update_user'); ?>
                            <input type="button" name="wp-submit" value="Save" class="btn btn-primary" onclick="return validate_accountsetting();" />                        </div>
                    </form>
                        </div>


                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 removeLeftpadding  ">
                        <div class="Detailedbox">
                            <form name="changepassword" id="changepassword" action="" method="post">
                            <div class="titleOrangeBox registerBox registerBoxTitle">
                                <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Reset your password
                            </div>


                            <div class="rowField">
								<small class="text-primary">Note: Password should be a combination of special character, uppercase, lowercase and number.</small>
                                <input name="current_password" id="current_password" placeholder="*Current Password" class="form-control input-md" required type="password">

                            </div>
                            <div class="rowField">
                                <input name="new_password" id="new_password" placeholder="*Password" class="form-control input-md" required type="password">

                            </div>

                            <div class="rowField">
                                <input name="con_password" id="con_password" placeholder="*Confirm Password" class="form-control input-md" required type="password">

                            </div>
                            <div class="change-pass-error text-danger"></div>
                            <input type="hidden" name="action" value="updatepassword" />
                            <?php wp_nonce_field( 'update_user_password'); ?>
                            <input type="button" name="wp-c-submit" value="Confirm and save" class="btn btn-primary" onclick="return validate_passwordchange();" />

                        </form>
                        </div>


                    </div>
       

                    <div class="clearfix"></div>

                </div>


                </div>

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 orangeHolder deactivateaccount">
 

                <div class="widget JoinConversation hide">
                    <div class="titleOrangeBox">
                        <i class="fa fa-chevron-right nextOrange" aria-hidden="true"></i>Deactivate account
                    </div>
                    <div class="inpuNewsletterHolder margin-top-big">
                        <a href="forum.html" class="menuReturn">Confirm <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div><!-- end col-lg-6 -->

            <div class="clearfix"></div>


</div>
              
              </div>
          </div>
        </div>
        </div>
      </section>
<script>
<?php
if(isset($_GET['update']))
{
	$update = $_GET['update'];
?>
jQuery( document ).ready(function() {

<?php
	if($update=="p")
	{
?>
    jQuery('#alertmodal .alert-danger').html('You have successfully saved your password.');
    jQuery('#alertmodal').modal('show');
<?php
}
else if($update=="a")
{
?>
    jQuery('#alertmodal .alert-danger').html('You have successfully saved your profile.');
    jQuery('#alertmodal').modal('show');
<?php
}
else if($update=="f")
{
?>
    jQuery('#alertmodal .alert-danger').html('Your current password is wrong, please enter correct password.');
    jQuery('#alertmodal').modal('show');
<?php
}
?>

});
<?php
}
?>
</script>
<?php get_footer(); ?>