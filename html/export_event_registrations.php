<?php
ob_start();
ini_set("display_errors",false);
//include('admin-header.php');
require_once('wp-config.php');

header("Content-Type: application/excel");
$fName = "Event_Registrations_".date("d-m-Y").".xls";
header("Content-Disposition: attachment; filename=$fName"); 

mysql_query("SET NAMES 'utf8'");
global $user_ID,$wpdb;
$deptaccess = get_user_meta( $user_ID, 'deptaccess', true);
$ctr=1;	
	if($deptaccess=="")
	{
		$message = 300;
		$content = "<strong>Not Authorised to access reports, contact the website administrator to get access!</strong>";
		echo $content;
	}
	else
	{
				$serviceid=mysql_real_escape_string($_REQUEST['service_id']);
				$status=mysql_real_escape_string($_REQUEST['status']);

?>
<table width="80%" border="1" align="center" cellpadding="1" cellspacing="0" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px">
<!-- start label for the headers -->
	<tr>
		<td width="3%" height="25" align="left"><strong>No:</strong></td>
		<td width="19%" align="left"><strong>Title</strong></td>
	  	<td width="16%" align="left"><strong>First Name</strong></td>
		<td width="19%" align="left"><strong>Last Name</strong></td>
        <td width="20%" align="left"><strong>Designation</strong></td>
		<td width="17%" align="left"><strong>Company</strong></td>
        <td width="25%" align="left"><strong>Email</strong></td>
        <td width="25%" align="left"><strong>Phone</strong></td>
        <td width="25%" align="left"><strong>Fax</strong></td>
        <td width="25%" align="left"><strong>Mobile</strong></td>
        <td width="25%" align="left"><strong>Status</strong></td>
        <?php if($serviceid==138 || $serviceid==139 || $serviceid==140) { ?>
         <td width="25%" align="left"><strong>Complimentary Date</strong></td>
     	<?php } ?>
          <?php if($serviceid==229 || $serviceid==227  || $serviceid==260 ) { ?>
     	<td width="25%" align="left"><strong>Session Date</strong></td>
         <?php } ?>
      <!-- end label for the headers-->
</tr>
<?php			
				$sqlf="";
				if($status==2) $sqlf=" and (service_status='Free' || service_status='Paid') ";
				if($status==3) $sqlf=" and (service_status='Free' || service_status!='paid') ";
				if($serviceid)
				$transactions = $wpdb->get_results("SELECT * FROM dcci_transaction WHERE service_id = '".$serviceid."' $sqlf ORDER BY ID ASC");
				
				if($transactions)
				{
					
					foreach ($transactions as $transaction)
					{
						if( ! $transaction->user_id)
							continue;
						
	                    $users = $wpdb->get_row("SELECT * FROM dcci_usermeta WHERE user_id  = '".$transaction->user_id."' and meta_key='pnumber' ");
						$userphone = $users->meta_value;
					   
					    $users = $wpdb->get_row("SELECT * FROM dcci_usermeta WHERE user_id  = '".$transaction->user_id."' and meta_key='fax' ");
						$userfax = $users->meta_value;
					    $users = $wpdb->get_row("SELECT * FROM dcci_usermeta WHERE user_id  = '".$transaction->user_id."' and meta_key='comdate' ");
						$usercomdate = $users->meta_value;

						$sessiondate = get_user_meta( $transaction->user_id, 'sessiondate', true);

					/*	$user_info = get_userdata($transaction->user_id);
						$title = get_user_meta( $transaction->user_id, 'title', true);
						$first_name = get_user_meta( $transaction->user_id, 'first_name', true);
						$last_name = get_user_meta( $transaction->user_id, 'last_name', true);
						$designation = get_user_meta( $transaction->user_id, 'designation', true);
						$company = get_user_meta( $transaction->user_id, 'company', true);
						$pnumber = get_user_meta( $transaction->user_id, 'pnumber', true);
						$fax = get_user_meta( $transaction->user_id, 'fax', true);
						$mobile = get_user_meta( $transaction->user_id, 'mobile', true);
						$email = $user_info->user_login;
						$dcci_membership = get_user_meta( $transaction->user_id, 'dcci_membership', true);
						$notforprofit = get_user_meta( $transaction->user_id, 'notforprofit', true);
						$notforprofit = ($notforprofit == "yes") ? "Yes" : "No";
*/
						$guests = $wpdb->get_results("SELECT * FROM dcci_ticket WHERE transaction  = '".$transaction->ID."' ORDER BY ID ASC");
						$ginfotitle = array();
						$ginfoname = array();
						$ginfofirstname = array();
						$ginfodesignation = array();
						$ginfocompany = array();
						$ginfoticket = array();
						$ginfoemail = array();
						$ginfomobile = array();	
						
						$service_status=$transaction->service_status;
						foreach ($guests as $guest) {
							switch ($guest->meta_key){
							case "tickets":
								$tickets = $guest->meta_value;
								break;
							case "ticket":
								$ginfoticket[] = $guest->meta_value;
								break;
							case "title":
								$ginfotitle[] = $guest->meta_value;
								break;
							case "name":
								$ginfoname[] = $guest->meta_value;
								break;
							case "fname":
								$ginfofirstname[] = $guest->meta_value;
								break;
							case "designation":
								$ginfodesignation[] = $guest->meta_value;
								break;
							case "company":
								$ginfocompany[] = stripslashes($guest->meta_value);
								break;
							case "email":
								$ginfoemail[] = $guest->meta_value;
								break;
							case "mobile":
								$ginfomobile[] = $guest->meta_value;
								break;
							default:
								break;
							}
						}

						for($j=0;$j<$tickets;$j++)
						{
?>
	<tr>
		<td align="left" valign="top"><div align="center"><?php echo $ctr; ?></div></td>
		<td align="left" valign="top"><?php echo $ginfotitle[$j]; ?></td>
    <td align="left" valign="top"><?php echo $ginfofirstname[$j]; ?></td>
    <td align="left" valign="top"><?php echo $ginfoname[$j]; ?></td>
    <td align="left" valign="top"><?php echo $ginfodesignation[$j]; ?></td>
    <td align="left" valign="top"><?php echo $ginfocompany[$j]; ?></td>
    <td align="left" valign="top"><?php echo $ginfoemail[$j]; ?></td>
    <td align="left" valign="top"><?php echo $userphone; ?></td>
    <td align="left" valign="top"><?php echo $userfax; ?></td>
    <td align="left" valign="top"><?php echo $ginfomobile[$j]; ?></td>
    <td align="left" valign="top"><?php echo $service_status;?></td>
    <?php if($serviceid==227) { ?>
    <td  align="left"><?php echo $usercomdate;?></td>
    <?php } ?>
     <?php if($serviceid==229  || $serviceid==260) { ?>
    <td  align="left"><?php echo $sessiondate;?></td>
    <?php } ?>
  </tr>
<?php 
							$ctr++;
						}
					} 

}
}
?>
</table>
