<?php
    include('config.php');
    
    $survey_id = $_POST['survey_id'];
    $question = $_POST['question'];
    $email = $_POST['user_email'];
    $ip_address = get_client_ip();
    $date_created = date('Y-m-d h:i:s');

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo 'invalid';
    }
    else
    {
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error){
            echo 'connection';
        }
        $query = mysqli_query($conn, "SELECT * FROM shub_survey WHERE email='".$email."'");
        if (!$query)
        {
            die('connection');
        }
        if(mysqli_num_rows($query) > 0){
            echo 'exist';
        }
        else{
            $stmt = $conn -> prepare("INSERT INTO `shub_survey`(`survey_id`, `question`, `email`, `ip_address`, `date_created`) VALUES (?,?,?,?,?)");
            $stmt->bind_param("sssss", $survey_id, $question, $email, $ip_address, $date_created);
            $stmt->execute();
            echo 'success';
            $stmt->close();
        }
        $conn->close();
    }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
?>