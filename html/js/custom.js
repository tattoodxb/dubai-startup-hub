(function($) {
 "use strict"

	// Page Preloader
	//$(window).load(function() {
	//	$(".loader").delay(300).fadeOut();
	//	$(".animationload").delay(600).fadeOut("slow");
	//});

 $(".RegisterBtn").click(function () {
   
 $(".registrationbox").show();


 });

 $(".startConversation").click(function () {

     $(".conversationBox").show();


 });
 $(document).mouseup(function (e) {
     var container = $(".registrationbox");
     var container1 = $(".conversationBox");
     // if the target of the click isn't the container nor a descendant of the container
     if (!container.is(e.target) && container.has(e.target).length === 0) {
         container.hide();
     }
     if (!container1.is(e.target) && container1.has(e.target).length === 0) {
         container1.hide();
     }

 });
	

    //heade toggle
	$(".burgermenu , .closeMenu").click(function () {
	    $(".menu_Holder").slideToggle("1000", function () {
	        // Animation complete.
	    });
	});


    //login  toggle
	$(".loginBtn , .closeMenu").click(function () {
	    $(".login_Holder").slideToggle("1000", function () {
	        // Animation complete.
	    });
	});

    //autocomplete
	$(function () {
	    $(".scroll-Area").scrollbar();
	});


	$(function () {
	    $("#ui-id-1").scrollbar();
	});


	



	$("#Keywords").keyup(function () {


	    $(".scroll-Area").show("fast", function () {

	    });



	});


	$("#Keywords").blur(function () {


	    $(".scroll-Area").hide("fast", function () {

	    });



	});




	$("#topic").keyup(function () {


	    $(".topicHolder").show("fast", function () {

	    });



	});


	$("#topic").blur(function () {


	    $(".topicHolder").hide("fast", function () {

	    });



	});



	$("#btnclosePopup").click(function () {


	
	    $(".conversationBox") .fadeOut( "fast", function() {
	
	    });


	});

	$("#btnclosePopupRegistration").click(function () {



	    $(".registrationbox").fadeOut("fast", function () {

	    });


	});


	$(".startConversation").click(function () {



	    $(".conversationBox").fadeIn("fast", function () {

	   
	 
	        //$("html, body").animate({ scrollTop: '200px' }, "slow");
	      
	    });


	});
	
	


	$("#topicRegister").keyup(function () {


	    $(".topicRegisterHolder").show("fast", function () {

	    });



	});


	$("#topicRegister").blur(function () {


	    $(".topicRegisterHolder").hide("fast", function () {

	    });



	});




	$("#tag").keyup(function () {


	    $(".tagHolder").show("fast", function () {

	    });



	});


	$("#tag").blur(function () {


	    $(".tagHolder").hide("fast", function () {

	    });



	});

	$(document).ready(function () {
	    $('.thefilecss').change(function () {
	        $('.imageHolderUpload').text(this.files.length + " file(s) selected");
	    });
	});


	$(".scroll-DownBtn").click(function () {
	    $('html, body').animate({
	        scrollTop: $("#mainContent").offset().top
	    }, 1000);
	});

// OWL Carousel
	$("#owl-testimonial").owlCarousel({
		items : 3,
		lazyLoad : true,
		navigation : false,
		autoPlay: true
    });



	$("#owl-testimonial-widget, #owl-blog").owlCarousel({
		items : 3,
		lazyLoad : true,
		navigation : true,
		pagination : false,
		autoPlay: false
    });
	
	$("#owl_blog_three_line, #owl_portfolio_two_line, #owl_blog_two_line").owlCarousel({
		items : 2,
		lazyLoad : true,
		navigation : true,
		pagination : false,
		autoPlay: true
    });

	$("#owl_shop_carousel, #owl_shop_carousel_1").owlCarousel({
		items : 3,
		lazyLoad : true,
		navigation : true,
		pagination : false,
		autoPlay: true
    });
	
	$("#services").owlCarousel({
		items : 1,
		lazyLoad : true,
		navigation : true,
		pagination : true,
		autoPlay: true,
		afterInit: attachEvent

    });

	function attachEvent() {
	    var numItems = $('.owl-page').length;
	    $(".owl-buttons").width(numItems*50);

	}

	$(".buddy_carousel").owlCarousel({
		items : 9,
		lazyLoad : true,
		navigation : false,
		pagination : true,
		autoPlay: true
    });
	

	$('.buddy_tooltip').popover({
		container: '.buddy_carousel, .buddy_members'
	});
		
// Parallax
	$(window).bind('body', function() {
		parallaxInit();
	});
	function parallaxInit() {
		$('#one-parallax').parallax("30%", 0.1);
		$('#two-parallax').parallax("30%", 0.1);
		$('#three-parallax').parallax("30%", 0.1);
		$('#four-parallax').parallax("30%", 0.4);
		$('#five-parallax').parallax("30%", 0.4);
		$('#six-parallax').parallax("30%", 0.4);
		$('#seven-parallax').parallax("30%", 0.4);
	}

 
// Fun Facts
	function count($this){
		var current = parseInt($this.html(), 10);
		current = current + 1; /* Where 50 is increment */
		
		$this.html(++current);
			if(current > $this.data('count')){
				$this.html($this.data('count'));
			} else {    
				setTimeout(function(){count($this)}, 50);
			}
		}        
		
		$(".stat-count").each(function() {
		  $(this).data('count', parseInt($(this).html(), 10));
		  $(this).html('0');
		  count($(this));
	});
	
// WOW
	new WOW(
		{ offset: 300 }
	).init();
		
// DM Top
	//jQuery(window).scroll(function(){
	//	if (jQuery(this).scrollTop() > 1) {
	//		jQuery('.dmtop').css({bottom:"25px"});
	//	} else {
	//		jQuery('.dmtop').css({bottom:"-100px"});
	//	}
	//});
	//jQuery('.dmtop').click(function(){
	//	jQuery('html, body').animate({scrollTop: '0px'}, 800);
	//	return false;
	//});
	
// Rotate Text
	$(".rotate").textrotator({
		animation: "flipUp",
		speed: 2300
	});


	
// TOOLTIP
    $('.social-icons, .bs-example-tooltips').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
    })
	
// Accordion Toggle Items
   var iconOpen = 'fa fa-minus',
        iconClose = 'fa fa-plus';

    $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
        var $target = $(e.target)
          $target.siblings('.accordion-heading')
          .find('em').toggleClass(iconOpen + ' ' + iconClose);
          if(e.type == 'show')
              $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
          if(e.type == 'hide')
              $(this).find('.accordion-toggle').not($target).removeClass('active');
    });

    $('body').on('click', '.btn-send', function(){
    	var family_name = $('#family_name');
    	var first_name = $('#first_name');
    	var subject = $('#subject');
    	var user_email = $('#email');
    	var comments = $('#comments');

    	var err = 0;
		if(family_name.val()=="")
		{
			family_name.addClass('error');
			$('.family_name').show();
			err = 1;
		}
		else
		{
			family_name.removeClass('error');
			$('.family_name').hide();
		}

		if(first_name.val()=="")
		{
			first_name.addClass('error');
			$('.first_name').show();
			err = 1;
		}
		else
		{
			first_name.removeClass('error');
			$('.first_name').hide();
		}

		if(subject.val()=="")
		{
			subject.addClass('error');
			$('.subject').show();
			err = 1;
		}
		else
		{
			subject.removeClass('error');
			$('.subject').hide();
		}

		if(user_email.val()=="")
		{
			user_email.addClass('error');
			$('.email').show();
			err = 1;
		}
		else if( !isValidEmailAddress( user_email.val() ) ) 
		{ 
			user_email.addClass('error');
			$('.email').show();
			err = 1;
		}
		else
		{
			user_email.removeClass('error');
			$('.email').hide();
		}
		if(err == 1)
		{
			return false;
		}
		else
		{
			var data = {
				family_name: family_name.val(),
				first_name: first_name.val(),
				subject: subject.val(),
				user_email: user_email.val(),
				comments: comments.val(),
			};
			$('.send').removeClass('btn-send');
			$('.send').html('Please wait...');
			$.post('savecontact.php', data, function(response) {				
				if(response=='success')
				{
					$('.contactform').hide();
					$('.thankyou').show();
				}
				else
				{
					alert('There was error processing your data due to invalid data. please contact us on our email address');
				}
		 	});
		 	return false;
		 }
	});
	$('body').on('click', '.BannerCopy_3', function(){
			var cur_id = $(this).data("id");
			$('#question_id').val(cur_id);
			$('#question').val($(this).html());
			$('.surveyform').hide();
			$('.thankyou').show();
	});

	$('body').on('click', '.survey_btn', function(){
    	var user_email = $('#email');
    	var err = 0;
		if(user_email.val()=="")
		{
			user_email.addClass('error');
			$('.email').show();
			err = 1;
		}
		else if( !isValidEmailAddress( user_email.val() ) ) 
		{ 
			user_email.addClass('error');
			$('.email').show();
			err = 1;
		}
		else
		{
			user_email.removeClass('error');
			$('.email').hide();
		}

		if(err == 1)
		{
			return false;
		}
		else
		{
			var data = {
				survey_id: $('#question_id').val(),
				question: $('#question').val(),
				user_email: user_email.val(),
			};
			$.post('savesurvey.php', data, function(response) {				
				if(response=='success')
				{
					$('.s_form').hide();
					$('.t_form').show();
				}
				else if(response=='exist')
				{
					$('.s_form').hide();
					$('.t_form').show();
					$('.t_form').html('<br><br><br>You have already taken part in this survey');
				}
				else
				{
					alert('There was error processing your data due to invalid data.');
				}
		 	});
		 	return false;
		 }
	});
	
})(jQuery);

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};