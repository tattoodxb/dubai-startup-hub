<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dc_startuphub');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y4r@Q-aT%[QlZFZmP8UXp{uv97L_KL1C-[])UpA6}io`m%2%fNm*poW/Ww*(tw[@');
define('SECURE_AUTH_KEY',  'N2mkW2&_([y!9#P~Bt 5#V>|O&Refjc8gt=<R[/Gp7A^ch~o++z;~&XBzwr<u$>@');
define('LOGGED_IN_KEY',    ':5KMUYUg83B=:F~tOmpN-B-qSsTvT@Y]Ap~B`%Lg-nIw&0*r([rtrA5*&R^B/@Ls');
define('NONCE_KEY',        '-4n{c]>ksW!!w]k6!A:IBDet89jp0gLg}$;nQwBUNqJuX|C]4yx!O~J~:Hes_?LO');
define('AUTH_SALT',        'z;RcrrXTFjSL=x:qeckLOIOwF4:=M uiBl7}^GFCsN9-H2s,HgLpEo0yn`lX. w_');
define('SECURE_AUTH_SALT', 'qAfmkp}OSs4g#F<g$UW1pc 43=CrG[BYC_(}.@0v4YX_{2~*S[9! ``7{a+,xhaL');
define('LOGGED_IN_SALT',   'sZNi0Ks?$HSO/Jz] 6+8tzbzA>j7^EG()^F%(Va?_*i%O5C{J6q_T5 kmB&pY#5U');
define('NONCE_SALT',       '34XO|wv~rB9Rb52.MqtRwFXI3f--f6Nx!Ob+)dniUJ.DX4-x^n6EF_@EuimOT<<?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'shub_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
